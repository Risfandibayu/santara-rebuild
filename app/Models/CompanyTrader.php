<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CompanyTrader extends Model
{
    use HasFactory;
    protected $table = "company_traders";
}
