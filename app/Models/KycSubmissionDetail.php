<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KycSubmissionDetail extends Model
{
    use HasFactory;
    protected $table = "kyc_submission_details";
}
