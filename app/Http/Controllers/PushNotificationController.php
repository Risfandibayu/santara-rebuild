<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\notification;
use App\Models\Users;
use App\Models\trader;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Response;

class PushNotificationController extends Controller
{

    public $limit = 100;

    public function index($broadcastId)
    {
        $detailBroadcast = $this->getDetailBroadcast($broadcastId);
        $kategori = $detailBroadcast['broadcast_category_name'];
        $namaBroadcast = $detailBroadcast['name'];
        $targets = $detailBroadcast['target'];
        $notif = $detailBroadcast['list'][0];
        $limit = $this->limit;
        //return response()->json(["data" => $detailBroadcast]);
        return view('admin.crm.push-notif', compact('broadcastId', 'targets', 'kategori', 'notif', 'namaBroadcast', 'limit'));
    }
    
    public function pushNotif($id)
    {
        ini_set('memory_limit', '-1');
        $detailBroadcast = $this->getDetailBroadcast($id);
        $userId = [];
        $statusKYC = "";
        $gender = "";
        $umurAwal = 0;
        $umurAkhir = 0;
        $pendapatanAwal = 0;
        $pendapatanAkhir = 0;
        $depoAwal = 0;
        $depoAkhir = 0;
        $regencyName = '';
        $provinsiName = '';
        $kepemilikanSaham = ''; 
        $totalSahamMinimal = 0;
        $totalSahamMaksimal = 0;
        $jumlahSahamMinimal = 0;
        $jumlahSahamMaksimal = 0;
        $unlimitedInvest = false;
        $limitAwalInvest = 0;
        $limitAkhirInvest = 0;
        $rataPembelianAwal = 0;
        $rataPembelianAkhir = 0;
        $statusSID = '';
        $skemaKYC = '';
        $skemaGender = '';
        $skemaUmur = '';
        $skemaPendapatan = '';
        $skemaKota = '';
        $skemaProvinsi = '';
        $skemaKepemilikanSaham = '';
        $skemaJumlahSahamRP = '';
        $skemaJumlahSahamCount = '';
        $skemaSisaLimitInvestasi = '';
        $skemaRataRataPembelian = '';
        $skemaDeposit = '';
        $skemaSID = '';
        $skemaIOS = '';
        $skemaAndroid = '';
        $skemaEmail = '';
        $listEmail = [];

        foreach($detailBroadcast['target'] as $target){
            if($target['name'] == 'Status KYC'){
                $skemaKYC .= 1;
                $statusKYC = $target['params'];
            }
            if($target['name'] == 'Gender'){
               $skemaGender .= 2;
               $gender = $target['params'];
            }
            if($target['name'] == 'Umur'){
                $skemaUmur .= 3; 
                $age = explode(" ", $target['params']);
                $umurAwal = $age[0];
                $umurAkhir = $age[2];
            }
            if($target['name'] == 'Pendapatan Per Tahun'){
                $skemaPendapatan .= 4; 
                $income = explode(" ", $target['params']);
                $pendapatanAwal = $income[0];
                $pendapatanAkhir = $income[2];
            }
            if($target['name'] == 'Kota/Kabupaten'){
                $skemaKota .= 5; 
                $regencyName = $target['params'];
            }
            if($target['name'] == 'Provinsi'){
                $skemaProvinsi .= 6; 
                $provinsiName = $target['params'];
            }
            if($target['name'] == 'Kepemilikan Saham'){
                $skemaKepemilikanSaham .= 7; 
                $kepemilikanSaham = $target['params'];
            }
            if($target['name'] == 'Jumlah Saham (Rp)'){
                $skemaJumlahSahamRP .= 8; 
                $saham = explode(" ", $target['params']);
                $totalSahamMinimal = $saham[0];
                $totalSahamMaksimal = $saham[2];
            }
            if($target['name'] == 'Jumlah Saham (Count)'){
                $skemaJumlahSahamCount .= 9; 
                $saham = explode(" ", $target['params']);
                $jumlahSahamMinimal = $saham[0];
                $jumlahSahamMaksimal = $saham[2];
            }
            if($target['name'] == 'Sisa Limit Investasi'){
                $skemaSisaLimitInvestasi .= 10; 
                if($target['params'] == '999999999999'){
                    $unlimitedInvest = true;
                }else{
                    $limit = explode(" ", $target['params']);
                    $limitAwalInvest = $limit[0];
                    $limitAkhirInvest = $limit[2];
                }
            }
            if($target['name'] == 'Rata-rata Pembelian'){
                $skemaRataRataPembelian .= 11; 
                $rata2 = explode(" ", $target['params']);
                $rataPembelianAwal = $rata2[0];
                $rataPembelianAkhir = $rata2[2];
            }
            if($target['name'] == 'Deposit'){
                $skemaDeposit .= 12; 
                $depo = explode(" ", $target['params']);
                $depoAwal = $depo[0];
                $depoAkhir = $depo[2];
            }
            if($target['name'] == 'SID'){
                $skemaSID .= 13; 
                $statusSID = $target['params'];
            }
            if($target['name'] == 'Versi Aplikasi (iOs)'){
                $skemaIOS .= 14; 
            }
            if($target['name'] == 'Versi Aplikasi (Android)'){
                $skemaAndroid .= 15; 
            }
            if($target['name'] == 'Email'){
                $skemaEmail .= 16;
                $listEmail = explode(",", $target['params']);
            }
        }
        
            $traders = trader::query();

            $traders->join('users as u', 'u.id', '=', 'traders.user_id')
                ->leftJoin('kyc_submissions as ks', 'ks.id', '=', 'traders.last_kyc_submission_id')
                ->leftJoin('jobs as j', 'j.trader_id', '=', 'traders.id')
                ->leftJoin('trader_banks as tb', 'tb.trader_id', 'traders.id')
                ->leftJoin('deposits as depo', 'depo.trader_id', '=', 'traders.id')
                ->leftJoin('transactions as tr', 'tr.trader_id', '=', 'traders.id');
            $traders->select('traders.user_id', 'traders.birth_date', 'depo.amount', 'tr.amount as amo', 'u.email',
                    'traders.name', 'traders.phone',
                    'j.income', 'tr.trader_id', 'tr.is_deleted', 'tr.last_status', \DB::raw('SUM(tr.amount) as total'));
            // $traders->select('traders.user_id');
            $traders->where('traders.is_deleted', 0);
            $traders->where('u.is_verified', 1);
            $traders->where('u.role_id', 2);
            
            if($skemaKYC == 1){
                if($statusKYC == 1){
                    $traders->where('ks.status', 'verified');
                }else{
                    $traders->where('ks.status', 'verifying');
                }
            }

            if($skemaGender == 2){
                $traders->where('traders.gender', $gender);
            }

            if($skemaUmur == 3){
                $traders->having(\DB::raw('FLOOR(DATEDIFF(CURDATE(), traders.birth_date) / 365)'), '>=' ,$umurAwal);
                $traders->having(\DB::raw('FLOOR(DATEDIFF(CURDATE(), traders.birth_date) / 365)'), '<=' ,$umurAkhir);
            }

            if($skemaPendapatan == 4){
                $traders->having(\DB::raw('j.income'), '>=', $pendapatanAwal);
                $traders->having(\DB::raw('j.income'), '<=', $pendapatanAkhir);
            }

            if($skemaKota == 5){
                $traders->where('traders.regency', $regencyName);
            }

            if($skemaProvinsi == 6){
                $traders->where('traders.province', $provinsiName);
            }

            if($skemaProvinsi == 7){
                if($kepemilikanSaham == "1"){
                    $traders->whereNotNull("tr.trader_id");
                }else{
                    $traders->whereNull("tr.trader_id");
                }
            }

            if($skemaJumlahSahamRP == 8){
                $traders->having(\DB::raw('SUM(amo)'), '>=' ,$totalSahamMinimal);       
                $traders->having(\DB::raw('SUM(amo)'), '<=' ,$totalSahamMaksimal);
                $traders->where('tr.is_deleted', 0);
                $traders->where('tr.last_status', 'VERIFIED');
                $traders->groupBy('tr.trader_id');
            }

            if($skemaJumlahSahamCount == 9){
                $traders->having(\DB::raw('COUNT(amo)'), '>=' ,$jumlahSahamMinimal);       
                $traders->having(\DB::raw('COUNT(amo)'), '<=' ,$jumlahSahamMaksimal);
                $traders->where('tr.is_deleted', 0);
                $traders->where('tr.last_status', 'VERIFIED');
            }

            if($skemaSisaLimitInvestasi == 10){
                if($unlimitedInvest){
                    $traders->where('j.is_unlimited_invest', 1);
                }else{
                    $traders->where('j.is_unlimited_invest', 0);
                    if($limitAwalInvest > 500000000){
                        $traders->having(\DB::raw('income * 10 /100'), '>=' ,$limitAwalInvest); 
                        $traders->having(\DB::raw('income * 10 /100'), '<=' ,$limitAkhirInvest); 
                    }else{
                        $traders->having(\DB::raw('income * 5 /100'), '>=' ,$limitAwalInvest); 
                        $traders->having(\DB::raw('income * 5 /100'), '<=' ,$limitAkhirInvest); 
                    }
                }
            }

            if($skemaRataRataPembelian == 11){
                $traders->having(\DB::raw('SUM(amo) / COUNT(amo)'), '>=' ,$rataPembelianAwal); 
                $traders->having(\DB::raw('SUM(amo) / COUNT(amo)'), '<=' ,$rataPembelianAkhir); 
            }

            if($skemaDeposit == 12){
                $traders->having(\DB::raw('SUM(depo.amount)'), '>=' ,$depoAwal);       
                $traders->having(\DB::raw('SUM(depo.amount)'), '<=' ,$depoAkhir);
                $traders->groupBy('depo.trader_id');
            }

            if($skemaSID == 13){
                if($statusSID == 'IS NOT NULL'){
                    $traders->whereNotNull('tb.sid_number');
                }else{
                    $traders->whereNull('tb.sid_number');
                }
            }

            if($skemaEmail == 16){
                $traders->where('u.email', $listEmail[0]);
                $ulangEmail = count($listEmail) - 1;
                for($i = 1; $i <= $ulangEmail; $i++){
                    $traders->orWhere('u.email', $listEmail[$i]);
                }
            }

            $traders->groupBy('traders.user_id');
            $traders->orderBy('traders.user_id', 'ASC');
            $results = $traders->get();
            $data = json_encode($results);
            // $results = $traders->paginate($this->limit);
            $fileName = time(). '_broadcast_user.json';
            \File::put(public_path('/file-json/'.$fileName),$data);
            return response()->download(public_path('/file-json/'.$fileName));
        //return response()->json(["results" => $results]);
    }

    public function broadcastNotif(Request $request)
    {
        ini_set('memory_limit', '-1');
        $file = $request->json_users;   
        $users = json_decode(file_get_contents($file), true);

        $insert_data = collect();
        foreach ($users as $row) {
            $message = $request->message;
            if(strpos($request->message, "@user") !== false){
                $message = str_replace("@user",  $row['name'], $message);
            }
            if(strpos($request->message, "@call") !== false){
                $message = str_replace("@call",  $row['phone'], $message);
            }
            if(strpos($request->message, "@email") !== false){
                $message = str_replace("@email", $row['email'], $message);
            }

            $insert_data->push([
                "uuid" => \Str::uuid(),
                "action" => $request->redirection,
                "user_id" => $row['user_id'],
                "message" => $message,
                "title" => $request->title,
                "broadcast_id" => $request->broadcast_id,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
                "is_deleted" => 0,
                "created_by" => \Auth::user()->id
            ]);
        }

        foreach ($insert_data->chunk(2000) as $chunk)
        {
            \DB::table('notifications')->insert($chunk->toArray());
        }
        return response()->json([
            "code" => 200,
            "message" => "Berhasil melakukan broadcast",
            "file" => $users,
        ]);       
    }

    public function schedulerbroadcastNotif(Request $request)
    {
        $s = Carbon::now();
        $broad = DB::table('broadcasts')->whereDate('send_on',$s->toDateString())->first();
        // $id = $broad->id;
        // $test = $this->pushNotif($id);
        // if ($broad) {
        //     # code...
        //     $userId = explode(",", $request->userId);
        //     for($i = 0; $i < count($userId); $i++){
        //         $notif = new notification();
        //         $notif->uuid = \Str::uuid();
        //         $notif->action = 'Informasi';
        //         $notif->user_id = $userId[$i];
        //         //$notif->user_id = 190382;
        //         $notif->message = $request->message;
        //         $notif->title = $request->title;
        //         $notif->created_at = Carbon::now();
        //         $notif->updated_at = Carbon::now();
        //         $notif->is_deleted = 0;
        //         $notif->created_by = Auth::user()->id;
        //         $notif->save();
        //     }
        // }
        // $userId = explode(",", $request->userId);
        // for($i = 0; $i < count($userId); $i++){
        //     $notif = new notification();
        //     $notif->uuid = \Str::uuid();
        //     $notif->action = 'Informasi';
        //     $notif->user_id = $userId[$i];
        //     //$notif->user_id = 190382;
        //     $notif->message = $request->message;
        //     $notif->title = $request->title;
        //     $notif->created_at = Carbon::now();
        //     $notif->updated_at = Carbon::now();
        //     $notif->is_deleted = 0;
        //     $notif->created_by = \Auth::user()->id;
        //     $notif->save();
        // }
        // return response()->json(["code" => 200, "message" => "Berhasil melakukan broadcast"]);
        
        dd($this->getDetailBroadcast(321));
        // echo $dt->toDateString();	2015-04-21
        // echo $dt->toFormattedDateString();	Apr 21, 2015
        // echo $dt->toTimeString();	22:32:05
        // echo $dt->toDateTimeString();	2015-04-21 22:32:05
        // echo $dt->toDayDateTimeString();	Tue, Apr 21, 2015 10:32 PM
    }

    public function broadcastEmail(Request $request)
    {
        // $email = explode(",", $request->email);
        // $traderNames = explode(",", $request->namaTrader);
        // $phones = explode(",", $request->phones);
        // for($i = 0; $i < count($email); $i++){
        //     $message = $request->message;
        //     if(strpos($request->message, "@user") !== false){
        //         $message = str_replace("@user",  $traderNames[$i], $message);
        //     }
        //     if(strpos($request->message, "@call") !== false){
        //         $message = str_replace("@call",  $phones[$i], $message);
        //     }
        //     if(strpos($request->message, "@email") !== false){
        //         $message = str_replace("@email",  $email[$i], $message);
        //     }
        //     $details = [
        //         'title' => $request->title,
        //         'body' => $message,
        //         'image' => $request->image,
        //         'redirection' => $request->redirection,
        //         'subject' => $request->namaBroadcast
        //     ]; 
    
        //     \Mail::to($email[$i])->send(new \App\Mail\NotificationMail($details));
        // }

        ini_set('memory_limit', '-1');
        $file = request()->json_users;   
        $users = json_decode(file_get_contents($file), true);
        $insert_data = collect();
        foreach ($users as $row) {
            $message = $request->message;
            if(strpos($request->message, "@user") !== false){
                $message = str_replace("@user",  $row['name'], $message);
            }
            if(strpos($request->message, "@call") !== false){
                $message = str_replace("@call",  $row['phone'], $message);
            }
            if(strpos($request->message, "@email") !== false){
                $message = str_replace("@email", $row['email'], $message);
            }

            $insert_data->push([
                "email" => $row['email'],
                "subject" => $request->namaBroadcast,
                "user_id" => $row['user_id'],
                "message" => $message,
                "image" => $request->image,
                "title" => $request->title,
                "redirection" => $request->redirection
            ]);
        }

        foreach ($insert_data->chunk(2000) as $chunk)
        {
            foreach ($chunk as $row) {
                $details = [
                    'title' => $row['title'],
                    'body' => $row['message'],
                    'image' => $row['image'],
                    'redirection' =>$row['redirection'],
                    'subject' =>$row['subject']
                ]; 
                \Mail::to($row['email'])->send(new \App\Mail\NotificationMail($details));
            }
        }
          
        return response()->json(["code" => 200, "message" => "Berhasil melakukan broadcast email"]);
    }

    public function getDetailBroadcast($id)
    {
        $result = null;
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION'). 'broadcast/detail/' . $id, [
                'form_params' => [
                    'token'     => app('request')->session()->get('token'),
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                $detail = json_decode($response->getBody()->getContents(), TRUE);
                $result = $detail['data'];
            }
        } catch (\Exception $exception) {
            $result = null;
        }

        return $result;
    }

    public function hasilNotifikasi($broadcastId)
    {
        $broadcast = DB::table('broadcasts')
            ->join('broadcast_groups as group', 'group.id', '=', 'broadcasts.broadcast_group_id')
            ->where('broadcasts.broadcast_group_id', $broadcastId)
            ->select('broadcasts.title', 'broadcasts.content', 'broadcasts.broadcast_group_id as broadcast_group_id')
            ->first();
        return view('admin.crm.hasil-notifikasi', compact('broadcast'));
    }

    public function fetchDataNotifikasi(Request $request, $broadcastId)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $statusFilter = $request->get('status');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $totalRecords = notification::join('users as u', 'u.id', '=', 'notifications.user_id')
            ->join('traders as t', 't.user_id', '=', 'u.id')
            ->where('notifications.broadcast_id', $broadcastId)
            ->count();

        $totalRecordswithFilter = notification::join('users as u', 'u.id', '=', 'notifications.user_id')
            ->join('traders as t', 't.user_id', '=', 'u.id')
            ->where('notifications.broadcast_id', $broadcastId)
            ->where(function($search) use ($searchValue){
                $search->where('u.email', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.name', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchValue.'%');
            })
            ->count();

        $notifications = notification::join('users as u', 'u.id', '=', 'notifications.user_id')
            ->join('traders as t', 't.user_id', '=', 'u.id')
            ->where('notifications.broadcast_id', $broadcastId)
            ->where(function($search) use ($searchValue){
                $search->where('u.email', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.name', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchValue.'%');
            })
            ->skip($start)
            ->take($rowperpage)
            ->select('notifications.id', 'u.email', 't.name', 't.phone','notifications.is_deleted', 'notifications.created_at')
            ->get();

        $data = [];
        foreach($notifications as $row){
            $status = "<span class='badge badge-success'>Terkirim</span>";
            if($row->is_deleted == 1){
                $status = "<span class='badge badge-danger'>Dibatalkan</span>";
            }
            array_push($data, [
                'id' => $row->id,
                'email' => $row->email,
                'phone' => $row->phone,
                'name' => $row->name,
                'is_deleted' => $status,
                'created_at' => tgl_indo(date('Y-m-d', strtotime($row->created_at))).' '.formatJam($row->created_at)
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
        
        echo json_encode($response);
        exit;
    }

    public function batalkanNotifikasi(Request $request)
    {
        ini_set('memory_limit', '-1');
        $notification = notification::where('broadcast_id', $request->broadcastId)
            ->update([
                'is_deleted' => 1
            ]);
        return response()->json(["code" => 200, "message" => "Notifikasi Berhasil dibatalkan"]);
    }

}
