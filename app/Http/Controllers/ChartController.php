<?php

namespace App\Http\Controllers;

use App\Charts\ChatChart;
use App\Charts\ConversationChart;
use App\Charts\Top10GroupChart;
use App\Charts\Top10UserChart;
use App\Models\Conversations;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    const MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const MONTHS_NUM = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

    public function index()
    {
        $conversation = array_map(function ($num){
            return Conversations::whereMonth('created_at', $num)->count();
        }, self::MONTHS_NUM);

        $chartConv = new ConversationChart();
        $chartConv->labels(self::MONTHS);
        $chartConv->dataset('Conversations', 'line', $conversation)->color('#7F1D1D');

        $group = DB::connection('chat')->table('groups')->count();
        $groupUser = DB::connection('chat')->table('group_users')->count();

        $chatGroup = new ChatChart();
        $chatGroup->labels(['Groups', 'Group Users']);
        $chatGroup->dataset('Groups', 'doughnut', [$group, $groupUser])->backgroundColor([
            '#7F1D1D', '#2C343B'
        ]);

        $users = DB::connection('chat')
            ->table('conversations')
            ->select('conversations.from_id', DB::raw('COUNT(*) as count'), 'users.name')
            ->join('users', 'conversations.from_id', '=', 'users.id')
            ->where('conversations.from_id', '!=', 'null')
            ->groupBy('conversations.from_id')
            ->orderBy('count', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();

        $chartUser = new Top10UserChart();
        $chartUserLabel = array_map(function ($value){
            return $value->name;
        }, $users);

        $chartUser->labels = $chartUserLabel;
        $chartUser->dataset('Top 10 Users', 'bar', array_map(function ($value){
            return $value->count;
        }, $users))->backgroundColor('#7F1D1D');

        $emitens = DB::connection('chat')
            ->table('conversations')
            ->select('conversations.to_id', 'conversations.to_type', DB::raw('COUNT(*) as count'), 'groups.name')
            ->join('groups', 'conversations.to_id', '=', 'groups.id')
            ->where('conversations.from_id', '!=', 'null')
            ->where('conversations.to_type', '=', "App\\Models\\Group")
            ->groupBy('conversations.to_id')
            ->orderBy('count', 'DESC')
            ->limit(10)
            ->get()
            ->toArray();

        $chartEmiten = new Top10GroupChart();
        $chartEmitenLabel = array_map(function ($value){
            return $value->name;
        }, $emitens);

        $chartEmiten->labels = $chartEmitenLabel;
        $chartEmiten->dataset('Top 10 Groups/Emiten', 'bar', array_map(function ($value){
            return $value->count;
        }, $emitens))->backgroundColor('#7F1D1D');

        return view('chart', [
            'chartConv' => $chartConv,
            'chartGroup' => $chatGroup,
            'chartUser' => $chartUser,
            'chartEmiten' => $chartEmiten
        ]);
    }
}
