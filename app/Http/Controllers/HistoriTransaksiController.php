<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Markets;
use App\Models\Deposit;
use App\Models\Withdraw;
use Carbon\Carbon;
use DB;

class HistoriTransaksiController extends Controller
{
    //
    public function index()
    {
        return view('admin.histori_transaksi.index');
    }

    public function fetchDataUsers(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $searchData = $request->cari;

        $queryRecords = User::query();
        $queryRecords->join('roles as r', 'r.id', '=', 'users.role_id')
            ->join('traders as t', 't.id', '=', 't.user_id')
            ->where('users.is_deleted', 0)
            ->orderBy('t.name', 'ASC')
            ->select('count(*) as allcount');

        $queryFilterRecords = User::query();
        $queryFilterRecords->join('roles as r', 'r.id', '=', 'users.role_id')
            ->join('traders as t', 'users.id', '=', 't.user_id')
            ->where('users.is_deleted', 0)
            ->orderBy('t.name', 'ASC');

        $query = User::query();
        $query->join('roles as r', 'r.id', '=', 'users.role_id')
            ->join('traders as t', 'users.id', '=', 't.user_id')
            ->where('users.is_deleted', 0)
            ->orderBy('t.name', 'ASC')
            ->skip($start)
            ->take($rowperpage)
            ->select('users.id', 'users.uuid', 't.name', 'users.email', 
                    't.phone', 'users.attempt', 'users.created_at', 'r.name as role_name');
                
        if($searchData != ""){
            $queryFilterRecords->where(function($search) use ($searchData){
                $search->where('users.email', 'like', '%'.$searchData.'%')
                    ->orWhere('t.name', 'like', '%'.$searchData.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchData.'%');
            });
            $query->where(function($search) use ($searchData){
                $search->where('users.email', 'like', '%'.$searchData.'%')
                    ->orWhere('t.name', 'like', '%'.$searchData.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchData.'%');
            });
        }
        
        $totalRecords = $queryRecords->count();
        $totalRecordswithFilter = $queryFilterRecords->count();
        $users = $query->get();

        $data = [];
        foreach($users as $row){
            $btn_action = '<button type="button" id="btnDetail" data-id="'.$row->id.'" data-tradernama="'.$row->name.'"  class="btn btn-primary btn-sm">Detail</button>';
            array_push($data, [
                'name' => $row->name,
                'email' => $row->email,
                'phone' => $row->phone,
                'created_at' => tgl_indo(date('Y-m-d', strtotime($row->created_at))).' '.formatJam($row->created_at),
                'aksi' => $btn_action
            ]);
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function fetchDataTransaksi(Request $request, $userId)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $startDate = $request->startDate;
        $endDate = $request->endDate;

        if($request->filter != ""){
            $totalRecords = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                ->where('tr.last_status', $request->filter)
                ->where('tr.is_deleted', 0)
                ->where('users.id', $userId)
                ->select('count(*) as allcount')
                ->count();
            $totalRecordswithFilter = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                ->where('tr.last_status', $request->filter)
                ->where('tr.is_deleted', 0)
                ->where('users.id', $userId)
                ->where('t.name', 'like', '%' .$searchValue . '%')
                ->count();
    
            $transactions = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                ->where('tr.is_deleted', 0)
                ->where('tr.last_status', $request->filter)
                ->where('users.id', $userId)
                ->skip($start)
                ->take($rowperpage)
                ->select('tr.id', 'tr.uuid', 't.name as trader_name', 'users.email as user_email', 
                    't.id as trader_id', 'e.code_emiten', DB::raw('CONCAT("SAN","-", tr.id, "-", e.code_emiten) as transaction_serial'), 
                    'tr.channel', 'tr.description', 'tr.is_verified', 'tr.split_fee', 'tr.created_at as created_at', 
                    'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                    'tr.last_status as status', 't.phone', 'onepay.transaction_no')
                ->orderBy('tr.created_at', 'DESC')
                ->get();
        }else{
            if($startDate != "" && $endDate != ""){
                $totalRecords = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->whereDate('tr.created_at', '>=', $startDate)
                    ->whereDate('tr.created_at', '<=', $endDate)
                    ->where('tr.is_deleted', 0)
                    ->where('users.id', $userId)
                    ->select('count(*) as allcount')
                    ->count();
                $totalRecordswithFilter = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->whereDate('tr.created_at', '>=', $startDate)
                    ->whereDate('tr.created_at', '<=', $endDate)
                    ->where('tr.is_deleted', 0)
                    ->where('users.id', $userId)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->count();
        
                $transactions = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->whereDate('tr.created_at', '>=', $startDate)
                    ->whereDate('tr.created_at', '<=', $endDate)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->where('tr.is_deleted', 0)
                    ->where('users.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('tr.id', 'tr.uuid', 't.name as trader_name', 'users.email as user_email', 
                        't.id as trader_id', 'e.code_emiten', DB::raw('CONCAT("SAN","-", tr.id, "-", e.code_emiten) as transaction_serial'), 
                        'tr.channel', 'tr.description', 'tr.is_verified', 'tr.split_fee', 'tr.created_at as created_at', 
                        'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                        'tr.last_status as status', 't.phone', 'onepay.transaction_no')
                    ->orderBy('tr.created_at', 'DESC')
                    ->get();
            }else{
                $totalRecords = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->where('tr.is_deleted', 0)
                    ->select('count(*) as allcount')
                    ->where('users.id', $userId)
                    ->count();
                $totalRecordswithFilter = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->where('tr.is_deleted', 0)
                    ->where('users.id', $userId)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->count();
        
                $transactions = User::join('traders as t', 't.user_id', '=', 'users.id')
                    ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                    ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.transaction_id', '=', 'tr.id')
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->where('tr.is_deleted', 0)
                    ->where('users.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('tr.id', 'tr.uuid', 't.name as trader_name', 'users.email as user_email', 
                        't.id as trader_id', 'e.code_emiten', DB::raw('CONCAT("SAN","-", tr.id, "-", e.code_emiten) as transaction_serial'), 
                        'tr.channel', 'tr.description', 'tr.is_verified', 'tr.split_fee', 'tr.created_at as created_at', 
                        'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                        'tr.last_status as status', 't.phone', 'onepay.transaction_no')
                    ->orderBy('tr.created_at', 'DESC')
                    ->get();
            }
            //$query->getQuery()->from(\DB::raw('`onepay_transaction` USE INDEX (transaction_no)'));
            //$transactions = $query->get();
        }
       
        $data = [];
        foreach($transactions as $row){
            $class_action = "btn btn-outline-info btn-sm";
			$text_action  = "Detail";

            $stock = 0;
            $stock_price = 0;

            if($row->status == 'CREATED'){
                $status = '<div class="status badge badge-secondary">Belum Konfirmasi</div>';
				$status_transaction = 1;
            }elseif($row->status == 'WAITING FOR VERIFICATION'){
				$status = '<div class="status badge badge-warning">Menunggu Konfirmasi</div>';
				$status_transaction = 2;
				// confirm button
				$class_action = "btn btn-info btn-sm";
				$text_action = "Konfirmasi";
            }elseif ($row->status == 'VERIFIED') {
                $status = '<div class="status badge badge-success">Lunas</div>';
				$status_transaction = 3;
            }elseif ($row->status == 'EXPIRED'){
				$status = '<div class="status badge badge-danger">Kadaluarsa</div>';
				$status_transaction = 4;
            }else{
				$status = '<div class="status badge badge-secondary">Belum Konfirmasi</div>';
				$status_transaction = 5;
            }

            //$member = '<div class="row"></div>'
            if ($row->channel == 'VA') :
				$channel = 'Virtual Account';
			elseif ($row->channel == 'BANKTRANSFER') :
				$channel = 'Transfer Bank';
			elseif ($row->channel == 'WALLET') :
				$channel = 'Saldo Dompet';
			elseif ($row->channel == 'DANA') :
				$channel = 'DANA';
			elseif ($row->channel == 'MARKET') :
				$channel = 'MARKET (' . strtoupper($row->description) . ')';
			else :
				$channel = '-';
			endif;

            if($row->channel == "MARKET"){
                $market = Markets::where('transaction_id', $row->id)
                    ->select('stock', 'stock_price')
                    ->first();
                if($market != null){
                    $stock = $market->stock;
                    $stock_price = $market->stock_price;
                }
            }

            $idTransaksi = "";
            if($row->transaction_no != null){
                $idTransaksi = $row->transaction_no;
            }else{
                $idTransaksi = $row->transaction_serial;
            }
                
            $transaction = '<div class="row"><div class="col-4">ID:</div><div class="col-8">'.$idTransaksi.'</div></div><div class="row"><div class="col-4">Stock:</div><div class="col-8">'.$row->code_emiten.'</div></div><div class="row">
                <div class="col-4">Price:</div><div class="col-8">'.rupiah2($stock_price).'</div></div><div class="row"> <div class="col-4">Qty:</div><div class="col-8">'.$stock.'</div></div>
                <div class="row"> <div class="col-4">Sumber Dana:</div><div class="col-8">'.$channel.'</div></div>';

            $member = '<div class="col-12">'.$row->trader_name.'</div>'
                .'<div class="col-12">'.$row->user_email.'</div>'
                .'<div class="col-12">'.$row->phone.'</div>';

            $created_at = '<div class="col-12">'.tgl_indo(date('Y-m-d', strtotime($row->created_at)))
                .'</div><div class="col-12">'.formatJam($row->created_at).'</div>';              

            array_push($data, [
                "id" => $row->id,
                "uuid" => $row->uuid,
                "transaksi" => $transaction,
                "member" => $member,
                "amount" => rupiah($row->amount + $row->fee),
                "created_at" => $created_at,
                "split_fee" => rupiah($row->split_fee),
                "status" => $status,
                "link" => '<a href="'.url('/admin/transaction/detail/'.$row->uuid.'/'.$status_transaction).'" class="'.$class_action.'">'.$text_action.'</a>'
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function fetchDataDeposit(Request $request, $userId)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $startDate = $request->startDate;
        $endDate = $request->endDate;

        if($request->filter != ""){
            if($request->filter == "menuggupembayaran"){
                $totalRecords = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->whereNull('deposits.confirmation_photo')  
			        ->whereNull('deposits.bank_to')
                    ->where('u.id', $userId)
                    ->select('count(*) as allcount')
                    ->count();
                $totalRecordswithFilter = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->whereNull('deposits.confirmation_photo')  
			        ->whereNull('deposits.bank_to')
                    ->where('u.id', $userId)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->count();
                $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->whereNull('deposits.confirmation_photo')  
			        ->whereNull('deposits.bank_to')
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('deposits.id', 'deposits.uuid', 'deposits.amount', 'deposits.fee', 
                        'u.email', 'deposits.confirmation_photo', 'deposits.split_fee',
                        'deposits.bank_to', 'deposits.bank_from', 'deposits.channel', 'deposits.account_number', 
                        'deposits.status', 'deposits.created_at', 'deposits.updated_at', 't.name as trader_name', 
                        'deposits.created_by', 'va.account_number as va_account_number', 'va.bank as va_bank',
                        'onepay.transaction_no')
                    ->orderBy('deposits.created_at', 'DESC')
                    ->get();
            }else{
                $totalRecords = $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->select('count(*) as allcount')
                    ->where('u.id', $userId)
                    ->count();
                $totalRecordswithFilter = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->where('u.id', $userId)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->count();
                $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('deposits.status', $request->filter)
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('deposits.id', 'deposits.uuid', 'deposits.amount', 'deposits.fee', 
                        'u.email', 'deposits.confirmation_photo', 'deposits.split_fee',
                        'deposits.bank_to', 'deposits.bank_from', 'deposits.channel', 'deposits.account_number', 
                        'deposits.status', 'deposits.created_at', 'deposits.updated_at', 't.name as trader_name', 
                        'deposits.created_by', 'va.account_number as va_account_number', 'va.bank as va_bank', 't.phone',
                        'onepay.transaction_no')
                    ->orderBy('deposits.created_at', 'DESC')
                    ->get();
            }
        }else{
            if($startDate != "" && $endDate != ""){
                $totalRecords = $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->whereDate('deposits.created_at', '>=', $startDate)
                    ->whereDate('deposits.created_at', '<=', $endDate)
                    ->select('count(*) as allcount')
                    ->where('u.id', $userId)
                    ->count();
                $totalRecordswithFilter = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->whereDate('deposits.created_at', '>=', $startDate)
                    ->whereDate('deposits.created_at', '<=', $endDate)
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->where('u.id', $userId)
                    ->count();
                $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->whereDate('deposits.created_at', '>=', $startDate)
                    ->whereDate('deposits.created_at', '<=', $endDate)
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('deposits.id', 'deposits.uuid', 'deposits.amount', 'deposits.fee', 
                        'u.email', 'deposits.confirmation_photo', 'deposits.split_fee',
                        'deposits.bank_to', 'deposits.bank_from', 'deposits.channel', 'deposits.account_number', 
                        'deposits.status', 'deposits.created_at', 'deposits.updated_at', 't.name as trader_name', 
                        'deposits.created_by', 'va.account_number as va_account_number', 'va.bank as va_bank', 't.phone',
                        'onepay.transaction_no')
                    ->orderBy('deposits.created_at', 'DESC')
                    ->get();
            }else{
                $totalRecords = $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->select('count(*) as allcount')
                    ->where('u.id', $userId)
                    ->count();
                $totalRecordswithFilter = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('t.name', 'like', '%' .$searchValue . '%')
                    ->where('u.id', $userId)
                    ->count();
                $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
                    ->leftJoin('onepay_transaction as onepay', 'onepay.deposit_id', '=', 'deposits.id')
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->select('deposits.id', 'deposits.uuid', 'deposits.amount', 'deposits.fee', 
                        'u.email', 'deposits.confirmation_photo', 'deposits.split_fee',
                        'deposits.bank_to', 'deposits.bank_from', 'deposits.channel', 'deposits.account_number', 
                        'deposits.status', 'deposits.created_at', 'deposits.updated_at', 't.name as trader_name', 
                        'deposits.created_by', 'va.account_number as va_account_number', 'va.bank as va_bank', 't.phone',
                        'onepay.transaction_no')
                    ->orderBy('deposits.created_at', 'DESC')
                    ->get();
            }
        }

        $data = [];
        foreach($deposit as $row){

            $totalDeposit = rupiah($row->amount);
            if(($row->status == 1 && $row->confirmation_photo != null && $row->bank_to != null) || ($row->status == 1 && $row->confirmation_photo == null && $row->bank_to == null)) {
                $status = '<div class="status badge badge-success badge-pill badge" style="margin-bottom:0.5rem;">Sudah<br/>Verifikasi</div> ';
                if($row->channel == 'BANKTRANSFER') {
                    //$status .= '<a href="#" onClick="detailDeposit(\'' . BASE_API_FILE . '' . $deposit->confirmation_photo . '?token=' . $this->session->token . '\')" class="btn btn-info btn-sm btn-block" title="Detail" >Lihat Bukti</a>';
                    $status .= '<a href="#" class="btn btn-info btn-sm" title="Detail">Lihat Bukti</a>'; 
                }
            }elseif($row->status == 0 && $row->confirmation_photo != null && $row->bank_to != null){
                $photo = null;
                //if(ispermitted('CONFIRM_DEPOSIT')){
                    // Nanti dulu
                    // if(empty($deposit->account_number) || $deposit->channel == 'BANKTRANSFER') {
                    //     $photo = BASE_API_FILE . '' . $deposit->confirmation_photo . '?token=' . $this->session->token;
                    // }
                    // $status = '<a href="#" onClick="confirmDeposit(
                    //     \'' . $row->uuid . '\',
                    //     \'' . $photo . '\',
                    //     \'' . $row->trader_name . '\',
                    //     \'' . $totalDeposit . '\')" class="btn btn-info btn-sm btn-block" title="Verifikasi" >
                    //     Verifikasi
                    // </a>';
                    // $status .= '<a href="#" onClick="rejectDeposit(\'' . $deposit->uuid . '\')" class="btn btn-danger btn-sm btn-block" title="Tolak" >Tolak</a>';
                    $status = '<a href="#" onClick="confirmDeposit(
                        //     \'' . $row->uuid . '\',
                        //     \'' . $photo . '\',
                        //     \'' . $row->trader_name . '\',
                        //     \'' . $totalDeposit . '\')" class="btn btn-info btn-sm" title="Verifikasi" >
                        //     Verifikasi
                        // </a>';
                    $status .= '<a href="#" onClick="rejectDeposit(\''. $row->uuid.'\')" class="btn btn-danger btn-sm" title="Tolak" >Tolak</a>';
                //} else {
                //    $status = '<div class="status badge badge-warning badge-pill badge" style="display: block;">Menuggu Verifikasi</div>';
                //}
            }elseif($row->status == 2) {
                $status = '<div class="status badge badge-danger badge-pill badge">Ditolak</div>';
            }else {
                $status = '<div class="status badge badge-warning badge-pill badge">Menunggu Pembayaran</div>';
            }

            $bank_to = '-';
            $bank_from = '-';
            $account_number = '-';
            if($row->channel){
                if($row->channel == 'VA' || isset($row->virtual_account)) {
                    $channel        = 'Virtual Account';
                    $account_number = $row->va_account_number;
                    $bank_to        = $row->va_bank;
                }elseif($row->channel == 'BANKTRANSFER') {
                    $channel        = 'Bank Transfer';
                    $bank_from      = $row->bank_from;
                    $account_number = $row->account_number;

                    switch($row->bank_to) {
                        case 1:
                            $bank_to = 'BCA';
                            break;
                        case 2:
                            $bank_to = 'MANDIRI';
                            break;
                        case 3:
                            $bank_to = 'BRI';
                            break;
                        default:
                            $bank_to = '-';
                            break;
                    }
                }elseif($row->channel == 'DANA') {
                    $channel = 'DANA';
                }elseif($row->channel == 'ONEPAY') {
                    $channel = 'Other Payment (ONEPAY)';
                }else{
                    $channel = 'Bank Transfer';
                }
            }else{
                $channel = $row->created_by;
            }

            $member = '<div class="col-12">ID : '.$row->transaction_no.'</div><div class="col-12">'.$row->trader_name.'</div>'.
                '<div class="col-12">'.$row->email.'</div>'.
                '<div class="col-12">'.$row->phone.'</div>';
            $payment = '<div class="row"><div class="col-6">Method:</div><div class="col-6">'.$channel.'</div></div>'.
                '<div class="row"><div class="col-6">Sender:</div><div class="col-6">'.$bank_from.'</div></div>'.
                '<div class="row"><div class="col-6">Receiver:</div><div class="col-6">'.$bank_to.'</div></div>'.
                '<div class="row"><div class="col-6">Account:</div><div class="col-6">'.$account_number.'</div></div>';
            $created_at = '<div class="col-12">'.tgl_indo(date('Y-m-d', strtotime($row->created_at)))
                .'</div><div class="col-12">'.formatJam($row->created_at).'</div>';              

            array_push($data, [
                "member" => $member,
                "nominal" => rupiah($row->amount + $row->fee),
                "payment" => $payment,
                "created_at" => $created_at,
                "split_fee" => rupiah($row->split_fee),
                "status" => $status
            ]);
        }            
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function fetchDataWithdraw(Request $request, $userId)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $startDate = $request->startDate;
        $endDate = $request->endDate;

        if($request->filter != ""){
            $totalRecords = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                ->join('users as u', 'u.id', '=', 't.user_id')
                ->where('withdraws.is_deleted', 0)
                ->where('withdraws.is_verified', $request->filter)
                ->orderBy('withdraws.id', 'DESC')
                ->where('u.id', $userId)
                ->select('count(*) as allcount')
                ->count();
            $totalRecordswithFilter = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                ->join('users as u', 'u.id', '=', 't.user_id')
                ->where('withdraws.is_deleted', 0)
                ->where('withdraws.is_verified', $request->filter)
                ->where('withdraws.account_name', 'like', '%' .$searchValue . '%')
                ->orderBy('withdraws.id', 'DESC')
                ->where('u.id', $userId)
                ->count();

            $withdraws = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                ->join('users as u', 'u.id', '=', 't.user_id')
                ->where('withdraws.is_deleted', 0)
                ->where('withdraws.is_verified', $request->filter)
                ->skip($start)
                ->take($rowperpage)
                ->where('u.id', $userId)
                ->orderBy('withdraws.id', 'DESC')
                ->select('withdraws.uuid', 't.uuid as trader_uuid', 'withdraws.id', 
                    'withdraws.is_verified', 'withdraws.account_name','withdraws.account_number', 
                    'withdraws.bank_to', 'withdraws.amount', 'withdraws.fee', 'withdraws.created_at', 
                    'withdraws.updated_at', 't.id as trader_id', 't.name as trader_name', 't.phone', 'u.email', 
                    'withdraws.split_fee', 'withdraws.external_id')
                ->get();
        }else{
            if($startDate != "" && $endDate != ""){
                $totalRecords = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->whereDate('withdraws.created_at', '>=', $startDate)
                    ->whereDate('withdraws.created_at', '<=', $endDate)
                    ->where('u.id', $userId)
                    ->orderBy('withdraws.id', 'DESC')
                    ->select('count(*) as allcount')
                    ->count();
                $totalRecordswithFilter = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->where('withdraws.account_name', 'like', '%' .$searchValue . '%')
                    ->whereDate('withdraws.created_at', '>=', $startDate)
                    ->whereDate('withdraws.created_at', '<=', $endDate)
                    ->where('u.id', $userId)
                    ->orderBy('withdraws.id', 'DESC')
                    ->count();
                $withdraws = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->whereDate('withdraws.created_at', '>=', $startDate)
                    ->whereDate('withdraws.created_at', '<=', $endDate)
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->orderBy('withdraws.id', 'DESC')
                    ->select('withdraws.uuid', 't.uuid as trader_uuid', 'withdraws.id', 
                        'withdraws.is_verified', 'withdraws.account_name','withdraws.account_number', 
                        'withdraws.bank_to', 'withdraws.amount', 'withdraws.fee', 'withdraws.created_at', 
                        'withdraws.updated_at', 't.id as trader_id', 't.name as trader_name', 't.phone', 'u.email', 
                        'withdraws.split_fee', 'withdraws.external_id')
                    ->get();
            }else{
                $totalRecords = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->orderBy('withdraws.id', 'DESC')
                    ->where('u.id', $userId)
                    ->select('count(*) as allcount')
                    ->count();
                $totalRecordswithFilter = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->where('withdraws.account_name', 'like', '%' .$searchValue . '%')
                    ->where('u.id', $userId)
                    ->orderBy('withdraws.id', 'DESC')
                    ->count();
                $withdraws = Withdraw::join('traders as t', 't.id', '=', 'withdraws.trader_id')
                    ->join('users as u', 'u.id', '=', 't.user_id')
                    ->where('withdraws.is_deleted', 0)
                    ->where('u.id', $userId)
                    ->skip($start)
                    ->take($rowperpage)
                    ->orderBy('withdraws.id', 'DESC')
                    ->select('withdraws.uuid', 't.uuid as trader_uuid', 'withdraws.id', 
                        'withdraws.is_verified', 'withdraws.account_name','withdraws.account_number', 
                        'withdraws.bank_to', 'withdraws.amount', 'withdraws.fee', 'withdraws.created_at', 
                        'withdraws.updated_at', 't.id as trader_id', 't.name as trader_name', 't.phone', 'u.email', 
                        'withdraws.split_fee', 'withdraws.external_id')
                    ->get();
            }
        }

        $data = [];
        foreach($withdraws as $row){
            $totalWithdraw = rupiahBiasa(($row->amount - $row->fee));
            $saldoAvailable = rupiahBiasa($this->get_saldo($row->trader_uuid));
            $member = '<div class="col-12">ID : '.$row->external_id.' </div><div class="col-12">'.$row->trader_name
                .'</div><div class="col-12">'.$row->email.'</div><div class="col-12">'.$row->phone.'</div><div class="col-12">'
                .$row->bank_to.'</div>';
            $date = '<div class="col-12">'.tgl_indo(date('Y-m-d', strtotime($row->created_at)))
                .'</div><div class="col-12">'.formatJam($row->created_at).'</div>';
            $amount = '<div class="row"><div class="col-6">Withdrawal :</div><div class="col-6">'.rupiah($row->amount).'</div></div><div class="row"><div class="col-6">Fee :</div><div class="col-6">'
                .rupiah($row->fee).'</div></div><div class="row"><div class="col-6">Total :</div><div class="col-6">'.(rupiah($row->amount - $row->fee)).'</div></div>';
            
            if($row->is_verified == 0 || $row->is_verified == null){
                $status = '<a href="#" class="btn btn-info btn-sm btn-block" title="Verifikasi" onClick="confirmWithdraw(\'' . $row->uuid . '\',
                    \'' . $row->account_name . '\',
                    \'' . $row->account_number . '\',
                    \'' . $row->bank_to . '\',                                                                    
                    \'' . $totalWithdraw . '\',
                    \'' . $saldoAvailable . '\')" 
                        >Verifikasi</a> 
                    <a href="#" onClick="rejectWithdraw(\'' . $row->uuid . '\')" class="btn btn-danger btn-sm btn-block" title="Tolak" >Tolak</a>';
            }else if($row->is_verified == 2){
                $status = '<div class="status badge badge-danger badge-pill">Ditolak</div>';
            }elseif($row->is_verified == 1) {
                $status = '<div class="status badge badge-success badge-pill">Sudah Verifikasi</div>';
            }else{
                $status = '<div class="status badge badge-warning badge-pill">Tidak Diketahui</div>';
            }

            $created_at = tgl_indo(date('Y-m-d', strtotime($row->created_at))).' '.formatJam($row->created_at);
            
            array_push($data, [
                "member" => $member,
                "date" => $date,
                "amount" => $amount,
                "split_fee" => rupiah($row->split_fee),
                "status" => $status
            ]);
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    private function get_saldo($uuid)
    {
        $saldo = 0;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/idr/' . $uuid, [
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $saldo = $data['idr'];
            }
        } catch (\Exception $exception) {
            $saldo = null;
        }

        return $saldo;
    }

}
