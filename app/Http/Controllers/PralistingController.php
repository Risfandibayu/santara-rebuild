<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\emiten;
use App\Models\Category;
use App\Models\Regency;
use App\Models\EmitenStatusHistori;
use App\Models\trader;
use App\Exports\CalonPenerbit;
use App\Exports\KycBisnis;
use Maatwebsite\Excel\Facades\Excel;

class PralistingController extends Controller
{

    public function index()
    {
        $jumlahData = $this->getCountTerferifikasi();
        return view('admin.pralisting.index', compact('jumlahData'));
    }

    public function fetchDataPralisting(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $totalRecords = $this->getCountTerferifikasi();
        $totalRecordswithFilter = $this->getCountFilterTerferifikasi($searchValue);
        $pralisting = $this->getDataAPIPralisting($searchValue, $start, $rowperpage);

        $data = [];
        foreach($pralisting as $row){
            $status = \DB::table('emiten_status_histories')->where('emiten_id', $row->id)
                ->select('status')->orderBy('id', 'DESC')->limit(1)
                ->first();

            $investment = \DB::table('emiten_pre_investment_plans')->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COALESCE(SUM(amount),0) as investment'))
                ->first();

            $total_likes = \DB::table('emiten_votes')->where('likes', 1)
                ->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COUNT(likes) as total_likes'))
                ->first();

            $total_votes = \DB::table('emiten_votes')->where('vote', 1)
                ->where('emiten_id', $row->id)
                ->where('is_deleted', 0)
                ->select(\DB::raw('COUNT(vote) as total_votes'))
                ->first();

            $statusVerifikasi = "";
            if($row->is_verified_bisnis == 1){
                $statusVerifikasi = "<span class='badge badge-success'>Terverifikasi</span>";
            }else if($row->is_verified_bisnis == 2){
                $statusVerifikasi = "<span class='badge badge-danger'>Ditolak</span>";
            }else if($row->is_verified_bisnis == null){
                $statusVerifikasi = "-";
            }

            $totalComents = \DB::select('(SELECT COALESCE(COUNT(comment), 0) + COALESCE(COUNT(ch.comment_histories), 0) as total_coments from emiten_comments left join (select emiten_comment_id, COUNT(comment) as comment_histories from emiten_comment_histories where is_deleted = 0 group by id) as ch on emiten_comments.id = ch.emiten_comment_id where emiten_comments.emiten_id = '.$row->id.' and emiten_comments.is_deleted = 0)');

            $button = '<button type="button" onClick="updateStatus(\'' . $row->id . '\',  \'' . $row->company_name . '\',)" class="btn btn-sm btn-success mb-1">Update Status</button>
            <a href="'.url('admin/emiten/edit/'.$row->id).'" class="btn btn-block btn-sm btn-warning">Edit</a>';

            array_push($data, [
                "id" => $row->id,
                'company_name' => $row->company_name,
                'trademark' => $row->trademark,
                'capital_needs' => rupiah($row->avg_capital_needs),
                'is_verified' => $row->is_verified,
                'created_at' => tgl_indo(date('Y-m-d', strtotime($row->created_at))),
                'trader_name' => $row->name,
                'phone' => $row->phone,
                "status" => $statusVerifikasi,
                'investment' => intval($row->avg_general_share_amount).' %',
                'total_likes' => $total_likes->total_likes,
                'total_votes' => $total_votes->total_votes,
                'total_coments' => $totalComents[0]->total_coments,
                'buttonAksi' => $button
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function indexKycBisnis()
    {
        $jumlahData = $this->getCount();
        return view('admin.pralisting.kyc-bisnis', compact('jumlahData'));
    }

    public function fetchDataKYC(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $totalRecords = $this->getCount("", 2, null);
        $totalRecordswithFilter = $this->getCountFilter($searchValue, "", 2, null);
        $pralisting = $this->getDataPralisting($searchValue, $start, $rowperpage);

        $data = [];
        foreach($pralisting as $row){
            if($row->is_verified_bisnis != 1){
                $status = \DB::table('emiten_status_histories')->where('emiten_id', $row->id)
                    ->select('status')->orderBy('id', 'DESC')->limit(1)
                    ->first();
    
                $investment = \DB::table('emiten_pre_investment_plans')->where('emiten_id', $row->id)
                    ->where('is_deleted', 0)
                    ->select(\DB::raw('COALESCE(SUM(amount),0) as investment'))
                    ->first();
    
                $total_likes = \DB::table('emiten_votes')->where('likes', 1)
                    ->where('emiten_id', $row->id)
                    ->where('is_deleted', 0)
                    ->select(\DB::raw('COUNT(likes) as total_likes'))
                    ->first();
    
                $total_votes = \DB::table('emiten_votes')->where('vote', 1)
                    ->where('emiten_id', $row->id)
                    ->where('is_deleted', 0)
                    ->select(\DB::raw('COUNT(vote) as total_votes'))
                    ->first();
    
                $statusVerifikasi = "";
                if($row->is_verified_bisnis == 1){
                    $statusVerifikasi = "<span class='badge badge-success'>Terverifikasi</span>";
                }else if($row->is_verified_bisnis == 2){
                    $statusVerifikasi = "<span class='badge badge-danger'>Ditolak</span>";
                }else if($row->is_verified_bisnis == null){
                    $statusVerifikasi = "-";
                }
    
                $totalComents = \DB::select('(SELECT COALESCE(COUNT(comment), 0) + COALESCE(COUNT(ch.comment_histories), 0) as total_coments from emiten_comments left join (select emiten_comment_id, COUNT(comment) as comment_histories from emiten_comment_histories where is_deleted = 0 group by id) as ch on emiten_comments.id = ch.emiten_comment_id where emiten_comments.emiten_id = '.$row->id.' and emiten_comments.is_deleted = 0)');
    
                $action = '<a href="'.url('admin/kyc-bisnis/konfirmasi/'.$row->trader_uuid).'?tab=biodata-perusahaan&emiten_id='.$row->id.'" class="btn btn-info btn-sm btn-block" title="konfirmasi">Detail</a> ';
                $action .= '<a class="btn btn-info-ghost btn-sm btn-block" href="' . url('admin/pralisting/konfirmasi/' . $row->id) . '" >Konfirmasi</a>';
                $action .= '<a href="#" onClick="deleteBisnis(\'' . $row->uuid . '\',\'' . $row->trademark . '\')"  class="btn btn-danger btn-sm btn-block" title="Hapus">Hapus</a>';
    
                array_push($data, [
                    "id" => $row->id,
                    'company_name' => $row->company_name,
                    'trademark' => $row->trademark,
                    'capital_needs' => rupiah($row->avg_capital_needs),
                    'is_verified' => $row->is_verified,
                    'created_at' => tgl_indo(date('Y-m-d', strtotime($row->created_at))),
                    'trader_name' => $row->name,
                    'phone' => $row->phone,
                    "status" => $statusVerifikasi,
                    'investment' => intval($row->avg_general_share_amount).' %',
                    'total_likes' => $total_likes->total_likes,
                    'total_votes' => $total_votes->total_votes,
                    'total_coments' => $totalComents[0]->total_coments,
                    'buttonAksi' => $action
                ]);
            }
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }


    public function exportCalonPenerbit(Request $request)
    {
        $start = $request->start;
        $rowperpage = $request->rowperpage;
        return Excel::download(new CalonPenerbit($start, $rowperpage), 'Data Coming Soon.xlsx');
    }

    
    public function exportKycBisnis(Request $request)
    {
        $start = $request->start;
        $rowperpage = $request->rowperpage;
        return Excel::download(new KycBisnis($start, $rowperpage), 'Data KYC Bisnis.xlsx');
    }


    public function flagNowPlaying()
    {
        $pralisting = $this->getDataPralisting("", 1, null);
        $emiten = [];
        foreach($pralisting as $row){
            array_push($emiten, [
                'id' => $row['id'],
                'company_name' => $row['company_name'],
                'trademark' => $row['trademark'],
                'code_emiten' => $row['code_emiten'],
                'price' => $row['price'],
                'supply' => $row['supply'],
                'is_deleted' => $row['is_deleted'],
                'is_active' => $row['is_active'],
                'begin_period' => $row['begin_period'],
                'created_at' => $row['created_at'],
                'ktg' => $row['ktg']
            ]);
        }
        return view('admin.pralisting.flag-now-playing', compact('emiten'));
    }

    public function getCount()
    {
        $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
            ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.is_deleted',0)
            ->where('emitens.is_verified',1)
            ->where('emitens.is_pralisting',1)
            ->where('emitens.is_coming_soon',1)
            ->groupBy('emitens.id')
            ->get();
        $data = [];
        foreach($pralisting as $row){
            if($row->is_verified_bisnis != 1){
                array_push($data, [
                    'id' => $row->id
                ]);
            }
        }
        return count($data);

    }

    public function getCountFilter($searchValue)
    {
        $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
            ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
            ->where('emitens.is_deleted',0)
            ->where('emitens.is_verified',1)
            ->where('emitens.is_pralisting',1)
            ->where('emitens.is_coming_soon',1)
            ->groupBy('emitens.id')
            ->get();
        $data = [];
        foreach($pralisting as $row){
            if($row->is_verified_bisnis != 1){
                array_push($data, [
                    'id' => $row->id
                ]);
            }
        }
        return count($data);
    }

    public function getCountTerferifikasi()
    {
        $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
            ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.is_deleted',0)
            ->where('emitens.is_verified',1)
            ->where('emitens.is_pralisting',1)
            ->where('emitens.is_coming_soon',1)
            ->where('emitens.is_verified_bisnis', 1)
            ->groupBy('emitens.id')
            ->get();
        return count($pralisting);

    }

    public function getCountFilterTerferifikasi($searchValue)
    {
        $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
            ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
            ->where('emitens.is_deleted',0)
            ->where('emitens.is_verified',1)
            ->where('emitens.is_pralisting',1)
            ->where('emitens.is_coming_soon',1)
            ->where('emitens.is_verified_bisnis', 1)
            ->groupBy('emitens.id')
            ->get();
    
        return count($pralisting);
    }


    public function getDataPralisting($searchValue, $start, $rowperpage)
    {
        $pralisting = emiten::query();
        $pralisting->leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
            ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id');

        $pralisting->where(function($search) use ($searchValue){
			$search->where('t.name', 'like', '%'.$searchValue.'%')
				->orWhere('emitens.company_name', 'like', '%'.$searchValue.'%');
		});
            
        $pralisting->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader',
                'emitens.avg_general_share_amount', 'emitens.avg_capital_needs')
            ->where('emitens.is_deleted',0)
            ->where('emitens.is_verified',1)
            ->where('emitens.is_pralisting',1)
            ->where('emitens.is_coming_soon',1)
            ->groupBy('emitens.id')
            ->orderby('emitens.created_at','DESC')
            ->skip($start)
            ->take($rowperpage);
        return $pralisting->get();

    }

    public function getDataAPIPralisting($searchValue, $start, $rowperpage)
    {
            $pralisting = emiten::leftjoin('categories', 'categories.id','=','emitens.category_id')
                    ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                    ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                    ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                    ->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                        't.name', 't.phone', 'emitens.is_verified_bisnis', 't.uuid as trader_uuid', 't.name as nama_trader',
                        'emitens.avg_general_share_amount', 'emitens.avg_capital_needs')
                    ->where('emitens.is_deleted',0)
                    ->where('emitens.is_verified',1)
                    ->where('emitens.is_pralisting',1)
                    ->where('emitens.is_coming_soon',1)
                    ->where('emitens.is_verified_bisnis', 1)
                    ->groupBy('emitens.id')
                    ->orderby('emitens.created_at','DESC')
                    ->skip($start)
                    ->take($rowperpage)
                    ->get();
            return $pralisting;
    }

    public function verifiedEmitenBisnis(Request $request)
    {
        $uuid = $request->uuid;
        $status = $request->status;

        $bisnis = emiten::where('uuid', $uuid)->update([
            'is_verified_bisnis' => $status,
            'is_active' => 1,
        ]);

        if($bisnis){
            echo json_encode(['msg' => '200']);
        }else{
            echo json_encode(['msg' => '404']);
        }
    }


    public function konfirmasi($id)
    {
        $emiten = emiten::where('id', $id)
            ->first();
        $trader = trader::find($emiten->trader_id);
        $category = Category::find($emiten->category_id);
        $subcategory = \DB::table('sub_categories')->where('id', $emiten->sub_category_id)->first();
        $regency = Regency::find($emiten->regency_id);
        $emiten->category = $category != null ? $category->category : "";
        $emiten->subcategory = $subcategory != null ? $subcategory->sub_category : "";
        $emiten->regency = $regency != null ? $regency->name : "";
        $emiten->pictures = explode(',', $emiten->pictures);
        $type = "konfirmasi";
        $tabVerifikasiKYC = $this->dataTabVerifikasiKYC($trader->uuid, $emiten->id);
        return view('admin.pralisting.konfirmasi', compact('emiten', 'type', 'tabVerifikasiKYC'));
    }

    public function acceptPralisting(Request $request)
    {
        $uuid = $request->uuid;
        $status = $request->status;
        emiten::where('uuid', $uuid)->update([
            'is_verified' => $status
        ]);

        if($status == 1){
            $status = 'verified';
        }elseif($status == 2){
            $status = 'rejected';
        }else{
            $status = 'waiting for verification';
        }
        $emiten = emiten::where('uuid', $uuid)->first();
        $emitenStatusHistori = new EmitenStatusHistori();
        $emitenStatusHistori->uuid = \Str::uuid();
        $emitenStatusHistori->emiten_id = $emiten->id;
        $emitenStatusHistori->status = $status;
        $emitenStatusHistori->save();


        if ($status == 2) {
            $input = $request->input;
            $user = emiten::join('traders as t', 'emitens.trader_id', '=', 't.id')
                    ->join('users as u', 't.user_id', '=', 'u.id')
                    ->where('emitens.uuid', $uuid)
                    ->select('emitens.id', 'emitens.company_name', 
                        'emitens.trademark', 't.name', 'u.email' )->first();
            $details = [
                'name' => $user->name,
                'trademark' => $user->trademark,
                'reason' => $input
            ];
                        
            $kirimEmail = \Mail::to($user->email)->send(new \App\Mail\RejectPenerbit($details));
            if($kirimEmail){
                echo json_encode(['msg' => 200, "emiten" => $emiten->id]);
            }else{
                echo json_encode(['msg' => 404, "det" => "Gagal Kirim Email"]);
            }
        }else {
            echo json_encode(['msg' => 200, "emiten" => $emiten->id]);
        }
        
    }

    public function acceptpOffice(Request $request)
    {
        $uuid = $request->uuid;
        $status = $request->status;

        $bisnis = emiten::where('uuid', $uuid)->update([
            'is_verified' => 0,
            'is_pralisting' => 0
        ]);

        if($bisnis){
            $this->notification($uuid);
            echo json_encode(['msg' => '200']);
        }else{
            echo json_encode(['msg' => '404']);
        }
    }

    public function notification($uuid) {
        $return = false;
        $client = new \GuzzleHttp\Client();
        $headers = [
            'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
            'Accept'        => 'application/json',
            'Content-type'  => 'application/json'
        ];  
        $endpoint = '/emitens/new-emiten-notification/';

        try {        
            $response = $client->request('GET',  config('global.BASE_API_CLIENT_URL').'/'.config('global.API_CLIENT_VERSION') . $endpoint . $uuid, [
                'headers' => $headers,
            ]);

            if ( $response->getStatusCode() == 200 ) {
                $return = true;
            }

        } catch (\Exception $exception) {
            $return = false;
        }

        return $return;
	}

    public function delete($uuid)
    {
        // $emiten = emiten::where('uuid', $uuid)
        //     ->select('trader_id')
        //     ->first();
        // if ($emiten->trader_id == null) {
        //     echo json_encode(['msg' => '400']);
        //     return;
        // }
        $delete = emiten::where('uuid', $uuid)->update([
                'is_deleted' => 1
            ]);
        if ($delete) {
            echo json_encode(['msg' => '200']);
        } else {
            echo json_encode(['msg' => '400']);
        }
    }

    private function dataTabVerifikasiKYC($uuid, $emitenId){
        $data = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
            ->select( 
              'company_traders.status_kyc1',
              'company_traders.status_kyc2',
              'company_traders.status_kyc3',
              'company_traders.status_kyc4',
              'company_traders.status_kyc5',
              'company_traders.status_kyc6',
              'company_traders.status_kyc7',
              'company_traders.status_kyc8')
            ->where('traders.uuid', $uuid)
            ->where('e.id', $emitenId)
            ->orderBy('company_traders.created_at', 'DESC')
            ->first();

           if($data != null){
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : ""],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : ""],
              ];
          }else{
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
              ];
          }

      return $tab;
        // $data = null;

        //     try {
        //         $client = new \GuzzleHttp\Client();
                        
        //         $headers = [
        //             'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
        //             'Accept'        => 'application/json',
        //             'Content-type'  => 'application/json'
        //         ];
                
        //         $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/status-kyc-trader/'. $uuid, [
        //             'headers' => $headers,
        //   ]);

        //         if ( $response->getStatusCode() == 200 ) {
        //             $data= (object)json_decode($response->getBody()->getContents(), TRUE);
        //         }
        //     } catch (\Exception $exception) {
        //         $data = null;
        // }

        // $data = (object)$data;


        //     if(count(get_object_vars(($data))) != 0){
        //         $tab = (object)[
        //             '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : "verifying"],
        //             '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : "verifying"],
        //             '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : "verifying"],
        //             '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : "verifying"],
        //             '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : "verifying"],				
        //             '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : "verifying"],
        //             '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : "verifying"],
        //             '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : "verifying"],
        //         ];
        //     }else{
        //         $tab = (object)[
        //             '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => 'verifying'],
        //             '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => 'verifying'],
        //             '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => 'verifying'],
        //             '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => 'verifying'],
        //             '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => 'verifying'],				
        //             '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => 'verifying'],
        //             '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => 'verifying'],
        //             '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => 'verifying'],
        //         ];
        //     }

        // return $tab;
	}

}
