<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Permission;

class RoleController extends Controller
{
    
    public function index()
    {
        $roles = Role::where('is_deleted', 0)->get();
        return view('admin.role.index', compact('roles'));
    }

    public function edit($id)
    {
        $permissions = Permission::where('permission', '<>', 'NONE')
            ->orderBy('permission', 'ASC')
            ->get();
        $user_permissions = Role::where('id', $id)->select('permissions')->first();
        $user_permissions =  explode(',', $user_permissions->permissions);
        $role = Role::where('id', $id)->first();

        foreach ( $permissions as $p ) {
			foreach ( $user_permissions as $up ) {
				if ( $p->id == $up ) {
					$p->selected = true;
				}
			}
		}

        return view('admin.role.edit', compact('id', 'permissions', 'role'));
    }

    public function update(Request $request, $id)
    {
        $permissions = implode(',', $request->permissions);
        $role = Role::find($id);
        $role->name = $request->name;
        $role->permissions = $permissions;
        $role->save();
        $notif = array(
            'message' => 'Berhasil mengubah role '.$role->name,
            'alert-type' => 'success'
        );
        return redirect('admin/role')->with($notif);
    }

}
