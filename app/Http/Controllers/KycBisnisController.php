<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\CompanyTrader;
use App\Models\trader;
use App\Models\KycSubmissionDetail;
use App\Models\KycSubmission;
use App\Models\KycSubmissionStatus;
use App\Models\emiten;
use Google\Cloud\Storage\StorageClient;
use DB;

class KycBisnisController extends Controller
{

    public function konfirmasi(Request $request, $uuid)
    {
        $emitenId = $request->emiten_id;
        return $this->detail( $uuid , 'konfirmasi', $emitenId);
    }
    
    public function detail($uuid, $action, $emitenId)
    {
        $country = DB::table('countries')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $province = DB::table('provinces')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $companyCharacters = DB::table('company_characters')->where('is_deleted', 0)
            ->select('id', 'character as label')->get();
        $companyTypes = DB::table('company_types')->where('is_deleted', 0)
            ->select('id', 'type as label')->get();
        $bankInvestors = DB::table('bank_investors')->where('is_deleted', 0)
            ->select('id', 'bank as label')
            ->orderBy('bank', 'ASC')->get();
        $syariah = [];
        for($i = 0; $i < 2; $i++){
            if($i == 0){
                array_push($syariah, [
                    "id" => "Y",
                    "label" => "Ya"
                ]);
            }else{
                array_push($syariah, [
                    "id" => "N",
                    "label" => "Tidak"
                ]);
            }
        }
        $pendapatan = [];
        array_push($pendapatan, [
            "id" => 1,
            "label" => "< Rp 100 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 2,
            "label" => "Rp 100 Milyar - Rp 500 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 3,
            "label" => "Rp 500 Milyar - Rp 1 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 4,
            "label" => "Rp 1 Triliun - Rp 5 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 5,
            "label" => "> Rp 5 Triliun"
        ]);

        $reasonToJoin = [];
        array_push($reasonToJoin, [
            "id" => 'Lain-lain',
            "label" => "Lain-lain"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Price appreciation',
            "label" => "Price appreciation"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Investasi jangka panjang',
            "label" => "Investasi jangka panjang"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Spekulasi',
            "label" => 'Spekulasi'
        ]);
        array_push($reasonToJoin, [
            "id" => 'Pendapatan',
            "label" => "Pendapatan"
        ]);

        $pendapatan2 = [];
        array_push($pendapatan2, [
            "id" => 1,
            "label" => "< Rp 1 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 2,
            "label" => "Rp 1 Milyar - Rp 5 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 3,
            "label" => "Rp 5 Milyar - Rp 10 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 4,
            "label" => "Rp 10 Milyar - Rp 50 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 5,
            "label" => "> Rp 50 Milyar"
        ]);

        $pendidikan = [];
        array_push($pendidikan, [
            "id" => 'Lainnya',
            "label" => "Lainnya"
        ]);
        array_push($pendidikan, [
            "id" => 'SD',
            "label" => 'SD'
        ]);
        array_push($pendidikan, [
            "id" => 'SMP',
            "label" => 'SMP'
        ]);
        array_push($pendidikan, [
            "id" => 'SMA / SMK / D1 / D2',
            "label" => 'SMA / SMK / D1 / D2'
        ]);
        array_push($pendidikan, [
            "id" => 'D3 / Akademi',
            "label" => 'D3 / Akademi'
        ]);
        array_push($pendidikan, [
            "id" => 'S1',
            "label" => 'S1'
        ]);
        array_push($pendidikan, [
            "id" => 'S2',
            "label" => 'S2'
        ]);
        array_push($pendidikan, [
            "id" => 'S3',
            "label" => 'S3'
        ]);
        

        $itemSelect['company_character'] = $companyCharacters;
        $itemSelect['company_type2'] = $companyTypes;
        $itemSelect['country'] = $country;
        $itemSelect['province'] = $province;
        $itemSelect['regencies'] = $regencies;
        $itemSelect['company_syariah'] = $syariah;
        $itemSelect['pendidikan'] = $pendidikan;
        $itemSelect['pendapatan'] = $pendapatan;
        $itemSelect['pendapatan2'] = $pendapatan2;
        $itemSelect['reasonToJoin'] = $reasonToJoin;
        $itemSelect['bankInvestor'] = $bankInvestors;

        $kyc = array();
        $kyc[1] = $this->getDataKYC1($uuid, 'biodata-perusahaan', $emitenId);
        $kyc[2] = $this->getDataKYC2($uuid, 'pajak-perizinan', $emitenId);
        $kyc[3] = $this->getDataKYC3($uuid, 'alamat', $emitenId);
        $kyc[4] = $this->getDataKYC4($uuid, 'penanggung-jawab', $emitenId);
        $kyc[5] = $this->getDataKYC5($uuid, 'aset-perusahaan', $emitenId);
        $kyc[6] = $this->getDataKYC6($uuid, 'profit-preferensi', $emitenId);
        $kyc[7] = $this->getDataKYC7($uuid, 'dokumen-perusahaan', $emitenId);
        $kyc[8] = $this->getDataKYC8($uuid, 'bank-perusahaan', $emitenId);

        $tab = $this->getDataTabKYC($uuid, $emitenId);
        $active = 'kyc-bisnis';
        $title = 'Detail KYC Bisnis';
        $update_url = url('admin/kyc-bisnis/update_url');
        //$confirm_url = url('admin/kyc-bisnis/confirm_url');
        $confirm_url = url("admin/kyc-bisnis/approval-kyc/".$emitenId);
        $form_footer = 'form_footer';

        // dd($kyc);
        return view('admin.kyc_bisnis.detail', compact('active', 'kyc', 'tab', 'action', 'title', 
            'update_url', 'confirm_url', 'form_footer', 'itemSelect', 'emitenId'));

    }

    public function getKYCCompany($uuid)
    {
        $kyc = array();
        $tab = array(
          '1' => 'biodata-perusahaan', 
          '2' => 'pajak-perizinan', 
          '3' => 'alamat', 
          '4' => 'penanggung-jawab', 
          '5' => 'aset-perusahaan', 
          '6' => 'profit-preferensi',
          '7' => 'dokumen-perusahaan', 
          '8' => 'bank-perusahaan'); 

        $kyc[1] = $this->getDataKYC1($uuid, 'biodata-perusahaan');
        $kyc[2] = $this->getDataKYC2($uuid, 'pajak-perizinan');
        $kyc[3] = $this->getDataKYC3($uuid, 'alamat');
        $kyc[4] = $this->getDataKYC4($uuid, 'penanggung-jawab');
        $kyc[5] = $this->getDataKYC5($uuid, 'aset-perusahaan');
        $kyc[6] = $this->getDataKYC6($uuid, 'profit-preferensi');
        $kyc[7] = $this->getDataKYC7($uuid, 'dokumen-perusahaan');
        $kyc[8] = $this->getDataKYC8($uuid, 'bank-perusahaan');

        $dtTab = $this->getDataTabKYC($uuid);

        echo json_encode(["data" => $dtTab]);
    }

    public function getDataKYC1($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('company_characters', 'company_traders.company_character', '=', 'company_characters.id')
          ->join('company_types', 'company_traders.company_type', '=', 'company_types.id')
          ->join('countries', 'company_traders.company_country_domicile', '=', 'countries.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->leftJoin('regencies', 'company_traders.company_country_domicile', '=', 'regencies.id')
          ->join('users', 'traders.user_id', 'users.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_photo, "/uploads/trader/", "")) as company_photo'),
            'company_traders.name as company_name',
            'company_characters.id as company_char_id',
            'company_characters.character as company_char_name',
            'company_traders.company_type as business_type_id',
            'company_types.type as business_type_name',
            'company_traders.company_syariah',
            'company_traders.company_country_domicile',
            'countries.name as country_name',
            'company_traders.company_establishment_place',
            'regencies.name as company_establishment_place_name',
            'company_traders.company_date_establishment',
            'users.email',
            'company_traders.another_email',
            'company_traders.description',
            'company_traders.has_submit_kyc1',
            'company_traders.last_kyc_submission_id'
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          if($trader->last_kyc_submission_id != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 1)
                ->get();

            $trader->submission = $kycSubmissionDetail;
          }else{
            $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                ->where('kyc_submissions.trader_id', $trader->id)
                ->where('step_id', 1)
                ->orderBy('kyc_submissions.id', 'DESC')
                ->select('kyc_submission_details.*')
                ->get();
  
            $trader->submission = $kycSubmissionDetail;
          }
        }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];

        return $kyc;
    }

    public function getDataKYC2($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->select( 
            'company_traders.id',
            'company_traders.tax_account_code',
            'company_traders.lkpub_code',
            'company_traders.npwp',
            'company_traders.registration_date_npwp',
            'company_traders.siup',
            'company_traders.company_certificate_number',
            'company_traders.nib',
            'company_traders.has_submit_kyc2',
            'company_traders.last_kyc_submission_id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          if($trader->last_kyc_submission_id != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 2)
                ->get();

            $trader->submission = $kycSubmissionDetail;
          }else{
            $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                ->where('kyc_submissions.trader_id', $trader->id)
                ->where('step_id', 2)
                ->orderBy('kyc_submissions.id', 'DESC')
                ->select('kyc_submission_details.*')
                ->get();
  
            $trader->submission = $kycSubmissionDetail;
          }
        }

        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];

        return $kyc;
    }

    public function getDataKYC3($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('countries', 'company_traders.company_country_address', '=', 'countries.id')
          ->join('provinces', 'company_traders.company_province_address', '=', 'provinces.id')
          ->join('regencies', 'company_traders.company_regency_address', 'regencies.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.company_country_address',
            'countries.name as country_name',
            'company_traders.company_province_address',
            'provinces.name as province_name',
            'company_traders.company_regency_address',
            'regencies.name as regency_name',
            'company_traders.company_address',
            'company_traders.postal_code',
            'company_traders.company_phone_number',
            'company_traders.fax',
            'company_traders.has_submit_kyc3',
            'company_traders.last_kyc_submission_id',
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          if($trader->last_kyc_submission_id != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 3)
                ->get();

            $trader->submission = $kycSubmissionDetail;
          }else{
            $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                ->where('kyc_submissions.trader_id', $trader->id)
                ->where('step_id', 3)
                ->orderBy('kyc_submissions.id', 'DESC')
                ->select('kyc_submission_details.*')
                ->get();
  
            $trader->submission = $kycSubmissionDetail;
          }
        }

        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
    }

    public function getDataKYC4($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.company_responsible_name1',
            'company_traders.company_responsible_position1',
            'company_traders.company_responsible_idcard_number1',
            'company_traders.company_responsible_expired_date_idcard1',
            'company_traders.company_responsible_npwp1',
            'company_traders.company_responsible_passport1',
            'company_traders.company_responsible_expired_date_passport1',
            'company_traders.company_responsible_name2',
            'company_traders.company_responsible_position2',
            'company_traders.company_responsible_idcard_number2',
            'company_traders.company_responsible_expired_date_idcard2',
            'company_traders.company_responsible_npwp2',
            'company_traders.company_responsible_passport2',
            'company_traders.company_responsible_expired_date_passport2',
            'company_traders.company_responsible_name3',
            'company_traders.company_responsible_position3',
            'company_traders.company_responsible_idcard_number3',
            'company_traders.company_responsible_expired_date_idcard3',
            'company_traders.company_responsible_npwp3',
            'company_traders.company_responsible_passport3',
            'company_traders.company_responsible_expired_date_passport3',
            'company_traders.company_responsible_name4',
            'company_traders.company_responsible_position4',
            'company_traders.company_responsible_idcard_number4',
            'company_traders.company_responsible_expired_date_idcard4',
            'company_traders.company_responsible_npwp4',
            'company_traders.company_responsible_passport4',
            'company_traders.company_responsible_expired_date_passport4',
            'company_traders.has_submit_kyc4',
            'company_traders.last_kyc_submission_id',
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            if($trader->last_kyc_submission_id != null){
              $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                  ->where('step_id', 4)
                  ->get();
  
              $trader->submission = $kycSubmissionDetail;
            }else{
              $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                  ->where('kyc_submissions.trader_id', $trader->id)
                  ->where('step_id', 4)
                  ->orderBy('kyc_submissions.id', 'DESC')
                  ->select('kyc_submission_details.*')
                  ->get();
    
              $trader->submission = $kycSubmissionDetail;
            }
          }

          $kyc = (object)[
            'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
            'page' 		=> $title,
            'uuid' 		=> $uuid
          ];
    
          return $kyc;
    }

    public function getDataKYC5($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.source_of_funds',
            'company_traders.company_total_property1',
            'company_traders.company_total_property2',
            'company_traders.company_total_property3',
            'company_traders.has_submit_kyc5',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            if($trader->last_kyc_submission_id != null){
              $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                  ->where('step_id', 5)
                  ->get();
  
              $trader->submission = $kycSubmissionDetail;
            }else{
              $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                  ->where('kyc_submissions.trader_id', $trader->id)
                  ->where('step_id', 5)
                  ->orderBy('kyc_submissions.id', 'DESC')
                  ->select('kyc_submission_details.*')
                  ->get();
    
              $trader->submission = $kycSubmissionDetail;
            }
          }


        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=>  $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
    }

    public function getDataKYC6($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('e.id', $emitenId)
          ->where('traders.uuid', $uuid)
          ->select(
            'company_traders.id',
            'company_traders.company_income1',
            'company_traders.company_income2',
            'company_traders.company_income3',
            'company_traders.reason_to_join',
            'company_traders.has_submit_kyc6',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            if($trader->last_kyc_submission_id != null){
              $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                  ->where('step_id', 6)
                  ->get();
  
              $trader->submission = $kycSubmissionDetail;
            }else{
              $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                  ->where('kyc_submissions.trader_id', $trader->id)
                  ->where('step_id', 6)
                  ->orderBy('kyc_submissions.id', 'DESC')
                  ->select('kyc_submission_details.*')
                  ->get();
    
              $trader->submission = $kycSubmissionDetail;
            }
          }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataKYC7($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'traders.id',
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_document, "/uploads/trader/", "")) as company_document'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_document_change, "/uploads/trader/", "")) as company_document_change'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.document_sk_kemenkumham, "/uploads/trader/", "")) as document_sk_kemenkumham'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.document_npwp, "/uploads/trader/", "")) as document_npwp'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.siup_file, "/uploads/trader/", "")) as siup_file'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.idcard_director, "/uploads/trader/", "")) as idcard_director'),
            'company_traders.has_submit_kyc7',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            if($trader->last_kyc_submission_id != null){
              $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                  ->where('step_id', 7)
                  ->get();
  
              $trader->submission = $kycSubmissionDetail;
            }else{
              $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                  ->where('kyc_submissions.trader_id', $trader->id)
                  ->where('step_id', 7)
                  ->orderBy('kyc_submissions.id', 'DESC')
                  ->select('kyc_submission_details.*')
                  ->get();
    
              $trader->submission = $kycSubmissionDetail;
            }
          }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataKYC8($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('trader_banks', 'traders.id', '=', 'trader_banks.trader_id')
          ->join('bank_investors as bank1', 'trader_banks.bank_investor1', '=', 'bank1.id')
          ->leftJoin('bank_investors as bank2', 'trader_banks.bank_investor2', '=', 'bank2.id')
          ->leftJoin('bank_investors as bank3', 'trader_banks.bank_investor3', '=', 'bank3.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'traders.id',
            'trader_banks.account_name1',
            'bank1.bank as bank1',
            'trader_banks.account_number1',
            'trader_banks.account_name2',
            'bank2.bank as bank2',
            'trader_banks.account_number2',
            'trader_banks.account_name3',
            'bank3.bank as bank3',
            'trader_banks.account_number3',
            'company_traders.has_submit_kyc8',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            if($trader->last_kyc_submission_id != null){
              $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                  ->where('step_id', 8)
                  ->get();
  
              $trader->submission = $kycSubmissionDetail;
            }else{
              $kycSubmissionDetail = KycSubmissionDetail::join('kyc_submissions', 'kyc_submissions.id', '=', 'kyc_submission_details.kyc_submission_id')
                  ->where('kyc_submissions.trader_id', $trader->id)
                  ->where('step_id', 8)
                  ->orderBy('kyc_submissions.id', 'DESC')
                  ->select('kyc_submission_details.*')
                  ->get();
    
              $trader->submission = $kycSubmissionDetail;
            }
          }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=>  $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataTabKYC($uuid, $emitenId)
    {
        $data = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
            ->select( 
              'company_traders.status_kyc1',
              'company_traders.status_kyc2',
              'company_traders.status_kyc3',
              'company_traders.status_kyc4',
              'company_traders.status_kyc5',
              'company_traders.status_kyc6',
              'company_traders.status_kyc7',
              'company_traders.status_kyc8')
            ->where('traders.uuid', $uuid)
            ->where('e.id', $emitenId)
            ->orderBy('company_traders.created_at', 'DESC')
            ->first();

          if($data != null){
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : ""],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : ""],
              ];
          }else{
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
              ];
          }

      return $tab;
    }

    private function dataKyc($uuid) {
        $kyc = array();
        $tab = array(
          '1' => 'biodata-perusahaan', 
          '2' => 'pajak-perizinan', 
          '3' => 'alamat', 
          '4' => 'penanggung-jawab', 
          '5' => 'aset-perusahaan', 
          '6' => 'profit-preferensi',
          '7' => 'dokumen-perusahaan', 
          '8' => 'bank-perusahaan'); 

        foreach ($tab as $k => $v): 
          $kyc[$k] = $this->getDetailKyc($uuid,$k,$v);
        endforeach;

        return $kyc;
	}

    private function getDetailKyc($uuid, $phase, $page) {
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/company-'.$phase.'/' . $uuid, [
                    'headers' => $headers,
          ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $kyc = (object)[
          'data' 		=> (count((array)$data) > 0) ? (object)$data : null,
          'page' 		=> $page,
          'uuid' 		=> $uuid
        ];
        
        return $kyc;		
	  }

    private function dataTab($uuid){
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/status-kyc-trader/'. $uuid, [
                    'headers' => $headers,
          ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= (object)json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $data = (object)$data;


            if(count(get_object_vars(($data))) != 0){
                $tab = (object)[
                    '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
                    '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
                    '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
                    '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
                    '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
                    '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""],
                    '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : ""],
                    '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : ""],
                ];
            }else{
                $tab = (object)[
                    '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                    '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                    '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                    '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                    '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                    '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                    '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                    '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
                ];
            }

        return $tab;
	}

    public function confirm(Request $request) {
      $user = $request->user;
          $last_kyc_submission_id = $user['last_kyc_submission_id'];
      $data = $this->getDataConfirmation($request->kyc, $user);		

      try {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'kyc-submission-detail/submit/'. $last_kyc_submission_id, [
          'headers' => [
            'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
            'Origin'        => config('global.BASE_FILE_URL'),
          ],
          'json' => $data->submission
        ]);
        
        if ( $response->getStatusCode() == 200 ) {
          $uuid = User::join('traders as t', 't.user_id', '=', 'users.id')
                      ->where('t.uuid', $data->submission['trader_uuid'])
                      ->select('users.uuid')
                      ->first();
          if( ($data->status == true) && ($this->statuskyc($data->submission['trader_uuid']) == true) ){
            $this->notification($uuid->uuid);
          }
          echo json_encode(['msg' => $response->getStatusCode() ]); 
          return;	
        }

      } catch (\Exception $exception) {
        echo json_encode(['msg' => $exception]);
        return;	
      }	
	}

  public function approvalKYC(Request $request, $emitenId)
  {
    DB::beginTransaction();
      try{
        $user = $request->user;
        $last_kyc_submission_id = $user['last_kyc_submission_id'];
        $kyc = $request->kyc;	

        $data = $this->getDataConfirmation($request->kyc, $user);	

        $steps = [];
        if($user['step_id'] == 1){
          $steps = [
            'company_photo',
            'company_name',
            'company_character',
            'company_type2',
            'company_syariah',
            'company_country_domicile',
            'company_establishment_place',
            'company_date_establishment',
            'company_email',
            'company_another_email',
            'company_description',
          ];
        }elseif($user['step_id'] == 2){
          $steps = [
            'company_tax_account_code',
            'company_lkpub_code',
            'company_npwp',
            'company_registration_date_npwp',
            'company_siup',
            'company_certificate_number',
            'company_nib',
          ];
        }elseif($user['step_id'] == 3){
          $steps = [
            'company_country_address',
            'company_province_address',
            'company_regency_address',
            'company_address',
            'company_postal_code',
            'company_phone_number',
            'company_fax',
            'phone',
          ];
        }elseif($user['step_id'] == 4){
          $steps = [
            'company_responsible_name1',
            'company_responsible_position1',
            'company_responsible_idcard_number1',
            'company_responsible_expired_date_idcard1',
            'company_responsible_npwp1',
            'company_responsible_passport1',
            'company_responsible_expired_date_passport1',
            'company_responsible_name2',
            'company_responsible_position2',
            'company_responsible_idcard_number2',
            'company_responsible_expired_date_idcard2',
            'company_responsible_npwp2',
            'company_responsible_passport2',
            'company_responsible_expired_date_passport2',
            'company_responsible_name3',
            'company_responsible_position3',
            'company_responsible_idcard_number3',
            'company_responsible_expired_date_idcard3',
            'company_responsible_npwp3',
            'company_responsible_passport3',
            'company_responsible_expired_date_passport3',
            'company_responsible_name4',
            'company_responsible_position4',
            'company_responsible_idcard_number4',
            'company_responsible_expired_date_idcard4',
            'company_responsible_npwp4',
            'company_responsible_passport4',
            'company_responsible_expired_date_passport4',
          ];
        }elseif($user['step_id'] == 5){
          $steps = [
            'company_source_of_funds',
            'company_total_property1',
            'company_total_property2',
            'company_total_property3',
          ];
        }elseif($user['step_id'] == 6){
          $steps = [
            'company_income1', 'company_income2', 'company_income3', 'company_reason_to_join'
          ];
        }elseif($user['step_id'] == 7){
          $steps = [
            'company_document',
            'company_document_change',
            'document_sk_kemenkumham',
            'document_npwp',
            'idcard_director',
            'document_siup',
          ];
        }elseif($user['step_id'] == 8){
          $steps = [
            'account_name1',
            'bank_investor1',
            'account_number1',
            'account_name2',
            'bank_investor2',
            'account_number2',
            'account_name3',
            'bank_investor3',
            'account_number3',          
          ];
        }

        $countError = 0;

        $trader = emiten::find($emitenId);

        if($last_kyc_submission_id == null){

          $kycSubmission = new KycSubmission();
          $kycSubmission->trader_id = $trader->trader_id;
          $kycSubmission->save();

          $kycSubmissionStatus = new KycSubmissionStatus();
          $kycSubmissionStatus->kyc_submission_id = $kycSubmission->id;
          $kycSubmissionStatus->save();

          $last_kyc_submission_id = $kycSubmission->id;

          CompanyTrader::join('emitens as e', 'e.trader_id', '=', 'company_traders.trader_id')
            ->where('company_traders.trader_id', $trader->trader_id)
            ->where('e.id', $emitenId)
            ->update([
              'last_kyc_submission_id' => $last_kyc_submission_id
            ]);

            for ($i=0; $i < count($steps); $i++) { 
              $kycSubmissionDetail = new KycSubmissionDetail();
              $kycSubmissionDetail->field_id = $steps[$i];
              $kycSubmissionDetail->step_id = $user['step_id'];
              $kycSubmissionDetail->status = isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->status : 0;
              $kycSubmissionDetail->error = isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->error : null;
              $kycSubmissionDetail->kyc_submission_id = $last_kyc_submission_id;
              $kycSubmissionDetail->save();

              if(isset($data->submission[$steps[$i]][0])) {
                if($data->submission[$steps[$i]][0]->status == 0){
                  $countError++;
                }
              }
            }

        }else {

          $cekKycSubmission = KycSubmissionDetail::where('step_id', $user['step_id'])
            ->where('kyc_submission_id', $last_kyc_submission_id)
            ->first();

          if($cekKycSubmission == null){
      
              for ($i=0; $i < count($steps); $i++) { 
                $kycSubmissionDetail = new KycSubmissionDetail();
                $kycSubmissionDetail->field_id = $steps[$i];
                $kycSubmissionDetail->step_id = $user['step_id'];
                $kycSubmissionDetail->status = isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->status : 0;
                $kycSubmissionDetail->error = isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->error : null;
                $kycSubmissionDetail->kyc_submission_id = $last_kyc_submission_id;
                $kycSubmissionDetail->save();
    
                if(isset($data->submission[$steps[$i]][0])) {
                  if($data->submission[$steps[$i]][0]->status == 0){
                    $countError++;
                  }
                }
              }
    

          }else{
      
              for ($i=0; $i < count($steps); $i++) { 
               
                  KycSubmissionDetail::where('step_id', $user['step_id'])
                    ->where('kyc_submission_id', $last_kyc_submission_id)
                    ->where('field_id', $steps[$i])
                    ->update([
                      'status' => isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->status : 0,
                      'error' => isset($data->submission[$steps[$i]][0]) ? $data->submission[$steps[$i]][0]->error : null
                    ]);
      
                  if(isset($data->submission[$steps[$i]][0])) {
                    if($data->submission[$steps[$i]][0]->status == 0){
                      $countError++;
                    }
                  }
              }

          }

        }

        $status = "";
        if($countError == 0){
          $status = "verified";
        }else{
          $status = "rejected";
        }

        CompanyTrader::where('trader_id', $trader->trader_id)
          ->where('last_kyc_submission_id', $last_kyc_submission_id)
          ->update([
            'status_kyc'.$user['step_id'] => $status
          ]);

        $cekSudahTerverifikasi =  CompanyTrader::where('trader_id', $trader->trader_id)
            ->where('last_kyc_submission_id', $last_kyc_submission_id)
            ->first();

        if($cekSudahTerverifikasi->status_kyc1 == "verified" && $cekSudahTerverifikasi->status_kyc2 == "verified" &&
          $cekSudahTerverifikasi->status_kyc3 == "verified" && $cekSudahTerverifikasi->status_kyc4 == "verified" &&
          $cekSudahTerverifikasi->status_kyc5 == "verified" && $cekSudahTerverifikasi->status_kyc6 == "verified" &&
          $cekSudahTerverifikasi->status_kyc7 == "verified" && $cekSudahTerverifikasi->status_kyc8 == "verified"){

            $kycSubmission = KycSubmission::find($last_kyc_submission_id);
            $kycSubmission->status = $status;
            $kycSubmission->save();

            $kycSubmissionStatus = KycSubmissionStatus::where('kyc_submission_id', $last_kyc_submission_id)
              ->update([
                'status' => $status
              ]);

          }

        DB::commit();

        echo json_encode(['msg' => 200 ]); 
        return;

     }catch(\Exception $e){
        DB::rollback();
        echo json_encode(['msg' => 404 ]); 
        return;
     }
  }

  public function statuskyc($uuid) {
		$data = null;
		$return = false;

        try {
            $client = new \GuzzleHttp\Client();
                    
            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];
            
            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/status-kyc-trader/'. $uuid, [
                'headers' => $headers,
			]);

            if ( $response->getStatusCode() == 200 ) {
                $data= (object)json_decode($response->getBody()->getContents(), TRUE);
            }
        } catch (\Exception $exception) {
            $data = null;
		}

		$value = array_values((array)$data);
		if (array_unique($value) === array('verified')) { 
			$return = true;
		}

		return $return;
	}

    private function getDataConfirmation($input, $user){
        $status = true;
        $error = [];
        $data = [
          'trader_uuid' => $user['trader_uuid'],
          'step_id'     => $user['step_id']
        ];
        
        foreach ($input as $k => $v) {
          if(substr($v['name'],0,6) == 'error_'){
            $error[$v['name']] = $v['value'];				
          }
        }

        foreach ($input as $k => $v) {
          if(strpos($v['name'], 'field_') !== false){
            // Jika input name mengandung kata2 field_ maka2 abaikan
          }else {
            if(substr($v['name'],0,6) != 'error_'){
              if($v['value'] == 0){
                $data[$v['name']] = [(object)[
                  'status' => FALSE, 
                  'error' => $error['error_'.$v['name']]
                ]];
                $status = false;
              }
        
              if($v['value'] == 1){
                $data[$v['name']] = [(object)[
                  'status' => TRUE,
                  'error' => null 
                ]];
              }
            }
          }
        }
        

        $result = (object)[];
        $result->submission = $data;
        $result->status = $status;

        return $result;
	  }

    public function updateKycBisnisBiodata(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->name = $request->field_company_name;

        if($request->hasFile('field_company_photo')){
            $filePicture = fopen($request->file('field_company_photo')->getPathName(), 'r');
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
            $bucket = $storage->bucket($storageBucketName);
            $image_name = 'photo-' . time() . $request->file('field_company_photo')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($filePicture, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_photo = '/uploads/trader/'.$image_name;
        }
        $company->company_character = $request->field_company_character;
        $company->company_type = $request->field_company_type2;
        $company->company_syariah = $request->field_company_syariah;
        $company->company_country_domicile = $request->field_company_country_domicile;
        $company->company_establishment_place = $request->field_company_establishment_place;
        $company->company_date_establishment = $request->field_company_date_establishment;
        $company->another_email = $request->field_company_another_email;
        $company->description = $request->field_company_description;
        $company->has_submit_kyc1 = 1;
        $company->save();
        $user = User::find($trader->user_id);
        $user->email = $request->field_company_email;
        $user->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah biodata perusahaan"]);
    }

    public function updateKycBisnisPajakIzin(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->tax_account_code = $request->field_company_tax_account_code;
        if ($request->field_company_tax_account_code == '1016') {
             $company->tax_account_company = 'ASABRI';
          } else if ($request->field_company_tax_account_code == '1018') {
             $company->tax_account_company = 'BADAN USAHA TETAP';
          } else if ($request->field_company_tax_account_code == '1077') {
             $company->tax_account_company = 'BADAN USAHA TETAP KHUSUS NON TAX';
          } else if ($request->field_company_tax_account_code == '1008') {
             $company->tax_account_company = 'BANK - DOMESTIC';
          } else if ($request->field_company_tax_account_code == '1009') {
             $company->tax_account_company = 'BANK - FOREIGN(JOINT VENTURE)';
          } else if ($request->field_company_tax_account_code == '1036') {
             $company->tax_account_company = 'BANK - PURE FOREIGN';
          } else if ($request->field_company_tax_account_code == '1001') {
             $company->tax_account_company = 'BROKER (LOCAL BROKER)';
          } else if ($request->field_company_tax_account_code == '1002') {
             $company->tax_account_company = 'CUSTODIAN BANK';
          } else if ($request->field_company_tax_account_code == '1067') {
             $company->tax_account_company = 'FINANCIAL INSTITUION';
          } else if ($request->field_company_tax_account_code == '1078') {
             $company->tax_account_company = 'GOVERMENT OF INDONESIA';
          } else if ($request->field_company_tax_account_code == '1035') {
             $company->tax_account_company = 'INSTITUTION - DOMESTIC (NON FI)';
          } else if ($request->field_company_tax_account_code == '1017') {
             $company->tax_account_company = 'INSTITUTION - FOREIGN (NON FI)';
          } else if ($request->field_company_tax_account_code == '1004') {
             $company->tax_account_company = 'INSTITUTION FOREIGN NO TAX';
          } else if ($request->field_company_tax_account_code == '1100') {
             $company->tax_account_company = 'INSURANCE NON NPWP';
          } else if ($request->field_company_tax_account_code == '1019') {
             $company->tax_account_company = 'INSURANCE NPWP';
          } else if ($request->field_company_tax_account_code == '1005') {
             $company->tax_account_company = 'ISSUER';
          } else if ($request->field_company_tax_account_code == '1015') {
             $company->tax_account_company = 'JAMSOSTEK JHT';
          } else if ($request->field_company_tax_account_code == '1082') {
             $company->tax_account_company = 'JAMSOSTEK NON JHT';
          } else if ($request->field_company_tax_account_code == '1013') {
             $company->tax_account_company = 'KOPERASI';
          } else if ($request->field_company_tax_account_code == '1006') {
             $company->tax_account_company = 'MUTUAL FUND';
          } else if ($request->field_company_tax_account_code == '1080') {
             $company->tax_account_company = 'MUTUAL FUND MORE THAN 5 YEAR';
          } else if ($request->field_company_tax_account_code == '1007') {
             $company->tax_account_company = 'PENSION FUND';
          } else if ($request->field_company_tax_account_code == '1098') {
             $company->tax_account_company = 'PERUSAHAAN TERBATAS NON NPWP';
          } else if ($request->field_company_tax_account_code == '1037') {
             $company->tax_account_company = 'PERUSAHAAN TERBATAS NPWP';
          } else if ($request->field_company_tax_account_code == '1014') {
             $company->tax_account_company = 'TASPEN';
          } else if ($request->field_company_tax_account_code == '1099') {
             $company->tax_account_company = 'YAYASAN NON NPWP';
          } else if ($request->field_company_tax_account_code == '1012') {
             $company->tax_account_company = 'YAYASAN NPWP';
          }
        $company->lkpub_code = $request->field_company_lkpub_code;
        if ($request->field_company_lkpub_code == '001') {
            $company->lkpub = 'BANK INDONESIA';
        } else if ($request->field_company_lkpub_code == '100') {
            $company->lkpub = 'PEMERINTAH PUSAT';
          } else if ($request->field_company_lkpub_code == '200') {
            $company->lkpub = 'BANK SENTRAL DI LN';
          } else if ($request->field_company_lkpub_code == '290') {
            $company->lkpub = 'BANK UMUM DI INDONESIA';
          } else if ($request->field_company_lkpub_code == '299') {
            $company->lkpub = 'BANK UMUM DI LUAR NEGERI';
          } else if ($request->field_company_lkpub_code == '310') {
            $company->lkpub = 'PERUSAHAAN PEMBIAYAAN';
          } else if ($request->field_company_lkpub_code == '320') {
            $company->lkpub = 'MODAL VENTURA';
          } else if ($request->field_company_lkpub_code == '330') {
            $company->lkpub = 'PERUSAHAAN SEKURITAS';
          } else if ($request->field_company_lkpub_code == '340') {
            $company->lkpub = 'PERUSAHAAN ASURANSI';
          } else if ($request->field_company_lkpub_code == '350') {
            $company->lkpub = 'DANA PENSIUN';
          } else if ($request->field_company_lkpub_code == '360') {
            $company->lkpub = 'REKSADANA';
          } else if ($request->field_company_lkpub_code == '410') {
            $company->lkpub = 'BUMN NON LEMBAGA KEUANGAN';
          } else if ($request->field_company_lkpub_code == '430') {
            $company->lkpub = 'PERUSAHAAN LAINNYA';
          } else if ($request->field_company_lkpub_code == 'XXX') {
            $company->lkpub = 'PERUSAHAAN ASING';
          } else if ($request->field_company_lkpub_code == '999') {
            $company->lkpub = 'LAINNYA';
          }
        $company->npwp = $request->field_company_npwp;
        $company->registration_date_npwp = $request->field_company_registration_date_npwp;
        $company->siup = $request->field_company_siup;
        $company->company_certificate_number = $request->field_company_certificate_number;
        $company->nib = $request->field_company_nib;
        $company->has_submit_kyc2 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah pajak & perizinan"]);
    }

    public function updateKycBisnisAlamat(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->company_country_address = $request->field_company_country_address;
        $company->company_province_address = $request->field_company_province_address;
        $company->company_regency_address = $request->field_company_regency_address;
        $company->company_address = $request->field_company_address;
        $company->postal_code = $request->field_postal_code;
        $company->company_phone_number = str_replace('0', '62', $request->field_company_phone_number);
        $company->fax = $request->field_company_fax;
        $company->has_submit_kyc3 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisPenanggungJawab(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->company_responsible_name1 = $request->field_company_responsible_name1;
        $company->company_responsible_position1 = $request->field_company_responsible_position1;
        $company->company_responsible_idcard_number1 = $request->field_company_responsible_idcard_number1;
        $company->company_responsible_expired_date_idcard1 = $request->field_company_responsible_expired_date_idcard1;
        $company->company_responsible_npwp1 = $request->field_company_responsible_npwp1;
        $company->company_responsible_passport1 = $request->field_company_responsible_passport1;
        $company->company_responsible_expired_date_passport1 = $request->field_company_responsible_expired_date_passport1;
        $company->has_submit_kyc4 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisAsetPerusahaan(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->source_of_funds = $request->field_company_source_of_funds;
        if ($request->field_company_total_property1 == '1') {
            $company->company_total_property1 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property1 == '2') {
            $company->company_total_property1 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property1 == '3') {
            $company->company_total_property1 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property1 == '4') {
            $company->company_total_property1 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property1 == '5') {
            $company->company_total_property1 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_total_property2 == '1') {
            $company->company_total_property2 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property2 == '2') {
            $company->company_total_property2 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property2 == '3') {
            $company->company_total_property2 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property2 == '4') {
            $company->company_total_property2 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property2 == '5') {
            $company->company_total_property2 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_total_property3 == '1') {
            $company->company_total_property3 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property3 == '2') {
            $company->company_total_property3 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property3 == '3') {
            $company->company_total_property3 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property3 == '4') {
            $company->company_total_property3 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property3 == '5') {
            $company->company_total_property3 = '> Rp 50 Milyar';
          } 
        $company->has_submit_kyc5 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisProfitReferensi(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        if ($request->field_company_income1 == '1') {
            $company->company_income1 = '< Rp 1 Milyar';
          } else if ($request->field_company_income1 == '2') {
            $company->company_income1 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income1 == '3') {
            $company->company_income1 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income1 == '4') {
            $company->company_income1 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income1 == '5') {
            $company->company_income1 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_income2 == '1') {
            $company->company_income2 = '< Rp 1 Milyar';
          } else if ($request->field_company_income2 == '2') {
            $company->company_income2 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income2 == '3') {
            $company->company_income2 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income2 == '4') {
            $company->company_income2 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income2 == '5') {
            $company->company_income2 = '> Rp 50 Milyar';
          }  
          if ($request->field_company_income3 == '1') {
            $company->company_income3 = '< Rp 1 Milyar';
          } else if ($request->field_company_income3 == '2') {
            $company->company_income3 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income3 == '3') {
            $company->company_income3 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income3 == '4') {
            $company->company_income3 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income3 == '5') {
            $company->company_income3 = '> Rp 50 Milyar';
          } 
        if ($request->field_reason_to_join == 'Lain-lain') {
            $company->reason_to_join_code = '1';
          } else if ($request->field_reason_to_join == 'Price appreciation') {
            $company->reason_to_join_code = '2';
          } else if ($request->field_reason_to_join == 'Investasi jangka panjang') {
            $company->reason_to_join_code = '3';
          } else if ($request->field_reason_to_join == 'Spekulasi') {
            $company->reason_to_join_code = '4';
          } else if ($request->field_reason_to_join == 'Pendapatan') {
            $company->reason_to_join_code = '5';
          }
        $company->reason_to_join = $request->field_reason_to_join;
        $company->has_submit_kyc6 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah profit & preferensi"]);
    }

    public function updateKycBisnisDokumenPerusahaan(Request $request)
    {
        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
          'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
        $bucket = $storage->bucket($storageBucketName);

        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        if($request->hasFile('field_company_document')){
            $fileDocument = fopen($request->file('field_company_document')->getPathName(), 'r');
            $companyDokument = 'dokumen-perusahaan-' . time() . $request->file('field_company_document')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokument;
            $bucket->upload($fileDocument, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document = '/uploads/trader/'.$companyDokument;
        }
        if($request->hasFile('field_company_document_change')){
            $fileDocumentChange = fopen($request->file('field_company_document_change')->getPathName(), 'r');
            $companyDokumentChange = 'dokumen-perusahaan2-' . time() . $request->file('field_company_document_change')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokumentChange;
            $bucket->upload($fileDocumentChange, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document_change = '/uploads/trader/'.$companyDokumentChange;
        }
        if($request->hasFile('field_document_sk_kemenkumham')){
            $fileDocumentSKKemenkumham = fopen($request->file('field_document_sk_kemenkumham')->getPathName(), 'r');
            $image_name = 'dokumen-sk-kemenkumham-' . time() . $request->file('field_document_sk_kemenkumham')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSKKemenkumham, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_sk_kemenkumham = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_npwp')){
            $fileDocumentNPWP = fopen($request->file('field_document_npwp')->getPathName(), 'r');
            $image_name = 'document_npwp-' . time() . $request->file('field_document_npwp')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentNPWP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_npwp = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_siup')){
            $fileDocumentSIUP = fopen($request->file('field_document_siup')->getPathName(), 'r');
            $image_name = 'document_siup-' . time() . $request->file('field_document_siup')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->siup_file = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_idcard_director')){
            $fileDocumentSIUP = fopen($request->file('field_idcard_director')->getPathName(), 'r');
            $image_name = 'idcard_director-' . time() . $request->file('field_idcard_director')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->idcard_director = '/uploads/trader/'.$image_name;
        }
        $company->has_submit_kyc7 = 1;
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah dokumen perusahaan"]);
    }

    public function updateKycBankPerusahaan(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->has_submit_kyc8 = 1;
        $company->save();

        $cekTraderBank = DB::table('trader_banks')->where('trader_id', $trader->id)->count();
        if($cekTraderBank > 0){
          DB::table('trader_banks')->where('trader_id', $trader->id)->update([
            'bank_investor1' => $request->field_bank_investor1,
            'account_name1' => $trader->name,
            'account_number1' => $request->field_account_number1
          ]);
        }else{
          DB::table('trader_banks')->insert([
            'uuid' => \Str::uuid(),
            'trader_id' => $trader->id,
            'bank_investor1' => $request->field_bank_investor1,
            'account_name1' => $trader->name,
            'account_number1' => $request->field_account_number1
          ]);
        }

        return response()->json(["code" => 200, "message" => "Berhasil mengubah bank perusahaan"]);
    }


    private function notification($uuid)
    {
        $return = false;
        $client = new \GuzzleHttp\Client();
        $headers = [
            'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
            'Accept'        => 'application/json',
            'Content-type'  => 'application/json'
        ];  

        try {        
            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/trader-verify-notification/' . $uuid, [
                'headers' => $headers,
            ]);

            if ( $response->getStatusCode() == 200 ) {
                $return = true;
            }

        } catch (\Exception $exception) {
            $return = false;
        }

        return $return;
    }




}
