<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\emiten;
use App\Models\Devidend_old;
use App\Models\emiten_journey;
use App\Models\Dividen;
use App\Models\StopNotification;
use Carbon\Carbon;

class NotifDividenController extends Controller
{
    
    public function index()
    {
        return view('admin.emiten.notif-dividen');
    }

    public function fetchData(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $statusFilter = $request->get('status');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
        $searchValue = $search_arr['value'];

        $queryRecords = emiten::query();
        $queryRecords->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.is_deleted',0)
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
                IF(
                (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                    (SUM(
                        IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                        and 
                        emitens.is_deleted = 0
                        and emitens.is_active = 1
                        and emitens.begin_period < now()');
        
        $queryFilterRecords = emiten::query();
        $queryFilterRecords->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.is_deleted',0)
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
                IF(
                (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                    (SUM(
                        IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                        and 
                        emitens.is_deleted = 0
                        and emitens.is_active = 1
                        and emitens.begin_period < now()');

        
        $query = emiten::query();
        $query->select('emitens.id', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
            'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
            'emitens.begin_period as sd', 'emitens.end_period as ed', 't.user_id', 'emitens.period')
            ->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->where('emitens.is_deleted',0)
            ->skip($start)
            ->take($rowperpage)
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
                IF(
                (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                    (SUM(
                        IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                        and 
                        emitens.is_deleted = 0
                        and emitens.is_active = 1
                        and emitens.begin_period < now()');

        if($searchValue != ""){
            $queryFilterRecords->where(function($search) use ($searchValue){
                $search->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->orWhere('emitens.code_emiten', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.name', 'like', '%'.$searchValue.'%');
            });
            $query->where(function($search) use ($searchValue){
                $search->where('emitens.company_name', 'like', '%'.$searchValue.'%')
                    ->orWhere('emitens.code_emiten', 'like', '%'.$searchValue.'%')
                    ->orWhere('t.name', 'like', '%'.$searchValue.'%');
            });
        }

        $queryRecords->get();
        $queryFilterRecords->get();
        $totalRecords = count($queryRecords->get());
        $totalRecordswithFilter = count($queryFilterRecords->get());
        $sold_out = $query->get();

        $data = [];
        foreach($sold_out as $row){
            $bulan = str_replace("1 tahun lalu", '', $row->period);
            $bulan = intval(str_replace(" bulan", '', $row->period));
            $dividen_date = "";
            $dividen = Dividen::where('emiten_id', $row->id)
                ->orderBy('id', 'DESC')
                ->first();
            if($dividen != null){
                $dividen_date = Carbon::parse($dividen->devidend_date)
                    ->addMonths($bulan);
            }else{
                $tmpyd = emiten_journey::select('emiten_journeys.date', 'emiten_journeys.end_date')
                    ->leftJoin('emitens', 'emitens.id','=','emiten_journeys.emiten_id')
                    ->where('emiten_journeys.emiten_id', $row->id)
                    ->where('emiten_journeys.title','Penyerahan Dana')
                    ->where('emitens.is_active',1)
                    ->where('emitens.is_deleted',0)
                    ->groupBy('emitens.id')
                    ->first();
                if($tmpyd != null){
                    $dividen_date = Carbon::parse($tmpyd->date)
                        ->addMonths($bulan);
                }
            }

            $stopNotifDividen = StopNotification::where('user_id', $row->user_id)
                ->whereMonth('date_notification', $dividen_date)
                ->where('type_notification', 'dividend')
                ->where('is_deleted', 0)
                ->count();
            $statusNotifikasiDividen = "";
            $button = "";
            if($stopNotifDividen == 0){
                $statusNotifikasiDividen = "-";
                $button = '<button class="btn btn-danger btn-sm" onClick="actionNotif(\'' . $row->user_id.'\', 
                    \''.$dividen_date. '\', \''."turnOff".'\')" >Stop Notifikasi</button>';
            }else{
                $statusNotifikasiDividen = "<span class='badge badge-danger'>STOP BY ADMIN</span>";
                $button = '<button class="btn btn-primary btn-sm" onClick="actionNotif(\'' . $row->user_id.'\', 
                    \''.$dividen_date. '\', \''."turnOn".'\')" >Hidupkan Notifikasi</button>';
            }

            array_push($data, [
                'id' => $row->id,
                'company_name' => $row->company_name,
                'code_emiten' => $row->code_emiten,
                'date_dividend' => tgl_indo(date('Y-m-d', strtotime($dividen_date))),
                'status' => $statusNotifikasiDividen,
                'aksi' => $button
            ]);
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function stopNotifDividen(Request $request)
    {
        $message = "";
        if($request->status == "turnOff"){
            $stopNotifDividen = new StopNotification();
            $stopNotifDividen->type_notification = "dividend";
            $stopNotifDividen->user_id = $request->user_id;
            $stopNotifDividen->date_notification = $request->date_notification;
            $stopNotifDividen->is_deleted = 0;
            $stopNotifDividen->save();
            $message = "Notifikasi Pemberitahuan Berhasil Dihentikan";
        }elseif($request->status == "turnOn") {
            $formatDate = Carbon::parse($request->date_notification)->format('Y-m-d');
            StopNotification::where('user_id', $request->user_id)
                ->where('date_notification', $formatDate)
                ->update([
                    'is_deleted' => 1
                ]);
            $message = "Notifikasi Pemberitahuan Berhasil Diaktifkan";
        }
        return response()->json(["code" => 200, "message" => $message]);
    }

    public function sendNotif($id)
    {
        $emiten = emiten::join('traders as t', 't.id', '=', 'emitens.trader_id')
            ->join('users as u', 'u.id', '=', 't.user_id')
            ->where('emitens.id', $id)
            ->select('u.email', 'emitens.company_name', 'emitens.begin_period')
            ->first();
        $details = [
            'subject' => 'Pembagian Dividen',
            'company_name' => $emiten->company_name,
            'tanggal_dividen' => $emiten->begin_period
        ];
        \Mail::to($emiten->email)->send(new \App\Mail\NotifPemberitahuanDividen($details));
        return response()->json(["code" => 200, "message" => "Berhasil kirim email pemberitahuan"]);
    }

}
