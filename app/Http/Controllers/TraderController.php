<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelper;
use App\Models\Conversations;
use App\Models\Deposit;
use App\Models\GroupChatUser;
use App\Models\notification;
use App\Models\riwayat_user;
use App\Models\trader;
use App\Models\User;
use App\Models\Withdraw;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Client;

use function PHPSTORM_META\type;

class TraderController extends Controller
{
    //
    public function edit_profile($id){
        $user = User::where('id',$id)->first();
        return view('user.profile.edit',compact('user'));
    }
    public function update_profile(request $request,user $user,$id){
        $user = User::where('id',$id)->first();
        if($request->profile == null){
            $profile = 'default1.png';
        }else{
            $profile = str_replace('public/storage/pictures/','',$request->profile);
        }
        $trader = trader::where('user_id',$id)->first();
        $trader->name = $request->name;
        $trader->photo = $profile;
        $trader->save();
        $notif = array(
            'message' => 'Edit Profile Berhasil!!',
            'alert-type' => 'success'
        );
        return redirect()->back()->with($notif);
    }

    public function portofolio(){
        $uid = Auth::user()->id;
        // $port = User::join('traders as t', 't.user_id', '=', 'users.id')
        //         ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
        //         ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
        //         ->leftjoin('categories as c','c.id','=','e.category_id')
        //         ->where('users.id', $uid)
        //         ->where('tr.is_deleted', 0)
        //         ->where('tr.last_status', 'VERIFIED')
        //         ->select('c.category as cat','e.code_emiten','e.company_name','e.trademark',db::raw('MAX(tr.created_at) as cr'),db::raw('SUM(tr.amount/e.price) as lembar'),db::raw('SUM(tr.amount) as tot'))
        //         ->groupBy('e.id')
        //         ->get();
        // return view('user.portofolio.index',compact('port'));
        $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' .app('request')->session()->get('token'),
            ];

            $responseToken = $client->request('GET', config('global.BASE_API_CLIENT_URL'). '/v3.7.1/portofolio/?category=', [
                'headers' => $headers,
            ]);

            if ($responseToken->getStatusCode() == 200) {
                $tokens = json_decode($responseToken->getBody()->getContents(), TRUE);
                // echo json_encode($tokens);
                // return;
                $port = collect($tokens);
                 return view('user.portofolio.index',compact('port'));
                
            }
    }

    public function getdata($id){
        $trx = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->where('users.id', Auth::user()->id)
                ->where('tr.is_deleted', 0)
                ->where('e.id', $id)
                ->where('tr.last_status', 'VERIFIED')
                ->select('tr.id', 'tr.uuid','e.pictures', 't.name as trader_name', 'users.email as user_email', 
                    't.id as trader_id','e.trademark','e.company_name', 'e.code_emiten', DB::raw('CONCAT("SAN","-", tr.id, "-", e.code_emiten) as transaction_serial'), 
                    'tr.channel', 'tr.description', 'tr.is_verified', 'tr.split_fee', 'tr.created_at as created_at', 
                    'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                    'tr.last_status as status')
                ->orderBy('tr.created_at','DESC')
                ->orderBy('tr.id','DESC')
                ->get();
        $cmt = "<table class='table'>
        <thead>
          <tr>
            <th>Tanggal</th>
            <th>No Transaksi</th>
            <th>Harga Saham</th>
            <th>Total Saham</th>
            <th>Biaya Admin</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>";
        foreach ($trx as $key) {
            $cmt .= "<tr>
                    <td>".tgl_indo(date('Y-m-d',
                    strtotime($key->created_at))).'
                    '.formatJam($key->created_at)."</td>
                    <td>".$key->transaction_serial."</td>
                    <td> Rp. ".number_format($key->price,0,',','.')."</td>
                    <td>".number_format($key->qty,0,',','.')." Lembar</td>
                    <td> Rp. ".number_format($key->fee,0,',','.')."</td>
                    <td> Rp. ".number_format($key->amount+$key->fee,0,',','.')."</td>
                  </tr>
                </tbody>";
        }
         $cmt .= "</table>";

        echo $cmt;
    }

    public function history(){
        $jour = riwayat_user::where('trader_id',Auth::user()->trader->id)
        ->orderBy('id','DESC')
        ->get();
        // dd($jour);
        return view('user.riwayat_user.index',compact('jour'));
    }

    public function video(){
        
    
        // if (session('search_query')) {
        //     $videoLists = $this->_videoLists(session(['search_query' => '']));
        // } else {
            $videoLists = $this->_videoLists(session(['search_query' => '']));
        // }
        return view('user.video.index', compact('videoLists'));
        // dd($videoLists);
        // return view('index', compact('videoLists'));
    }

    public function results(Request $request)
    {
        session(['search_query' => $request->search_query]);
        $videoLists = $this->_videoLists($request->search_query);
        return view('user.video.index', compact('videoLists'));
    }

    public function watch($id)
    {
        $singleVideo = $this->_singleVideo($id);
        if (session('search_query')) {
            $videoLists = $this->_videoLists(session('search_query'));
        } else {
            $videoLists = $this->_videoLists(session(['search_query' => '']));
        }
        return view('user.video.watch', compact('singleVideo', 'videoLists'));
    }

    // We will get search result here
    protected function _videoLists($keywords)
    {
        $part = 'snippet';
        $country = 'ID';
        $channelId = 'UCUW2hstBsaIbZFIi3ea4DTQ';
        $apiKey = config('services.youtube.api_key');
        $maxResults = 12;
        $youTubeEndPoint = config('services.youtube.search_endpoint');
        $type = 'video'; // You can select any one or all, we are getting only videos

        $url = "$youTubeEndPoint?part=$part&channelId=$channelId&maxResults=$maxResults&regionCode=$country&type=$type&key=$apiKey&q=$keywords";
        $response = Http::get($url);
        $results = json_decode($response);

        // We will create a json file to see our response
        File::put(storage_path() . '/app/public/results.json', $response->body());
        return $results;
    }

    protected function _singleVideo($id)
    {
        $apiKey = config('services.youtube.api_key');
        $part = 'snippet';
        $url = "https://www.googleapis.com/youtube/v3/videos?part=$part&id=$id&key=$apiKey";
        $response = Http::get($url);
        $results = json_decode($response);

        // Will create a json file to see our single video details
        File::put(storage_path() . '/app/public/single.json', $response->body());
        return $results;
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            // 'name' => ['required', 'string', 'max:255'],
            'pin' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }
    public function pinv(){
        return view('user.pin');
    }

    public function pin(request $request){
        if ($request->pin != $request->cpin) {
            # code...
            $notif = array(
                'message' => 'Pin Konfirmasi Tidak Sama',
                'alert-type' => 'fail'
            );
            return redirect()->back()->with($notif);
        }else{

            $user = User::where('id',$request->userid)->first();
            $user->pin = Hash::make($request->pin);
            $user->save();
            $notif = array(
                'message' => 'Pin Berhasil Di buat',
                'alert-type' => 'success'
            );
            return redirect()->to('/user')->with($notif);
        }
    }
    public function pin_reset(){
        return view('user.pin_reset');
    }

    public function pin_reset_post(request $request){
        if ($request->pin != $request->cpin) {
            # code...
            $notif = array(
                'message' => 'Pin Konfirmasi Tidak Sama',
                'alert-type' => 'fail'
            );
            return redirect()->back()->with($notif);
        }else{

            $user = User::where('id',$request->userid)->first();
            $user->pin = Hash::make($request->pin);
            $user->save();
            $notif = array(
                'message' => 'Pin Berhasil Di buat',
                'alert-type' => 'success'
            );
            return redirect()->to('/user')->with($notif);
        }
    }

    public function read_message(){

        $notif = notification::where('user_id',Auth::user()->id)->update(['is_deleted' => 1]);

        return redirect()->back();
    }

    public function add_bank(request $request){
        // echo $request->bank;
        $client = new Client();
                    $res = $client->request('POST', config('global.BASE_API_CLIENT_URL')  . '/v3.7.1/withdraw/insert-new-bankwd', [
                        'headers' => [
                            'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                        ],
                        'form_params' => [
                            "bank_code" => $request->bank,
                            "account_number" => $request->norek,
                            "account_name" =>Auth::user()->trader->name,
                            "account_currency"=>"IDR",
                        ]
                    ]);

                    echo $res->getBody()->getContents();
    }

    public function pin_check(Request $request){
        $pin = Auth::user()->pin;
        if (Hash::check($request->pin, $pin) == true) {
            dd('ok');
        }else{
            dd('salah');
        }
    }

    public function user_wallet(Request $request){
        $deposit = Deposit::join('traders as t', 't.id', '=', 'deposits.trader_id')
            ->join('users as u', 'u.id', '=', 't.user_id')
            ->leftJoin('virtual_accounts as va', 'va.deposit_id', '=', 'deposits.id')
            ->leftJoin('onepay_transaction as ot','ot.deposit_id','=','deposits.id')
            ->where('deposits.trader_id',Auth::user()->trader->id)
            ->select('deposits.id','ot.redirect_url', 'deposits.uuid', 'deposits.amount', 'deposits.fee', 
                'u.email', 'deposits.confirmation_photo', 'deposits.split_fee',
                'deposits.bank_to', 'deposits.bank_from', 'deposits.channel', 'deposits.account_number', 
                'deposits.status', 'deposits.created_at', 'deposits.updated_at', 't.name as trader_name', 
                'deposits.created_by', 'va.account_number as va_account_number', 'va.bank as va_bank')
            ->orderBy('deposits.created_at','DESC')
            ->get();
            $wd = Withdraw::where('trader_id',Auth::user()->trader->id)
            ->orderBy('id','DESC')
            ->get();
            $trader_bank = DB::table('trader_banks')
            ->select('trader_banks.*','bank_withdraws.bank','bank_withdraws.bank_code')
            ->join('bank_withdraws','bank_withdraws.id','=','trader_banks.bank_wd_id')
            ->where('trader_id',Auth::user()->trader->id)->first();
            $bwd = db::table('bank_withdraws')->select('*')->where('is_deleted',0)->get();

            if (!empty($request->start) && !empty($request->end) ) {
                # code...
                $se = db::select(db::raw("SELECT deposits.created_at,'DEPOSIT',deposits.amount,onepay_transaction.redirect_url,deposits.`status` from deposits 
                LEFT JOIN onepay_transaction on onepay_transaction.deposit_id = deposits.id
                where deposits.trader_id = ".Auth::user()->trader->id."
                and DATE(deposits.created_at) BETWEEN '".$request->start."' AND '".$request->end."'
                and deposits.created_at >= last_day(now()) + interval 1 day - interval 3 month
                UNION ALL
                SELECT created_at,'WITHDRAW',amount,'-',is_verified from withdraws where trader_id = ".Auth::user()->trader->id."
                and DATE(created_at) BETWEEN '".$request->start."' AND '".$request->end."'
                and created_at >= last_day(now()) + interval 1 day - interval 3 month
                UNION ALL
								SELECT trx.created_at,'TRANSAKSI',trx.amount,'-',CASE trx.last_status 
								WHEN 'VERIFIED' THEN 1
								WHEN 'EXPIRED' THEN 2
								ELSE 0
								END as last_status
								from users as us
								INNER JOIN traders as tr on tr.user_id = us.id
								INNER JOIN transactions as trx on trx.trader_id = tr.id
								INNER JOIN emitens as emt on emt.id = trx.emiten_id
								WHERE trx.is_deleted = 0 and tr.id = ".Auth::user()->trader->id."
                                and DATE(trx.created_at) BETWEEN '".$request->start."' AND '".$request->end."'
								and trx.created_at >= last_day(now()) + interval 1 day - interval 3 month
								ORDER BY created_at DESC"));
            }else{
                $se = db::select(db::raw("SELECT deposits.created_at,'DEPOSIT',deposits.amount,onepay_transaction.redirect_url,deposits.`status` from deposits 
                LEFT JOIN onepay_transaction on onepay_transaction.deposit_id = deposits.id
                where deposits.trader_id = ".Auth::user()->trader->id."
                and deposits.created_at >= last_day(now()) + interval 1 day - interval 3 month
                UNION ALL
                SELECT created_at,'WITHDRAW',amount,'-',is_verified from withdraws where trader_id = ".Auth::user()->trader->id."
                and created_at >= last_day(now()) + interval 1 day - interval 3 month
                UNION ALL
								SELECT trx.created_at,'TRANSAKSI',trx.amount,'-',CASE trx.last_status 
								WHEN 'VERIFIED' THEN 1
								WHEN 'EXPIRED' THEN 2
								ELSE 0
								END as last_status
								from users as us
								INNER JOIN traders as tr on tr.user_id = us.id
								INNER JOIN transactions as trx on trx.trader_id = tr.id
								INNER JOIN emitens as emt on emt.id = trx.emiten_id
								WHERE trx.is_deleted = 0 and tr.id = ".Auth::user()->trader->id."
								and trx.created_at >= last_day(now()) + interval 1 day - interval 3 month
								ORDER BY created_at DESC"));
            }


            // dd($request->start);

            // dd($se);

        return view('user.wallet.index',compact('deposit','wd','trader_bank','bwd','se'));
    }

    public function user_deviden(){
        return view('user.deviden.index');
    }

    public function email_verify($uuid){
        // echo $uuid;
        $user = user::where('uuid',$uuid)->first();
        $user->is_verified = 1;
        $user->save();
        // dd($user);
        $notif = array(
            'message' => 'Verifikasi E-mail Berhasil!! Silahkan Login',
            'alert-type' => 'success'
        );
        return redirect()->route('login')->with($notif);
    }

    public function secmar(){
        $secmar = json_decode(app('request')->session()->get('secondary_market'),TRUE);
        return view('user.secmar',compact('secmar'));
        // $sec = collect($sec);
        // echo $secmar;
        // dd($secmar);
    }

    public function mobile_reset($token){
        
        return view('auth.passwords.email',compact('token'));
    }

    public function joinGroup($uuid){
        // $response = Http::withHeaders([
        //     "Content-Type" => "application/json",
        //     "Accept" => "application/json",
        //     "email" => AuthHelper::getEmail(),
        //     "password" => AuthHelper::getPassword(),
        // ])->post(env('SANTARA_CHAT_BASE_URL') . '/api/memberJoin/'.$uuid, [
        //     "name" => 'tes'
        // ])->json();

        // if (!$response['success']) {
        //     return redirect()->back()->with([
        //        'message' => 'Error when creating group chat',
        //        'alert-type' => 'danger'
        //     ]);
        // }

        // dd($response);
        $groupUsers = GroupChatUser::where('group_id','=',$uuid)->where('user_id','=',Auth::user()->id)->first();

        // // if (in_array(Auth::user()->id, $groupUsers)) { // if already in group

        // // }
        if($groupUsers){
        //     // dd('-');
            $notif = array(
                'message' => 'Anda sudah bergabung ke dalam group!',
                'alert-type' => 'warn'
            );
            return redirect()->back()->with($notif);
        // }else{

        //     GroupChatUser::create([
        //         'user_id'  => Auth::user()->id,
        //         'group_id' => $uuid,
        //         'added_by' => 1,
        //         'role'     => 1,
        //     ]);

        //     Conversations::create([
        //         'to_id'        => $uuid,
        //         'to_type'      => "App\Models\Group",
        //         'message'      => Auth::user()->trader->name." has joined the group",
        //         'message_type' => 9,
        //         'add_members'  => true,
        //     ]);
        //     // dd('ok');
        //     $notif = array(
        //         'message' => 'Anda berhasil bergabung ke dalam group!',
        //         'alert-type' => 'success'
        //     );
        //     return redirect()->back()->with($notif);
        // }
        // dd($groupUsers);
        // dd('o');
        }else{
        $response = Http::withHeaders([
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "email" => AuthHelper::getEmail(),
            "password" => AuthHelper::getPassword(),
        ])->post(env('SANTARA_CHAT_BASE_URL') . '/api/memberJoin/'.$uuid, [
            "members" => [Auth::user()->id],
        ])->json();
        // dd($uuid);

        // if (!$response['success']) {
        //     return redirect()->back()->with([
        //        'message' => 'Gagal Bergabung Ke Grup Chat',
        //        'alert-type' => 'danger'
        //     ]);
        // }else{
            return redirect()->back()->with([
                'message' => 'Anda berhasil bergabung ke dalam group!',
                'alert-type' => 'success'
             ]);
        // dd($response);
        }
    }

    public function kyc_individu($uuid)
    {
        return $this->detail( $uuid , 'edit');
    }
    
    public function detail($uuid, $action)
    {
        $country = DB::table('countries')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $province = DB::table('provinces')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $companyCharacters = DB::table('company_characters')->where('is_deleted', 0)
            ->select('id', 'character as label')->get();
        $companyTypes = DB::table('company_types')->where('is_deleted', 0)
            ->select('id', 'type as label')->get();
        $bankInvestors = DB::table('bank_investors')->where('is_deleted', 0)
            ->select('id', 'bank as label')
            ->orderBy('bank', 'ASC')->get();
        $pendidikan = DB::table('educations')->select('id','education as label')->get();
        $syariah = [];
        for($i = 0; $i < 2; $i++){
            if($i == 0){
                array_push($syariah, [
                    "id" => "Y",
                    "label" => "Ya"
                ]);
            }else{
                array_push($syariah, [
                    "id" => "N",
                    "label" => "Tidak"
                ]);
            }
        }
        $pendapatan = [];
        array_push($pendapatan, [
            "id" => 1,
            "label" => "< Rp 100 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 2,
            "label" => "Rp 100 Milyar - Rp 500 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 3,
            "label" => "Rp 500 Milyar - Rp 1 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 4,
            "label" => "Rp 1 Triliun - Rp 5 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 5,
            "label" => "> Rp 5 Triliun"
        ]);
        $reasonToJoin = [];
        array_push($reasonToJoin, [
            "id" => 'Lain-lain',
            "label" => "Lain-lain"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Price appreciation',
            "label" => "Price appreciation"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Investasi jangka panjang',
            "label" => "Investasi jangka panjang"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Spekulasi',
            "label" => 'Spekulasi'
        ]);
        array_push($reasonToJoin, [
            "id" => 'Pendapatan',
            "label" => "Pendapatan"
        ]);

        $pendapatan2 = [];
        array_push($pendapatan2, [
            "id" => 1,
            "label" => "< Rp 1 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 2,
            "label" => "Rp 1 Milyar - Rp 5 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 3,
            "label" => "Rp 5 Milyar - Rp 10 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 4,
            "label" => "Rp 10 Milyar - Rp 50 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 5,
            "label" => "> Rp 50 Milyar"
        ]);

        $gender = [];
        array_push($gender, [
            "id" => "m",
            "label" => "Laki-Laki"
        ]);
        array_push($gender, [
            "id" => "f",
            "label" => "Perempuan"
        ]);

        // '1' => 'Single',
        // '2' => 'Menikah (Married)',
        // '3' => 'Duda (Widower)',
        // '4' => 'Janda (Widow)'

        $marital = [];
        array_push($marital, [
            "id" => "1",
            "label" => "Single"
        ]);
        array_push($marital, [
            "id" => "2",
            "label" => "Menikah (Married)"
        ]);
        array_push($marital, [
            "id" => "3",
            "label" => "Duda (Widower)"
        ]);
        array_push($marital, [
            "id" => "4",
            "label" => "Janda (Widow)"
        ]);

        $job = [];
        array_push($job, [
            "id" => "Pegawai Swasta",
            "label" => "Pegawai Swasta"
        ]);
        array_push($job, [
            "id" => "Pegawai Negeri Sipil",
            "label" => "Pegawai Negeri Sipil"
        ]);
        array_push($job, [
            "id" => "Ibu Rumah Tangga",
            "label" => "Ibu Rumah Tangga"
        ]);
        array_push($job, [
            "id" => "Wiraswasta / Pengusaha",
            "label" => "Wiraswasta / Pengusaha"
        ]);
        array_push($job, [
            "id" => "Pelajar",
            "label" => "Pelajar"
        ]);
        array_push($job, [
            "id" => "TNI / Polisi",
            "label" => "TNI / Polisi"
        ]);
        array_push($job, [
            "id" => "Pensiunan",
            "label" => "Pensiunan"
        ]);
        array_push($job, [
            "id" => "Guru",
            "label" => "Guru"
        ]);
        array_push($job, [
            "id" => "Lainnya",
            "label" => "Lainnya"
        ]);

        $dana = [];
        array_push($dana, [
            "id" => "Gaji",
            "label" => "Gaji"
        ]);
        array_push($dana, [
            "id" => "Profit Bisnis",
            "label" => "Profit Bisnis"
        ]);
        array_push($dana, [
            "id" => "Bunga",
            "label" => "Bunga"
        ]);
        array_push($dana, [
            "id" => "Warisan",
            "label" => "Warisan"
        ]);
        array_push($dana, [
            "id" => "Hibah dari Orang Tua Atau Anak",
            "label" => "Hibah dari Orang Tua Atau Anak"
        ]);
        array_push($dana, [
            "id" => "Hibah dari Pasangan",
            "label" => "Hibah dari Pasangan"
        ]);
        array_push($dana, [
            "id" => "Dana Pensiunan",
            "label" => "Dana Pensiunan"
        ]);
        array_push($dana, [
            "id" => "Undian",
            "label" => "Undian"
        ]);
        array_push($dana, [
            "id" => "Hasil Investasi",
            "label" => "Hasil Investasi"
        ]);
        array_push($dana, [
            "id" => "Deposito",
            "label" => "Deposito"
        ]);
        array_push($dana, [
            "id" => "Modal",
            "label" => "Modal"
        ]);
        array_push($dana, [
            "id" => "Pinjaman",
            "label" => "Pinjaman"
        ]);
        array_push($dana, [
            "id" => "Lain-Lain",
            "label" => "Lain-Lain"
        ]);
        

        $mtvs = [];
        array_push($mtvs, [
            "id" => "Lain-lain",
            "label" => "Lain-lain"
        ]);
        array_push($mtvs, [
            "id" => "Price appreciation",
            "label" => "Price appreciation"
        ]);
        array_push($mtvs, [
            "id" => "Investasi jangka panjang",
            "label" => "Investasi jangka panjang"
        ]);
        array_push($mtvs, [
            "id" => "Spekulasi",
            "label" => "Spekulasi"
        ]);
        array_push($mtvs, [
            "id" => "Pendapatan",
            "label" => "Pendapatan"
        ]);

        $tax_code = [];
        array_push($tax_code, [
            "id" => "1010",
            "label" => "1010 Individual - Domestik"
        ]);
        array_push($tax_code, [
            "id" => "1011",
            "label" => "1011 Individual - Foreign"
        ]);
        array_push($tax_code, [
            "id" => "1083",
            "label" => "1083 Individual Foreign - Kitas"
        ]);
        array_push($tax_code, [
            "id" => "1242",
            "label" => "1242 Individual Foreign Kitas - NPWP"
        ]);

        $itemSelect['company_character'] = $companyCharacters;
        $itemSelect['company_type2'] = $companyTypes;
        $itemSelect['country'] = $country;
        $itemSelect['province'] = $province;
        $itemSelect['regencies'] = $regencies;
        $itemSelect['company_syariah'] = $syariah;
        $itemSelect['pendidikan'] = $pendidikan;
        $itemSelect['pendapatan'] = $pendapatan;
        $itemSelect['pendapatan2'] = $pendapatan2;
        $itemSelect['reasonToJoin'] = $reasonToJoin;
        $itemSelect['bankInvestor'] = $bankInvestors;
        $itemSelect['gender'] = $gender;
        $itemSelect['marital'] = $marital;
        $itemSelect['job'] = $job;
        $itemSelect['dana'] = $dana;
        $itemSelect['mtvs'] = $mtvs;
        $itemSelect['tax_code'] = $tax_code;

        // $kyc = $this->dataKyc($uuid);
		// $tab = $this->dataTab($uuid);

        if($action == 'edit'){
            $country = DB::table('countries')->where('is_deleted', 0)->get();
            $province = DB::table('provinces')->where('is_deleted', 0)->get();
            $regencies = DB::table('regencies')->where('is_deleted', 0)->get();
            $kyc2['address']['country'] = $country;
            $kyc2['address']['province'] = $province;
            $kyc2['address']['regencies'] = $regencies;
        }

		// $traderKTP = trader::where('uuid', $uuid)->select('idcard_photo')->first();

        $kyc = $this->dataKyc($uuid);
		$tab = $this->dataTab($uuid);


        $trader = Auth::user()->trader;
        $t = $trader->toArray();

        // if($kyc[3]->data != null){
        //     $kyc[3]->data->idcard_photo = ($kyc[1]->data->idcard_photo) ? $kyc[1]->data->idcard_photo : '';
        // }
        
        $active = 'kyc-individu';
        $title = 'KYC Individu';
        $update_url = url('admin/kyc-bisnis/update_url');
        $confirm_url = url('admin/kyc-bisnis/confirm_url');
        $form_footer = 'form_footer';

        $traderKTP = trader::where('uuid', $uuid)->select('idcard_photo')->first();

        // dd($t['status_kyc'.'1']);
        // dd($tab[1]);
        // dd($itemSelect);
        // dd($kyc);

        return view('user.kyc.kyc', compact('active', 'kyc','kyc2', 'tab', 'action', 'title', 
            'update_url', 'confirm_url', 'form_footer', 'itemSelect','traderKTP','trader','t'));

    }

    public function getMarital($id = null) {
		$result = '';
		if ($id != null ){
			$marital = array (
				'1' => 'Single',
				'2' => 'Menikah (Married)',
				'3' => 'Duda (Widower)',
				'4' => 'Janda (Widow)'
			);            
			$result = ($id == 'all') ? $marital : $marital[$id];
		}
		return $result;
	}

    private function dataKyc($uuid) {
        $kyc = array();
        $tab = array(
			'1' => 'biodata-pribadi', 
			'2' => 'biodata-keluarga', 
			'3' => 'alamat', 
			'4' => 'pekerjaan', 
			'5' => 'informasi-pajak', 
			'6' => 'bank'); 

        foreach ($tab as $k => $v): 
          $kyc[$k] = $this->getDetailKyc($uuid,$k,$v);
        endforeach;

        return $kyc;
	}

    private function getDetailKyc($uuid, $phase, $page) {
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_CLIENT_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/individual-'.$phase, [
                    'headers' => $headers,
          ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $kyc = (object)[
          'data' 		=> (count((array)$data) > 0) ? (object)$data : null,
          'page' 		=> $page,
          'uuid' 		=> $uuid
        ];
        
        return $kyc;		
	  }

    private function dataTab($uuid){
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_CLIENT_URL').'/'.config('global.API_ADMIN_VERSION') . 'traders/status-kyc-trader', [
                    'headers' => $headers,
            ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= (object)json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $data = (object)$data;


        $tab = (object)[
			'1' => (object)['title' => 'Biodata Pribadi', 'page' => 'biodata-pribadi', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
			'2' => (object)['title' => 'Biodata Keluarga', 'page' => 'biodata-keluarga' , 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
			'3' => (object)['title' => 'Alamat', 'page' => 'alamat' , 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
			'4' => (object)['title' => 'Pekerjaan', 'page' => 'pekerjaan' , 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
			'5' => (object)['title' => 'Informasi Pajak', 'page' => 'informasi-pajak' , 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
			'6' => (object)['title' => 'Bank', 'page' => 'bank' , 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""]
		];

        return $tab;
	}

    public function updateKycIndividu(Request $request){
        // return response()->json(["code" => 200, "message" => json_encode($request->all())]);

        dd($request->all());
        // dd($request->file('field_idcard_photo'));
    }
    public function updateKycIndividuBiodata(Request $request){
        // return response()->json(["code" => 200, "message" => 'ok']);
        // // dd($request->all());
        // try {
        // $this->validate($request, ['file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',]);
        // $request->validate([
        //     'field_idcard_photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000',
        // ]); 

        $type = 'E-KTP';
        $data_array = [
            [
                'name' => 'name',
                'contents'=>$request->field_full_name
            ],                        
            [
                'name' => 'birth_place',
                'contents'=>$request->field_birth_place
            ],
            [
                'name' => 'birth_date',
                'contents'=>$request->field_birth_date
            ],                        
            [
                'name' => 'gender',
                'contents'=>$request->field_gender
            ],						
            [
                'name' => 'education_id',
                'contents'=>$request->field_education_id
            ],
            [
                'name' => 'country_id',
                // 'contents'=>$request->field_country_id
                'contents'=>78
            ],
            [
                'name' => 'idcard_number',
                'contents'=>$request->field_idcard_number
            ],
            [
                'name' => 'regis_date_idcard',
                'contents'=>$request->field_regis_date_idcard
            ]
            ,[
                'name' => 'type_idcard',
                // 'contents'=>$request->field_type_idcard
                'contents'=>'E-KTP'
            ]
            ,[
                'name' => 'expired_date_idcard',
                'contents'=>$request->field_expired_date_idcard
            ]
            ,[
                'name' => 'passport_number',
                'contents'=>$request->field_passport_number
            ]
            ,[
                'name' => 'expired_date_passport',
                'contents'=>$request->field_expired_date_passport
            ]
            ,[
                'name' => 'another_email',
                'contents'=>$request->field_another_email
            ]
            ,[
                'name' => 'alt_phone',
                'contents'=>$request->field_alt_phone
            ]
            
        ];

        if($request->hasFile('field_photo')) {			
            $photo = [
                [
                    'name' => 'photo',
                    'contents' => fopen($request->file('field_photo')->getPathName(), 'r'),
                    'filename' => $request->file('field_photo')->getClientOriginalName()
                ]
            ];

            $data_array = array_merge($data_array, $photo);   
        }
        
        if($request->hasFile('field_idcard_photo')) {			
            $idcard_photo = [
                [
                    'name' => 'idcard_photo',
                    'contents' => fopen($request->file('field_idcard_photo')->getPathName(), 'r'),
                    'filename' => $request->file('field_idcard_photo')->getClientOriginalName()
                ]
            ];

            $data_array = array_merge($data_array, $idcard_photo);   
        }
        if($request->hasFile('field_verification_photo')) {			
            $verification_photo = [
                [
                    'name' => 'verification_photo',
                    'contents' => fopen($request->file('field_verification_photo')->getPathName(), 'r'),
                    'filename' => $request->file('field_verification_photo')->getClientOriginalName()
                ]
            ];

            $data_array = array_merge($data_array, $verification_photo);   
        }

        // dd($request->file('field_idcard_photo')->getClientOriginalName());
            
            $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-1', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'multipart' => $data_array
                    // 'multipart' => [
                    //     'name'=>$request->field_full_name,
                    //     'birth_place'=>$request->field_birth_place,
                    //     'birth_date'=>$request->field_birth_date,
                    //     'gender'=>$request->field_gender,
                    //     'education_id'=>$request->field_education_id,
                    //     'country_id'=>78,
                    //     'regis_date_idcard'=>$request->field_regis_date_idcard,
                    //     'type_idcard'=>$type,
                    //     'expired_date_idcard'=>$request->field_expired_date_idcard,
                    //     'passport_number'=>$request->field_passport_number,
                    //     'expired_date_passport'=>$request->field_expired_date_passport,
                    //     'another_email'=>$request->field_another_email,
                    //     'alt_phone'=>$request->field_alt_phone,
                    //     'photo'=>$request->file('field_photo'),
                    //     'idcard_photo'=>$field_idcard_photo,
                    //     'verification_photo'=>$request->file('field_verification_photo'),
                    //     'idcard_number'=>$request->field_idcard_number,
                    // ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }

    public function updateKycIndividuBioKeluarga(Request $request){
        $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-2', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'form_params' => [
                        "marital_status"=> $request->field_marital_status, 
                        "spouse_name"=> $request->field_spouse_name, 
                        "mother_maiden_name"=> $request->field_mother_maiden_name, 
                        "heir"=> $request->field_heir,
                        "heir_relation"=> $request->field_heir_relation,
                        "heir_phone"=>$request->field_heir_phone
                    ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }
    public function updateKycIndividuAlamat(Request $request){
        $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-3', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'form_params' => [
                        "idcard_country"=> $request->field_company_country_address,
                        "idcard_province"=> $request->field_company_province_address,
                        "idcard_regency"=> $request->field_company_regency_address,
                        "idcard_address"=> $request->field_idcard_address,
                        "idcard_postal_code"=> $request->field_idcard_postal_code,
                        "address_same_with_idcard"=> true,
                        "country_domicile"=> $request->ield_company_country_address,
                        "province"=> "",
                        "regency"=> "",
                        "address"=> "",
                        "postal_code"=> ""
                    ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }
    public function updateKycIndividuPekerjaan(Request $request){
        // dd($request->all());
        // dd(implode(',',$request->dana));
        $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-4', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'form_params' => [
                        "name"=>$request->field_job_name, 
                        "description_job"=>$request->field_description_job, 
                        "source_of_investor_funds"=>implode(',',$request->dana),
                        "total_assets"=>$request->field_total_assets_of_investor,	
                        "reason"=>$request->field_reason,
                        "code"=> "OTH",
                        "reason_code"=>1
                    ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }
    
    public function updateKycIndividuInfoPajak(Request $request){
        // dd($request->all());
        $data_array = [
            [
                'name' => 'income',
                'contents'=>$request->field_income
            ],
            [
                'name' => 'tax_account_code',
                'contents'=>$request->field_tax_account_code
            ]
            ,[
                'name' => 'npwp',
                'contents'=>$request->field_npwp
            ]
            ,[
                'name' => 'date_registration_of_npwp',
                'contents'=>$request->field_date_registration_of_npwp
            ]
            ,[
                'name' => 'have_securities_account',
                'contents'=> isset($request->field_sid_number) ? 'true' : 'false'
            ]
            ,[
                'name' => 'securities_account_date_registration',
                'contents'=>$request->field_securities_account_date_registration
            ]
            ,[
                'name' => 'sid_number',
                'contents'=>$request->field_sid_number
            ]
            
        ];

        if($request->hasFile('field_securities_account')) {			
            $photo = [
                [
                    'name' => 'securities_account',
                    'contents' => fopen($request->file('field_securities_account')->getPathName(), 'r'),
                    'filename' => $request->file('field_securities_account')->getClientOriginalName()
                ]
            ];

            $data_array = array_merge($data_array, $photo);   
        }

        $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-5', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'multipart' => $data_array
                    // 'form_params' => [
                    //     'income'=>$request->field_income,
                    //     'tax_account_code'=>$request->field_tax_account_code,
                    //     'npwp'=>$request->field_npwp,
                    //     'date_registration_of_npwp'=>$request->field_date_registration_of_npwp,
                    //     'have_securities_account'=>false,
                    //     'securities_account_date_registration'=>$request->field_securities_account_date_registration,
                    //     'sid_number'=>$request->field_sid_number,
                    //     'securities_account'=>$request->file('field_securities_account'),
                    // ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }
    public function updateKycIndividuBank(Request $request){
        $client = new Client();
            try {
                $response = $client->request('POST', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/traders/individual-kyc-6', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                    ],
                    'form_params' => [
                        "account_name1"=> $request->field_account_name1,
                        "bank_investor1"=> $request->field_bank_investor1,
                        "account_number1"=> $request->field_account_number1,
                    ]
                ]);

            if ($response->getStatusCode() == 200) {
                // $message = json_decode($response->getBody()->getContents(),true);
                return response()->json(['code' => $response->getStatusCode() , 'message' => "Berhasil Submit Data"]);
            }
        } catch (\GuzzleHttp\Exception\ClientException $exception) {
            $statusCode = $exception->getResponse();
            $message = json_decode($statusCode->getBody()->getContents(),true);
            return response()->json(['code' => $statusCode->getStatusCode(), 'message' => $message['message']]);
        }
    }
}
