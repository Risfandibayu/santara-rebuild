<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelper;
use App\Helpers\Misc;
use App\Models\CompanyTrader;
use App\Models\Deposit;
use App\Models\emiten;
use App\Models\emiten_journey;
use App\Models\kategori;
use App\Models\trader;
use App\Models\User;
use App\Models\Markets;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Google\Cloud\Storage\StorageClient;
use Carbon\Carbon;
use DB;
use App\Exports\TransaksiEmiten;
use App\Models\AnnualFinancialReport;
use App\Models\KycSubmissionDetail;
use App\Models\Province;
use App\Models\Regency;
use Maatwebsite\Excel\Facades\Excel;

class EmitenController extends Controller
{
    //
    public function index(){
        $soldout = emiten::select('emitens.id', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
            'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
            'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed')
            ->leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->where('emitens.is_deleted',0)
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
                IF(
                  (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                      (SUM(
                        IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                        and
                        emitens.is_deleted = 0
                        and emitens.is_active = 1
                        and emitens.begin_period < now()')
            ->get();
        // $commingsoon = emiten::select('emitens.id', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
        //     'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
        //     'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed')
        //     ->leftjoin('categories', 'categories.id','=','emitens.category_id')
        //     ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
        //     ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
        //     ->where('emitens.is_deleted',0)
        //     ->where('emitens.is_verified',1)
        //     ->where('emitens.is_pralisting',1)
        //     ->where('emitens.is_coming_soon',1)
        //     ->groupBy('emitens.id')
        //     ->orderby('created_at','DESC')
        //     ->get();
        $dtNowPlaying = emiten::select('emitens.id', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
        'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
        'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed',
        DB::raw('CONVERT(ROUND(
            IF(
              (SUM(
                IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                  (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) as sold_out'))
            ->leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('transactions', function($query){
                $query->on('transactions.emiten_id','=','emitens.id')
                    ->where('transactions.channel', '<>', 'MARKET');
            })
            ->where('emitens.is_active', 1)
            ->where('emitens.is_deleted',0)
            ->groupBy('emitens.id')
            ->get();
        $nowPlaying = [];

        // $collection = collect($soldout);
        // $merged = $collection->merge($commingsoon);
        // $mergedData = $merged->all();

        $mergedData = [];
        foreach($soldout as $row){
            array_push($mergedData, [
                'id' => $row->id,
                'company_name' => $row->company_name,
                'trademark' => $row->trademark,
                'code_emiten' => $row->code_emiten,
                'price' => $row->price,
                'supply' => $row->supply,
                'is_deleted' => $row->is_deleted,
                'is_active' => $row->is_active,
                'begin_period' => $row->begin_period,
                'created_at' => $row->created_at,
                'ktg' => $row->ktg,
            ]);
        }

        foreach($dtNowPlaying as $row){
            if($row->sold_out != '100.00'){
                array_push($mergedData, [
                    'id' => $row->id,
                    'company_name' => $row->company_name,
                    'trademark' => $row->trademark,
                    'code_emiten' => $row->code_emiten,
                    'price' => $row->price,
                    'supply' => $row->supply,
                    'is_deleted' => $row->is_deleted,
                    'is_active' => $row->is_active,
                    'begin_period' => $row->begin_period,
                    'created_at' => $row->created_at,
                    'ktg' => $row->ktg,
                ]);
            }
        }
        $emiten = [];
        foreach($mergedData as $row){
                $latestJourney = $this->getLatestJourney($row['id']);
                array_push($emiten, [
                    'id' => $row['id'],
                    'company_name' => $row['company_name'],
                    'trademark' => $row['trademark'],
                    'code_emiten' => $row['code_emiten'],
                    'price' => $row['price'],
                    'supply' => $row['supply'],
                    'is_deleted' => $row['is_deleted'],
                    'is_active' => $row['is_active'],
                    'begin_period' => $row['begin_period'],
                    'created_at' => $row['created_at'],
                    'last_emiten_journey' => $latestJourney['title'],
                    'ktg' => $row['ktg'],
                    'sd' => $latestJourney['date'],
                    'ed' => $latestJourney['end_date']
                ]);
        }
        //echo json_encode($emiten);
        return view('admin.emiten.index',compact('emiten'));
    }

    public function getDetailEmitenTransaksi($emitenID)
    {
        $emiten = emiten::where('id', $emitenID)->select('id', 'company_name')
            ->where('is_deleted', 0)->first();
        return view('admin.emiten.transaksi-emiten', compact('emiten'));
    }

    public function fetchDataHistoriTransaksiEmiten(Request $request, $emitenID)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');
        $statusFilter = $request->get('status');

        $columnIndex = $columnIndex_arr[0]['column']; 
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir']; 
		$columnSortName = $order_arr[0]['column']; 
        $searchValue = $search_arr['value'];

        $orderColumn = "";
        $sort = $columnSortOrder;
		if($columnSortName == 0){
			$orderColumn = 'tr.created_at';
			$sort = 'DESC';
		}elseif($columnSortName == 1){
			$orderColumn = 't.name';
		}
		elseif($columnSortName == 2){
			$orderColumn = 'users.email';
		}
        elseif($columnSortName == 5){
			$orderColumn = 'tr.amount';
		}
		elseif($columnSortName == 6){
			$orderColumn = 'tr.created_at';
		}

        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $queryRecords = User::query();
        $queryRecords->join('traders as t', 't.user_id', '=', 'users.id')
            ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
            ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
            ->where('tr.is_deleted', 0)
            ->where('tr.last_status', 'VERIFIED')
            ->where('e.id', $emitenID);

        $queryFilterRecords = User::query();
        $queryFilterRecords->join('traders as t', 't.user_id', '=', 'users.id')
            ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
            ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
            ->where('tr.is_deleted', 0)
            ->where('tr.last_status', 'VERIFIED')
            ->where('e.id', $emitenID);

        $query = User::query();
        $query->join('traders as t', 't.user_id', '=', 'users.id')
            ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
            ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
            ->select('tr.id', 't.name as trader_name', 'users.email as user_email', 
                    'e.code_emiten', 'e.company_name',
                    'tr.channel', 'tr.split_fee', 'tr.created_at as created_at', 
                    'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                    't.phone')
            ->where('tr.is_deleted', 0)
            ->where('tr.last_status', 'VERIFIED')
            ->where('e.id', $emitenID)
            ->orderBy($orderColumn, $sort)
            ->skip($start)
            ->take($rowperpage);

        if($startDate != "" && $endDate != ""){
            $queryRecords->whereDate('tr.created_at', '>=', $startDate)
                ->whereDate('tr.created_at', '<=', $endDate);
            $queryFilterRecords->whereDate('tr.created_at', '>=', $startDate)
                ->whereDate('tr.created_at', '<=', $endDate);
            $query->whereDate('tr.created_at', '>=', $startDate)
                ->whereDate('tr.created_at', '<=', $endDate);
        }


        $searchData = $request->get('cari');
        if($searchData != ""){
            $queryFilterRecords->where(function($search) use ($searchData){
                $search->where('users.email', 'like', '%'.$searchData.'%')
                    ->orWhere('t.name', 'like', '%'.$searchData.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchData.'%');
            });
            $query->where(function($search) use ($searchData){
                $search->where('users.email', 'like', '%'.$searchData.'%')
                    ->orWhere('t.name', 'like', '%'.$searchData.'%')
                    ->orWhere('t.phone', 'like', '%'.$searchData.'%');
            });
        }

        $totalRecords = $queryRecords->count();
        $totalRecordswithFilter = $queryFilterRecords->count();
        $transactions = $query->get();

        $data = [];
        foreach($transactions as $row){

            $stock = 0;
            $stock_price = 0;

            if ($row->channel == 'VA') :
				$channel = 'Virtual Account';
			elseif ($row->channel == 'BANKTRANSFER') :
				$channel = 'Transfer Bank';
			elseif ($row->channel == 'WALLET') :
				$channel = 'Saldo Dompet';
			elseif ($row->channel == 'DANA') :
				$channel = 'DANA';
			elseif ($row->channel == 'MARKET') :
				$channel = 'MARKET (' . strtoupper($row->description) . ')';
			else :
				$channel = '-';
			endif;

            if($row->channel == "MARKET"){
                $market = Markets::where('transaction_id', $row->id)
                    ->select('stock', 'stock_price')
                    ->first();
                if($market != null){
                    $stock = $market->stock;
                    $stock_price = $market->stock_price;
                }
            }else{
                $stock = $row->qty;
                $stock_price = $row->price;
            }

            $created_at = tgl_indo(date('Y-m-d', strtotime($row->created_at))).' '.formatJam($row->created_at);  

            array_push($data, [
                "id" => $row->id,
                "trader" => $row->trader_name,
                "email" => $row->user_email,
                "stock" => number_format($stock,0,',','.').' Lembar',
                "stock_price" => rupiah($stock_price),
                "amount" => rupiah($row->amount + $row->fee),
                "created_at" => $created_at,
            ]);
        }

        $response = array(
            "status" => $searchData,
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );
    
        echo json_encode($response);
        exit;
    }

    public function exportTransaksiEmiten(Request $request, $emitenID)
    {
        $emiten = emiten::where('id', $emitenID)->select('company_name')->first();
        return Excel::download(new TransaksiEmiten($request->start_date, $request->end_date, 
            $emitenID, $request->cari), 'Data Investor Emiten '.$emiten->company_name.'.xlsx');
    }

    public function getLatestJourney($emitenId)
    {
        $emitenJourney = emiten_journey::where('emiten_id', $emitenId)
            ->orderBy('created_at', 'DESC')
            ->first();
        return $emitenJourney;
    }

    public function add(){
        $badanUsaha = (object) [
            '1' => 'PT',
            '2' => 'CV',
            '3' => 'UD',
            '4' => 'Firma',
            '5' => 'Koperasi',
            '6' => 'Yang Lain'
        ];
        $sistemPencatatan = (object) [
            '1' => 'Terkomputerisasi/Software akuntansi',
            '2' => 'Catatan pembukuan sederhana/POS',
            '3' => 'Hanya berupa bukti dokumentasi',
            '4' => 'Tidak ada'
        ];
        $posisiPasar = (object) [
            '1' => 'Tidak memiliki pinjaman',
            '2' => 'Memiliki pinjaman lancar',
            '3' => 'Pernah bermasalah namun lunas',
            '4' => 'Sedang/pernah bermasalah dan belum lunas'
        ];
        $marketPositition = (object) [
            '1' => 'Pemimpin pasar lokal/nasional',
            '2' => 'Mampu bersaing di pasar lokal/nasional',
            '3' => 'Berusaha bersaing di pasar lokal/nasional',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $strategiEmiten = (object) [
            '1' => 'Punya milestone jangka panjang owner & infrastruktur siap',
            '2' => 'Milestone sedang disusun owner & infrastruktur sedang diperkuat',
            '3' => 'Lebih menekankan strategi jangka pendek agar optimal',
            '4' => 'Strategi case by case/tentavie agar efektif'
        ];
        $statusKantor = (object) [
            '1' => 'Milik sendiri/sewa > 5 Tahun',
            '2' => 'Sewa > 2 s.d 5 Tahun',
            '3' => 'Sewa < 2 Tahun',
            '4' => 'Sewa Bulanan'
        ];
        $levelKompetisi = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Sedang/Pernah bermasalah dan belum lunas'
        ];
        $kemapuanManager = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $kemapuanTeknis = (object) [
            '1' => 'Owner/Manajemen ahli di bisnis ini',
            '2' => 'Owner/Manajemen baru dibisnis ini namun memiliki pengalaman bisnis yang sejenis',
            '3' => 'Owner/Manajemen belum pernah memiliki keahlian/pengalaman di bisnis ini dan sejenisnya namun telah memiliki pengalaman di sektor lain',
            '4' => 'Owner/Manajemen baru mulai berbisnis/belum ada track record'
        ];
        return view('admin.emiten.add', compact('badanUsaha',
            'sistemPencatatan',
            'posisiPasar',
            'marketPositition',
            'strategiEmiten',
            'statusKantor',
            'levelKompetisi',
            'kemapuanManager',
            'kemapuanTeknis'
        ));
    }

    public function getCategories(Request $request)
    {
        $search = $request->search;
        if($search != ""){
            $kategori = kategori::where('is_deleted', 0)
                ->where('category', 'like', '%'.$search.'%')
                ->select('id', 'category')
                ->get();
        }else{
            $kategori = kategori::where('is_deleted', 0)
                ->limit(5)
                ->select('id', 'category')
                ->get();
        }
        return response()->json($kategori);
    }

    public function getUser(Request $request)
    {
        $search = $request->search;
        if($search != ""){
            $users = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->where('users.role_id', 2)
                ->where('users.is_deleted', 0)
                ->where('users.email', 'like', '%'.$search.'%')
                ->limit(5)
                ->select('t.id', 'users.email')
                ->get();
        }else{
            $users = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->where('users.role_id', 2)
                ->where('users.is_deleted', 0)
                ->limit(5)
                ->select('t.id', 'users.email')
                ->get();
        }
        return response()->json($users);
    }

    public function validator(array $data){
        return Validator::make($data,[
            'company_name' => ['required'],
            'logo' => ['required'],

        ]);
    }

    public function addDate45(Request $request)
    {
        $startDate = Carbon::parse($request->start_date);
        $newDate = $startDate->addDays(45);
        return response()->json(["data" => $newDate]);
    }

    public function store(Request $request)
    {
        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
        $bucket = $storage->bucket($storageBucketName);
        $folderName = 'santara.co.id/token';

        if(!isset($request->thumbnail)){
            $logo = 'no-image.png';
        }else{
            $logo = $request->thumbnail;
        }
        if(!isset($request->banner)){
            $cover = 'no-image.png';
        }else{
            $cover = $request->banner;
        }
        if(!isset($request->galeri1)){
            $galeri = 'no-image.png';
        }else{
            $galeri = $request->galeri1;
        }
        if(!isset($request->galeri2)){
            $galeri2 = 'no-image.png';
        }else{
            $galeri2 = $request->galeri2;
        }
        if(!isset($request->galeri3)){
            $galeri3 = 'no-image.png';
        }else{
            $galeri3 = $request->galeri3;
        }
        if(!isset($request->owner)){
            $owner = 'no-image.png';
        }else{
            $owner = $request->owner;
        }

        $em = new emiten();
        $em->uuid = \Str::uuid();
        $em->company_name = $request->get('company_name');
        $em->owner_name = $request->get('nama_owner');
        $em->category_id = $request->get('kategori');
        $em->trader_id = $request->get('trader');
        // $em->is_active = 1;
        $em->avg_annual_turnover_previous_year = str_replace(".", "", $request->get('omset1'));
        $em->avg_annual_turnover_current_year = str_replace(".", "", $request->get('omset2'));
        $em->avg_capital_needs = str_replace(".", "", $request->get('perkiraan_dana'));
        $em->avg_general_share_amount = str_replace(".", "", $request->get('saham_dilepas'));
        $em->avg_turnover_after_becoming_a_publisher= str_replace(".", "", $request->get('omset_penerbit'));
        $em->avg_annual_dividen= str_replace(".", "", $request->get('deviden_tahunan'));
        $em->youtube= str_replace("youtu.be/", "www.youtube.com/embed/", $request->get('video_profile'));
        $em->facebook= $request->get('fb');
        $em->website= $request->get('web');
        $em->instagram= $request->get('ig');
        $em->business_description= $request->get('deskripsi');
        $em->admin_desc= $request->get('bio_owner');
        $em->period = $request->get('period');
        $em->pictures = $logo.','.$cover.','.$owner.','.$galeri.','.$galeri2.','.$galeri3;
        $em->code_emiten = $request->get('code_emiten');
        $em->trademark = $request->get('brand');
        $em->price = str_replace(".", "", $request->get('harga_saham'));
        if(isset($request->regency_id)){
            $em->regency_id = $request->regency_id;
        }
        if(isset($request->business_entity)){
            $em->business_entity =  $request->business_entity;
        }
        if(isset($request->address)){
            $em->address = $request->address;
        }
        if(isset($request->business_lifespan)){
            $em->business_lifespan = $request->business_lifespan;
        }
        if(isset($request->branch_company)){
            $em->branch_company = $request->branch_company;
        }
        if(isset($request->employee)){
            $em->employee = $request->employee;
        }
        if(isset($request->capital_needs)){
            $em->capital_needs = $request->capital_needs;
        }
        if(isset($request->monthly_turnover)){
            $em->monthly_turnover = $request->monthly_turnover;
        }
        if(isset($request->monthly_profit)){
            $em->monthly_profit = $request->monthly_profit;
        }
        if(isset($request->monthly_turnover_previous_year)){
            $em->monthly_turnover_previous_year =  $request->monthly_turnover_previous_year;
        }
        if(isset($request->monthly_profit_previous_year)){
            $em->monthly_profit_previous_year =  $request->monthly_profit_previous_year;
        }
        if(isset($request->total_bank_debt)){
            $em->total_bank_debt =  $request->total_bank_debt;
        }
        if(isset($request->bank_name_financing)){
            $em->bank_name_financing =  $request->bank_name_financing;
        }
        if(isset($request->total_paid_capital)){
            $em->total_paid_capital = $request->total_paid_capital;
        }
        if(isset($request->financial_recording_system)){
            $em->financial_recording_system =  $request->financial_recording_system;
        }
        if(isset($request->bank_loan_reputation)){
            $em->bank_loan_reputation = $request->bank_loan_reputation;
        }
        if(isset($request->market_position_for_the_product)){
            $em->market_position_for_the_product = $request->market_position_for_the_product;
        }
        if(isset($request->strategy_emiten)){
            $em->strategy_emiten =  $request->strategy_emiten;
        }
        if(isset($request->office_status)){
            $em->office_status =  $request->office_status;
        }
        if(isset($request->level_of_business_competition)){
            $em->level_of_business_competition =  $request->level_of_business_competition;
        }
        if(isset($request->managerial_ability)){
            $em->managerial_ability = $request->managerial_ability;
        }if(isset($request->technical_ability)){
            $em->technical_ability = $request->technical_ability;
        }
        if(isset($request->dynamic_link)){
            $em->dynamic_link = $request->dynamic_link;
        }

        if($request->hasFile("prospektus")){
            $fileProspektus = fopen($request->file('prospektus')->getPathName(), 'r');
            $prospektusFileSave = 'prospektus'.time().'.pdf';
            $fileProspektus = $folderName.'/'.$prospektusFileSave;
            $bucket->upload($fileProspektus, [
                'predefinedAcl' => 'publicRead',
                'name' => $fileProspektus
            ]);
            $em->prospektus = $prospektusFileSave;
        }

        if(isset($request->video_url)){
            $em->video_url = $request->video_url;
        }
        $em->save();

        $emj = new emiten_journey();
        $emj->emiten_id = $em->id;
        $emj->title = "Pra Penawaran Saham";
        $emj->save();
        $notif = array(
            'message' => 'Emiten Berhasil Di Tambahkan',
            'alert-type' => 'success'
        );

        // $array = $logoFileSave.','.$coverFileSave.','.$galeriFileSave.','.$ownerFileSave;
        // dd($em);
        // return response()->json(['status' => 'Mantap']);
        return redirect()->back()->with($notif);
    }

    public function edit(emiten $emiten,$id){
        $emiten = emiten::where('emitens.id',$id)->leftJoin('categories as ct', 'ct.id', '=', 'emitens.category_id')
            ->leftJoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->leftJoin('users as u', 'u.id', '=', 't.user_id')
            ->leftJoin('regencies as r', 'r.id', '=', 'emitens.regency_id')
            ->select('emitens.*', 'ct.category', 'u.email', 'r.name as kota')->first();
        $picture = explode(',',$emiten->pictures);
        if(empty($picture[0])){
            $picture[0] = '-';
        }else{
            $picture[0];
        }
        if(empty($picture[1])){
            $picture[1] = '-';
        }else{
            $picture[1];
        }
        if(empty($picture[2])){
            $picture[2] = '-';
        }else{
            $picture[2];
        }
        if(empty($picture[3])){
            $picture[3] = '-';
        }else{
            $picture[3];
        }
        if(empty($picture[4])){
            $picture[4] = '-';
        }else{
            $picture[4];
        }
        if(empty($picture[5])){
            $picture[5] = '-';
        }else{
            $picture[5];
        }
        if(empty($picture[6])){
            $picture[6] = '-';
        }else{
            $picture[6];
        }
        // dd($s);
        $badanUsaha = (object) [
            '1' => 'PT',
            '2' => 'CV',
            '3' => 'UD',
            '4' => 'Firma',
            '5' => 'Koperasi',
            '6' => 'Yang Lain'
        ];
        $sistemPencatatan = (object) [
            '1' => 'Terkomputerisasi/Software akuntansi',
            '2' => 'Catatan pembukuan sederhana/POS',
            '3' => 'Hanya berupa bukti dokumentasi',
            '4' => 'Tidak ada'
        ];
        $posisiPasar = (object) [
            '1' => 'Tidak memiliki pinjaman',
            '2' => 'Memiliki pinjaman lancar',
            '3' => 'Pernah bermasalah namun lunas',
            '4' => 'Sedang/pernah bermasalah dan belum lunas'
        ];
        $marketPositition = (object) [
            '1' => 'Pemimpin pasar lokal/nasional',
            '2' => 'Mampu bersaing di pasar lokal/nasional',
            '3' => 'Berusaha bersaing di pasar lokal/nasional',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $strategiEmiten = (object) [
            '1' => 'Punya milestone jangka panjang owner & infrastruktur siap',
            '2' => 'Milestone sedang disusun owner & infrastruktur sedang diperkuat',
            '3' => 'Lebih menekankan strategi jangka pendek agar optimal',
            '4' => 'Strategi case by case/tentavie agar efektif'
        ];
        $statusKantor = (object) [
            '1' => 'Milik sendiri/sewa > 5 Tahun',
            '2' => 'Sewa > 2 s.d 5 Tahun',
            '3' => 'Sewa < 2 Tahun',
            '4' => 'Sewa Bulanan'
        ];
        $levelKompetisi = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Sedang/Pernah bermasalah dan belum lunas'
        ];
        $kemapuanManager = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $kemapuanTeknis = (object) [
            '1' => 'Owner/Manajemen ahli di bisnis ini',
            '2' => 'Owner/Manajemen baru dibisnis ini namun memiliki pengalaman bisnis yang sejenis',
            '3' => 'Owner/Manajemen belum pernah memiliki keahlian/pengalaman di bisnis ini dan sejenisnya namun telah memiliki pengalaman di sektor lain',
            '4' => 'Owner/Manajemen baru mulai berbisnis/belum ada track record'
        ];
        return view('admin.emiten.edit',compact(
            'emiten','picture','badanUsaha',
            'sistemPencatatan',
            'posisiPasar',
            'marketPositition',
            'strategiEmiten',
            'statusKantor',
            'levelKompetisi',
            'kemapuanManager',
            'kemapuanTeknis'));
    }

    public function edit_bisnis($uuid){
        $emiten = emiten::where('emitens.uuid',$uuid)->leftJoin('categories as ct', 'ct.id', '=', 'emitens.category_id')
        ->join('traders as t', 't.id', '=', 'emitens.trader_id')
        ->leftJoin('company_traders as cpt','cpt.trader_id','=','t.id')
        ->join('users as u', 'u.id', '=', 't.user_id')
        ->leftJoin('regencies as r', 'r.id', '=', 'emitens.regency_id')
        ->select('emitens.*','emitens.id as eid', 'ct.category', 'u.email', 'r.name as kota','cpt.*','t.id as tid')->first();
        $picture = explode(',',$emiten->pictures);
        if(empty($picture[0])){
            $picture[0] = '-';
        }else{
            $picture[0];
        }
        if(empty($picture[1])){
            $picture[1] = '-';
        }else{
            $picture[1];
        }
        if(empty($picture[2])){
            $picture[2] = '-';
        }else{
            $picture[2];
        }
        if(empty($picture[3])){
            $picture[3] = '-';
        }else{
            $picture[3];
        }
        if(empty($picture[4])){
            $picture[4] = '-';
        }else{
            $picture[4];
        }
        if(empty($picture[5])){
            $picture[5] = '-';
        }else{
            $picture[5];
        }
        if(empty($picture[6])){
            $picture[6] = '-';
        }else{
            $picture[6];
        }
        $badanUsaha = (object) [
            '1' => 'PT',
            '2' => 'CV',
            '3' => 'UD',
            '4' => 'Firma',
            '5' => 'Koperasi',
            '6' => 'Yang Lain'
        ];
        $sistemPencatatan = (object) [
            '1' => 'Terkomputerisasi/Software akuntansi',
            '2' => 'Catatan pembukuan sederhana/POS',
            '3' => 'Hanya berupa bukti dokumentasi',
            '4' => 'Tidak ada'
        ];
        $posisiPasar = (object) [
            '1' => 'Tidak memiliki pinjaman',
            '2' => 'Memiliki pinjaman lancar',
            '3' => 'Pernah bermasalah namun lunas',
            '4' => 'Sedang/pernah bermasalah dan belum lunas'
        ];
        $marketPositition = (object) [
            '1' => 'Pemimpin pasar lokal/nasional',
            '2' => 'Mampu bersaing di pasar lokal/nasional',
            '3' => 'Berusaha bersaing di pasar lokal/nasional',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $strategiEmiten = (object) [
            '1' => 'Punya milestone jangka panjang owner & infrastruktur siap',
            '2' => 'Milestone sedang disusun owner & infrastruktur sedang diperkuat',
            '3' => 'Lebih menekankan strategi jangka pendek agar optimal',
            '4' => 'Strategi case by case/tentavie agar efektif'
        ];
        $statusKantor = (object) [
            '1' => 'Milik sendiri/sewa > 5 Tahun',
            '2' => 'Sewa > 2 s.d 5 Tahun',
            '3' => 'Sewa < 2 Tahun',
            '4' => 'Sewa Bulanan'
        ];
        $levelKompetisi = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Sedang/Pernah bermasalah dan belum lunas'
        ];
        $kemapuanManager = (object) [
            '1' => 'Mampu memenangkan persaingan',
            '2' => 'Mampu bersaing namun bukan pemimpin pasar',
            '3' => 'Berusaha bersaing namun bukan pemimpin pasar',
            '4' => 'Tidak mampu bersaing di pasar'
        ];
        $kemapuanTeknis = (object) [
            '1' => 'Owner/Manajemen ahli di bisnis ini',
            '2' => 'Owner/Manajemen baru dibisnis ini namun memiliki pengalaman bisnis yang sejenis',
            '3' => 'Owner/Manajemen belum pernah memiliki keahlian/pengalaman di bisnis ini dan sejenisnya namun telah memiliki pengalaman di sektor lain',
            '4' => 'Owner/Manajemen baru mulai berbisnis/belum ada track record'
        ];
        $kategori = kategori::where('is_deleted', 0)
        ->select('id', 'category')
        ->get();
        // $user = User::where('role_id',2)
        // ->limit(100)
        // ->get();
        // dd($s);


        return view('user.emiten.edit',compact('kategori','emiten','picture','badanUsaha',
        'sistemPencatatan',
        'posisiPasar',
        'marketPositition',
        'strategiEmiten',
        'statusKantor',
        'levelKompetisi',
        'kemapuanManager',
        'kemapuanTeknis'));
    }

    public function update(request $request,emiten $emiten,$id){
        $emiten = emiten::find($id);
        $picture = explode(',',$emiten->pictures);

        if(empty($picture[0])){
            $picture[0] = '-';
        }else{
            $picture[0];
        }
        if(empty($picture[1])){
            $picture[1] = '-';
        }else{
            $picture[1];
        }
        if(empty($picture[2])){
            $picture[2] = '-';
        }else{
            $picture[2];
        }
        if(empty($picture[3])){
            $picture[3] = '-';
        }else{
            $picture[3];
        }
        if(empty($picture[4])){
            $picture[4] = '-';
        }else{
            $picture[4];
        }
        if(empty($picture[5])){
            $picture[5] = '-';
        }else{
            $picture[5];
        }
        if(empty($picture[6])){
            $picture[6] = '-';
        }else{
            $picture[6];
        }

        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
        $bucket = $storage->bucket($storageBucketName);
        $folderName = 'santara.co.id/token';

        if(!isset($request->thumbnail)){
            $logo = 'no-image.png';
        }else{
            $logo = $request->thumbnail;
        }
        if(!isset($request->banner)){
            $cover = 'no-image.png';
        }else{
            $cover = $request->banner;
        }
        if(!isset($request->galeri1)){
            $galeri = 'no-image.png';
        }else{
            $galeri = $request->galeri1;
        }
        if(!isset($request->galeri2)){
            $galeri2 = 'no-image.png';
        }else{
            $galeri2 = $request->galeri2;
        }
        if(!isset($request->galeri3)){
            $galeri3 = 'no-image.png';
        }else{
            $galeri3 = $request->galeri3;
        }
        if(!isset($request->owner)){
            $owner = 'no-image.png';
        }else{
            $owner = $request->owner;
        }

        $emiten->company_name = $request->get('company_name');
        $emiten->owner_name = $request->get('nama_owner');
        if(isset($request->kategori)){
            $emiten->category_id = $request->get('kategori');
        }
        if(isset($request->trader)){
            $emiten->trader_id = $request->get('trader');
        }
        $emiten->avg_annual_turnover_previous_year = str_replace(".", "", $request->get('omset1'));
        $emiten->avg_annual_turnover_current_year = str_replace(".", "", $request->get('omset2'));
        $emiten->avg_capital_needs = str_replace(".", "", $request->get('perkiraan_dana'));
        $emiten->avg_general_share_amount = str_replace(".", "", $request->get('saham_dilepas'));
        $emiten->avg_turnover_after_becoming_a_publisher= str_replace(".", "", $request->get('omset_penerbit'));
        $emiten->avg_annual_dividen= str_replace(".", "", $request->get('deviden_tahunan'));
        // $emiten->youtube= $request->get('video_profile');
        $emiten->youtube= str_replace("youtu.be/", "www.youtube.com/embed/", $request->get('video_profile'));
        $emiten->facebook= $request->get('fb');
        $emiten->website= $request->get('web');
        $emiten->instagram= $request->get('ig');
        $emiten->business_description= $request->get('deskripsi');
        $emiten->admin_desc= $request->get('bio_owner');
        $emiten->pictures = $logo.','.$cover.','.$owner.','.$galeri.','.$galeri2.','.$galeri3;
        //$emiten->pictures = $request->thumbnail.','.$request->banner.','.$request->owner.','.$request->galer1.','.$request->galer2.','.$request->galer3;
        $emiten->code_emiten = $request->get('code_emiten');
        $emiten->trademark = $request->get('brand');
        $emiten->price = str_replace(".", "", $request->get('harga_saham'));
        if(isset($request->regency_id)){
            $emiten->regency_id = $request->get('regency_id');
        }
        $emiten->business_entity = $request->business_entity;
        $emiten->address = $request->address;
        $emiten->business_lifespan = $request->business_lifespan;
        $emiten->branch_company = $request->branch_company;
        $emiten->employee = $request->employee;
        $emiten->capital_needs = $request->capital_needs;
        $emiten->monthly_turnover = $request->monthly_turnover;
        $emiten->monthly_profit = $request->monthly_profit;
        $emiten->monthly_turnover_previous_year = $request->monthly_turnover_previous_year;
        $emiten->monthly_profit_previous_year = $request->monthly_profit_previous_year;
        $emiten->total_bank_debt = $request->total_bank_debt;
        $emiten->bank_name_financing = $request->bank_name_financing;
        $emiten->total_paid_capital = $request->total_paid_capital;
        $emiten->financial_recording_system = $request->financial_recording_system;
        $emiten->bank_loan_reputation = $request->bank_loan_reputation;
        $emiten->market_position_for_the_product = $request->market_position_for_the_product;
        $emiten->strategy_emiten = $request->strategy_emiten;
        $emiten->office_status = $request->office_status;
        $emiten->level_of_business_competition = $request->level_of_business_competition;
        $emiten->managerial_ability = $request->managerial_ability;
        $emiten->dynamic_link = $request->dynamic_link;
        $emiten->period = $request->period;
        $emiten->technical_ability = $request->technical_ability;
        if($request->hasFile("prospektus")){
            $fileProspektus = fopen($request->file('prospektus')->getPathName(), 'r');
            $prospektusFileSave = 'prospektus'.time().$request->file('prospektus')->getClientOriginalName();
            $bucket->upload($fileProspektus, [
                'predefinedAcl' => 'publicRead',
                'name' => $folderName.'/'.$prospektusFileSave
            ]);
            $emiten->prospektus = $prospektusFileSave;
        }
        $emiten->save();
        $notif = array(
            'message' => 'Bisnis Berhasil Di Edit',
            'alert-type' => 'success'
        );
        // dd($logoFileSave);
        return redirect()->back()->with($notif);
        // dd(str_replace(".", "", $request->get('omset2')));
    }

    public function update_bisnis(request $request,emiten $emiten,$id){
        $emiten = emiten::where('id',$id)->first();
        $picture = explode(',',$emiten->pictures);

        if(empty($picture[0])){
            $picture[0] = '-';
        }else{
            $picture[0];
        }
        if(empty($picture[1])){
            $picture[1] = '-';
        }else{
            $picture[1];
        }
        if(empty($picture[2])){
            $picture[2] = '-';
        }else{
            $picture[2];
        }
        if(empty($picture[3])){
            $picture[3] = '-';
        }else{
            $picture[3];
        }
        if(empty($picture[4])){
            $picture[4] = '-';
        }else{
            $picture[4];
        }
        if(empty($picture[5])){
            $picture[5] = '-';
        }else{
            $picture[5];
        }
        if(empty($picture[6])){
            $picture[6] = '-';
        }else{
            $picture[6];
        }

        if($request->hasFile('thumbnail')){
            $logoNameWithExt = $request->file('thumbnail')->getClientOriginalName() ;
            $logoFileName = pathinfo ($logoNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('thumbnail')->getClientoriginalExtension();
            $logoFileSave = 'thumbnail'.time().'.'.$extension;
            $path = $request->file('thumbnail')->storeAs('public/pictures',$logoFileSave) ;
        }else{
            $logoFileSave = $picture[0];
        }

        if($request->hasFile('banner')){
            $coverNameWithExt = $request->file('banner')->getClientOriginalName() ;
            $coverFileName = pathinfo ($coverNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('banner')->getClientoriginalExtension();
            $coverFileSave = 'banner'.time().'.'.$extension;
            $path = $request->file('banner')->storeAs('public/pictures',$coverFileSave) ;
        }else{
            $coverFileSave = $picture[1];
        }

        if($request->hasFile("owner")){
            $ownerNameWithExt = $request->file('owner')->getClientOriginalName() ;
            $ownerFileName = pathinfo ($ownerNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('owner')->getClientoriginalExtension();
            $ownerFileSave = 'owner'.time().'.'.$extension;
            $path = $request->file('owner')->storeAs('public/pictures',$ownerFileSave) ;
        }else{
            $ownerFileSave = $picture[2];
        }
        if($request->hasFile("galeri1")){
            $galeriNameWithExt = $request->file('galeri1')->getClientOriginalName() ;
            $galeriFileName = pathinfo ($galeriNameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('galeri1')->getClientoriginalExtension();
            $galeriFileSave = 'galeri1'.time().'.'.$extension;
            $path = $request->file('galeri1')->storeAs('public/pictures',$galeriFileSave) ;
        }else{
            $galeriFileSave = $picture[3];
        }
        if($request->hasFile("galeri2")){
            $galeri2NameWithExt = $request->file('galeri2')->getClientOriginalName() ;
            $galeri2FileName = pathinfo ($galeri2NameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('galeri2')->getClientoriginalExtension();
            $galeri2FileSave = 'galeri2'.time().'.'.$extension;
            $path = $request->file('galeri2')->storeAs('public/pictures',$galeri2FileSave) ;
        }else{
            $galeri2FileSave = $picture[4];
        }
        if($request->hasFile("galeri3")){
            $galeri3NameWithExt = $request->file('galeri3')->getClientOriginalName() ;
            $galeri3FileName = pathinfo ($galeri3NameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('galeri3')->getClientoriginalExtension();
            $galeri3FileSave = 'galeri3'.time().'.'.$extension;
            $path = $request->file('galeri3')->storeAs('public/pictures',$galeri3FileSave) ;
        }else{
            $galeri3FileSave = $picture[4];
        }

        if($request->logo == null){
            $logo = 'default1.png';
        }else{
            $logo = str_replace('public/upload/','',$request->logo);
        }
        if($request->cover == null){
            $cover = 'default2.png';
        }else{
            $cover = str_replace('public/upload/','',$request->cover);
        }
        if($request->galeri == null){
            $galeri = 'default.png';
        }else{
            $galeri = str_replace('public/upload/','',$request->galeri);
        }
        if($request->galeri2 == null){
            $galeri2 = 'default.png';
        }else{
            $galeri2 = str_replace('public/upload/','',$request->galeri2);
        }
        if($request->galeri3 == null){
            $galeri3 = 'default.png';
        }else{
            $galeri3 = str_replace('public/upload/','',$request->galeri3);
        }
        if($request->owner == null){
            $owner = 'default1.png';
        }else{
            $owner = str_replace('public/upload/','',$request->owner);
        }

        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
        $bucket = $storage->bucket($storageBucketName);
        $folderName = 'santara.co.id/token';

        $emiten->company_name = $request->get('company_name');
        $emiten->owner_name = $request->get('nama_owner');
        $emiten->category_id = $request->get('kategori');
        $emiten->avg_annual_turnover_previous_year = str_replace(".", "", $request->get('omset1'));
        $emiten->avg_annual_turnover_current_year = str_replace(".", "", $request->get('omset2'));
        $emiten->avg_capital_needs = str_replace(".", "", $request->get('perkiraan_dana'));
        $emiten->avg_general_share_amount = str_replace(".", "", $request->get('saham_dilepas'));
        $emiten->avg_turnover_after_becoming_a_publisher= str_replace(".", "", $request->get('omset_penerbit'));
        $emiten->avg_annual_dividen= str_replace(".", "", $request->get('deviden_tahunan'));
        // $emiten->youtube= $request->get('video_profile');
        $emiten->youtube= str_replace("youtu.be/", "www.youtube.com/embed/", $request->get('video_profile'));
        $emiten->facebook= $request->get('fb');
        $emiten->website= $request->get('web');
        $emiten->instagram= $request->get('ig');
        $emiten->business_description= $request->get('deskripsi');
        $emiten->admin_desc= $request->get('bio_owner');
        $emiten->pictures = $logo.','.$cover.','.$owner.','.$galeri.','.$galeri2.','.$galeri3;
        // $emiten->pictures = $logoFileSave.','.$coverFileSave.','.$ownerFileSave.','.$galeriFileSave.','.$galeri2FileSave.','.$galeri3FileSave;
        $emiten->code_emiten = $request->get('code_emiten');
        $emiten->trademark = $request->get('brand');
        $emiten->price = str_replace(".", "", $request->get('harga_saham'));
        if(isset($request->regency_id)){
            $emiten->regency_id = $request->get('regency_id');
        }
        $emiten->business_entity = $request->business_entity;
        $emiten->address = $request->address;
        $emiten->business_lifespan = $request->business_lifespan;
        $emiten->branch_company = $request->branch_company;
        $emiten->employee = $request->employee;
        $emiten->capital_needs = $request->capital_needs;
        $emiten->monthly_turnover = $request->monthly_turnover;
        $emiten->monthly_profit = $request->monthly_profit;
        $emiten->monthly_turnover_previous_year = $request->monthly_turnover_previous_year;
        $emiten->monthly_profit_previous_year = $request->monthly_profit_previous_year;
        $emiten->total_bank_debt = $request->total_bank_debt;
        $emiten->bank_name_financing = $request->bank_name_financing;
        $emiten->total_paid_capital = $request->total_paid_capital;
        $emiten->financial_recording_system = $request->financial_recording_system;
        $emiten->bank_loan_reputation = $request->bank_loan_reputation;
        $emiten->market_position_for_the_product = $request->market_position_for_the_product;
        $emiten->strategy_emiten = $request->strategy_emiten;
        $emiten->office_status = $request->office_status;
        $emiten->level_of_business_competition = $request->level_of_business_competition;
        $emiten->managerial_ability = $request->managerial_ability;
        $emiten->technical_ability = $request->technical_ability;
        if($request->hasFile("prospektus")){
            $fileProspektus = fopen($request->file('prospektus')->getPathName(), 'r');
            $prospektusFileSave = 'prospektus'.time().$request->file('prospektus')->getClientOriginalName();
            $bucket->upload($fileProspektus, [
                'predefinedAcl' => 'publicRead',
                'name' => $folderName.'/'.$prospektusFileSave
            ]);
            $emiten->prospektus = $prospektusFileSave;
        }
        $emiten->save();

        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
          'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
        $bucket = $storage->bucket($storageBucketName);

        // $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $emiten->trader_id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $emiten->trader_id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $emiten->trader_id;
            $company->uuid = \Str::uuid();
        }
        if($request->hasFile('field_company_document')){
            $fileDocument = fopen($request->file('field_company_document')->getPathName(), 'r');
            $companyDokument = 'dokumen-perusahaan-' . time() . $request->file('field_company_document')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokument;
            $bucket->upload($fileDocument, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document = '/uploads/trader/'.$companyDokument;
        }
        if($request->hasFile('field_company_document_change')){
            $fileDocumentChange = fopen($request->file('field_company_document_change')->getPathName(), 'r');
            $companyDokumentChange = 'dokumen-perusahaan2-' . time() . $request->file('field_company_document_change')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokumentChange;
            $bucket->upload($fileDocumentChange, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document_change = '/uploads/trader/'.$companyDokumentChange;
        }
        if($request->hasFile('field_document_sk_kemenkumham')){
            $fileDocumentSKKemenkumham = fopen($request->file('field_document_sk_kemenkumham')->getPathName(), 'r');
            $image_name = 'dokumen-sk-kemenkumham-' . time() . $request->file('field_document_sk_kemenkumham')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSKKemenkumham, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_sk_kemenkumham = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_npwp')){
            $fileDocumentNPWP = fopen($request->file('field_document_npwp')->getPathName(), 'r');
            $image_name = 'document_npwp-' . time() . $request->file('field_document_npwp')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentNPWP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_npwp = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_siup')){
            $fileDocumentSIUP = fopen($request->file('field_document_siup')->getPathName(), 'r');
            $image_name = 'document_siup-' . time() . $request->file('field_document_siup')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->siup_file = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_idcard_director')){
            $fileDocumentSIUP = fopen($request->file('field_idcard_director')->getPathName(), 'r');
            $image_name = 'idcard_director-' . time() . $request->file('field_idcard_director')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->idcard_director = '/uploads/trader/'.$image_name;
        }
        $company->has_submit_kyc7 = 1;
        $company->save();

        $notif = array(
            'message' => 'Bisnis Berhasil Di Edit',
            'alert-type' => 'success'
        );
        // dd($logoFileSave);
        return redirect('/user/bisnis_anda')->with($notif);
    }

    public function delete($id){
        $emiten = emiten::where('id',$id)->first();
        $emiten->is_deleted = 1;
        $emiten->save();
        $notif = array(
            'message' => 'Emiten Berhasil Di Hapus',
            'alert-type' => 'success'
        );
        return redirect('/admin/emiten')->with($notif);
    }

    public function emiten_status(Request $request,$id){

        DB::transaction(function() use ($request, $id) {
            $emj = new emiten_journey();
            $emj->emiten_id = $id;
            $emj->title = $request->get('title');
            $emj->date = $request->get('start_date');
            $emj->end_date = $request->get('end_date');
            $emj->save();

            $emiten = emiten::find($id);
            $emiten->begin_period = $request->get('start_date');
            $emiten->end_period = $request->get('end_date');
            $emiten->last_emiten_journey = $request->get('title');
            if($request->get('title') == 'Penawaran Saham'){
                $emiten->is_coming_soon = 0;
            }
            if($request->get('title') == 'Pra Penawaran Saham'){
                $emiten->is_active = 0;
            }else{
                $emiten->is_active = 1;
            }
            $emiten->save();

            $picture = explode(',',$emiten->pictures);

            $transactions = $emiten->transactions->where('is_verified', '=', 1)
                ->where('is_deleted', '=', 0)
                ->groupBy('trader_id');

            if ($request->get('title') == 'Penawaran Saham') {
                $users = AuthHelper::getUserIdentitiesByGroup($transactions);

                $response = Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "email" => AuthHelper::getEmail(),
                    "password" => AuthHelper::getPassword(),
                ])->post(env('SANTARA_CHAT_BASE_URL') . '/api/groups', [
                    "name" => $emiten->trademark,
                    "description" => $emiten->company_name,
                    "group_type" => 1, //closed group
                    "privacy" => 2, //private group
                    "photo_url" => $picture[0],
                    "users" => [1],
                    "emiten_id" => $emiten->id
                ])->json();

                if (!$response['success']) {
                    return redirect()->back()->with([
                       'message' => 'Error when creating group chat',
                       'alert-type' => 'danger'
                    ]);
                }
            }
        });

        $notif = array(
            'message' => 'Update Status Emiten Berhasil!!',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notif);
    }
    public function logocropImg()
    {


            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'thumbnail_' . time() . '.png';
            // file_put_contents($image_name, $data);

            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
            $bucket = $storage->bucket($storageBucketName);
            $folderName = 'santara.co.id/token';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($data, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);

            echo $image_name;
    }
    public function profilecropImg()
    {
          $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'profile_' . time() . '.png';
            // file_put_contents($image_name, $data);
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
            $bucket = $storage->bucket($storageBucketName);
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($data, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            echo $image_name;
    }
    public function galericropImg()
    {
          $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'galeri_' . time() . '.png';
            // file_put_contents($image_name, $data);
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
            $bucket = $storage->bucket($storageBucketName);
            $folderName = 'santara.co.id/token';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($data, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            echo $image_name;
    }
    public function covercropImg()
    {
          $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'banner_' . time() . '.png';
            // file_put_contents($image_name, $data);
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
            $bucket = $storage->bucket($storageBucketName);
            $folderName = 'santara.co.id/token';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($data, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            echo $image_name;
    }
    public function ownercropImg()
    {
          $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            $image_name = 'owner_' . time() . '.png';
            // file_put_contents($image_name, $data);
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
            $bucket = $storage->bucket($storageBucketName);
            $folderName = 'santara.co.id/token';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($data, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            echo $image_name;
    }

    public function index_user(){
        $emiten = emiten::where('emitens.is_deleted',0)
        ->select('emitens.*','categories.category as ktg','emiten_journeys.title as sts','emiten_journeys.date as sd', 'emiten_journeys.end_date as ed')
        ->leftjoin('categories', 'categories.id','=','emitens.category_id')
        ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
        ->whereRaw('emiten_journeys.created_at in (SELECT max(created_at) from emiten_journeys GROUP BY emiten_journeys.emiten_id)')
        ->get();

        return view('user.emiten.index',compact('emiten'));
    }

    public function user_emiten(){
        $emiten = emiten::where('emitens.is_deleted',0)
        ->select('emitens.id as id',db::raw("SUBSTRING_INDEX(emitens.pictures,',', 1) as picture"),'emitens.trademark as trademark','emitens.company_name as company_name','emitens.uuid as auuid','categories.category as category','emiten_journeys.title as status','emiten_journeys.date as sd', 'emiten_journeys.end_date as ed')
        ->leftjoin('categories', 'categories.id','=','emitens.category_id')
        // ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
        ->leftjoin('emiten_journeys',function($query){
            $query->on('emiten_journeys.emiten_id','=','emitens.id')
                ->whereRaw('emiten_journeys.created_at in (SELECT max(created_at) from emiten_journeys GROUP BY emiten_journeys.emiten_id)');
        })
        ->where('emitens.trader_id',Auth::user()->trader->id)
        ->where('emitens.is_deleted',0)
                ->where('emitens.is_coming_soon',1)
                // ->where('emitens.is_active',0)
                    ->where('emitens.is_verified',1)
                    // ->whereRaw('emiten_journeys.created_at in (SELECT max(created_at) from emiten_journeys GROUP BY emiten_journeys.emiten_id)')
        ->groupBy('emitens.id')
        ->get();
        $data2 = $emiten->toArray();
//  dd($emiten);
        // $data = null;
        // $data['list'] = $emiten;


        // foreach($emiten as $row){
        //     $sd = new DateTime($row['sd']);
        //     $ed = new DateTime($row['ed']);
        //     array_push($data2, [
        //         "uuid" => $row['uuid'],
        //         "url" => 1,
        //         "company_name" => $row["company_name"],
        //         "trademark" => $row["trademark"],
        //         "category" =>2,
        //         "day_remaining" => $sd->diff($ed),
        //         "total_investor" => 2,
        //         "picture" => 2,
        //         "percent" =>2,
        //         "status" => 'Pra Penawaran Saham',
        //         "report_deadline" => 2,
        //         'id' => $row['aid'],
        //     ]);
        // }

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_CLIENT_URL'). '/v3.7.1/finance-report/all-business/', [
                'headers' => $headers,
            ]);


            $data = json_decode($response->getBody()->getContents(), TRUE)['data'];
            // $data = null;
            // dd($data);
        } catch (\Exception $exception) {
            $data = null;
        }
        // $data2 = [];
        foreach($data['list'] as $row){
            $id = emiten::where('uuid','=',$row['uuid'])->first();
            array_push($data2, [
                "auuid" => $row['uuid'],
                "url" => $row['url'],
                "company_name" => $row["company_name"],
                "trademark" => $row["trademark"],
                "category" => $row["category"],
                "day_remaining" => $row["day_remaining"],
                "total_investor" => $row["total_investor"],
                "picture" => $row["picture"],
                "percent" => $row["percent"],
                "status" => $row["status"],
                "report_deadline" => $row["report_deadline"],
                'id' => $id->id,
            ]);
        }

        
        return view('user.emiten.bisnis',compact('data2'));
        // return $data;
        // dd($data['list']);
        // dd($data2);

    }

    public function fetchEmiten(Request $request)
    {
        $emiten = emiten::where('is_deleted', 0)
            ->where('company_name', 'like', '%'.$request->emiten.'%')
            ->select('id', 'company_name')
            ->groupBy('id')
            ->limit(5)
            ->get();
        return response()->json($emiten);
    }

    public function detail_bisnis($uuid){
        $reports = $this->getDataReport($uuid, 0, 12);
        $data = $this->getDataPlan($uuid);
        $last_report = $this->getLastReport($uuid);
        $emiten = emitenbyuuid($uuid);
        // if($emiten->supply == null || empty($emiten->supply)){
        //     $emiten->avg_general_share_amount = $emiten->supply;
        // }
        // $emiten->supply = 200;
        // dd($emiten);
        // dd($emiten->supply);
        // if(empty($emiten->supply)){
        //     if (!isset($emiten->avg_capital_needs)) {
        //         # code...
        //         $emiten->supply = round($emiten->avg_capital_needs,0) / round($emiten->price); 
        //     }else{
        //         $emiten->supply = round($emiten->capital_needs) / round($emiten->price); 

        //     }
        // }
        // dd($emiten);
        $tutorial = 0;
        $tersisa = ($emiten->supply - $emiten->terjual > 0) ? ($emiten->supply - $emiten->terjual) : 0;
        $terjual = ($emiten->terjual > $emiten->supply) ? $emiten->supply : $emiten->terjual;
        $terjual_percentage = $terjual > 0 ? ($terjual / $emiten->supply) * 100 : 0;
        $terjual_percentage = ($terjual_percentage >= 0) ? ($terjual_percentage > 100 ? 100 : $terjual_percentage) : 0;
        $progress = number_format($terjual_percentage, 2, '.', '.');

        $info  = (object)[
            "tersisa_percentage" => $tersisa > 0 ? number_format($tersisa / $emiten->supply * 100, 2, ',', '.') : 0,
            "tersisa_total"      => $tersisa > 0 ? number_format($tersisa, 0, ',', '.') : 0,
            "tersisa_total_rp"     => $tersisa > 0 ? number_format($tersisa * $emiten->price, 0, ',', '.') : 0,
            "terjual_percentage" => number_format($terjual_percentage, 2, ',', '.'),
            "terjual_total"      => $terjual > 0 ? number_format($terjual, 0, ',', '.') : 0,
            "terjual_total_rp"     =>$terjual > 0 ? number_format($terjual * $emiten->price, 0, ',', '.') : 0
        ];

        $now              = new DateTime(); // or your date as well
        $finish           = new DateTime($emiten->end_period);
        $diff_now         = $finish->diff($now);
        $sisa_waktu     = "0 Hari";
        if ($now < $finish) {
            $format = ($diff_now->days > 0) ? "%a Hari" : "%h Jam %i Menit";
            $sisa_waktu     = $diff_now->format($format);
        }
        $type          = ($data != null) ? 'update' : 'create';
        // dd($reports);
        return view('user.emiten.detail',compact('emiten','progress','last_report','info','sisa_waktu','uuid','tutorial','data','type'));
    }

    private function getDataPlan($uuid)
    {
        // if (!$this->session->user) {
        //     redirect('user/login');
        // }

        $data = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/finance-report/fund-plans/' . $uuid, [
                'headers' => $headers,
            ]);


            $data = json_decode($response->getBody()->getContents(), TRUE)['data'];
        } catch (\Exception $exception) {
            $data = null;
        }

        // echo json_encode(['data' => $data]);
        return $data;
    }

    private function getLastReport($uuid)
    {
        // if (!$this->session->user) {
        //     redirect('user/login');
        // }

        $data = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_CLIENT_URL'). '/v3.7.1/finance-report/last/' . $uuid, [
                'headers' => $headers,
            ]);


            $data = json_decode($response->getBody()->getContents(), TRUE)['data'];
        } catch (\Exception $exception) {
            $data = null;
        }

        return $data;
    }

    private function getDataReport($uuid, $limit, $offset)
    {
        // if (!$this->session->user) {
        //     redirect('user/login');
        // }

        $data = null;
        $limit = $limit + 1;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_CLIENT_URL') . '/v3.7.1/finance-report/list/' . $uuid . '?limit=' . $limit . '&offset=' . $offset . '', [
                'headers' => $headers,
            ]);

            $data = json_decode($response->getBody()->getContents(), TRUE)['data'];
        } catch (\Exception $exception) {
            $data = null;
        }

        return $data;
    }

    public function get_riwayat_laporan_keuangan(Request $request,$uuid)
    {
        // if (!$this->session->user) {
        //     redirect('user/login');
        // }

        $trademark = $request->get("trademark");
        // $this->load->helper('month');

        $draw   = intval($request->get("draw"));
        $start  = intval($request->get("start"));
        $length = intval($request->get("length"));

        // $this->load->model('Deposit_model');
        $filter = $request->get("filter");
        $reports = $this->getDataReport($uuid, $start, $length);

        $data = [];
        $no   = 1;
        foreach ($reports['data'] as $report) {

            $action = '';

            if ((($report['status'] != 'update data')  || (($report['status'] == 'update data') && ($report['last_status'] == 'rejected')))) {
                $action .=  '<a href="' . $report['finance_report'] . ' " class="btn btn-santara-red btn-sm btn-block" title="Lihat" >Lihat</a>';
            }

            if ($report['editable'] == 1 && $report['status'] != 'verifying') :
                $action .=
                    '<a href="'.url("user/laporan-keuangan/detail"). '/' . $uuid . '/' . $report['id'] . '" type="button" class="btn btn-sm btn-santara-white btn-block">
                    <span class="menu-title" data-i18n="">Edit</span>
                </a>
                <button type="button" onclick="return deleteReport(\'' . $report['id'] . '\', \'' . $uuid . '\')" class="btn btn-sm btn-santara-white btn-block">
                    <span class="menu-title" data-i18n="">Hapus</span>
                </button>';
            endif;

            if ($report['status'] == 'rejected') {
                $status = '<a href="#" title="deskripsi" onclick="showDesc(\'' . $report['last_status_desc'] . '\')">Ditolak</a>';
            } elseif ($report['status'] == 'verifying') {
                $status = 'Menunggu Verifikasi';
            } elseif ($report['status'] == 'verified') {
                $status = 'Terverifikasi';
            } elseif ($report['status'] == 'update data') {
                if ($report['last_status'] == 'rejected') {
                    $status = '<a href="#" title="deskripsi" onclick="showDesc(\'' . $report['last_status_desc'] . '\')">Ditolak</a>';
                } else {
                    $status = 'Perbaharui Data';
                }
            } else {
                $status = '-';
            }

            array_push($data, [
                $no++,
                $report['id'],
                $report['version'],
                $report['periode'],
                $status,
                $action
            ]);
        }

        $output = [
            "draw"            => $draw,
            "recordsTotal"    => Deposit::count(),
            "recordsFiltered" => count($data),
            "data"            => $data
        ];
        echo json_encode($output);
        exit();
        // echo $trademark;
    }

    public function savePlan(Request $request,$type)
    {
        // if (!$this->session->user) {
        //     redirect('user/login');
        // }

        // $data = array('emiten_uuid' => $request->emiten_uuid,'list_fund_plans' => $request->list_fund_plans);
        $data = array_merge($request->all(), ['index' => 'value']);
        // $data = array('0');

        $data['emiten_uuid'] = strip_tags($data['emiten_uuid']);
        $list_fund_plans = array_values((array)json_decode(json_encode($data['list_fund_plans'])));
        foreach ($list_fund_plans as $key => $value) {
            $value->name        = strip_tags($value->name);
            $value->subtotal    = str_replace(".", "", strip_tags($value->subtotal));
            $value->desc        = (isset($value->desc)) ? strip_tags($value->desc) : '';
            $value->sublist     = array_values((array)$value->sublist);
            foreach ($value->sublist as $v) {
                $v->amount = str_replace(".", "", strip_tags($v->amount));
            }
        }

        $data['list_fund_plans'] = $list_fund_plans;

        $method = 'POST';
        $url = '/v3.7.1/finance-report/fund-plans/';
        if ($type == 'update') {
            $method = 'PUT';
            $url = '/v3.7.1/finance-report/fund-plans/' . $data['emiten_uuid'];
        }

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request($method, config('global.BASE_API_CLIENT_URL') . $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function kyc_bisnis($uuid, Request $request)
    {
        $bis = emiten::where('trader_id', Auth::user()->trader->id)->first();
        $emitenId = $bis->id;
        return $this->detail( $uuid , 'konfirmasi', $emitenId);
    }
        
    public function detail($uuid, $action, $emitenId)
    {
        $country = DB::table('countries')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $province = DB::table('provinces')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $regencies = DB::table('regencies')->where('is_deleted', 0)
            ->select('id', 'name as label')
            ->orderBy('name', 'ASC')->get();
        $companyCharacters = DB::table('company_characters')->where('is_deleted', 0)
            ->select('id', 'character as label')->get();
        $companyTypes = DB::table('company_types')->where('is_deleted', 0)
            ->select('id', 'type as label')->get();
        $bankInvestors = DB::table('bank_investors')->where('is_deleted', 0)
            ->select('id', 'bank as label')
            ->orderBy('bank', 'ASC')->get();
        $syariah = [];
        for($i = 0; $i < 2; $i++){
            if($i == 0){
                array_push($syariah, [
                    "id" => "Y",
                    "label" => "Ya"
                ]);
            }else{
                array_push($syariah, [
                    "id" => "N",
                    "label" => "Tidak"
                ]);
            }
        }
        $pendapatan = [];
        array_push($pendapatan, [
            "id" => 1,
            "label" => "< Rp 100 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 2,
            "label" => "Rp 100 Milyar - Rp 500 Milyar"
        ]);
        array_push($pendapatan, [
            "id" => 3,
            "label" => "Rp 500 Milyar - Rp 1 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 4,
            "label" => "Rp 1 Triliun - Rp 5 Triliun"
        ]);
        array_push($pendapatan, [
            "id" => 5,
            "label" => "> Rp 5 Triliun"
        ]);
        $reasonToJoin = [];
        array_push($reasonToJoin, [
            "id" => 'Lain-lain',
            "label" => "Lain-lain"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Price appreciation',
            "label" => "Price appreciation"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Investasi jangka panjang',
            "label" => "Investasi jangka panjang"
        ]);
        array_push($reasonToJoin, [
            "id" => 'Spekulasi',
            "label" => 'Spekulasi'
        ]);
        array_push($reasonToJoin, [
            "id" => 'Pendapatan',
            "label" => "Pendapatan"
        ]);

        $pendapatan2 = [];
        array_push($pendapatan2, [
            "id" => 1,
            "label" => "< Rp 1 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 2,
            "label" => "Rp 1 Milyar - Rp 5 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 3,
            "label" => "Rp 5 Milyar - Rp 10 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 4,
            "label" => "Rp 10 Milyar - Rp 50 Milyar"
        ]);
        array_push($pendapatan2, [
            "id" => 5,
            "label" => "> Rp 50 Milyar"
        ]);

        $itemSelect['company_character'] = $companyCharacters;
        $itemSelect['company_type2'] = $companyTypes;
        $itemSelect['country'] = $country;
        $itemSelect['province'] = $province;
        $itemSelect['regencies'] = $regencies;
        $itemSelect['company_syariah'] = $syariah;
        $itemSelect['pendapatan'] = $pendapatan;
        $itemSelect['pendapatan2'] = $pendapatan2;
        $itemSelect['reasonToJoin'] = $reasonToJoin;
        $itemSelect['bankInvestor'] = $bankInvestors;

        $kyc = array();
        $kyc[1] = $this->getDataKYC1($uuid, 'biodata-perusahaan', $emitenId);
        $kyc[2] = $this->getDataKYC2($uuid, 'pajak-perizinan', $emitenId);
        $kyc[3] = $this->getDataKYC3($uuid, 'alamat', $emitenId);
        $kyc[4] = $this->getDataKYC4($uuid, 'penanggung-jawab', $emitenId);
        $kyc[5] = $this->getDataKYC5($uuid, 'aset-perusahaan', $emitenId);
        $kyc[6] = $this->getDataKYC6($uuid, 'profit-preferensi', $emitenId);
        $kyc[7] = $this->getDataKYC7($uuid, 'dokumen-perusahaan', $emitenId);
        $kyc[8] = $this->getDataKYC8($uuid, 'bank-perusahaan', $emitenId);

        $tab = $this->getDataTabKYC($uuid, $emitenId);
        
        $active = 'kyc-bisnis';
        $title = 'KYC Bisnis';
        $update_url = url('admin/kyc-bisnis/update_url');
        $confirm_url = url('admin/kyc-bisnis/confirm_url');
        $form_footer = 'form_footer';
        // dd($kyc);
        return view('user.emiten.kyc.kyc', compact('active', 'kyc', 'tab', 'action', 'title', 
            'update_url', 'confirm_url', 'form_footer', 'itemSelect'));

    }

    private function dataKyc($uuid) {
        $kyc = array();
        $tab = array(
          '1' => 'biodata-perusahaan', 
          '2' => 'pajak-perizinan', 
          '3' => 'alamat', 
          '4' => 'penanggung-jawab', 
          '5' => 'aset-perusahaan', 
          '6' => 'profit-preferensi',
          '7' => 'dokumen-perusahaan', 
          '8' => 'bank-perusahaan'); 

        foreach ($tab as $k => $v): 
          $kyc[$k] = $this->getDetailKyc($uuid,$k,$v);
        endforeach;

        return $kyc;
	}

    private function getDetailKyc($uuid, $phase, $page) {
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_CLIENT_URL').'/v3.7.1/traders/company-'.$phase, [
                    'headers' => $headers,
          ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $kyc = (object)[
          'data' 		=> (count((array)$data) > 0) ? (object)$data : null,
          'page' 		=> $page,
          'uuid' 		=> $uuid
        ];
        
        return $kyc;		
	  }

    private function dataTab($uuid){
        $data = null;

            try {
                $client = new \GuzzleHttp\Client();
                        
                $headers = [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token'),        
                    'Accept'        => 'application/json',
                    'Content-type'  => 'application/json'
                ];
                
                $response = $client->request('GET', config('global.BASE_API_CLIENT_URL').'/v3.7.1/traders/status-kyc-trader', [
                    'headers' => $headers,
            ]);

                if ( $response->getStatusCode() == 200 ) {
                    $data= (object)json_decode($response->getBody()->getContents(), TRUE);
                }
            } catch (\Exception $exception) {
                $data = null;
        }

        $data = (object)$data;


            if(count(get_object_vars(($data))) != 0){
                $tab = (object)[
                    '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
                    '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
                    '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
                    '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
                    '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
                    '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""],
                    '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : ""],
                    '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : ""],
                ];
            }else{
                $tab = (object)[
                    '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                    '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                    '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                    '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                    '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                    '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                    '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                    '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
                ];
            }

        return $tab;
	}

    
    public function getRegency(Request $request)
    {
        $search = $request->search;
        if($search != ""){
            $regency = Regency::where('is_deleted', 0)
                ->where('name', 'like', '%'.$search.'%')
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }else{
            $regency = Regency::where('is_deleted', 0)
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }
        return response()->json($regency);
    }
    public function usergetRegency(Request $request)
    {
        $search = $request->search;
        if($search != ""){
            $regency = Regency::where('is_deleted', 0)
                ->where('name', 'like', '%'.$search.'%')
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }else{
            $regency = Regency::where('is_deleted', 0)
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }
        return response()->json($regency);
    }
    
    public function getProvince(Request $request)
    {
        $search = $request->search;
        if($search != ""){
            $provinsi = Province::where('is_deleted', 0)
                ->where('name', 'like', '%'.$search.'%')
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }else{
            $provinsi = Province::where('is_deleted', 0)
                ->limit(5)
                ->select('id', 'name')
                ->get();
        }
        return response()->json($provinsi);
    }

    public function getCityByProvinsi($provinsi)
    {
        $regency = Regency::where('is_deleted', 0)
                ->where('province_id', $provinsi)
                ->select('id', 'name as label')
                ->orderBy('name', 'ASC')
                ->get();
        return response()->json(["code" => 200, "data" => $regency]);
    }


    public function sub_kyc_bisnis(Request $request){
        dd($request->all());
    }


    public function updateKycBisnisBiodata(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->name = $request->field_company_name;

        if($request->hasFile('field_company_photo')){
            $filePicture = fopen($request->file('field_company_photo')->getPathName(), 'r');
            $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
            $storage = new StorageClient([
                'keyFile' => json_decode($googleConfigFile, true)
            ]);
            $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
            $bucket = $storage->bucket($storageBucketName);
            $image_name = 'photo-' . time() . $request->file('field_company_photo')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($filePicture, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_photo = '/uploads/trader/'.$image_name;
        }
        $company->company_character = $request->field_company_character;
        $company->company_type = $request->field_company_type2;
        $company->company_syariah = $request->field_company_syariah;
        $company->company_country_domicile = $request->field_company_country_domicile;
        $company->company_establishment_place = $request->field_company_establishment_place;
        $company->company_date_establishment = $request->field_company_date_establishment;
        $company->another_email = $request->field_company_another_email;
        $company->description = $request->field_company_description;
        $company->has_submit_kyc1 = 1;
        $company->status_kyc1 = 'verifying';
        $company->save();
        $user = User::find($trader->user_id);
        $user->email = $request->field_company_email;
        $user->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah biodata perusahaan"]);
    }

    public function updateKycBisnisPajakIzin(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->tax_account_code = $request->field_company_tax_account_code;
        if ($request->field_company_tax_account_code == '1016') {
             $company->tax_account_company = 'ASABRI';
          } else if ($request->field_company_tax_account_code == '1018') {
             $company->tax_account_company = 'BADAN USAHA TETAP';
          } else if ($request->field_company_tax_account_code == '1077') {
             $company->tax_account_company = 'BADAN USAHA TETAP KHUSUS NON TAX';
          } else if ($request->field_company_tax_account_code == '1008') {
             $company->tax_account_company = 'BANK - DOMESTIC';
          } else if ($request->field_company_tax_account_code == '1009') {
             $company->tax_account_company = 'BANK - FOREIGN(JOINT VENTURE)';
          } else if ($request->field_company_tax_account_code == '1036') {
             $company->tax_account_company = 'BANK - PURE FOREIGN';
          } else if ($request->field_company_tax_account_code == '1001') {
             $company->tax_account_company = 'BROKER (LOCAL BROKER)';
          } else if ($request->field_company_tax_account_code == '1002') {
             $company->tax_account_company = 'CUSTODIAN BANK';
          } else if ($request->field_company_tax_account_code == '1067') {
             $company->tax_account_company = 'FINANCIAL INSTITUION';
          } else if ($request->field_company_tax_account_code == '1078') {
             $company->tax_account_company = 'GOVERMENT OF INDONESIA';
          } else if ($request->field_company_tax_account_code == '1035') {
             $company->tax_account_company = 'INSTITUTION - DOMESTIC (NON FI)';
          } else if ($request->field_company_tax_account_code == '1017') {
             $company->tax_account_company = 'INSTITUTION - FOREIGN (NON FI)';
          } else if ($request->field_company_tax_account_code == '1004') {
             $company->tax_account_company = 'INSTITUTION FOREIGN NO TAX';
          } else if ($request->field_company_tax_account_code == '1100') {
             $company->tax_account_company = 'INSURANCE NON NPWP';
          } else if ($request->field_company_tax_account_code == '1019') {
             $company->tax_account_company = 'INSURANCE NPWP';
          } else if ($request->field_company_tax_account_code == '1005') {
             $company->tax_account_company = 'ISSUER';
          } else if ($request->field_company_tax_account_code == '1015') {
             $company->tax_account_company = 'JAMSOSTEK JHT';
          } else if ($request->field_company_tax_account_code == '1082') {
             $company->tax_account_company = 'JAMSOSTEK NON JHT';
          } else if ($request->field_company_tax_account_code == '1013') {
             $company->tax_account_company = 'KOPERASI';
          } else if ($request->field_company_tax_account_code == '1006') {
             $company->tax_account_company = 'MUTUAL FUND';
          } else if ($request->field_company_tax_account_code == '1080') {
             $company->tax_account_company = 'MUTUAL FUND MORE THAN 5 YEAR';
          } else if ($request->field_company_tax_account_code == '1007') {
             $company->tax_account_company = 'PENSION FUND';
          } else if ($request->field_company_tax_account_code == '1098') {
             $company->tax_account_company = 'PERUSAHAAN TERBATAS NON NPWP';
          } else if ($request->field_company_tax_account_code == '1037') {
             $company->tax_account_company = 'PERUSAHAAN TERBATAS NPWP';
          } else if ($request->field_company_tax_account_code == '1014') {
             $company->tax_account_company = 'TASPEN';
          } else if ($request->field_company_tax_account_code == '1099') {
             $company->tax_account_company = 'YAYASAN NON NPWP';
          } else if ($request->field_company_tax_account_code == '1012') {
             $company->tax_account_company = 'YAYASAN NPWP';
          }
        $company->lkpub_code = $request->field_company_lkpub_code;
        if ($request->field_company_lkpub_code == '001') {
            $company->lkpub = 'BANK INDONESIA';
        } else if ($request->field_company_lkpub_code == '100') {
            $company->lkpub = 'PEMERINTAH PUSAT';
          } else if ($request->field_company_lkpub_code == '200') {
            $company->lkpub = 'BANK SENTRAL DI LN';
          } else if ($request->field_company_lkpub_code == '290') {
            $company->lkpub = 'BANK UMUM DI INDONESIA';
          } else if ($request->field_company_lkpub_code == '299') {
            $company->lkpub = 'BANK UMUM DI LUAR NEGERI';
          } else if ($request->field_company_lkpub_code == '310') {
            $company->lkpub = 'PERUSAHAAN PEMBIAYAAN';
          } else if ($request->field_company_lkpub_code == '320') {
            $company->lkpub = 'MODAL VENTURA';
          } else if ($request->field_company_lkpub_code == '330') {
            $company->lkpub = 'PERUSAHAAN SEKURITAS';
          } else if ($request->field_company_lkpub_code == '340') {
            $company->lkpub = 'PERUSAHAAN ASURANSI';
          } else if ($request->field_company_lkpub_code == '350') {
            $company->lkpub = 'DANA PENSIUN';
          } else if ($request->field_company_lkpub_code == '360') {
            $company->lkpub = 'REKSADANA';
          } else if ($request->field_company_lkpub_code == '410') {
            $company->lkpub = 'BUMN NON LEMBAGA KEUANGAN';
          } else if ($request->field_company_lkpub_code == '430') {
            $company->lkpub = 'PERUSAHAAN LAINNYA';
          } else if ($request->field_company_lkpub_code == 'XXX') {
            $company->lkpub = 'PERUSAHAAN ASING';
          } else if ($request->field_company_lkpub_code == '999') {
            $company->lkpub = 'LAINNYA';
          }
        $company->npwp = $request->field_company_npwp;
        $company->registration_date_npwp = $request->field_company_registration_date_npwp;
        $company->siup = $request->field_company_siup;
        $company->company_certificate_number = $request->field_company_certificate_number;
        $company->nib = $request->field_company_nib;
        $company->has_submit_kyc2 = 1;
        $company->status_kyc2 = 'verifying';
        $company->save();
        // $kycs = DB::table('kyc_submissions')->insert(
        //     ['trader_id' => Auth::user()->trader->id, 'status' => 'verifying']
        // );
        return response()->json(["code" => 200, "message" => "Berhasil mengubah pajak & perizinan"]);
    }

    public function updateKycBisnisAlamat(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->company_country_address = $request->field_company_country_address;
        $company->company_province_address = $request->field_company_province_address;
        $company->company_regency_address = $request->field_company_regency_address;
        $company->company_address = $request->field_company_address;
        $company->postal_code = $request->field_postal_code;
        $company->company_phone_number = str_replace('0', '62', $request->field_company_phone_number);
        $company->fax = $request->field_company_fax;
        $company->has_submit_kyc3 = 1;
        $company->status_kyc3 = 'verifying';
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisPenanggungJawab(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->company_responsible_name1 = $request->field_company_responsible_name1;
        $company->company_responsible_position1 = $request->field_company_responsible_position1;
        $company->company_responsible_idcard_number1 = $request->field_company_responsible_idcard_number1;
        $company->company_responsible_expired_date_idcard1 = $request->field_company_responsible_expired_date_idcard1;
        $company->company_responsible_npwp1 = $request->field_company_responsible_npwp1;
        $company->company_responsible_passport1 = $request->field_company_responsible_passport1;
        $company->company_responsible_expired_date_passport1 = $request->field_company_responsible_expired_date_passport1;
        $company->has_submit_kyc4 = 1;
        $company->status_kyc4 = 'verifying';
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisAsetPerusahaan(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->source_of_funds = $request->field_company_source_of_funds;
        if ($request->field_company_total_property1 == '1') {
            $company->company_total_property1 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property1 == '2') {
            $company->company_total_property1 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property1 == '3') {
            $company->company_total_property1 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property1 == '4') {
            $company->company_total_property1 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property1 == '5') {
            $company->company_total_property1 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_total_property2 == '1') {
            $company->company_total_property2 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property2 == '2') {
            $company->company_total_property2 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property2 == '3') {
            $company->company_total_property2 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property2 == '4') {
            $company->company_total_property2 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property2 == '5') {
            $company->company_total_property2 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_total_property3 == '1') {
            $company->company_total_property3 = '< Rp 1 Milyar';
          } else if ($request->field_company_total_property3 == '2') {
            $company->company_total_property3 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_total_property3 == '3') {
            $company->company_total_property3 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_total_property3 == '4') {
            $company->company_total_property3 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_total_property3 == '5') {
            $company->company_total_property3 = '> Rp 50 Milyar';
          } 
        $company->has_submit_kyc5 = 1;
        $company->status_kyc5 = 'verifying';
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah alamat perusahaan"]);
    }

    public function updateKycBisnisProfitReferensi(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        if ($request->field_company_income1 == '1') {
            $company->company_income1 = '< Rp 1 Milyar';
          } else if ($request->field_company_income1 == '2') {
            $company->company_income1 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income1 == '3') {
            $company->company_income1 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income1 == '4') {
            $company->company_income1 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income1 == '5') {
            $company->company_income1 = '> Rp 50 Milyar';
          } 
        if ($request->field_company_income2 == '1') {
            $company->company_income2 = '< Rp 1 Milyar';
          } else if ($request->field_company_income2 == '2') {
            $company->company_income2 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income2 == '3') {
            $company->company_income2 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income2 == '4') {
            $company->company_income2 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income2 == '5') {
            $company->company_income2 = '> Rp 50 Milyar';
          }  
          if ($request->field_company_income3 == '1') {
            $company->company_income3 = '< Rp 1 Milyar';
          } else if ($request->field_company_income3 == '2') {
            $company->company_income3 = 'Rp 1 Milyar - Rp 5 Milyar';
          } else if ($request->field_company_income3 == '3') {
            $company->company_income3 = 'Rp 5 Milyar - Rp 10 Milyar';
          } else if ($request->field_company_income3 == '4') {
            $company->company_income3 = 'Rp 10 Milyar - Rp 50 Milyar';
          } else if ($request->field_company_income3 == '5') {
            $company->company_income3 = '> Rp 50 Milyar';
          } 
        if ($request->field_reason_to_join == 'Lain-lain') {
            $company->reason_to_join_code = '1';
          } else if ($request->field_reason_to_join == 'Price appreciation') {
            $company->reason_to_join_code = '2';
          } else if ($request->field_reason_to_join == 'Investasi jangka panjang') {
            $company->reason_to_join_code = '3';
          } else if ($request->field_reason_to_join == 'Spekulasi') {
            $company->reason_to_join_code = '4';
          } else if ($request->field_reason_to_join == 'Pendapatan') {
            $company->reason_to_join_code = '5';
          }
        $company->reason_to_join = $request->field_reason_to_join;
        $company->has_submit_kyc6 = 1;
        $company->status_kyc6 = 'verifying';
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah profit & preferensi"]);
    }

    public function updateKycBisnisDokumenPerusahaan(Request $request)
    {
        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
          'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET2');
        $bucket = $storage->bucket($storageBucketName);

        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        if($request->hasFile('field_company_document')){
            $fileDocument = fopen($request->file('field_company_document')->getPathName(), 'r');
            $companyDokument = 'dokumen-perusahaan-' . time() . $request->file('field_company_document')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokument;
            $bucket->upload($fileDocument, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document = '/uploads/trader/'.$companyDokument;
        }
        if($request->hasFile('field_company_document_change')){
            $fileDocumentChange = fopen($request->file('field_company_document_change')->getPathName(), 'r');
            $companyDokumentChange = 'dokumen-perusahaan2-' . time() . $request->file('field_company_document_change')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$companyDokumentChange;
            $bucket->upload($fileDocumentChange, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->company_document_change = '/uploads/trader/'.$companyDokumentChange;
        }
        if($request->hasFile('field_document_sk_kemenkumham')){
            $fileDocumentSKKemenkumham = fopen($request->file('field_document_sk_kemenkumham')->getPathName(), 'r');
            $image_name = 'dokumen-sk-kemenkumham-' . time() . $request->file('field_document_sk_kemenkumham')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSKKemenkumham, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_sk_kemenkumham = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_npwp')){
            $fileDocumentNPWP = fopen($request->file('field_document_npwp')->getPathName(), 'r');
            $image_name = 'document_npwp-' . time() . $request->file('field_document_npwp')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentNPWP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->document_npwp = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_document_siup')){
            $fileDocumentSIUP = fopen($request->file('field_document_siup')->getPathName(), 'r');
            $image_name = 'document_siup-' . time() . $request->file('field_document_siup')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->siup_file = '/uploads/trader/'.$image_name;
        }
        if($request->hasFile('field_idcard_director')){
            $fileDocumentSIUP = fopen($request->file('field_idcard_director')->getPathName(), 'r');
            $image_name = 'idcard_director-' . time() . $request->file('field_idcard_director')->getClientOriginalName();
            $folderName = 'kyc';
            $pictures = $folderName.'/'.$image_name;
            $bucket->upload($fileDocumentSIUP, [
                'predefinedAcl' => 'publicRead',
                'name' => $pictures
            ]);
            $company->idcard_director = '/uploads/trader/'.$image_name;
        }
        $company->has_submit_kyc7 = 1;
        $company->status_kyc7 = 'verifying';
        $company->save();
        return response()->json(["code" => 200, "message" => "Berhasil mengubah dokumen perusahaan"]);
    }

    public function updateKycBankPerusahaan(Request $request)
    {
        $trader = trader::where('uuid', $request->trader_uuid2)->first();
        $cek = CompanyTrader::where('trader_id', $trader->id)->first();
        if($cek != null){
            $company = CompanyTrader::where('trader_id', $trader->id)->first();
        }else{
            $company = new CompanyTrader();
            $company->trader_id = $trader->id;
            $company->uuid = \Str::uuid();
        }
        $company->has_submit_kyc8 = 1;
        $company->status_kyc8 = 'verifying';
        $company->save();

        $cekTraderBank = DB::table('trader_banks')->where('trader_id', $trader->id)->count();
        if($cekTraderBank > 0){
          DB::table('trader_banks')->where('trader_id', $trader->id)->update([
            'bank_investor1' => $request->field_bank_investor1,
            'account_name1' => $trader->name,
            'account_number1' => $request->field_account_number1
          ]);
        }else{
          DB::table('trader_banks')->insert([
            'uuid' => \Str::uuid(),
            'trader_id' => $trader->id,
            'bank_investor1' => $request->field_bank_investor1,
            'account_name1' => $trader->name,
            'account_number1' => $request->field_account_number1
          ]);
        }

        return response()->json(["code" => 200, "message" => "Berhasil mengubah bank perusahaan"]);
    }


    public function getKYCCompany($uuid)
    {
        $kyc = array();
        $tab = array(
          '1' => 'biodata-perusahaan', 
          '2' => 'pajak-perizinan', 
          '3' => 'alamat', 
          '4' => 'penanggung-jawab', 
          '5' => 'aset-perusahaan', 
          '6' => 'profit-preferensi',
          '7' => 'dokumen-perusahaan', 
          '8' => 'bank-perusahaan'); 

        $kyc[1] = $this->getDataKYC1($uuid, 'biodata-perusahaan');
        $kyc[2] = $this->getDataKYC2($uuid, 'pajak-perizinan');
        $kyc[3] = $this->getDataKYC3($uuid, 'alamat');
        $kyc[4] = $this->getDataKYC4($uuid, 'penanggung-jawab');
        $kyc[5] = $this->getDataKYC5($uuid, 'aset-perusahaan');
        $kyc[6] = $this->getDataKYC6($uuid, 'profit-preferensi');
        $kyc[7] = $this->getDataKYC7($uuid, 'dokumen-perusahaan');
        $kyc[8] = $this->getDataKYC8($uuid, 'bank-perusahaan');

        $dtTab = $this->getDataTabKYC($uuid);

        echo json_encode(["data" => $dtTab]);
    }

    public function getDataKYC1($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('company_characters', 'company_traders.company_character', '=', 'company_characters.id')
          ->join('company_types', 'company_traders.company_type', '=', 'company_types.id')
          ->join('countries', 'company_traders.company_country_domicile', '=', 'countries.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->leftJoin('regencies', 'company_traders.company_country_domicile', '=', 'regencies.id')
          ->join('users', 'traders.user_id', 'users.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_photo, "/uploads/trader/", "")) as company_photo'),
            'company_traders.name as company_name',
            'company_characters.id as company_char_id',
            'company_characters.character as company_char_name',
            'company_traders.company_type as business_type_id',
            'company_types.type as business_type_name',
            'company_traders.company_syariah',
            'company_traders.company_country_domicile',
            'countries.name as country_name',
            'company_traders.company_establishment_place',
            'regencies.name as company_establishment_place_name',
            'company_traders.company_date_establishment',
            'users.email',
            'company_traders.another_email',
            'company_traders.description',
            'company_traders.has_submit_kyc1',
            'company_traders.last_kyc_submission_id'
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 1)
                ->get();
      
            $trader->submission = $kycSubmissionDetail;
          }
        
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];

        return $kyc;
    }

    public function getDataKYC2($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->select( 
            'company_traders.id',
            'company_traders.tax_account_code',
            'company_traders.lkpub_code',
            'company_traders.npwp',
            'company_traders.registration_date_npwp',
            'company_traders.siup',
            'company_traders.company_certificate_number',
            'company_traders.nib',
            'company_traders.has_submit_kyc2',
            'company_traders.last_kyc_submission_id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

      if($trader != null){
        $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
            ->where('step_id', 2)
            ->get();
  
        $trader->submission = $kycSubmissionDetail;
      }

        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];

        return $kyc;
    }

    public function getDataKYC3($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('countries', 'company_traders.company_country_address', '=', 'countries.id')
          ->join('provinces', 'company_traders.company_province_address', '=', 'provinces.id')
          ->join('regencies', 'company_traders.company_regency_address', 'regencies.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.company_country_address',
            'countries.name as country_name',
            'company_traders.company_province_address',
            'provinces.name as province_name',
            'company_traders.company_regency_address',
            'regencies.name as regency_name',
            'company_traders.company_address',
            'company_traders.postal_code',
            'company_traders.company_phone_number',
            'company_traders.fax',
            'company_traders.has_submit_kyc3',
            'company_traders.last_kyc_submission_id',
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
              ->where('step_id', 3)
              ->get();
    
          $trader->submission = $kycSubmissionDetail;
        }

        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
    }

    public function getDataKYC4($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.company_responsible_name1',
            'company_traders.company_responsible_position1',
            'company_traders.company_responsible_idcard_number1',
            'company_traders.company_responsible_expired_date_idcard1',
            'company_traders.company_responsible_npwp1',
            'company_traders.company_responsible_passport1',
            'company_traders.company_responsible_expired_date_passport1',
            'company_traders.company_responsible_name2',
            'company_traders.company_responsible_position2',
            'company_traders.company_responsible_idcard_number2',
            'company_traders.company_responsible_expired_date_idcard2',
            'company_traders.company_responsible_npwp2',
            'company_traders.company_responsible_passport2',
            'company_traders.company_responsible_expired_date_passport2',
            'company_traders.company_responsible_name3',
            'company_traders.company_responsible_position3',
            'company_traders.company_responsible_idcard_number3',
            'company_traders.company_responsible_expired_date_idcard3',
            'company_traders.company_responsible_npwp3',
            'company_traders.company_responsible_passport3',
            'company_traders.company_responsible_expired_date_passport3',
            'company_traders.company_responsible_name4',
            'company_traders.company_responsible_position4',
            'company_traders.company_responsible_idcard_number4',
            'company_traders.company_responsible_expired_date_idcard4',
            'company_traders.company_responsible_npwp4',
            'company_traders.company_responsible_passport4',
            'company_traders.company_responsible_expired_date_passport4',
            'company_traders.has_submit_kyc4',
            'company_traders.last_kyc_submission_id',
          )
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 4)
                ->get();
      
            $trader->submission = $kycSubmissionDetail;
          }

          $kyc = (object)[
            'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
            'page' 		=> $title,
            'uuid' 		=> $uuid
          ];
    
          return $kyc;
    }

    public function getDataKYC5($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'company_traders.id',
            'company_traders.source_of_funds',
            'company_traders.company_total_property1',
            'company_traders.company_total_property2',
            'company_traders.company_total_property3',
            'company_traders.has_submit_kyc5',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

          if($trader != null){
            $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
                ->where('step_id', 5)
                ->get();
      
            $trader->submission = $kycSubmissionDetail;
          }


        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=>  $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
    }

    public function getDataKYC6($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('e.id', $emitenId)
          ->where('traders.uuid', $uuid)
          ->select(
            'company_traders.id',
            'company_traders.company_income1',
            'company_traders.company_income2',
            'company_traders.company_income3',
            'company_traders.reason_to_join',
            'company_traders.has_submit_kyc6',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
            ->where('step_id', 6)
            ->get();

          $trader->submission = $kycSubmissionDetail;
        }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataKYC7($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'traders.id',
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_document, "/uploads/trader/", "")) as company_document'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.company_document_change, "/uploads/trader/", "")) as company_document_change'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.document_sk_kemenkumham, "/uploads/trader/", "")) as document_sk_kemenkumham'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.document_npwp, "/uploads/trader/", "")) as document_npwp'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.siup_file, "/uploads/trader/", "")) as siup_file'),
            DB::raw('CONCAT("'.config("global.STORAGE_BUCKET2").'", "kyc/", REPLACE(company_traders.idcard_director, "/uploads/trader/", "")) as idcard_director'),
            'company_traders.has_submit_kyc7',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
            ->where('step_id', 7)
            ->get();
            
          $trader->submission = $kycSubmissionDetail;
        }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=> $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataKYC8($uuid, $title, $emitenId)
    {
        $trader = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('trader_banks', 'traders.id', '=', 'trader_banks.trader_id')
          ->join('bank_investors as bank1', 'trader_banks.bank_investor1', '=', 'bank1.id')
          ->leftJoin('bank_investors as bank2', 'trader_banks.bank_investor2', '=', 'bank2.id')
          ->leftJoin('bank_investors as bank3', 'trader_banks.bank_investor3', '=', 'bank3.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
          ->where('traders.uuid', $uuid)
          ->where('e.id', $emitenId)
          ->select(
            'traders.id',
            'trader_banks.account_name1',
            'bank1.bank as bank1',
            'trader_banks.account_number1',
            'trader_banks.account_name2',
            'bank2.bank as bank2',
            'trader_banks.account_number2',
            'trader_banks.account_name3',
            'bank3.bank as bank3',
            'trader_banks.account_number3',
            'company_traders.has_submit_kyc8',
            'company_traders.last_kyc_submission_id')
          ->orderBy('company_traders.created_at', 'DESC')
          ->first();

        if($trader != null){
          $kycSubmissionDetail = KycSubmissionDetail::where('kyc_submission_id', $trader->last_kyc_submission_id)
            ->where('step_id', 8)
            ->get();
            
          $trader->submission = $kycSubmissionDetail;
        }
        
        $kyc = (object)[
          'data' 		=> (count((array)$trader) > 0) ? (object)$trader : null,
          'page' 		=>  $title,
          'uuid' 		=> $uuid
        ];
  
        return $kyc;
        
    }

    public function getDataTabKYC($uuid, $emitenId)
    {
        $data = trader::join('company_traders', 'company_traders.trader_id', '=', 'traders.id')
          ->join('emitens as e', 'e.trader_id', '=', 'traders.id')
            ->select( 
              'company_traders.status_kyc1',
              'company_traders.status_kyc2',
              'company_traders.status_kyc3',
              'company_traders.status_kyc4',
              'company_traders.status_kyc5',
              'company_traders.status_kyc6',
              'company_traders.status_kyc7',
              'company_traders.status_kyc8')
            ->where('traders.uuid', $uuid)
            ->where('e.id', $emitenId)
            ->orderBy('company_traders.created_at', 'DESC')
            ->first();
        if($data){

          if(count(get_object_vars(($data))) != 0){
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => isset($data->status_kyc1) ? $data->status_kyc1 : ""],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => isset($data->status_kyc2) ? $data->status_kyc2 : ""],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => isset($data->status_kyc3) ? $data->status_kyc3 : ""],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => isset($data->status_kyc4) ? $data->status_kyc4 : ""],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => isset($data->status_kyc5) ? $data->status_kyc5 : ""],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => isset($data->status_kyc6) ? $data->status_kyc6 : ""],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => isset($data->status_kyc7) ? $data->status_kyc7 : ""],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => isset($data->status_kyc8) ? $data->status_kyc8 : ""],
              ];
          }else{
              $tab = (object)[
                  '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                  '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                  '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                  '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                  '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                  '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                  '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                  '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
              ];
          }
        }else{
            $tab = (object)[
                '1' => (object)['title' => 'Biodata Perusahaan', 'page' => 'biodata-perusahaan', 'status' => ''],
                '2' => (object)['title' => 'Pajak & Perizinan', 'page' => 'pajak-perizinan', 'status' => ''],
                '3' => (object)['title' => 'Alamat', 'page' => 'alamat', 'status' => ''],
                '4' => (object)['title' => 'Penanggung Jawab', 'page' => 'penanggung-jawab', 'status' => ''],
                '5' => (object)['title' => 'Aset Perusahaan', 'page' => 'aset-perusahaan', 'status' => ''],				
                '6' => (object)['title' => 'Profit & Preferensi', 'page' => 'profit-preferensi', 'status' => ''],
                '7' => (object)['title' => 'Dokumen Perusahaan', 'page' => 'dokumen-perusahaan', 'status' => ''],
                '8' => (object)['title' => 'Bank Perusahaan', 'page' => 'bank-perusahaan', 'status' => ''],
            ];
        }

      return $tab;
    }


    public function lapkeutahunan($uuid){
        $com = emiten::where('uuid',$uuid)->first();
        $lap = AnnualFinancialReport::where('emiten_id',$com->id)->where('is_deleted',0)->get();
        // dd($com->id);
        return view('user.emiten.lapkeutahunan',compact('lap','uuid'));
    }

    public function addlapkeuth(Request $request){
        $com = emiten::where('uuid',$request->uuid)->first();
        $annualReport = new AnnualFinancialReport();

		$googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
        $bucket = $storage->bucket($storageBucketName);
        $folderName = 'santara.co.id/token';

		$fileReportTahunan = fopen($request->file('file_report')->getPathName(), 'r');
        $reportTahunanSave = 'Annual-Report'.time().$request->file('file_report')->getClientOriginalName();
        $bucket->upload($fileReportTahunan, [
            'predefinedAcl' => 'publicRead',
            'name' => $folderName.'/'.$reportTahunanSave
        ]);

		$annualReport->year = $request->year;
		$annualReport->emiten_id = $com->id;
		$annualReport->net_profit = str_replace(",", "",$request->net_profit);
        $annualReport->file_report = $reportTahunanSave;
		$annualReport->is_deleted = 0;
		$annualReport->save();

		$notif = array(
            'message' => 'Laporan Tahunan Berhasil Dibuat',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notif);
    }

    public function editlapkeuth(Request $request){
        $googleConfigFile = file_get_contents(config_path('santara-cloud-1261a9724a56.json'));
        $storage = new StorageClient([
            'keyFile' => json_decode($googleConfigFile, true)
        ]);
        $storageBucketName = config('global.STORAGE_GOOGLE_BUCKET');
        $bucket = $storage->bucket($storageBucketName);
        $folderName = 'santara.co.id/token';

		$annualReport = AnnualFinancialReport::find($request->id);
		$annualReport->year = $request->year;
		// $annualReport->emiten_id = $request->emiten_id;
		$annualReport->net_profit = str_replace(".", "",$request->net_profit);

		if($request->hasFile("file_report")){
			$fileReportTahunan = fopen($request->file('file_report')->getPathName(), 'r');
			$reportTahunanSave = 'Annual-Report'.time().$request->file('file_report')->getClientOriginalName();
			$bucket->upload($fileReportTahunan, [
				'predefinedAcl' => 'publicRead',
				'name' => $folderName.'/'.$reportTahunanSave
			]);
			$annualReport->file_report = $reportTahunanSave;
		}

		$annualReport->is_deleted = 0;
		$annualReport->save();

		$notif = array(
            'message' => 'Laporan Tahunan Berhasil Diedit',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notif);
    }

    public function dellapkeuth(Request $request){
        $annualReport = AnnualFinancialReport::find($request->id);
		$annualReport->is_deleted = 1;
		$annualReport->deleted_at = Carbon::now();
		$annualReport->deleted_by = \Auth::user()->id;
		$annualReport->save();
		
		$notif = array(
            'message' => 'Laporan Tahunan Berhasil DiHapus',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notif);
    }
}
