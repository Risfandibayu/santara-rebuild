<?php

namespace App\Http\Controllers;

use App\Exports\Chat;
use App\Models\Conversations;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class ChatController extends Controller
{
    public function index()
    {
        return view('admin.chat.index');
    }

    public function fetch(Request $request)
    {
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length");

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $filter = $request->get('filter');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column'];
        $columnName = $columnName_arr[$columnIndex]['data'];
        $columnSortOrder = $order_arr[0]['dir'];
        $searchValue = $search_arr['value'];

        $query = DB::connection('chat')->table('groups');
        $queryFilter = $query
            ->where('name', 'like', '%' .$searchValue . '%')
            ->orWhere('description', 'like', '%' .$searchValue . '%');

        $totalRecords = $query->count();

        $totalRecordswithFilter = $queryFilter->count();

        if ($searchValue != null) {
            $groups = $queryFilter->get();
        } else {
            $groups = $query->get();
        }

        $data = [];

        foreach($groups as $row){

            $data[] = [
                'id' => $row->id,
                'name' => $row->name,
                'description' => $row->description,
                'emitenID' => $row->emiten_id,
                'totalChat' => Conversations::where('to_id', $row->id)->count(),
                'totalMember' => DB::connection('chat')
                    ->table('group_users')
                    ->where('group_id', $row->id)
                    ->count()
            ];
        }
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
        exit;
    }

    public function export(Request $request)
    {
        ini_set('memory_limit', '-1');
        return Excel::download(new Chat($request->start_date, $request->end_date), 'Report chats.xlsx');
    }

}
