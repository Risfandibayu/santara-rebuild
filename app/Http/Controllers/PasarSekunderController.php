<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\emiten;
use DB;

class PasarSekunderController extends Controller
{
    
    public function indexPenerbit()
    {
        return view('admin.pasar-sekunder.penerbit');
    }

    public function fetchPenerbit(Request $request)
    {
        $start  = intval($request->start);
        $length = intval($request->length);
        $search = $request->search_keyword;

        $order = $request->order[0];
        $filter = null;
        $category = null;

        $category = $request->category;
        $filter = $request->filter;

        switch ($order['column']) {
            case 1:
                $sort = 'code';
                break;
            case 2:
                $sort = 'name';
                break;
            case 3:
                $sort = 'period';
                break;
            default:
                $sort = null;
        }

        $order = ($sort) ? $order['dir'] : null;
        $data_penerbit = $this->get_data_penerbit($start, $length, $search, $sort, $order, $filter, $category);

        $data = [];
        $no   = 1;
        if ($data_penerbit != null) {
            foreach ($data_penerbit as $penerbit) {

                $currenttime = date('Y-m-d H:i:s');
                $begin_period = date('Y-m-d H:i:s', strtotime($penerbit['begin_period']));
                $begin_period = new \DateTime($begin_period);
                $now = new \DateTime($currenttime);
                $usia = $begin_period->diff($now)->format('%y Tahun %m Bulan %d Hari');

                $ksei_text = 'No';
                $ksei_check = '';
                $ksei_toggle = 1;
                $tradeable_text = 'Non Aktif';
                $tradeable_status = 'disabled';
                $tradeable_check = '';
                $tradeable_toggle = 1;

                if ($penerbit['is_ksei']) {
                    $ksei_text = 'Yes';
                    $ksei_check = 'checked';
                    $ksei_toggle = 0;
                    $tradeable_status = '';
                    if ($penerbit['tradeable']) {
                        $tradeable_check = 'checked';
                        $tradeable_text = 'Aktif';
                        $tradeable_toggle = 0;
                    }
                }

                $is_ksei = '
                <div class="form-check">
                    <input type="checkbox" id="ksei_' . $penerbit['uuid'] . '" onChange="toggleKsei(\'' . $penerbit['uuid'] . '\', ' . $ksei_toggle . ',  \'' . $penerbit['company_name'] . '\')" class="form-check-input" ' . $ksei_check . '>
                    <label class="form-check-label" for="exampleCheck1">' . $ksei_text . '</label>
                </div>';

                $tradeable = '
                <div class="form-check">
                    <input type="checkbox" id="tradeable_' . $penerbit['uuid'] . '" onChange="toggleTradeable(\'' . $penerbit['uuid'] . '\', ' . $tradeable_toggle . ',  \'' . $penerbit['company_name'] . '\')" class="form-check-input" ' . $tradeable_status . ' ' . $tradeable_check . '>
                    <label class="form-check-label" for="exampleCheck1">' . $tradeable_text . '</label>
                </div>';

                $fundamental = '<a href="'.url("admin/pasar-sekunder/add-fundamental")."/".$penerbit['code_emiten'] . '" class="btn btn-info-ghost btn-block">Tambah</a>';
                array_push($data, [
                    $no++,
                    $penerbit['code_emiten'],
                    $penerbit['company_name'],
                    $usia,
                    $is_ksei,
                    $fundamental,
                    $tradeable
                ]);
            }
        }
        $output = [
            "data" => $data
        ];
        echo json_encode($output);
        exit();

    }

    private function get_data_penerbit($start, $length, $search, $sort, $order, $filter, $category)
    {
        $limit = $length;
        $offset = ($start / $length) + 1;

        $url = '?limit=' . $limit . '&offset=' . $offset . '&search=' . $search . '&sort=' . $sort . '&order=' . $order . '&filter=' . $filter . '&category=' . $category;
        $penerbit = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'emiten/' . $url, [
                'headers' => $headers,
            ]);


            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $penerbit = ($data['meta']['Total'] > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $penerbit = null;
        }

        return $penerbit;
    }

    public function updateKsei(Request $request)
    {
        $uuid  = $request->uuid;
        $status = $request->status;

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'emiten/ksei/' . $uuid, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'form_params' => [
                    'status' => strip_tags($status)
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                if ($status == 0) {
                    $resp = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'emiten/tradeable/' . $uuid, [
                        'headers' => [
                            'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                        ],
                        'form_params' => [
                            'status' => strip_tags($status)
                        ]
                    ]);

                    echo json_encode(['msg' => $resp->getStatusCode()]);
                    return;
                } else {
                    echo json_encode(['msg' => $response->getStatusCode()]);
                    return;
                }
            } else {
                echo json_encode(['msg' => 400]);
                return;
            }
        } catch (\Exception $exception) {
            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        }
    }

    public function updateTradeable(Request $request)
    {
        $uuid  = $request->uuid;
        $status = $request->status;

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'emiten/tradeable/' . $uuid, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'form_params' => [
                    'status' => strip_tags($status)
                ]
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        }
    }

    public function addEmitenFundamental($code_emiten = null)
    {
        $type = 'create';
        $kode = emiten::where('is_deleted', 0)
            ->where('code_emiten', '<>', '')
            ->select('id', 'uuid', 'code_emiten')
            ->orderBy('code_emiten')
            ->get();
        $code = emiten::where('is_deleted', 0)
            ->where('code_emiten', $code_emiten)
            ->count();
        $code_emiten = ($code > 0) ? $code_emiten : null;
        $period = [
            1 => 'Semester 1',
            2 => 'Semester 2'
        ];

        return view('admin.pasar-sekunder.form-tambah-rasio-fundamental', compact(
            'kode', 'code_emiten', 'type', 'period'
        ));
    }

    public function saveEmitenFundamental(Request $request)
    {
        $data['id'] = $request->id;
        $data['type'] = $request->type;
        $data['code_emiten'] = $request->code_emiten;
        $data['equity'] = $request->equity;
        $data['asset'] = $request->asset;
        $data['debt'] = $request->debt;
        if($data['type'] == 'create'){
            $data['year'] = $request->year;
            $data['period'] = $request->period;
        }
        $emiten_id = emiten::where('is_deleted', 0)
            ->where('code_emiten', $data['code_emiten'])
            ->select('id')
            ->first();
        $data['emiten_id'] =  $emiten_id->id;
        $url = ($data['type'] == 'create') ? 'emiten-fundamental/' : 'emiten-fundamental/id/' . $data['id'];
        $url_method = ($data['type'] == 'create') ? 'POST' : 'PUT';

        unset($data['id']);
        unset($data['type']);
        unset($data['code_emiten']);

        $data['equity'] = strip_tags(str_replace('.', '', $data['equity']));
        $data['asset'] = strip_tags(str_replace('.', '', $data['asset']));
        $data['debt'] = strip_tags(str_replace('.', '', $data['debt']));

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request($url_method, config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function indexRasioFundamental()
    {
        return view('admin.pasar-sekunder.rasio-fundamental');
    }

    public function fetchRasioFundamental(Request $request)
    {
        $draw  = intval($request->draw);
        $order = $request->order[0];
        $search = $request->search['value'];
        $start = $request->start;
        $end = $request->end;

        switch ($order['column']) {
            case 1:
                $sort = 'code';
                break;
            case 2:
                $sort = 'period';
                break;
            default:
                $sort = null;
        }

        $desc = ($sort) ? $order['dir'] : null;

        $data_emiten_fundamental = $this->get_data_emiten_fundamental($search, $sort, $desc, $start, $end);

        $data = [];
        $no   = 1;
        if ($data_emiten_fundamental != null) {
            foreach ($data_emiten_fundamental as $emiten_fundamental) {

                $action = '
                    <a href="' .url('admin/pasar-sekunder/edit-fundamental/'.$emiten_fundamental["id"]) . '" class="btn btn-sm btn-info">Edit</a> 
                    <button class="btn btn-sm btn-danger delete-emiten-fundamental" value="' . $emiten_fundamental['id'] . '">Delete</button>';

                $periode = 'Semester ' . $emiten_fundamental['period'] . ' ' . $emiten_fundamental['year'];
                array_push($data, [
                    $no++,
                    $emiten_fundamental['code_emiten'],
                    $periode,
                    number_format(is_numeric($emiten_fundamental['sales_growth']) ? $emiten_fundamental['sales_growth'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['gps']) ? $emiten_fundamental['gps'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['opm']) ? $emiten_fundamental['opm'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['npm']) ? $emiten_fundamental['npm'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['marketCap']) ? $emiten_fundamental['marketCap'] : 0, 0, ',', '.'),
                    number_format(is_numeric($emiten_fundamental['per']) ? $emiten_fundamental['per'] : 0, 0, ',', '.'),
                    number_format(is_numeric($emiten_fundamental['bvps']) ? $emiten_fundamental['bvps'] : 0, 0, ',', '.'),
                    number_format(is_numeric($emiten_fundamental['pbv']) ? $emiten_fundamental['pbv'] : 0, 0, ',', '.'),
                    number_format(is_numeric($emiten_fundamental['roa']) ? $emiten_fundamental['roa'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['roe']) ? $emiten_fundamental['roe'] : 0, 0, ',', '.') . ' %',
                    number_format(is_numeric($emiten_fundamental['ebitdta']) ? $emiten_fundamental['ebitdta'] : 0, 0, ',', '.'),
                    number_format(is_numeric($emiten_fundamental['der']) ? $emiten_fundamental['der'] : 0, 0, ',', '.') . ' %',
                    $action
                ]);
            }
        }
        $output = [
            "draw"  => $draw,
            "data" => $data
        ];
        echo json_encode($output);
        exit();
    }

    public function get_data_emiten_fundamental($search, $sort = null, $order = null, $start = null, $end = null)
    {
        $start_period = null;
        $start_year = null;
        $end_period = null;
        $end_year = null;

        if ($start != null) {
            $start = explode("-", $start);
            $start_period = $start[0];
            $start_year = $start[1];
        }

        if ($end != null) {
            $end = explode("-", $end);
            $end_period = $end[0];
            $end_year = $end[1];
        }

        $emiten_fundamental = null;
        $url = '/?search=' . $search . '&sort=' . $sort . '&desc=' . $order . '&start_period=' . $start_period . '&end_period=' . $end_period . '&start_year=' . $start_year . '&end_year=' . $end_year . '';

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') .  'emiten-fundamental' . $url, [
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $emiten_fundamental = ($data['meta']['Total'] > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $emiten_fundamental = null;
        }

        return $emiten_fundamental;
    }

    public function editEmitenFundamental($id)
    {
        $type = 'edit';
        $kode = emiten::where('is_deleted', 0)
            ->where('code_emiten', '<>', '')
            ->select('id', 'uuid', 'code_emiten')
            ->orderBy('code_emiten')
            ->get();
        $data = $this->get_emiten_fundamental_by_id($id);
        $code =  emiten::where('is_deleted', 0)
            ->where('id', $data['emiten_id'])
            ->select('code_emiten')
            ->first();
        $code_emiten = $code->code_emiten;
        $period = [
            1 => 'Semester 1',
            2 => 'Semester 2'
        ];

        return view('admin.pasar-sekunder.form-tambah-rasio-fundamental', compact(
            'kode', 'code_emiten', 'type', 'period', 'data'
        ));
    }

    public function get_emiten_fundamental_by_id($id)
    {
        $emiten_fundamental = null;
        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') .  'emiten-fundamental/id/' . $id, [
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $emiten_fundamental = (count($data) > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $emiten_fundamental = null;
        }

        return $emiten_fundamental;
    }

    public function deleteEmitenFundamental($id)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'emiten-fundamental/id/' . $id, [
                'form_params' => [
                    'token'     => app('request')->session()->get('token'),
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                echo json_encode(['msg' => $response->getStatusCode()]);
            }
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function indexPeriode()
    {
        return view('admin.pasar-sekunder.periode');
    }

    public function fetchPeriode(Request $request)
    {
        $draw   = intval($request->draw);
        $start  = intval($request->start);
        $length = intval($request->length);
        $search = $request->search;

        $order = $request->order[0];

        switch ($order['column']) {
            case 1:
                $sort = 'name';
                break;
            case 2:
                $sort = 'time';
                break;
            default:
                $sort = null;
        }

        $desc = ($sort) ? $order['dir'] : null;

        $data_periode = $this->get_data_periode($start, $length, $search['value'], $sort, $desc);

        $data = [];
        $no   = 1;
        if ($data_periode != null) {
            foreach ($data_periode as $periode) {

                if ($periode['status'] == 1) {
                    $status = 'Running';
                } else if ($periode['status'] == 2) {
                    $status = 'Schedule';
                } else if ($periode['status'] == 3) {
                    $status = 'Completed';
                } else {
                    $status = '-';
                }

                $action = '
                <a href="'.url("admin/pasar-sekunder/edit-periode/".$periode["id"]) . '" class="btn btn-sm btn-info">Edit</a> 
                <a href="#" onClick="confirmDelete(\'' . $periode["id"] . '\',  \'' . $periode['name'] . '\')" class="btn btn-sm btn-danger">Delete</a> 
                <a href="'.url("admin/pasar-sekunder/detail-pasar-sekunder/".$periode["id"]) . '" class="btn btn-sm btn-info-ghost">Detail</a>';

                array_push($data, [
                    $no++,
                    $periode['name'],
                    tgl_indo(date('Y-m-d', strtotime($periode['start_date']))).' '.formatJam($periode['start_date']),
                    tgl_indo(date('Y-m-d', strtotime($periode['end_date']))).' '.formatJam($periode['end_date']),
                    $status,
                    $action
                ]);
            }
        }
        $output = [
            "draw"  => $draw,
            "data" => $data
        ];
        echo json_encode($output);
        exit();
    }

    private function get_data_periode($start, $length, $search = null, $sort, $desc)
    {
        $limit = $length;
        $offset = ($start / $length) + 1;
        $search = (($search != null) && (strlen($search) > 3)) ? $search : '';

        $url = '?limit=' . $limit . '&offset=' . $offset . '&search=' . $search . '&sort=' . $sort . '&desc=' . $desc;
        $periode = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'market-period/' . $url, [
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $periode = ($data['meta']['Total'] > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $penerbit = null;
        }

        return $periode;
    }

    public function addPeriode()
    {
        $total = DB::table('market_periods')->where('is_deleted', 0)->count();
        $name = $total + 1;
        $name = 'Periode ' . $name;
        $type = 'created';

        $days = [
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu'
        ];
        return view('admin.pasar-sekunder.form-periode', compact('days', 'name', 'type'));
    }

    public function savePeriode(Request $request)
    {
        $data = (object)[];
        $data->name = strip_tags($request->name);
        $data->start_date = strip_tags($request->start_date);
        $data->end_date = strip_tags($request->end_date);
        $periode = null;

        $url = ($request->type == 'created') ? 'market-period/' : 'market-period/id/' . $request->id;
        $url_method = ($request->type == 'created') ? 'POST' : 'PUT';

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request($url_method, config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            if ($response->getStatusCode() == 200) {

                if ($request->type == 'created') :
                    $content = json_decode($response->getBody()->getContents(), TRUE);

                    if (!isset($content['data'])) {
                        echo json_encode(['msg' => $content['message']]);
                        return;
                    }

                    $periode = (count($content) > 0) ? $content['data'] : null;
                endif;

                $days = [
                    0 => false,
                    1 => false,
                    2 => false,
                    3 => false,
                    4 => false,
                    5 => false,
                    6 => false
                ];

                $days = array_replace($days, array_values($request->days));
                $workday = [];
                $url_method_day = ($request->type == 'created') ? 'POST' : (($request->periode_days) ? 'PUT' : 'POST');

                foreach ($days as $key => $value) {
                    $workday['market_id'] = ($request->type == 'created') ? $periode['id'] : $request->id;
                    $workday['day'] = $key;
                    $workday['is_enable'] = isset($value['enable']) ? true : false;
                    $workday['hour'] = (isset($value['hour']) && ($workday['is_enable'])) ? $value['hour'] : [];

                    $url_day = ($request->type == 'created') ? 'market-period-day/' : (($request->periode_days) ? 'market-period-day/id/' . $value['days_id'] : '/market-period-day/');

                    $responseDay = $client->request($url_method_day, config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url_day, [
                        'headers' => [
                            'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                        ],
                        'json' => $workday
                    ]);

                    sleep(1);

                    if ($responseDay->getStatusCode() !== 200) {
                        echo json_encode(['msg' => 'Gagal menyimpan hari kerja']);
                        return;
                    }
                }

                echo json_encode(['msg' => '200']);
            }
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => 'Gagal menyimpan periode']);
            return;
        }
    }

    public function editPeriode($id)
    {
        $periode = $this->get_periode_by_id($id);
        $days = (!empty($periode['days'])) ? array_slice($periode['days'], -7, 7, true) : $this->days;
        $periode_days = (!empty($periode['days'])) ? true : false;
        unset($periode['days']);

        $type = 'edit';

        $list_days = [
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu'
        ];
        return view('admin.pasar-sekunder.form-periode', compact('days', 'type', 'list_days', 'periode_days', 'periode'));
    }

    private function get_periode_by_id($id)
    {
        $periode = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'market-period/id/' . $id, [
                'headers' => $headers,
            ]);

            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $periode = (count($data) > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $periode = null;
        }

        return $periode;
    }

    public function detailPasarSekunder($id)
    {
        $periode = $this->get_periode_by_id($id);

        $periode['start_date'] = tgl_indo(date('Y-m-d', strtotime($periode['start_date'])));
        $periode['end_date'] = tgl_indo(date('Y-m-d', strtotime($periode['end_date'])));

        if ($periode['status'] == 1) {
            $periode['status'] = 'Running';
        } else if ($periode['status'] == 2) {
            $periode['status'] = 'Schedule';
        } else if ($periode['status'] == 3) {
            $periode['status'] = 'Completed';
        } else {
            $periode['status'] = '-';
        }

        return view('admin.pasar-sekunder.detail-pasar-sekunder', compact('periode'));
    }

    public function deletePeriode($id)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'market-period/id/' . $id, [
                'form_params' => [
                    'token'     => app('request')->session()->get('token'),
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                echo json_encode(['msg' => $response->getStatusCode()]);
            }
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function indexPricing()
    {
        $pricing = $this->get_data_pricing();
        return view('admin.pasar-sekunder.pricing', compact('pricing'));
    }

    private function get_data_pricing()
    {
        $pricing = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') .  'pricing', [
                'headers' => $headers,
            ]);


            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $pricing = (count($data) > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $batasan = null;
        }

        return $pricing;
    }

    public function get_pricing_by_type($type)
    {
        $pricing = $this->get_data_pricing();

        $data = [
            'service_fee' => isset($pricing[$type][0]) ? $pricing[$type][0]['value'] : 0,
            'vat' => isset($pricing[$type][1]) ? $pricing[$type][1]['value'] : 0,
            'ksei' => isset($pricing[$type][2]) ? $pricing[$type][2]['value'] : 0,
            'sales_tax' => isset($pricing[$type][3]) ? $pricing[$type][3]['value'] : 0,
            'all_in' => isset($pricing[$type][4]) ? $pricing[$type][4]['value'] : 0
        ];

        echo json_encode(['data' => $data]);
    }

    public function savePricing(Request $request)
    {
        $input['service_fee'] = $request->service_fee;
        $input['vat'] = $request->vat;
        $input['ksei'] = $request->ksei;
        $input['sales_tax'] = $request->sales_tax; 
        $input['type'] = $request->type;
        $input['all_in'] = $input['service_fee'] + $input['vat'] + $input['ksei'] + $input['sales_tax'];;

        if ($input['type'] == 'purchase') {
            $url = 'pricing/purchase';
            $data['buy'] = $input;
            unset($data['buy']['type']);
        } else {
            $url = 'pricing/sales';
            $data['sell'] = $input;
            unset($data['sell']['type']);
        }

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function indexBatasan()
    {
        $batasan = $this->get_data_batasan('min-purchase');
        $min = $batasan['data']['purchase'][0]['value'];
        return view('admin.pasar-sekunder.batasan', compact('min'));
    }

    public function getRejection(Request $request)
    {
        $draw = intval($request->draw);
        $data_rejection = $this->get_data_batasan('rejection');

        $data = [];
        $no   = 1;
        if ($data_rejection != null) {
            foreach ($data_rejection as $rejection) {

                $range = number_format($rejection['price_lower'], 0, ',', '.') . ' ' . $rejection['price_compare_type'] . ' ' . number_format($rejection['price_upper'], 0, ',', '.');
                $rejection_check = ($rejection['is_enable']) ? 'checked' : '';
                $rejection_status = ($rejection['is_enable']) ? 'Aktif' : 'Non Aktif';
                $rejection_update = ($rejection['is_enable']) ? 0 : 1;

                $status = '
                    <div class="form-check">
                        <input type="checkbox" id="rejection_' . $rejection['id'] . '" onChange="toggleAutoRejection(' . $rejection['id'] . ', ' . $rejection_update . ', \'' . $range . '\')" class="form-check-input" ' . $rejection_check . '>
                        <label class="form-check-label" for="exampleCheck1">' . $rejection_status . '</label>
                    </div>
                    ';

                $action = '<a href="'.url("admin/pasar-sekunder/edit-auto-rejection/".$rejection['id']) . '" class="btn btn-sm btn-info">Edit</a>';

                if (!$rejection['is_enable']) :
                    $action .= '<button class="btn btn-sm btn-warning delete-auto-rejection ml-1" value="' . $rejection['id'] . '">Delete</button>';
                endif;

                array_push($data, [
                    $no++,
                    $range,
                    $rejection['ara'] . ' %',
                    $rejection['arb'] . ' %',
                    $status,
                    $action
                ]);
            }
        }

        $output = [
            "draw"  => $draw,
            "data" => $data
        ];
        echo json_encode($output);
        exit();
    }

    private function get_data_batasan($type)
    {
        $batasan = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $type, [
                'headers' => $headers,
            ]);


            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $batasan = (count($data) > 0) ? $data : null;
            }
        } catch (\Exception $exception) {
            $batasan = null;
        }

        return $batasan;
    }

    public function addAutoRejection()
    {
        $type = "create";
        return view('admin.pasar-sekunder._batasan.form-auto-rejection', compact('type'));
    }

    public function editAutoRejection($id)
    {
        $data = $this->get_auto_rejection_by_id($id);
        $type = "edit";
        return view('admin.pasar-sekunder._batasan.form-auto-rejection', compact('type', 'data', 'id'));
    }

    public function get_auto_rejection_by_id($id)
    {
        $auto_rejection = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') .  'rejection/id/' . $id, [
                'headers' => $headers,
            ]);


            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $auto_rejection = (count($data) > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $auto_rejection = null;
        }

        return $auto_rejection;
    }

    public function save_auto_rejection(Request $request)
    {
        $data['type'] = $request->type;
        $data['id'] = $request->id;
        $data['price_lower'] = $request->price_lower;
        $data['price_compare_type'] = $request->price_compare_type;
        $data['price_upper'] = $request->price_upper;
        $data['ara'] = $request->ara;
        $data['arb'] = $request->arb;

        $url = ($data['type'] == 'create') ? 'rejection/' : 'rejection/id/' . $data['id'];
        $url_method = ($data['type'] == 'create') ? 'POST' : 'PUT';

        $data['status'] = 1;
        unset($data['id']);
        unset($data['type']);

        $data['price_lower'] = strip_tags((int)str_replace('.', '', $data['price_lower']));
        $data['price_upper'] = strip_tags((int)str_replace('.', '', $data['price_upper']));
        $data['ara'] = strip_tags(str_replace('.', '', $data['ara']));
        $data['arb'] = strip_tags(str_replace('.', '', $data['arb']));

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request($url_method, config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function delete_auto_rejection($id)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'rejection/id/' . $id, [
                'form_params' => [
                    'token'     => app('request')->session()->get('token'),
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                echo json_encode(['msg' => $response->getStatusCode()]);
            }
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function update_status_autorejection(Request $request)
    {
        $data['id'] = $request->id;
        $data['status'] = $request->status;

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'rejection/status/' . $data['id'], [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function addFraction()
    {
        $type = "create";
        return view('admin.pasar-sekunder._batasan.form-fraction', compact('type'));
    }

    public function editFraction($id)
    {
        $data = $this->get_fraction_by_id($id);
        $type = "edit";
        return view('admin.pasar-sekunder._batasan.form-fraction', compact('type', 'id', 'data'));
    }

    public function get_fraction_by_id($id)
    {
        $fraction = null;

        try {
            $client = new \GuzzleHttp\Client();

            $headers = [
                'Authorization' => 'Bearer ' . app('request')->session()->get('token'),
                'Accept'        => 'application/json',
                'Content-type'  => 'application/json'
            ];

            $response = $client->request('GET', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') .  'fraction/id/' . $id, [
                'headers' => $headers,
            ]);


            if ($response->getStatusCode() == 200) {
                $data = json_decode($response->getBody()->getContents(), TRUE);
                $fraction = (count($data) > 0) ? $data['data'] : null;
            }
        } catch (\Exception $exception) {
            $fraction = null;
        }

        return $fraction;
    }

    public function get_fraction(Request $request)
    {
        $draw   = intval($request->draw);
        $data_fraction = $this->get_data_batasan('fraction');

        $data = [];
        $no   = 1;
        if ($data_fraction != null) {
            foreach ($data_fraction as $fraction) {

                $range = number_format($fraction['price_lower'], 0, ',', '.') . ' ' . $fraction['price_compare_type'] . ' ' . number_format($fraction['price_upper'], 0, ',', '.');
                $fraction_check = ($fraction['is_enable']) ? 'checked' : '';
                $fraction_status = ($fraction['is_enable']) ? 'Aktif' : 'Non Aktif';
                $fraction_update = ($fraction['is_enable']) ? 0 : 1;

                $status = '
                    <div class="form-check">
                        <input type="checkbox" id="fraction_' . $fraction['id'] . '" onChange="toggleFraction(' . $fraction['id'] . ', ' . $fraction_update . ', \'' . $range . '\')" class="form-check-input" ' . $fraction_check . '>
                        <label class="form-check-label" for="exampleCheck1">' . $fraction_status . '</label>
                    </div>
                    ';

                $action = '
                    <a href="'.url("admin/pasar-sekunder/edit-fraction/".$fraction['id']). '" class="btn btn-sm btn-info">Edit</a>';

                if (!$fraction['is_enable']) :
                    $action .= '<button class="btn btn-sm btn-warning delete-fraction ml-1" value="' . $fraction['id'] . '">Delete</button>';
                endif;

                array_push($data, [
                    $no++,
                    $range,
                    $fraction['fraction'],
                    $fraction['max_change'],
                    $status,
                    $action
                ]);
            }
        }
        $output = [
            "draw"  => $draw,
            "data" => $data
        ];
        echo json_encode($output);
        exit();
    }


    public function save_fraction(Request $request)
    {
        $data['type'] = $request->type;
        $data['id'] = $request->id;
        $data['price_lower'] = $request->price_lower;
        $data['price_upper'] = $request->price_upper;
        $data['price_compare_type'] = $request->price_compare_type;
        $data['fraction'] = $request->fraction;
        $data['max_change'] = $request->max_change;

        $url = ($data['type'] == 'create') ? 'fraction/' : 'fraction/id/' . $data['id'];
        $url_method = ($data['type'] == 'create') ? 'POST' : 'PUT';

        $data['status'] = 1;
        unset($data['id']);
        unset($data['type']);

        $data['price_lower'] = strip_tags((int)str_replace('.', '', $data['price_lower']));
        $data['price_upper'] = strip_tags((int)str_replace('.', '', $data['price_upper']));
        $data['fraction'] = strip_tags(str_replace('.', '', $data['fraction']));
        $data['max_change'] = strip_tags(str_replace('.', '', $data['max_change']));

        $data['price_lower'] = (int)str_replace(' ', '', $data['price_lower']);
        $data['price_upper'] = (int)str_replace(' ', '', $data['price_upper']);
        $data['fraction'] = (int)str_replace(' ', '', $data['fraction']);
        $data['max_change'] = (int)str_replace(' ', '', $data['max_change']);

        if ($data['fraction'] < 0 || $data['fraction'] == 0) {
            echo json_encode(['msg' => 'Fraksi tidak boleh 0']);
            return;
        }

        if ($data['max_change'] < 0 || $data['max_change'] == 0) {
            echo json_encode(['msg' => 'Maksimal jenjang perubahan tidak boleh 0']);
            return;
        }

        if ($data['fraction'] > 0 || $data['max_change'] > 0) {
            try {
                $client = new \GuzzleHttp\Client();
                $response = $client->request($url_method, config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . $url, [
                    'headers' => [
                        'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                    ],
                    'json' => $data
                ]);

                echo json_encode(['msg' => $response->getStatusCode()]);
                return;
            } catch (\Exception $exception) {
                $response = $exception->getResponse();
                $responseBody = $response->getBody()->getContents();
                $body = json_decode($responseBody, true);
                echo json_encode(['msg' => $body['message']]);
                return;
            }
        }
    }

    public function delete_fraction($id)
    {
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('DELETE', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'fraction/id/' . $id, [
                'form_params' => [
                    'token' => app('request')->session()->get('token'),
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                echo json_encode(['msg' => $response->getStatusCode()]);
            }
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function update_status_fraction(Request $request)
    {
        $data['id'] = $request->id;
        $data['status'] = $request->status;

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'fraction/status/' . $data['id'], [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

    public function save_min_purchase(Request $request)
    {
        $data['value'] = str_replace('.', '', $request->value);

        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('PUT', config('global.BASE_API_ADMIN_URL').'/'.config('global.API_ADMIN_VERSION') . 'min-purchase/', [
                'headers' => [
                    'Authorization' => 'Bearer ' . app('request')->session()->get('token')
                ],
                'json' => $data
            ]);

            echo json_encode(['msg' => $response->getStatusCode()]);
            return;
        } catch (\Exception $exception) {
            $response = $exception->getResponse();
            $responseBody = $response->getBody()->getContents();
            $body = json_decode($responseBody, true);
            echo json_encode(['msg' => $body['message']]);
            return;
        }
    }

}
