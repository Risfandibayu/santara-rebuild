<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KYC
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->trader->status_kyc1 == 'verified'  || Auth::user()->trader->status_kyc2 == 'verified'  || Auth::user()->trader->status_kyc3 == 'verified'  || Auth::user()->trader->status_kyc4 == 'verified'  || Auth::user()->trader->status_kyc5 == 'verified'  || Auth::user()->trader->status_kyc6 == 'verified'   ) {
            return $next($request);
        }

        return response()->view('user.kyc');
    }
}
