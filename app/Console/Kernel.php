<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */

    protected $commands = [
        Commands\EmailNotifDividen::class,
        Commands\NotifNotSubmitLapkeu::class,
    ];

    protected function schedule(Schedule $schedule)
    {
        if (config('global.CONFIG_ENV_GLOBAL') == "DEV") {
            //$schedule->command('notifdividen:cron')->everyMinute();
            //$schedule->command('command:notsubmitlapkeu')->everyMinute();
            $schedule->command('notifdividen:cron')->dailyAt('13:00');
            $schedule->command('command:notsubmitlapkeu')->dailyAt('13:00');
        }else{
            $schedule->command('notifdividen:cron')->dailyAt('13:00');
            $schedule->command('command:notsubmitlapkeu')->dailyAt('13:00');
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
