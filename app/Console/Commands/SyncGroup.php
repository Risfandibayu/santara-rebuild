<?php

namespace App\Console\Commands;

use App\Helpers\EmitenHelper;
use App\Models\trader;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class SyncGroup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:group';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Running...");

        $emitensActive = EmitenHelper::Active();

        $this->info("Found " . $emitensActive->count() . " Emitens...");
        $this->info("Ready to executing...");

        foreach ($emitensActive as $emiten) {

            $db = DB::connection('chat');
            $groupExist = $db->table('groups')
                ->where('emiten_id', $emiten->id)
                ->first();

            if (!$groupExist) {
                $this->info("Creating group with emiten ID [". $emiten->id . "] ...");
                Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "email" => env("SANTARA_CHAT_ADMIN_EMAIL"),
                    "password" => env("SANTARA_CHAT_ADMIN_PASSWORD"),
                ])->post(env('SANTARA_CHAT_BASE_URL') .'/api/groups', [
                    "name" => $emiten->trademark,
                    "description" => $emiten->company_name,
                    "group_type" => 1, //closed group
                    "privacy" => 2,
                    "photo_url" => $emiten->pictures,
                    "users" => [1],
                    "emiten_id" => $emiten->id
                ])->json();

                $this->info("group created successfully");
                $this->info("Assign trader emiten owner...");

                $userByTrader = trader::find($emiten->trader_id);

                $db->table('users')
                    ->where('id', $userByTrader->user_id)
                    ->update([
                        'is_trader' => 1
                    ]);
            } else {
                $this->warn("groups with emiten ID [" . $emiten->id . "] already exist, skipped...");
            }
        }
    }
}
