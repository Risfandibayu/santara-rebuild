<?php

namespace App\Console\Commands;

use App\Helpers\EmitenHelper;
use App\Models\emitens_old;
use App\Models\trader;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class syncGroupNowPlaying extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:groupNp';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    function emitenn($limit, $offset, $search, $minimal, $maksimal, $category, $sort, $type, $jenis)
	{
		$data = null;
		$url = '/v3.7.1/emitens/emiten?projectValue1=' . $minimal . '&projectValue2=' . $maksimal . '&category=' . $category . '&search=' . $search . '&sort=' . $sort . '&pageSize=' . $limit . '&pageNumber=' . $offset . '&type=' . $type . '&jenis=' . $jenis;

		try {
			$client = new \GuzzleHttp\Client();

			$headers = [
				'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjMxNTQzMywiZGF0YSI6eyJpZCI6MzE1NDMzLCJ1dWlkIjoiZmUwMDVkNTEtYTg4OC00MThlLWJhYWMtNGYzNjdhNTBhMzQzIiwiZW1haWwiOiJhaHNpYXBAZ21haWwuY29tIiwiY3JlYXRlZF9hdCI6IjIwMjItMDMtMDEgMjA6NDE6MDYiLCJ1cGRhdGVkX2F0IjoiMjAyMi0wNy0wNSAyMTo0NjoxNyIsImRlbGV0ZWRfYXQiOm51bGwsInJvbGVfaWQiOjEsImlzX3ZlcmlmaWVkIjoxLCJ0d29fZmFjdG9yX2F1dGgiOjAsInR3b19mYWN0b3Jfc2VjcmV0IjpudWxsLCJpc19sb2dnZWRfaW4iOjEsImlzX2RlbGV0ZWQiOjAsImNyZWF0ZWRfYnkiOm51bGwsInVwZGF0ZWRfYnkiOm51bGwsImlzX290cCI6MSwiYXR0ZW1wdCI6MCwiYXR0ZW1wdF9lbWFpbCI6MCwiZmluZ2VyX3ByaW50IjpudWxsLCJhdHRlbXB0X290cCI6MCwiYXR0ZW1wdF9waW4iOjAsIm5hbWUiOiJBZG1pbiIsInRyYWRlcl90eXBlIjpudWxsLCJpc19yZXNldF9wYXNzd29yZCI6MSwiaXNfdmVyaWZpZWRfa3ljIjoyLCJyb2xlX25hbWUiOiJBZG1pbiJ9LCJpYXQiOjE2NTcwMzM1MDF9.GUQlcBAamo_zpb9SYw_5LJVaYCdm2q1-F8ZJnIneM2Y',
				'Accept'        => 'application/json',
				'Content-type'  => 'application/json'
			];

			$response = $client->request('GET', config('global.BASE_API_CLIENT_URL') . $url, [
				'headers' => $headers,
			]);

			if ($response->getStatusCode() == 200) {
				$data = json_decode($response->getBody()->getContents(), TRUE);
			}
		} catch (\Exception $exception) {
			$data = null;
		}

		return $data;
	}

    public function handle()
    {
        $this->info("Running...");

        // $np = EmitenHelper::NowPlaying(99, 1, null, null, null, null, null, 'saham', 'notfull');
        $np = EmitenHelper::QueryNowPlaying()->get()->toArray();

        $now_playing = collect($np);

        $this->info("Found " . $now_playing->count() . " Emitens...");
        $this->info("Ready to executing...");

        foreach ($now_playing as $emiten) {
            $picture = explode(',',$emiten['pictures']);
            $db = DB::connection('chat');

            $groupExist = $db->table('groups')
                ->where('emiten_id', $emiten['id'])
                ->first();

            if (!$groupExist) {
                $this->info("Creating group with emiten ID [". $emiten['id'] . "] ...");
                Http::withHeaders([
                    "Content-Type" => "application/json",
                    "Accept" => "application/json",
                    "email" => env("SANTARA_CHAT_ADMIN_EMAIL"),
                    "password" => env("SANTARA_CHAT_ADMIN_PASSWORD"),
                ])->post(env('SANTARA_CHAT_BASE_URL') .'/api/groups', [
                    "name" => $emiten['trademark'],
                    "description" => $emiten['company_name'],
                    "group_type" => 1, //closed group
                    "privacy" => 2,
                    // "photo_url" => str_replace('https://storage.googleapis.com/asset-santara/santara.co.id/token/','',$emiten['pictures'][0]['picture'])  ,
                    "photo_url" => $picture[0]  ,
                    "users" => [1],
                    "emiten_id" => $emiten['id']
                ])->json();
                $this->info("group created successfully");

                $emiten = emitens_old::find($emiten['id']);

                $trader = trader::find($emiten->trader_id);

                if ($trader) {
                    $this->info("Assigning emiten owner");

                    User::find($trader->user_id)
                        ->update([
                            'is_trader' => 1
                        ]);
                }
            } else {
                $this->warn("groups with emiten ID [" . $emiten['id'] . "] already exist, skipped...");
            }
        }
    }
}
