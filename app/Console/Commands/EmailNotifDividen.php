<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\emiten;
use App\Models\Dividen;
use App\Models\emiten_journey;
use App\Models\StopNotification;
use Carbon\Carbon;

class EmailNotifDividen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifdividen:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $soldout = emiten::select('emitens.id', 'emitens.company_name', 'emitens.price', 'emitens.supply', 
            'emitens.is_deleted', 'emitens.is_active', 'emitens.period', 'emitens.begin_period', 'u.email', 'u.id as user_id')
            ->leftJoin('traders as t', 't.id', '=', 'emitens.trader_id')
            ->leftJoin('users as u', 'u.id', '=', 't.user_id')
            ->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->where('emitens.is_deleted',0)
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
                IF(
                (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                    (SUM(
                        IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                        and 
                        emitens.is_deleted = 0
                        and emitens.is_active = 1
                        and emitens.begin_period < now()')
            ->get();
        $datas = [];
        foreach ($soldout as $row) {
            $bulan = str_replace("1 tahun lalu", '', $row->period);
            $bulan = intval(str_replace(" bulan", '', $row->period));
            $dividen_date = "";
            $dividen = Dividen::where('emiten_id', $row->id)
                ->orderBy('id', 'DESC')
                ->first();
            if($dividen != null){
                $dividen_date = Carbon::parse($dividen->devidend_date)
                    ->addMonths($bulan);
            }else{
                $tmpyd = emiten_journey::select('emiten_journeys.date', 'emiten_journeys.end_date')
                    ->leftJoin('emitens', 'emitens.id','=','emiten_journeys.emiten_id')
                    ->where('emiten_journeys.emiten_id', $row->id)
                    ->where('emiten_journeys.title','Penyerahan Dana')
                    ->where('emitens.is_active',1)
                    ->where('emitens.is_deleted',0)
                    ->groupBy('emitens.id')
                    ->first();
                if($tmpyd != null){
                    $dividen_date = Carbon::parse($tmpyd->date)
                        ->addMonths($bulan);
                }
            }
        
            if($dividen_date != null || $dividen_date != ""){
                $bulanCek = Carbon::parse($dividen_date)->subMonth()->format('m');
                $tglDividen =  Carbon::createFromFormat('Y-m-d H:i:s', $dividen_date);
                $bulanSekarang = Carbon::now()->format('m');
                $tglSekarang = Carbon::now();
                $cekTglSekarang = Carbon::now()->format('d');
                if($bulanSekarang == $bulanCek){
                    if($cekTglSekarang == 5){
                        $stopNotifDividen = StopNotification::where('user_id', $row->user_id)
                            ->whereMonth('date_notification', $dividen_date)
                            ->where('type_notification', 'dividend')
                            ->where('is_deleted', 0)
                            ->count();
                        if($stopNotifDividen == 0){
                            $cekSudahDividen = Dividen::where('emiten_id', $row->id)
                                ->whereMonth('devidend_date', $dividen_date)
                                ->count();
                            
                            if($cekSudahDividen == 0){
                                array_push($datas, [
                                    'id' => $row->id,
                                    'company_name' => $row->company_name,
                                    'email' => $row->email,
                                    'date' => $dividen_date,
                                    'period' => $bulan
                                ]);
                            }
                        }
                    }
                }elseif($tglSekarang->gt($tglDividen)){
                    $stopNotifDividen = StopNotification::where('user_id', $row->user_id)
                            ->whereMonth('date_notification', $dividen_date)
                            ->where('type_notification', 'dividend')
                            ->where('is_deleted', 0)
                            ->count();
                        if($stopNotifDividen == 0){
                            $cekSudahDividen = Dividen::where('emiten_id', $row->id)
                                ->whereMonth('devidend_date', $dividen_date)
                                ->count();
                            
                            if($cekSudahDividen == 0){
                                array_push($datas, [
                                    'id' => $row->id,
                                    'company_name' => $row->company_name,
                                    'email' => $row->email,
                                    'date' => $dividen_date,
                                    'period' => $bulan
                                ]);
                            }
                        }
                }
            }
        }

        if (config('global.CONFIG_ENV_GLOBAL') == "DEV"){
            foreach($datas as $row){
                if($row['email'] == 'fatakhulafi11@gmail.com'){
                    $details = [
                        'subject' => 'Pembagian Dividen',
                        'company_name' => $row['company_name'],
                        'tanggal_dividen' => $row['date']
                    ];
                    \Mail::to($row['email'])->send(new \App\Mail\NotifPemberitahuanDividen($details));
                }
            }
        }else{
            foreach($datas as $row){
                $details = [
                    'subject' => 'Pembagian Dividen',
                    'company_name' => $row['company_name'],
                    'tanggal_dividen' => $row['date']
                ];
                \Mail::to($row['email'])->send(new \App\Mail\NotifPemberitahuanDividen($details));
            }
        }
    }
}
