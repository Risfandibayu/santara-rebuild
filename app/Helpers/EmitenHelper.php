<?php

namespace App\Helpers;

use App\Models\emiten;
use App\Models\emitens_old;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;

class EmitenHelper
{
    const TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjMxNTQzMywiZGF0YSI6eyJpZCI6MzE1NDMzLCJ1dWlkIjoiZmUwMDVkNTEtYTg4OC00MThlLWJhYWMtNGYzNjdhNTBhMzQzIiwiZW1haWwiOiJhaHNpYXBAZ21haWwuY29tIiwiY3JlYXRlZF9hdCI6IjIwMjItMDMtMDEgMjA6NDE6MDYiLCJ1cGRhdGVkX2F0IjoiMjAyMi0wNy0wNSAyMTo0NjoxNyIsImRlbGV0ZWRfYXQiOm51bGwsInJvbGVfaWQiOjEsImlzX3ZlcmlmaWVkIjoxLCJ0d29fZmFjdG9yX2F1dGgiOjAsInR3b19mYWN0b3Jfc2VjcmV0IjpudWxsLCJpc19sb2dnZWRfaW4iOjEsImlzX2RlbGV0ZWQiOjAsImNyZWF0ZWRfYnkiOm51bGwsInVwZGF0ZWRfYnkiOm51bGwsImlzX290cCI6MSwiYXR0ZW1wdCI6MCwiYXR0ZW1wdF9lbWFpbCI6MCwiZmluZ2VyX3ByaW50IjpudWxsLCJhdHRlbXB0X290cCI6MCwiYXR0ZW1wdF9waW4iOjAsIm5hbWUiOiJBZG1pbiIsInRyYWRlcl90eXBlIjpudWxsLCJpc19yZXNldF9wYXNzd29yZCI6MSwiaXNfdmVyaWZpZWRfa3ljIjoyLCJyb2xlX25hbWUiOiJBZG1pbiJ9LCJpYXQiOjE2NTcwMzM1MDF9.GUQlcBAamo_zpb9SYw_5LJVaYCdm2q1-F8ZJnIneM2Y";

    public static function Active()
    {
        return self::Query()
            ->get();
    }

    // public static function NowPlaying($limit = 99, $offset = 1, $search = null, $minimal = null, $maksimal = null, $category = null, $sort = null, $type = "saham", $jenis = "notfull")
    // {
    //     $data = null;
    //     $url = '/v3.7.1/emitens/emiten?projectValue1='
    //         . $minimal
    //         . '&projectValue2='
    //         . $maksimal
    //         . '&category='
    //         . $category
    //         . '&search='
    //         . $search
    //         . '&sort='
    //         . $sort
    //         . '&pageSize='
    //         . $limit . '&pageNumber='
    //         . $offset . '&type='
    //         . $type . '&jenis='
    //         . $jenis;

    //     try {
    //         $client = new Client();

    //         $headers = [
    //             'Authorization' => self::TOKEN,
    //             'Accept'        => 'application/json',
    //             'Content-type'  => 'application/json'
    //         ];

    //         $response = $client->request('GET', config('global.BASE_API_CLIENT_URL') . $url, [
    //             'headers' => $headers,
    //         ]);

    //         if ($response->getStatusCode() == 200) {
    //             $data = json_decode($response->getBody()->getContents(), TRUE);
    //         }
    //     } catch (\Exception $exception) {
    //         $data = null;
    //     }

    //     return $data;
    // }

    public static function AdminEmitens()
    {
        $nowPlaying = array_map(function ($value) {
            return $value["id"];
        }, self::QueryNowPlaying()->get()->toArray());

        $active = array_map(function ($value) {
            return $value["id"];
        }, self::Active()->toArray());

        $diff = array_diff($nowPlaying, $active);

        return array_merge($active, $diff);
    }

    public static function TraderEmitens()
    {
        $userId = request()->header('userId');

        $emiten = emiten::select(
            'emitens.id',
            'emitens.company_name',
            'emitens.pictures',
            'emitens.trademark',
            'emitens.price',
            'emitens.supply',
            'emitens.is_deleted',
            'emitens.is_active',
            'emitens.begin_period')
            ->join('traders', 'traders.id', '=', 'emitens.trader_id')
            ->where('traders.user_id', $userId)
            ->where('emitens.is_active', 1)
            ->where('emitens.is_deleted', 0)
            ->get();

        return $emiten;
    }

    public static function Query()
    {
        return emitens_old::select(
            'emitens.id',
            'emitens.company_name',
            'emitens.pictures',
            'emitens.trademark',
            'emitens.price',
            'emitens.supply',
            'emitens.is_deleted',
            'emitens.is_active',
            'emitens.begin_period',
            'emitens.trader_id',
            'categories.category as ktg',
            DB::raw("SUM(Distinct(devidend.devidend)) as dvd"),
            DB::raw("COUNT(Distinct(devidend.id)) as dvc"))
            ->leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('devidend', 'devidend.emiten_id','=','emitens.id')
            ->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->orderby('emitens.id','DESC')
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
            IF(
              (SUM(
                IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                  (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) = 100.00
                    and
                    emitens.is_deleted = 0
                    and emitens.is_active = 1
                    and emitens.begin_period < now()'
            );
    }

    public static function QueryNowPlaying()
    {
        return emitens_old::select(
            'emitens.id',
            'emitens.company_name',
            'emitens.pictures',
            'emitens.trademark',
            'emitens.price',
            'emitens.supply',
            'emitens.is_deleted',
            'emitens.is_active',
            'emitens.begin_period',
            'emitens.trader_id',
            'categories.category as ktg',
            DB::raw("SUM(Distinct(devidend.devidend)) as dvd"),
            DB::raw("COUNT(Distinct(devidend.id)) as dvc"))
            ->leftjoin('categories', 'categories.id','=','emitens.category_id')
            ->leftjoin('devidend', 'devidend.emiten_id','=','emitens.id')
            ->leftjoin('transactions','transactions.emiten_id','=','emitens.id')
            ->orderby('emitens.id','DESC')
            ->groupBy('emitens.id')
            ->havingRaw('CONVERT(ROUND(
            IF(
              (SUM(
                IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply > 1, 1,
                  (SUM(
                    IF(transactions.is_verified = 1 and transactions.is_deleted = 0, transactions.amount, 0)) / emitens.price) / emitens.supply) * 100, 2), char) != 100.00
                    and
                    emitens.is_deleted = 0
                    and emitens.is_active = 1
                    and emitens.begin_period < now()'
            );
    }

}
