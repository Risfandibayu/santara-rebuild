<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthHelper
{
    public static function getUserIdentitiesByGroup($collection): array
    {
        return array_values($collection->map(function ($item){
            return $item[0]->trader_id;
        })->toArray());
    }

    public static function getEmail()
    {
        return Auth::user()->email;
    }
    public static function getPassword()
    {
        return Session::get('pwd');
    }

    public static function getMarital($id = null) {
		$result = '';
		if ($id != null ){
			$marital = array (
				'1' => 'Single',
				'2' => 'Menikah (Married)',
				'3' => 'Duda (Widower)',
				'4' => 'Janda (Widow)'
			);            
			$result = ($id == 'all') ? $marital : $marital[$id];
		}
		return $result;
	}

}
