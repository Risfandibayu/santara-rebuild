<?php

use App\Models\Role;
use App\Models\Permission;

function ispermitted($permission = '') {
    $user_permissions = Role::where('id', \Auth::user()->role_id)->select('permissions')->first();
    $user_permissions =  explode(',', $user_permissions->permissions);
    foreach ( $user_permissions as $up ) {
        $permissions = Permission::where('id', $up)
            ->first();
        if ( $permissions != null ) {
            if ( $permissions->id == $up && $permissions->permission == $permission ) {
                return true;
            }
        }
    }

    return false;
}