<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\User;
use DB;

class TransaksiEmiten implements FromView, WithTitle, ShouldAutoSize, WithEvents
{
    
    protected $tglAwal, $tglAkhir, $emitenID, $cari;

    public function __construct($tglAwal, $tglAkhir, $emitenID, $cari) {
        $this->tglAwal = $tglAwal;
        $this->tglAkhir = $tglAkhir;
        $this->emitenID = $emitenID;
        $this->cari = $cari;
    }

    public function view(): View {
        $cari = $this->cari;
        $emitenID = $this->emitenID;
        if($this->tglAwal != "" && $this->tglAkhir != ""){
            $transactions = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->where('tr.last_status', 'VERIFIED')
                ->where('e.id', $emitenID)
                ->whereDate('tr.created_at', '>=', $this->tglAwal)
                ->whereDate('tr.created_at', '<=', $this->tglAkhir)
                ->where('tr.is_deleted', 0)
                ->where(function($search) use ($cari){
                    $search->where('users.email', 'like', '%'.$cari.'%')
                        ->orWhere('t.name', 'like', '%'.$cari.'%')
                        ->orWhere('t.phone', 'like', '%'.$cari.'%');
                })
                ->select('tr.id', 't.name as trader_name', 'users.email as user_email', 
                    'e.code_emiten', 'e.company_name',
                    'tr.channel', 'tr.split_fee', 'tr.created_at as created_at', 
                    'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                    't.phone')
                ->orderBy('tr.created_at', 'DESC')
                ->get();
        }else{
            $transactions = User::join('traders as t', 't.user_id', '=', 'users.id')
                ->join('transactions as tr', 'tr.trader_id', '=', 't.id')
                ->join('emitens as e', 'e.id', '=', 'tr.emiten_id')
                ->where('tr.last_status', 'VERIFIED')
                ->where('e.id', $emitenID)
                ->where('tr.is_deleted', 0)
                ->where(function($search) use ($cari){
                    $search->where('users.email', 'like', '%'.$cari.'%')
                        ->orWhere('t.name', 'like', '%'.$cari.'%')
                        ->orWhere('t.phone', 'like', '%'.$cari.'%');
                })
                ->select('tr.id', 't.name as trader_name', 'users.email as user_email', 
                    'e.code_emiten', 'e.company_name',
                    'tr.channel', 'tr.split_fee', 'tr.created_at as created_at', 
                    'tr.amount', 'tr.fee', 'e.price', DB::raw('(tr.amount/e.price) as qty'), 
                    't.phone')
                ->orderBy('tr.created_at', 'DESC')
                ->get();
        }
        return view('admin.export.export-transaksi-emiten', ['transactions' => $transactions, 'tglAwal' => $this->tglAwal, 'tglAkhir' => $this->tglAkhir]);
    }

    public function title(): string
    {
        return "Data Transaksi";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A1:G1')
                                ->getAlignment()
                                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
   
            },
        ];
    }
}
