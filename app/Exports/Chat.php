<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithCustomChunkSize;
use App\Models\User;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class Chat implements FromView, WithTitle, ShouldAutoSize, WithEvents, WithCustomChunkSize
{

    protected $tglAwal, $tglAkhir;

    public function __construct($tglAwal, $tglAkhir) {
        $this->tglAwal = $tglAwal;
        $this->tglAkhir = $tglAkhir;
    }

    public function view(): View
    {
        return view('admin.export.export-chat', ['tglAwal' => $this->tglAwal, 'tglAkhir' => $this->tglAkhir]);
    }

    public function chunkSize(): int
    {
        return 500;
    }

    public function title(): string
    {
        return "Chat Report";
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A1:I1')
                                ->getAlignment()
                                ->setHorizontal(Alignment::HORIZONTAL_CENTER);

            },
        ];
    }
}
