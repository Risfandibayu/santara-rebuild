<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use App\Models\emiten;

class KycBisnis implements FromView, WithTitle, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $start, $end;

    public function __construct($start, $end) {
        $this->start = $start;
        $this->end = $end;
    }

    public function view(): View
    {
        $pralisting = emiten::select('emitens.id', 'emitens.uuid', 'emitens.company_name', 'emitens.trademark', 'emitens.code_emiten', 'emitens.price',
                'emitens.supply', 'emitens.is_deleted', 'emitens.is_active', 'emitens.begin_period', 'emitens.created_at',
                'categories.category as ktg','emitens.begin_period as sd', 'emitens.end_period as ed', 'emitens.capital_needs', 'emitens.is_verified',
                't.name', 't.phone', 'emitens.is_verified_bisnis', 'emitens.avg_general_share_amount', 'emitens.avg_capital_needs')
                ->leftjoin('categories', 'categories.id','=','emitens.category_id')
                ->leftjoin('emiten_votes as ev','ev.emiten_id','=','emitens.id')
                ->leftjoin('emiten_journeys','emiten_journeys.emiten_id','=','emitens.id')
                ->leftjoin('traders as t', 't.id', '=', 'emitens.trader_id')
                ->where('emitens.is_deleted',0)
                ->where('emitens.is_verified',1)
                ->where('emitens.is_pralisting',1)
                ->where('emitens.is_coming_soon',1)
                ->whereNull('emitens.is_verified_bisnis')
                ->orWhere('emitens.is_verified_bisnis', 2)
                ->groupBy('emitens.id')
                ->orderby('emitens.created_at','DESC')
                ->get();
        $data = [];
        $no = 0;
        foreach ($pralisting as $row) {
            $no++;
            if($no >= $this->start && $no <= $this->end){
                array_push($data, [
                    'id' => $row->id,
                    'company_name' => $row->company_name,
                    'trademark' => $row->trademark,
                    'avg_capital_needs' => $row->avg_capital_needs,
                    'is_verified' => $row->is_verified,
                    'created_at' => $row->created_at,
                    'trader_name' => $row->trader_name,
                    'phone' => $row->phone,
                    'is_verified_bisnis' => $row->is_verified_bisnis,
                    'avg_general_share_amount' => $row->avg_general_share_amount,
                ]);
            }
        }
        return view('admin.export.export-kyc-bisnis', ['emiten' => $data]);
    }

    public function title(): string
    {
        return "Data Calon Penerbit";
    }
}
