@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h2><strong>Push Broadcast Notification</strong></h2>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a href="{{ url('admin/histori-notifikasi/'.$broadcastId) }}" class="btn btn-primary">Riwayat Pemberitahuan</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body">
                                    {{-- <div class="bg-light p-1 mb-1">
                                        <h5>Mengirim ke broadcast ke <span id="totalUser"></span> user dengan setiap halaman
                                            {{ $limit }} user</h5>
                                    </div> --}}

                                    <form id="frmTambahNotifikasi" method="POST" enctype="multipart/form-data">
                                        <h3>Konten Pemberitahuan : </h3>
                                        <div class="card border border-light card-body">
                                            <div class="row justify-content-between">
                                                <div>
                                                    <h5 class="text-left text-danger"><strong>{{ $kategori }}</strong>
                                                    </h5>
                                                    <h2>{{ $notif['title'] }}</h2>
                                                    <p>{{ $notif['content'] }}</p>
                                                </div>
                                                <div>
                                                    @if ($notif['image'] != '')
                                                        <img class="img-broadcast" src="{{ $notif['image'] }}" />
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Target User</label>
                                            <a href="{{ url('admin/get-push-notif/' . $broadcastId) }}"
                                                class="btn btn-primary">
                                                Download Target User</a>
                                        </div>
                                        <div class="form-group">
                                            <label>Unggah Target User</label>
                                            <div class="custom-file">
                                                <input type="file" name="json_users" class="custom-file-input"
                                                    id="customFile">
                                                <label class="custom-file-label" for="customFile">Pilih Target User</label>
                                            </div>
                                        </div>
                                        {{-- <div class="progress">
                                            <div class="bar"></div>
                                            <div class="percent">0%</div>
                                        </div> --}}
                                        <input type="hidden" name="broadcast_id" value="{{ $broadcastId }}" />
                                        <input type="hidden" name="message" id="message"
                                            value="{{ $notif['content'] }}" />
                                        <input type="hidden" name="redirection" id="redirection"
                                            value="{{ $notif['redirection'] }}" />
                                        <input type="hidden" name="image" id="image"
                                            value="{{ $notif['image'] }}" />
                                        <input type="hidden" name="title" id="title"
                                            value="{{ $notif['title'] }}" />
                                        <input class="form-control" name="namaBroadcast" id="namaBroadcast" type="hidden"
                                            value="{{ $namaBroadcast }}" />
                                        <button class="btn btn-primary" id="btnPushNotif" type="submit">Kirim <i
                                                class="la la-bell"></i></button>
                                        <button class="btn btn-warning" id="btnPushNotifEmail" type="submit">Kirim
                                            Broadcast
                                            Email <i class="la la-envelop"></i></button>
                                    </form>
                                    {{-- <nav class="row justify-content-between p-2">
                                        <h5 class="mt-1"><strong>Halaman</strong></h5>
                                        <ul class="pagination" id="pagination"></ul>
                                    </nav> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script>
        function validate(formData, jqForm, options) {
            var form = jqForm[0];
            if (!form.file.value) {
                alert('File not found');
                return false;
            }
        }
        $(function() {

            $(document).ready(function() {

                bsCustomFileInput.init();

                // var bar = $('.bar');
                // var percent = $('.percent');
                // var status = $('#status');

                // $('form').ajaxForm({
                //     beforeSend: function() {
                //         status.empty();
                //         var percentVal = '0%';
                //         var posterValue = $('input[name=json_users]').fieldValue();
                //         bar.width(percentVal)
                //         percent.html(percentVal);
                //     },
                //     uploadProgress: function(event, position, total, percentComplete) {
                //         var percentVal = percentComplete + '%';
                //         bar.width(percentVal)
                //         percent.html(percentVal);
                //     },
                //     success: function() {
                //         var percentVal = 'Wait, Saving';
                //         bar.width(percentVal)
                //         percent.html(percentVal);
                //     },
                //     complete: function(data) {
                //         Swal.fire(
                //             'Berhasil!',
                //             data.message,
                //             'success'
                //         );
                        // window.location.href = "/file-upload";
                //     }
                // });

                // var bar = $('.bar');
                // var percent = $('.percent');

                // $('#frmTambahNotifikasi').ajaxForm({
                //     beforeSend: function() {
                //         var percentage = '0';
                //     },
                //     uploadProgress: function(event, position, total, percentComplete) {
                //         var percentage = percentComplete;
                //         $('.progress .progress-bar').css("width", percentage + '%');
                //         $(".progress .progress-bar").attr("aria-valuenow", percentage) + "%";
                //         $(".progress .progress-bar").html(percentage + " % ");
                //     },
                //     success: function() {
                //         $("#loader").show();
                //     },
                //     complete: function(data) {
                //         $("#loader").hide();
                //         Swal.fire(
                //             'Berhasil!',
                //             data.message,
                //             'success'
                //         );
                //     }
                // });
            });
        });

        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();

            $('#load a').css('color', '#dfecf6');
            $('#load').append(
                '<img style="position: absolute; left: 0; top: 0; z-index: 100000;" src="/images/loading.gif" />'
            );

            var url = $(this).attr('href');
            var page = $(this).attr('href').split('page=')[1];
            loadData(page);
            window.history.pushState("", "", url);
        });

        // loadData('{{ Request::get('page') }}');
        var dataUserId = [];
        var emailUser = [];
        var namaUsers = [];
        var phoneUsers = [];

        function loadStateCurrentPage() {
            var currentPage = '{{ Request::get('page') }}';
            currentPage = parseInt(currentPage);
            if (currentPage > 1) {
                $("#btnPushNotif").click();
            }
        }

        function loadData(page) {
            $.ajax({
                url: '{{ url('admin/get-push-notif/' . $broadcastId) }}' + '?page=' + page,
                type: 'GET',
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(res) {
                    $("#loader").hide();
                    dataUserId = [];
                    // for(var i = 0; i < res.results.length; i++){
                    //     dataUserId.push(res.results[i]['user_id']);
                    // }
                    // res.results.forEach(row => {
                    //     dataUserId.push(row['user_id']);
                    // emailUser.push(row['email']);
                    // namaUsers.push(row['name']);
                    // phoneUsers.push(row['phone']);
                    // });
                    $("#userId").val(JSON.stringify(res.results));
                    $("#email").val(emailUser);
                    $("#namaTrader").val(namaUsers);
                    $("#phoneUsers").val(phoneUsers);
                    $("#totalUser").html(res.results.total);
                    // var stringPaginate = "";
                    // res.results.links.forEach(row => {
                    //     stringPaginate += row['active'] ?
                    //         '<li class="page-item active"><a class="page-link" href="{{ url('admin/push-notif/' . $broadcastId) }}' +
                    //         '?page=' + parseInt(row['label']) + '">' + row['label'] + '</a></li>' :
                    //         '<li class="page-item"><a class="page-link" href="{{ url('admin/push-notif/' . $broadcastId) }}' +
                    //         '?page=' + parseInt(row['label']) + '">' + row['label'] + '</a></li>'
                    // });
                    // $("#pagination").html(stringPaginate);
                }
            })
        }

        $("#btnPushNotif").click(function(event) {
            event.preventDefault();
            var form = $("#frmTambahNotifikasi")[0];
            var data = new FormData(form);

            $.ajax({
                url: "{{ url('admin/broadcast-notif') }}",
                type: 'POST',
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(data) {
                    $("#loader").hide();
                    Swal.fire(
                        'Berhasil!',
                        data.message,
                        'success'
                    );
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                    console.log(errorThrown);
                    if (textStatus === "timeout" || textStatus === "error") {
                        $("#loader").hide();
                        Swal.fire({
                            title: 'Ooops...',
                            text: "Mohon periksa koneksi internet anda",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Muat ulang',
                            cancelButtonText: 'Tutup'
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        })
                    }
                },
                complete: function() {
                    $("#loader").hide();
                }
            });
        });

        $("#btnPushNotifEmail").click(function(event) {
            event.preventDefault();
            var form = $("#frmTambahNotifikasi")[0];
            var data = new FormData(form);

            $.ajax({
                url: "{{ url('admin/broadcast-email') }}",
                type: 'POST',
                dataType: "json",
                data: data,
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(data) {
                    $("#loader").hide();
                    Swal.fire(
                        'Berhasil!',
                        data.message,
                        'success'
                    );
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (textStatus === "timeout" || textStatus === "error") {
                        $("#loader").hide();
                        Swal.fire({
                            title: 'Ooops...',
                            text: "Mohon periksa koneksi internet anda",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Muat ulang',
                            cancelButtonText: 'Tutup'
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        })
                    }
                },
                complete: function() {
                    $("#loader").hide();
                }
            });
        });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
        integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .progress {
            position: relative;
            width: 100%;
            border: 1px solid #7F98B2;
            padding: 1px;
            border-radius: 3px;
        }

        .bar {
            background-color: #B4F5B4;
            width: 0%;
            height: 25px;
            border-radius: 3px;
        }

        .percent {
            position: absolute;
            display: inline-block;
            top: 3px;
            left: 48%;
            color: #7F98B2;
        }
    </style>
@endsection
