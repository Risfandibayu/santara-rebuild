@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Riwayat Pemberitahuan</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><button type="button" onclick="cancelBroadcast()"
                                                    class="btn btn-primary">Batalkan Pemberitahuan</button></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td>Judul</td>
                                                <td>{{ $broadcast->title }}</td>
                                            </tr>
                                            <tr>
                                                <td>Konten</td>
                                                <td>{{ $broadcast->content }}</td>
                                            </tr>
                                        </table>
                                        <div class="table-responsive">
                                            <table class="table" id="tableBroadcast">
                                                <thead>
                                                    <tr>
                                                        <th width="20">No</th>
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>No HP</th>
                                                        <th>Tanggal</th>
                                                        <th>Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        var broadcastID = '{{ $broadcast->broadcast_group_id }}';
        var tableBroadcast = $("#tableBroadcast").DataTable({
            ajax: '{{ url('/admin/fetch-notifications') }}' + '/' + broadcastID,
            responsive: true,
            processing: true,
            serverSide: true,
            oLanguage: {
                sProcessing: '<div id="tableloading" class="tableloading"></div>',
                sZeroRecords: 'Data tidak tersedia'
            },
            order: [
                [0, "asc"]
            ],
            columns: [{
                    data: "id",
                    render: function(data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: "name"
                },
                {
                    data: "email"
                },
                {
                    data: "phone"
                },
                {
                    data: "created_at"
                },
                {
                    data: "is_deleted",
                    className: "text-center"
                },
            ]
        });

        function cancelBroadcast() {
            Swal.fire({
                title: "Konfirmasi",
                text: "Apakah anda yakin data pemberitahuan akan dibatalkan",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Batalkan"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: "{{ url('admin/cancel-notification') }}",
                        type: "POST",
                        data: {
                            broadcastId: broadcastID
                        },
                        beforeSend: function() {
                            $("#loader").show();
                        },
                        success: function(response) {
                            $("#loader").hide();
                            Swal.fire(
                                "Berhasil!",
                                response.message,
                                "success"
                            );
                            window.location.reload();
                        }
                    });
                }
            });
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
