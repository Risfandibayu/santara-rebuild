@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Histori Transaksi</h1>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Pencarian</label>
                                        <input type="text" class="form-control" id="pencarian"
                                            placeholder="Nama/Email/No HP" />
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table" id="tableAccount">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Phone</th>
                                                        <th>Created at</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalHistori" data-backdrop="static" data-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Histori Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input id="user-id" type="hidden" />
                    <div class="row justify-content-between ml-2 mr-2">
                        <div class="form-inline mb-2">
                            <input type="text" class="form-control" name="daterange" />
                            <button id="btn-filter" class="btn btn-primary ml-2" type="button">Filter</button>
                            {{-- <button id="btn-export" class="btn btn-info ml-1" type="button">Export
                                Data</button> --}}
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-justified mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-transaksi-tab" data-toggle="pill" href="#pills-transaksi"
                                role="tab" aria-controls="pills-transaksi" aria-selected="true">
                                <span class="d-none d-lg-block">Transaksi</span>
                                <span class="d-lg-none">Data</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-deposit-tab" data-toggle="pill" href="#pills-deposit"
                                role="tab" aria-controls="pills-deposit" aria-selected="false">
                                <span class="d-none d-lg-block">Deposit</span>
                                <span class="d-lg-none">Deposit</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-penarikan-tab" data-toggle="pill" href="#pills-penarikan"
                                role="tab" aria-controls="pills-penarikan" aria-selected="false">
                                <span class="d-none d-lg-block">Penarikan</span>
                                <span class="d-lg-none">Penarikan</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        {{-- Transaksi --}}
                        <div class="tab-pane fade show active" id="pills-transaksi" role="tabpanel"
                            aria-labelledby="pills-transaksi-tab">
                            <div class="col-4 mb-3">
                                <select class="custom-select" onchange="filterTr()" id="filterTr">
                                    <option disabled selected>Filter Status</option>
                                    @foreach ([
            '' => 'Semua',
            'VERIFIED' => 'Lunas',
            'WAITING FOR VERIFICATION' => 'Menunggu Konfirmasi',
            'CREATED' => 'Belum Konfirmasi',
            'EXPIRED' => 'Kadaluarsa',
        ] as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tableTransaction" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Transaksi</th>
                                            <th>Member</th>
                                            <th>Created at</th>
                                            <th>Total (Rp)</th>
                                            <th>Split Fee</th>
                                            <th>Status</th>
                                            {{-- <th>Action</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        {{-- Deposit --}}
                        <div class="tab-pane fade" id="pills-deposit" role="tabpanel"
                            aria-labelledby="pills-deposit-tab">
                            <div class="col-4 mb-3">
                                <select class="custom-select" onchange="filterDeposit()" id="filterDeposit">
                                    <option disabled selected>Filter Status</option>
                                    @foreach ([
            '' => 'Semua',
            // 0 => 'Verifikasi',
            2 => 'Ditolak',
            1 => 'Sudah Verifikasi',
            'menuggupembayaran' => 'Menunggu Pembayaran',
        ] as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tableDeposit" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th width="20">No</th>
                                            <th>Member</th>
                                            <th>Tanggal</th>
                                            <th>Payment</th>
                                            <th>Nominal</th>
                                            <th>Split Fee</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                        {{-- Penarikan --}}
                        <div class="tab-pane fade" id="pills-penarikan" role="tabpanel"
                            aria-labelledby="pills-penarikan-tab">
                            <div class="col-4 mb-3">
                                <select class="custom-select" onchange="filterWithDraw()" id="filterWithDraw">
                                    <option disabled selected>Filter Status</option>
                                    @foreach ([
            '' => 'Semua',
            '0' => 'Verifikasi',
            '1' => 'Sudah Verifikasi',
            '2' => 'Ditolak',
        ] as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="tableWithDraw" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th width="20">No</th>
                                            <th>Member</th>
                                            <th>Created</th>
                                            <th>Amount</th>
                                            <th>Split Fee</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        var tglAwal = "";
        var tglAkhir = "";
        var pencarian = "";

        $('input[name="daterange"]').daterangepicker({
            opens: 'right',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf(
                    'month')]
            }
        }, function(start, end, label) {
            tglAwal = start.format('YYYY-MM-DD');
            tglAkhir = end.format('YYYY-MM-DD');
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end
                .format('YYYY-MM-DD'));
        });

        loadData("");

        $('#pencarian').keypress(function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $("#tableAccount").DataTable().clear().destroy();
                loadData(event.target.value);
            }
        });

        function loadData(cari) {
            var tableAccount = $("#tableAccount").DataTable({
                ajax: '{{ url('/admin/fetch-data-user') }}' + '?cari=' + cari,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [
                    [0, "asc"]
                ],
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "aksi",
                    },
                ]
            });
        }

        $('body').on('click', '#btnDetail', function() {
            $("#modalHistori").modal("show");
            var data_id = $(this).data('id');
            var tradernama = $(this).data('tradernama');
            $("#user-id").val(data_id);
            $("#tableTransaction").DataTable().clear().destroy();
            $("#tableDeposit").DataTable().clear().destroy();
            $("#tableWithDraw").DataTable().clear().destroy();
            loadTransaksi(data_id, "", "", "");
            loadDeposit(data_id, "", "", "");
            loadWithdraw(data_id, "", "", "");
        });

        $("#btn-filter").on("click", function() {
            const userId = $("#user-id").val();
            $("#tableTransaction").DataTable().clear().destroy();
            $("#tableDeposit").DataTable().clear().destroy();
            $("#tableWithDraw").DataTable().clear().destroy();
            loadTransaksi(userId, "", tglAwal, tglAkhir);
            loadDeposit(userId, "", tglAwal, tglAkhir);
            loadWithdraw(userId, "", "", tglAwal, tglAkhir);
        });


        function loadTransaksi(userId, filter, startDate, endDate) {
            var tableTransaction = $("#tableTransaction").DataTable({
                ajax: '{{ url('/admin/fetch-data-transaksi') }}' + '/' + userId + '?filter=' + filter +
                    '&startDate=' + startDate +
                    '&endDate=' + endDate,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [
                    [0, "asc"]
                ],
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                fixedColumns: true,
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        width: "200",
                        data: "transaksi"
                    },
                    {
                        width: "100",
                        data: "member"
                    },
                    {
                        width: "200",
                        data: "created_at"
                    },
                    {
                        data: "amount"
                    },
                    {
                        width: "100",
                        data: "split_fee"
                    },
                    {
                        width: "100",
                        data: "status",
                        className: "text-center"
                    },
                    // {
                    //     data: "link",
                    // },
                ]
            });
        }

        function loadDeposit(userId, filter, startDate, endDate) {
            var tableDeposit = $("#tableDeposit").DataTable({
                ajax: '{{ url('/admin/fetch-data-deposit') }}' + '/' + userId + '?filter=' + filter +
                    '&startDate=' + startDate +
                    '&endDate=' + endDate,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                order: [
                    [0, "asc"]
                ],
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "member"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "payment"
                    },
                    {
                        data: "nominal"
                    },
                    {
                        data: "split_fee"
                    },
                    {
                        data: "status",
                        className: "text-center"
                    },
                ]
            });
        }

        function loadWithdraw(userId, filter, startDate, endDate) {
            var tableWithDraw = $("#tableWithDraw").DataTable({
                ajax: '{{ url('/admin/fetch-data-withdraw') }}' + '/' + userId + '?filter=' + filter +
                    '&startDate=' + startDate +
                    '&endDate=' + endDate,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                order: [
                    [0, "asc"]
                ],
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "member"
                    },
                    {
                        data: "date"
                    },
                    {
                        data: "amount"
                    },
                    {
                        data: "split_fee"
                    },
                    {
                        data: "status",
                        className: "text-center"
                    },
                ]
            });
        }

        function filterTr() {
            const userId = $("#user-id").val();
            const filter = $("#filterTr").val();
            $("#tableTransaction").DataTable().clear().destroy();
            loadTransaksi(userId, filter, "", "");
        }

        function filterDeposit() {
            const userId = $("#user-id").val();
            const filter = $("#filterDeposit").val();
            $("#tableDeposit").DataTable().clear().destroy();
            loadDeposit(userId, filter, "", "");
        }

        function filterWithDraw() {
            const userId = $("#user-id").val();
            const filter = $("#filterWithDraw").val();
            $("#tableWithDraw").DataTable().clear().destroy();
            loadWithdraw(userId, filter, "", "");
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

    <style>
        .dataTables_wrapper .dataTables_filter {
            float: right;
            text-align: right;
            visibility: hidden;
        }
    </style>
@endsection
