@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Summary KYC</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements"></div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row justify-content-center my-3">
                                            <div class="col-md-2">
                                                <div class="count"
                                                    style="color: #000000; text-align: center; font-size: 24px;">
                                                    <?= $status != null ? angkaKoma($status->countBelumKyc) : 0 ?>
                                                </div>
                                                <div class="status" style="text-align: center;">Belum KYC</div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="count"
                                                    style="color: #666EE8; text-align: center; font-size: 24px;">
                                                    <?= $status != null ? angkaKoma($status->countPembaruanData) : 0 ?>
                                                </div>
                                                <div class="status" style="text-align: center;">Pembaruan Data</div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="count"
                                                    style="color: #EEAA5B; text-align: center; font-size: 24px;">
                                                    <?= $status != null ? angkaKoma($status->countMenungguVerifikasi) : 0 ?>
                                                </div>
                                                <div class="status" style="text-align: center;">Menunggu Verifikasi
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="count"
                                                    style="color: #BF2D30; text-align: center; font-size: 24px;">
                                                    <?= $status != null ? angkaKoma($status->countDitolak) : 0 ?>
                                                </div>
                                                <div class="status" style="text-align: center;">Ditolak</div>
                                            </div>
                                            {{-- <div>
                                                        <div class="count" style="color: #EEAA5B">
                                                            <?= $status->countMenungguVerifikasiKustodian ?></div>
                                                        <div class="status">Menunggu Verifikasi Kustodian</div>
                                                    </div>
                                                    <div>
                                                        <div class="count" style="color: #BF2D30">
                                                            <?= $status->countDitolakKustodian ?></div>
                                                        <div class="status">Ditolak Kustodian</div>
                                                    </div> --}}
                                            <div class="col-md-2">
                                                <div class="count"
                                                    style="color: #0E7E4A; text-align: center; font-size: 24px;">
                                                    <?= $status != null ? angkaKoma($status->countTerverifikasi) : 0 ?>
                                                </div>
                                                <div class="status" style="text-align: center;">Terverifikasi</div>
                                            </div>

                                            <div class="col-md-4 mt-3">
                                                <select id="filter" onchange="filterKYC()" class="form-control">
                                                    <option disabled selected>Filter Status</option>
                                                    <?php foreach ([
                                                        'no-filter'             => 'Semua',
                                                        'belum_kyc'             => 'Belum KYC',
                                                        'data_updated'          => 'Pembaruan Data',
                                                        'verifying'             => 'Menunggu Verifikasi',
                                                        'rejected'              => 'Ditolak',
                                                        // 'kustodian_verifying'   => 'Menunggu Verifikasi Kustodian',
                                                        // 'kustodian_rejected'    => 'Ditolak Kustodian',
                                                        'verified'              => 'Terverifikasi'
                                                    ] as $key => $value) : ?>
                                                    <option value="<?= $key ?>"
                                                        <?= isset($_GET['filter']) && $_GET['filter'] == $key ? 'selected' : '' ?>>
                                                        <?= $value ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row justify-content-end">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Pencarian</label>
                                                    <input type="text" class="form-control" id="pencarian"
                                                        placeholder="Nama/Email" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table" id="tableKYC">
                                                <thead>
                                                    <tr>
                                                        <th width="20">No</th>
                                                        <th>Nama</th>
                                                        <th>Email</th>
                                                        <th>Updated At</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script>
        var selectedFilter = localStorage.getItem("setFilterKYCIndividu");
        var filterBaru = "";
        if (selectedFilter == null) {
            filterBaru = "no-filter"
        } else {
            filterBaru = selectedFilter
        }
        loadData(filterBaru, "");

        $(document).ready(function() {
            var selectedFilter = localStorage.getItem("setFilterKYCIndividu");
            $("#filter option[value='" + selectedFilter + "']").prop('selected', true);
        });

        function filterKYC() {
            var filter = $("#filter").val();
            localStorage.setItem("setFilterKYCIndividu", filter);
            $("#tableKYC").DataTable().clear().destroy();
            loadData(filter, "");
        }

        function loadData(filter, search) {
            var tableKYC = $("#tableKYC").DataTable({
                ajax: '{{ url('/admin/kyc/fetch-kyc') }}' + '/' + filter,
                responsive: true,
                processing: true,
                serverSide: true,
                ordering: true,
                bStateSave: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "name"
                    },
                    {
                        data: "email"
                    },
                    {
                        data: "updated_at"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "action",
                        className: 'text-center'
                    },
                ]
            }).search(search).draw();
        }

        $('#pencarian').keypress(function(event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                $("#tableKYC").DataTable().clear().destroy();
                loadData(filterBaru, event.target.value);
            }
        });

        // $('#tableKYC_filter input').unbind().keyup(function(e) {
        //     var value = $(this).val();
        //     if (e.keyCode == 13) {
        //         $("#tableKYC").DataTable().clear().destroy();
        //         loadData(filterBaru, value);
        //     }
        // });
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
        integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />

    <style>
        .dataTables_wrapper .dataTables_filter {
            float: right;
            text-align: right;
            visibility: hidden;
        }
    </style>
@endsection
