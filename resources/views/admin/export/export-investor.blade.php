<table>
    <thead>
        <tr>
            <th colspan="9">
                Data User Rentang Waktu Pendaftaran {{ tgl_indo(date('Y-m-d', strtotime($tglAwal))) }} Sampai
                {{ tgl_indo(date('Y-m-d', strtotime($tglAkhir))) }}</th>
        </tr>
        <tr>
            {{-- <th>No</th> --}}
            <th>Nama</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Gender</th>
            <th>Tempat, Tanggal Lahir</th>
            <th>Pekerjaan</th>
            <th>Bank</th>
            <th>Account Number</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 0; @endphp
        <?php
            use App\Models\User;
            User::join('traders as t', 'users.id', '=', 't.user_id')
                ->leftJoin('trader_banks as bank', 'bank.trader_id', '=', 't.id')
                ->leftJoin('bank_investors as bank_invest', 'bank_invest.id', '=', 'bank.bank_investor1')
                ->leftJoin('regencies as reg', 'reg.id', '=', 't.birth_place')
                ->where('users.is_deleted', 0)
                ->whereBetween('users.created_at', [$tglAwal, $tglAkhir])
                ->orderBy('users.id', 'DESC')
                ->select('users.id', 'users.uuid', 't.name', 'users.email', 
                        't.phone', 't.job', 't.birth_place', 't.birth_date', 't.gender', 'bank.account_number1',
                        'bank_invest.bank', 'reg.name as tempat_lahir')
                ->chunk(500, function ($users) {
                    foreach ($users as $row) {
        ?>
            <tr>
                <td>{{ $row->name }}</td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->phone }}</td>
                <td>{{ ($row->gender == 'm' ? 'Laki-Laki' : $row->gender == 'f') ? 'Perempuan' : 'Tidak Diketahui' }}
                </td>
                <td>
                    @if($row->birth_place != null && $row->birth_date != null)
                        @if($row->tempat_lahir == null)
                            {{ $row->birth_place . ', ' . tgl_indo(date('Y-m-d', strtotime($row->birth_date))) }}
                        @else 
                            {{ $row->tempat_lahir . ', ' . tgl_indo(date('Y-m-d', strtotime($row->birth_date))) }}
                        @endif
                    @else
                        -
                    @endif
                </td>
                <td>{{ $row->job }}</td>
                <td>{{ $row->bank }}</td>
                <td>{{ $row->account_number1 }}</td>
            </tr>
        <?php }}); ?>
    </tbody>
</table>
