<table>
    <thead>
        <tr>
            <th colspan="9">
                Data Chat {{ tgl_indo(date('Y-m-d', strtotime($tglAwal))) }} Sampai
                {{ tgl_indo(date('Y-m-d', strtotime($tglAkhir))) }}</th>
        </tr>
        <tr>
            {{-- <th>No</th> --}}
            <th>Nama</th>
            <th>Description</th>
            <th>Emiten ID</th>
            <th>Total Chat</th>
            <th>Total Member</th>
        </tr>
    </thead>
    <tbody>
        @php $no = 0; @endphp
        <?php
            use Illuminate\Support\Facades\DB;

            DB::connection('chat')->table('groups')
                ->whereBetween('created_at', [$tglAwal, $tglAkhir])
                ->orderBy('name', 'ASC')
                ->select('id', 'name', 'description',
                        'emiten_id', 'created_at', 'is_active')
                ->chunk(500, function ($groups) {
                    foreach ($groups as $row) {
        ?>
            <tr>
                <td>{{ $row->name }}</td>
                <td>{{ $row->description }}</td>
                <td>{{ $row->emiten_id }}</td>
                <td>{{ App\Models\Conversations::where('to_id', $row->id)->count() }}</td>
                <td>{{ Illuminate\Support\Facades\DB::connection('chat')
                    ->table('group_users')
                    ->where('group_id', $row->id)
                    ->count() }}</td>
            </tr>
        <?php }}); ?>
    </tbody>
</table>
