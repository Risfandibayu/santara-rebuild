<table>
    <thead>
        <tr>
            <th colspan="7">
                Data Investor Emiten {{ $transactions[0]->company_name }} 
                @if($tglAwal != "" && $tglAkhir != "")
                    Rentang Waktu {{ tgl_indo(date('Y-m-d', strtotime($tglAwal))) }} Sampai {{ tgl_indo(date('Y-m-d', strtotime($tglAkhir))) }}
                @endif
            </th>
        </tr>
        <tr>
            <th>No</th>
            <th>Trader</th>
            <th>Email</th>
            <th>Stock</th>
            <th>Harga</th>
            <th>Total</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; 
        foreach($transactions as $row){
            $no++;
            $stock = 0;
            $stock_price = 0;

            if ($row->channel == 'VA') :
				$channel = 'Virtual Account';
			elseif ($row->channel == 'BANKTRANSFER') :
				$channel = 'Transfer Bank';
			elseif ($row->channel == 'WALLET') :
				$channel = 'Saldo Dompet';
			elseif ($row->channel == 'DANA') :
				$channel = 'DANA';
			elseif ($row->channel == 'MARKET') :
				$channel = 'MARKET (' . strtoupper($row->description) . ')';
			else :
				$channel = '-';
			endif;

            if($row->channel == "MARKET"){
                $market = App\Models\Markets::where('transaction_id', $row->id)
                    ->select('stock', 'stock_price')
                    ->first();
                if($market != null){
                    $stock = $market->stock;
                    $stock_price = $market->stock_price;
                }
            }else{
                $stock = $row->qty;
                $stock_price = $row->price;
            }

            $created_at = tgl_indo(date('Y-m-d', strtotime($row->created_at))).' '.formatJam($row->created_at);  
        ?>
        <tr>
            <td><?= $no ?></td>
            <td>{{ $row->trader_name }}</td>
            <td>{{ $row->user_email }}</td>
            <td>{{ number_format($stock,0,',','.').' Lembar' }}</td>
            <td><?= rupiahBiasa($stock_price) ?></td>
            <td><?= rupiahBiasa($row->amount + $row->fee) ?></td>
            <td>{{ $created_at }}</td>
        </tr>
        <?php } ?>
    </tbody>
</table>