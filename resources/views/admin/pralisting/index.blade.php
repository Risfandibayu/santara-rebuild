@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Coming Soon</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i>
                                    </a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li class="mr-2">
                                                <select class="custom-select" onchange="filterTr()" id="filter">
                                                    <option disabled selected>Filter Status</option>
                                                    @foreach ([
            '' => 'Semua',
            '1' => 'Terverifikasi',
            '0' => 'Ditolak',
        ] as $key => $value)
                                                        <option value="{{ $key }}">{{ $value }}</option>
                                                    @endforeach
                                                </select>
                                            </li>
                                            <li><a href="#" data-toggle="modal" data-target="#modalDownload"
                                                    class="btn btn-primary">Export Data</a></li>
                                        </ul>
                                    </div>
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard">
                                            <div class="table-responsive">
                                                <table class="table" id="tableCalonPenerbit">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nama</th>
                                                            <th>Perusahaan</th>
                                                            <th>Brand</th>
                                                            <th>HP</th>
                                                            <th>Tanggal</th>
                                                            <th>Dukung</th>
                                                            <th>Suka</th>
                                                            <th><i>Comment</i></th>
                                                            <th>Kebutuhan Dana</th>
                                                            <th><i>Share</i> Saham</th>
                                                            <th>Status</th>
                                                            <th>Aksi</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
        </div>
    </div>


    <div class="modal fade text-left" id="modalUpdateEmiten" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="form" id="formUpdateStatus" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel1">Update Status</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="projectinput6">Status</label>
                            <select id="projectinput6" name="title" class="form-control">
                                <option value="Penawaran Saham">Penawaran Saham</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="start_date">Tanggal Mulai</label>
                            <input type="datetime-local" class="form-control" name="start_date" onchange="getDate45()"
                                id="start_date">
                        </div>
                        <div class="form-group">
                            <label for="start_date">Tanggal Selesai</label>
                            <input type="datetime-local" class="form-control" name="end_date" id="end_date">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalDownload" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Download Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ url('admin/pralisting/export-calon-penerbit') }}" method="GET">
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label>Data Awal</label>
                                <select class="custom-select" name="start">
                                    @for ($i = 1; $i <= $jumlahData; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-6 form-group">
                                <label>Data Akhir</label>
                                <select class="custom-select" name="rowperpage">
                                    @for ($i = 1; $i <= $jumlahData; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Download</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        function filterTr() {
            const filter = $("#filter").val();
            $("#tableCalonPenerbit").DataTable().clear().destroy();
            loadData(filter);
        }

        loadData("");

        function loadData(filter) {
            var tableWithDraw = $("#tableCalonPenerbit").DataTable({
                ajax: '{{ url('/admin/pralisting/get-pralisting') }}' + "?filter=" + filter,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                order: [
                    [0, "asc"]
                ],
                columns: [{
                        data: "id",
                        render: function(data, type, row, meta) {
                            return meta.row + meta.settings._iDisplayStart + 1;
                        }
                    },
                    {
                        data: "trader_name"
                    },
                    {
                        data: "company_name"
                    },
                    {
                        data: "trademark"
                    },
                    {
                        data: "phone"
                    },
                    {
                        data: "created_at"
                    },
                    {
                        data: "total_votes"
                    },
                    {
                        data: "total_likes"
                    },
                    {
                        data: "total_coments"
                    },
                    {
                        data: "capital_needs"
                    },
                    {
                        data: "investment"
                    },
                    {
                        data: "status"
                    },
                    {
                        data: "buttonAksi"
                    },
                ]
            });
        }

        function updateStatus(id, company_name) {
            $("#modalUpdateEmiten").modal('show');
            $("#formUpdateStatus").attr('action', '{{ url('/emiten/update_status') }}' + '/' + id);
        }
    </script>
    <script>
        function deleteBisnis(uuid, name) {
            Swal.fire({
                html: '<strong>Yakin menghapus bisnis <b>' + name + '</b> ? </strong>',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    $.ajax({
                        type: 'GET',
                        url: '{{ url('admin/pralisting/delete') }}' + '/' + uuid,
                        cache: false,
                        success: function(data) {
                            $("#loader").hide();
                            Swal.fire("Success!", 'Data berhasil dihapus.', "success").then((
                                result) => {
                                window.location = '{{ url('admin/pralisting/pralisting') }}';
                            });
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", "Data gagal dihapus!", "error");
                        }
                    });
                }
            })
        }

        function getDate45() {
            var tgl = $("#start_date").val();
            $.ajax({
                url: '{{ url('admin/add_date_45') }}',
                type: 'GET',
                data: {
                    start_date: tgl
                },
                success: function(res) {
                    var newDate = res.data.replace(".000000Z", "");
                    $("#end_date").val(newDate);
                }
            })
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
        integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
