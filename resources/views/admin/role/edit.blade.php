@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Ubah Role User</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements"></div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form method="post" action="{{ url('admin/role/update/'.$id) }}" >
                                            @csrf
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" class="form-control" name="name" required
                                                    value="<?= $role->name ?>" />
                                            </div>
                                            <label>Permission</label>
                                            <div class="row mt-2">
                                                <?php foreach ( $permissions as $p ) { ?>
                                                <div class="col-4 mb-1">
                                                    <input type="checkbox" name="permissions[]" value="<?= $p->id ?>"
                                                        <?= isset($p->selected) ? 'checked' : '' ?>>
                                                    <?= $p->permission ?>
                                                </div>
                                                <?php } ?>
                                            </div>

                                            <br />
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <a href="{{ url()->previous() }}"
                                                        class="btn btn-warning btn-block">Kembali</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
