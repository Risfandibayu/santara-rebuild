@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Role</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements"></div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table class="table" style="width: 100%;">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Role</th>
                                                    <th>Created At</th>
                                                    <th>Updated At</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php $no = 0; @endphp
                                                @foreach ($roles as $role)
                                                    @php $no++; @endphp
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        <td>{{ $role->name }}</td>
                                                        <td>{{ $role->created_at }}</td>
                                                        <td>{{ $role->updated_at }}</td>
                                                        <td>
                                                            <a href="{{ url('admin/role/edit/'.$role->id) }}"
                                                                class="btn btn-info btn-sm" title="Edit"><i
                                                                    class="la la-wrench" style="font-size: 15px"></i></a>
                                                            <button class="btn btn-danger btn-sm" title="Delete"
                                                                onclick="ask('Yakin menghapus role ini?', '/user/roles/delete/<?= $role->id ?>')"><i
                                                                    class="la la-trash"
                                                                    style="font-size: 15px"></i></button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
