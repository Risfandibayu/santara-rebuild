@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Fundamental Perusahaan</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form id="formEmitenFundamental">
                                            <input type="hidden" name="type" id="type"
                                                value="<?= isset($type) ? $type : '' ?>">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Kode Saham {{ $type }}</b></label>
                                                        <?php if($code_emiten): ?>
                                                        <input type="text" class="form-control" name="code_emiten"
                                                            id="code_emiten" value="<?= $code_emiten ?>" readonly>
                                                        <?php else: ?>
                                                        <select class="form-control" name="code_emiten" id="code_emiten">
                                                            <option value="">Pilih...</option>
                                                            <?php foreach ( $kode as $key => $value ) : ?>
                                                            <option value="<?= $value->code_emiten ?>"
                                                                <?= isset($kode) && $value->code_emiten == $code_emiten ? 'selected' : '' ?>>
                                                                <?= $value->code_emiten ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                        <?php endif; ?>
                                                        <?php if ($type == 'edit') : ?>
                                                        <input type="hidden" name="emiten_id" id="emiten_id"
                                                            value="<?= isset($data) ? $data['emiten_id'] : '' ?>">
                                                        <input type="hidden" name="id" id="id"
                                                            value="<?= isset($data) ? $data['id'] : '' ?>">
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Ekuitas</b></label>
                                                        <input type="text" class="form-control format-number"
                                                            name="equity" id="equity"
                                                            value="<?= isset($data) ? number_format($data['equity'], 0, ',', '.') : '' ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Aset</b></label>
                                                        <input type="text" class="form-control format-number"
                                                            name="asset" id="asset"
                                                            value="<?= isset($data) ? number_format($data['asset'], 0, ',', '.') : '' ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Hutang</b></label>
                                                        <input type="text" class="form-control format-number"
                                                            name="debt" id="debt"
                                                            value="<?= isset($data) ? number_format($data['debt'], 0, ',', '.') : '' ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Periode</b></label>
                                                        <select name="period" class="form-control" name="period"
                                                            id="period" <?php if (isset($data)){ ?> disabled <?php } ?>>
                                                            <?php foreach ( $period as $key => $value ) : ?>
                                                            <option value="<?= $key ?>"
                                                                <?= (isset($data) && $data['period']) == $key ? 'selected' : '' ?>>
                                                                <?= $value ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label><b>Tahun</b></label>
                                                        <input type="text" <?php if (isset($data)){ ?> disabled
                                                            <?php } ?> class="form-control" name="year"
                                                            id="year" value="<?= isset($data) ? $data['year'] : '' ?>">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mt-3">
                                                    <a class="btn btn-santara-white btn-block"
                                                        href="javascript:window.history.go(-1);">Kembali</a>
                                                </div>
                                                <div class="col-md-6 mt-3">
                                                    <button type="button" class="btn btn-santara-red btn-block"
                                                        onClick="btnSaveEmitenFundamental()">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public') }}/assets/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('public') }}/assets/js/flatpickr.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#begin_period").flatpickr({
                enableTime: true,
                altFormat: "Y-m-d H:i",
                dateFormat: "Y-m-d H:i",
                minDate: "today"
            });

            $("#end_period").flatpickr({
                enableTime: true,
                altFormat: "Y-m-d H:i",
                dateFormat: "Y-m-d H:i"
            });

            $('#code_emiten').select2({
                maximumSelectionLength: 2,
                allowClear: true
            });
        });

        $(".format-number").on("keyup keypress blur", function(e) {
            this.value = parseInt(this.value.replace(/\./g, ""));
            if (this.value != "" && !isNaN(this.value)) {
                this.value = formatNumber(this.value);
            } else {
                this.value = 0;
            }
        });

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }

        function btnSaveEmitenFundamental() {
            var form = '#formEmitenFundamental';
            var data = $(form).serializeArray();
            $('#fundamentalPerusahaanModal').modal('hide');
            $("#loader").show();
            $.ajax({
                url: "{{ url('admin/pasar-sekunder/save-fundamental-data') }}",
                type: 'POST',
                cache: false,
                data: data,
                timeout: 40000,
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data emiten fundamental berhasil disimpan',
                            'success'
                        ).then((result) => {
                            window.location = "{{ url('admin/pasar-sekunder/rasio-fundamental') }}";
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        };
    </script>
@endsection

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('public') }}/assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
        integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
