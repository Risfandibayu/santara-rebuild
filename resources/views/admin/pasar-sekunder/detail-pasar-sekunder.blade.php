@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Detail Pasar Sekunder</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <h3><?= $periode['name'] ?></h3>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Status</label>
                                            <div class="col-sm-7">
                                                <input type="text" readonly class="form-control-plaintext" id="name"
                                                    value="<?= $periode['status'] ?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-3 col-form-label">Tanggal</label>
                                            <div class="col-sm-7">
                                                <input type="text" readonly class="form-control-plaintext" id="name"
                                                    value="<?= $periode['start_date'] . ' - ' . $periode['end_date'] ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-3">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="row">
                                            <div class="card-title-admin">
                                                <ul class="ul-title">
                                                    <li>Total Investor</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-dashboard-finance">
                                            <div class="card-header pb-1 pt-1">
                                                <div class="text-primary text-center">
                                                    <b><?= number_format($periode['totalInvestor']) ?></b>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body pt-0">
                                                    <h2 class="card-title text-center"><b>Orang</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="row">
                                            <div class="card-title-admin">
                                                <ul class="ul-title">
                                                    <li>Total Transaksi</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-dashboard-finance">
                                            <div class="card-header pb-1 pt-1">
                                                <div class="text-primary text-center">
                                                    <b><?= number_format($periode['totalTrx']) ?></b>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body pt-0">
                                                    <h2 class="card-title text-center"><b>Transaksi</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row mt-3">
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="row">
                                            <div class="card-title-admin">
                                                <ul class="ul-title">
                                                    <li>Total Saham</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-dashboard-finance">
                                            <div class="card-header pb-1 pt-1">
                                                <div class="text-primary text-center">
                                                    <b><?= number_format($periode['totalSaham']) ?></b>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body pt-0">
                                                    <h2 class="card-title text-center"><b>Lembar Saham</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="row">
                                            <div class="card-title-admin">
                                                <ul class="ul-title">
                                                    <li>Total Rupiah</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-dashboard-finance">
                                            <div class="card-header pb-1 pt-1">
                                                <div class="text-primary text-center">
                                                    <b><?= number_format($periode['totalDana']) ?></b>
                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <div class="card-body pt-0">
                                                    <h2 class="card-title text-center"><b>Rupiah</b></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
