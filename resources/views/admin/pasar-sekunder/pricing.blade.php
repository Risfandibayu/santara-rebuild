@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Kelola Harga</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="table-responsive">
                                            <table class="table table-hover" id="datatable" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <th>Service Fee</th>
                                                        <th>VAT</th>
                                                        <th>KSEI</th>
                                                        <th>Sales Tax</th>
                                                        <th>All In</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>Fee Beli</th>
                                                        <td><?= isset($pricing['purchase'][0]['value']) ? $pricing['purchase'][0]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['purchase'][1]['value']) ? $pricing['purchase'][1]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['purchase'][2]['value']) ? $pricing['purchase'][2]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['purchase'][3]['value']) ? $pricing['purchase'][3]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['purchase'][4]['value']) ? $pricing['purchase'][4]['value'] : 0 ?>
                                                            %</td>
                                                        <td>
                                                            <button class="btn btn-sm btn-info edit-pricing"
                                                                value="purchase">Edit</button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fee Jual</th>
                                                        <td><?= isset($pricing['sales'][0]['value']) ? $pricing['sales'][0]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['sales'][1]['value']) ? $pricing['sales'][1]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['sales'][2]['value']) ? $pricing['sales'][2]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['sales'][3]['value']) ? $pricing['sales'][3]['value'] : 0 ?>
                                                            %</td>
                                                        <td><?= isset($pricing['sales'][4]['value']) ? $pricing['sales'][4]['value'] : 0 ?>
                                                            %</td>
                                                        <td>
                                                            <button class="btn btn-sm btn-info edit-pricing"
                                                                value="sales">Edit</button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pricingModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="formPricing">
                        <input type="hidden" class="form-control" id="type" name="type">

                        <div class="form-group">
                            <label><b>Service Fee</b></label>
                            <div class="row">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="service_fee" name="service_fee">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label><b>VAT</b></label>
                            <div class="row">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="vat" name="vat">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label><b>KSEI</b></label>
                            <div class="row">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="ksei" name="ksei">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label><b>Sales Tax</b></label>
                            <div class="row">
                                <div class="input-group col-md-12">
                                    <input type="text" class="form-control" id="sales_tax" name="sales_tax">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-santara-white" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-santara-red" onClick="btnSavePricing()">Simpan</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection
@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).on('click', '.edit-pricing', function() {
            var type = $(this).val();

            $.ajax({
                url: '{{ url("admin/pasar-sekunder/get-pricing-by-type") }}' + '/' + type,
                type: 'GET',
                timeout: 20000, // sets timeout to 20 seconds
                cache: false,
                beforeSend: function(){
                    $("#loader").show();
                },
                success: function(data) {
                    data = JSON.parse(data);
                    data = data.data;
                    $("#loader").hide();
                    $('#service_fee').val(data.service_fee);
                    $('#vat').val(data.vat);
                    $('#ksei').val(data.ksei);
                    $('#sales_tax').val(data.sales_tax);

                    $('#type').val(type);
                    $('#pricingModal').modal('show');

                },
                error: function(msg) {
                    $("#loader").hide();
                    Swal.fire("Error!", "Data gagal diambil", "error").then((result) => {
                        location.reload();
                    });
                }
            });
        });

        $(document).on('hidden.bs.modal', '.modal', function() {
            $(this).removeData('bs.modal');
        });

        function btnSavePricing() {
            var form = '#formPricing';
            var data = $(form).serializeArray();
            $('#pricingModal').modal('hide');

            $.ajax({
                url: '{{ url("admin/pasar-sekunder/save-pricing") }}',
                type: 'POST',
                cache: false,
                data: data,
                beforeSend: function(){
                    $("#loader").show();
                },
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data pricing berhasil disimpan',
                            'success'
                        ).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        };
    </script>
@endsection

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
