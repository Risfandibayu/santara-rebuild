@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Periode Pasar Sekunder</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form id="formPeriode">

                                            <input type="hidden" name="type" id="type" value="<?= $type ?>">
                                            <input type="hidden" name="periode_days" id="periode_days"
                                                value="<?= $type == 'edit' ? $periode_days : null ?>">

                                            <input type="hidden" name="id" id="id"
                                                value="<?= isset($periode['id']) ? $periode['id'] : '' ?>">

                                            <div class="form-group">
                                                <label><b>Nama Periode</b></label>
                                                <input type="text" class="form-control" name="name" id="name"
                                                    value="<?= isset($periode['name']) ? $periode['name'] : $name ?>"
                                                    <?= $type == 'edit' ? 'readonly' : '' ?>>
                                            </div>

                                            <div class="form-group mt-3">
                                                <div class="row">
                                                    <div class="form-group col-md-6">
                                                        <label><b>Waktu Mulai</b></label>
                                                        <input type="text" class="form-control" id="start_date"
                                                            name="start_date"
                                                            value="<?= isset($periode['start_date']) ? date('Y-m-d', strtotime($periode['start_date'])) : '' ?>" />
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label><b>Waktu Selesai</b></label>
                                                        <input type="text" class="form-control" id="end_date"
                                                            name="end_date"
                                                            value="<?= isset($periode['end_date']) ? date('Y-m-d', strtotime($periode['end_date'])) : '' ?>" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label><b>Hari Kerja</b></label>
                                                <?php 
                                                foreach ($days as $key => $value) :
                                                ?>
                                                <div class="row">
                                                    <div class="form-inline py-2">
                                                        <div class="form-check" style="width: 7rem;">
                                                            <input class="form-check-input" type="checkbox"
                                                                onChange="toggleDay(<?= $key ?>)"
                                                                value="<?= isset($value['is_enable']) && $value['is_enable'] ? 'true' : 'false' ?>"
                                                                id="day_<?= $key ?>" name="days[<?= $key ?>][enable]"
                                                                <?= isset($value['is_enable']) && $value['is_enable'] ? 'checked' : '' ?>>
                                                            <label
                                                                class="form-check-label"><?= $type == 'created' ? $value : ($periode_days ? $list_days[$value['day']] : $value) ?></label>
                                                        </div>
                                                        <input type="hidden" name="days[<?= $key ?>][days_id]"
                                                            id="days[<?= $key ?>][days_id]"
                                                            value="<?= isset($value['id']) ? $value['id'] : '' ?>">
                                                        <div class="mx-sm-3" id="day_col_<?= $key ?>">
                                                            <?php 
                                                        $col = 0; 
                                                        if(isset($value['hour'])):
                                                            foreach ($value['hour'] as $k => $v) : 
                                                        ?>
                                                            <span id="time_<?= $key ?>_<?= $k ?>" class="row mr-3">
                                                                <div class="input-group mb-1">
                                                                    <input type="text"
                                                                        class="form-control time_start_hour"
                                                                        name="days[<?= $key ?>][hour][<?= $k ?>][start_hour]"
                                                                        value="<?= isset($v['start_hour']) ? date('H:i', strtotime($v['start_hour'])) : '' ?>">
                                                                    <input type="text"
                                                                        class="form-control time_end_hour"
                                                                        name="days[<?= $key ?>][hour][<?= $k ?>][end_hour]"
                                                                        value="<?= isset($v['end_hour']) ? date('H:i', strtotime($v['end_hour'])) : '' ?>">
                                                                    <div class="input-group-append">
                                                                        <button type="button"
                                                                            onClick="toggleTimeDelete(<?= $key ?>, <?= $k ?>)"
                                                                            class="btn btn-outline-warning"><i
                                                                                class="la la-trash"></i></button>
                                                                    </div>
                                                                </div>
                                                            </span>
                                                            <?php 
                                                            $col = $k + 1;
                                                            endforeach;
                                                            endif; 
                                                        ?>
                                                            <span id="time_<?= $key ?>_<?= $col ?>" class="row mr-3"></span>
                                                        </div>
                                                        <button type="button" id="add_time_<?= $key ?>"
                                                            onClick="toggleTime(<?= $key ?>,<?= $col ?>)"
                                                            class="btn btn-outline-info <?= !isset($value['hour']) ? 'hidden' : '' ?>"><i
                                                                class="la la-plus"></i></button>

                                                    </div>
                                                </div>
                                                <?php 
                                                endforeach; 
                                                ?>
                                            </div>

                                        </form>

                                        <div class="row mt-2">
                                            <div class="col-md-6">
                                                <a href="javascript:window.history.go(-1);"
                                                    class="btn btn-santara-white btn-block">Kembali</a>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit" id="submitPeriode" onClick="btnSavePeriode()"
                                                    class="btn btn-santara-red btn-block submit-form">Simpan</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public') }}/assets/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{ asset('public') }}/assets/js/flatpickr.min.js"></script>

    <script>
        $(document).ready(function() {
            $(".time_start_hour").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true
            });

            $(".time_end_hour").flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true
            });

            $("#start_date").flatpickr({
                altFormat: "j F Y",
                dateFormat: "Y-m-d",
                minDate: "today"
            });

            $("#end_date").flatpickr({
                altFormat: "j F Y",
                dateFormat: "Y-m-d"
            });

            const start_date = document.getElementById('start_date');
            start_date.addEventListener('change', function() {
                var startDate = start_date.value;

                flatpickr('#end_date', {
                    minDate: startDate,
                    altFormat: "j F Y",
                    dateFormat: "Y-m-d"
                });
            })
        });


        function toggleDay(day) {
            if ($('#day_' + day).prop("checked") == true) {
                $('#add_time_' + day).removeClass("hidden");
                $('#day_' + day).val(true);
            } else if ($('#day_' + day).prop("checked") == false) {
                $('#add_time_' + day).addClass("hidden");
                $('#day_' + day).val(false);
            }
        }

        var day_obj = {};
        var col = 0;

        function toggleTime(day, k) {
            if (!day_obj.hasOwnProperty(day)) {
                day_obj[day] = k;
            }

            $('#time_' + day + '_' + day_obj[day]).append($(`
        <div class="input-group mb-1">
            <input type="text" class="form-control time_start_hour" id="time_start_hour_${day}_${day_obj[day]}" name="days[${day}][hour][${day_obj[day]}][start_hour]" placeholder="HH:MM">    
            <input type="text" class="form-control time_end_hour" id="time_end_hour_${day}_${day_obj[day]}" name="days[${day}][hour][${day_obj[day]}][end_hour]" placeholder="HH:MM">
            <div class="input-group-append">
                <button type="button" onClick="toggleTimeDelete(${day}, ${day_obj[day]})" class="btn btn-outline-warning"><i class="la la-trash"></i></button>
            </div>
        </div>
    `));

            $('#time_start_hour_' + day + '_' + day_obj[day]).flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true
            });

            $('#time_end_hour_' + day + '_' + day_obj[day]).flatpickr({
                enableTime: true,
                noCalendar: true,
                dateFormat: "H:i",
                time_24hr: true
            });

            day_obj[day]++;
            $('#day_col_' + day).append($(`<span id="time_${day}_${day_obj[day]}" class="row"></span>`));
        }

        function toggleTimeDelete(day, k) {
            $('#time_' + day + '_' + k).html('');
        }

        function btnSavePeriode() {
            var form = '#formPeriode';
            var data = $(form).serializeArray();
            $("#loader").show();

            $.ajax({
                url: '{{ url('admin/pasar-sekunder/save-periode') }}',
                type: 'POST',
                cache: false,
                data: data,
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data Periode Pasar Sekunder berhasil disimpan',
                            'success'
                        ).then((result) => {
                            window.location = '{{ url("admin/pasar-sekunder/periode") }}';
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        }
    </script>
@endsection

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{ asset('public') }}/assets/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
        integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection
