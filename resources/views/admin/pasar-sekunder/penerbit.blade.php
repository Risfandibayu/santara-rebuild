@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Data Penerbit</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row mb-2">
                                            <div class="col-8"></div>
                                            <div class="col-2">
                                                <select id="category" class="form-control">
                                                    <option selected value="">Kategori</option>
                                                    <?php foreach (
                                                        [
                                                            'ksei'         => 'KSEI',
                                                            'market'        => 'Market'
                                                        ] as $key => $value
                                                    ): ?>
                                                    <option value="<?= $key ?>"><?= $value ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="col-2">
                                                <select id="filter" class="form-control" disabled="disabled">
                                                    <option selected value="">Filter</option>
                                                    <?php foreach (
                                                        [
                                                            'true'          => 'Yes',
                                                            'false'         => 'No'
                                                        ] as $key => $value
                                                    ): ?>
                                                    <option value="<?= $key ?>"><?= $value ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table" id="tablePenerbitMarket" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode</th>
                                                        <th>Nama Perusahaan</th>
                                                        <th>Usia Listing</th>
                                                        <th>KSEI</th>
                                                        <th>Rasio Fundamental</th>
                                                        <th>Pasar Sekunder</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div id="modalDesc" class="modal fade" role='dialog'>
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body" id="modal-body">
                    <p id="status_desc"></p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };


            var table = $("#tablePenerbitMarket").DataTable({
                buttons: [
                    'print', 'csv'
                ],
                search: {
                    "caseInsensitive": false,
                },
                scrollX: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia',
                    sSearchPlaceholder: 'Minimal 3 karakter'
                },
                processing: true,
                serverSide: true,
                bInfo: false,
                ajax: {
                    "url": "{{ url('admin/pasar-sekunder/fetch-penerbit-market') }}",
                    "type": "POST",
                    "data": function(d) {
                        var search = $('.dataTables_filter input').val();
                        if (search.length >= 3) {
                            d.search_keyword = search;
                        };
                        d.category = $('#category').val();
                        d.filter = $('#filter').val();
                    }
                },
                order: [
                    [0, 'desc']
                ],
                columnDefs: [{
                    "targets": [4, 5, 6],
                    "orderable": false
                }, ],
                rowCallback: function(row, data, iDisplayIndex) {
                    var info = this.fnPagingInfo();
                    var page = info.iPage;
                    var length = info.iLength;
                    var index = page * length + (iDisplayIndex + 1);
                    $('td:eq(0)', row).html(index);
                }
            });

            $('#category').change(function() {
                let category = $("#category").val();
                $("#filter").val('');
                if (category == '') {
                    $('#filter').attr('disabled', 'disabled');
                    table.draw();
                } else {
                    $('#filter').removeAttr('disabled');
                }
            });

            $('#filter').change(function() {
                table.draw();
            });
        });

        function toggleKsei(uuid, status, company_name) {
            var ksei_revert = ($("#ksei_" + uuid).is(":checked")) ? false : true;

            Swal.fire({
                title: company_name,
                text: 'Update status KSEI ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    var data = {
                        uuid: uuid,
                        status: status
                    };

                    $.ajax({
                        url: "{{ url('admin/pasar-sekunder/update-ksei') }}",
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire('Berhasil', 'berhasil diproses.', 'success').then((
                                result) => {
                                    location.reload();
                                });
                            } else {
                                $("#ksei_" + uuid)[0].checked = ksei_revert;
                                Swal.fire("Error!", 'gagal diproses', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            $("#ksei_" + uuid)[0].checked = ksei_revert;
                            Swal.fire("Error!", 'gagal diproses', "error");
                        }
                    });
                } else {
                    $("#ksei_" + uuid)[0].checked = ksei_revert;
                }
            });

        }

        function toggleTradeable(uuid, status, company_name) {
            var active = (status == 1) ? 'Aktifkan' : 'Non Aktifkan';
            var tradeable_revert = ($("#tradeable_" + uuid).is(":checked")) ? false : true;

            Swal.fire({
                title: company_name,
                text: active + ' pasar sekunder ?',
                type: 'info',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    var data = {
                        uuid: uuid,
                        status: status
                    };

                    $.ajax({
                        url: "{{ url('admin/pasar-sekunder/update-tradeable') }}",
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire('Berhasil', 'berhasil diproses.', 'success').then((
                                result) => {
                                    location.reload();
                                });
                            } else {
                                $("#loader").hide();
                                $("#tradeable_" + uuid)[0].checked = tradeable_revert;
                                Swal.fire("Error!", 'gagal diproses', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            $("#tradeable_" + uuid)[0].checked = tradeable_revert;
                            Swal.fire("Error!", 'gagal diproses', "error");
                        }
                    });
                } else {
                    $("#tradeable_" + uuid)[0].checked = tradeable_revert;
                }
            });
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
