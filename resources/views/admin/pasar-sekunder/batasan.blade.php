@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Data Batasan</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <?php $currentTab = Request::get('tab'); ?>
                                        <ul class="nav nav-justified mb-3" id="pills-tab" role="tablist">
                                            <li class="nav-item member-nav">
                                                <a class="nav-link member-nav-link <?= $currentTab == "autorejection" ? 'active' : '' ?>"
                                                    id="pills-autorejection-tab" data-toggle="tab" role="tab"
                                                    href="{{ url()->current() . '?tab=autorejection' }}"
                                                    data-id="autorejection"
                                                    aria-controls="pills-autorejection" aria-selected="true">
                                                    <span>Auto Rejection</span>
                                                </a>
                                            </li>

                                            <li class="nav-item member-nav">
                                                <a class="nav-link member-nav-link <?= $currentTab == "fraksi" ? 'active' : '' ?>"
                                                    id="pills-fraksi-tab" data-toggle="tab" role="tab"
                                                    aria-controls="pills-fraksi"
                                                    data-id="fraksi"
                                                    href="{{ url()->current() . '?tab=fraksi' }}" aria-selected="true">
                                                    <span>Fraksi</span>
                                                </a>
                                            </li>

                                            <li class="nav-item member-nav">
                                                <a class="nav-link member-nav-link <?= $currentTab == "minimalpembelian" ? 'active' : '' ?>"
                                                    id="pills-minimalpembelian-tab" data-toggle="tab" role="tab"
                                                    href="{{ url()->current() . '?tab=minimalpembelian' }}"
                                                    data-id="minimalpembelian"
                                                    aria-controls="pills-minimalpembelian" aria-selected="true">
                                                    <span>Minimal Pembelian</span>
                                                </a>
                                            </li>
                                        </ul>

                                        <div class="tab-content" id="pills-tabContent">
                                            <div class="tab-pane fade show <?= $currentTab == "autorejection" ? 'active' : '' ?>" id="pills-autorejection" role="tabpanel"
                                                aria-labelledby="pills-autorejection-tab">
                                                @include('admin/pasar-sekunder/_batasan/auto-rejection')
                                            </div>

                                            <div class="tab-pane fade show <?= $currentTab == "fraksi" ? 'active' : '' ?>" id="pills-fraksi" role="tabpanel"
                                                aria-labelledby="pills-fraksi-tab">
                                                @include('admin/pasar-sekunder/_batasan/fraksi')
                                            </div>

                                            <div class="tab-pane fade show <?= $currentTab == "minimalpembelian" ? 'active' : '' ?>" id="pills-minimalpembelian" role="tabpanel"
                                                aria-labelledby="pills-minimalpembelian-tab">
                                                @include('admin/pasar-sekunder/_batasan/minimal-pembelian',
                                                    ['min' => $min])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        var tabId = "{{ $currentTab }}";
        $('.nav-item').on('click', 'a.nav-link', function(e) {
            e.preventDefault();
            var data_id = $(this).data('id');
            tabId = data_id;
            $(".tab-content .active").removeClass("active");
            $("#pills-" + data_id).addClass("active");
            var url = $(this).attr('href');
            window.history.pushState("", "", url);
        });

        $(document).ready(function() {
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            var datatable_rejection = $("#datatable_rejection").DataTable({
                buttons: [
                    'print', 'csv'
                ],
                search: {
                    "caseInsensitive": false
                },
                scrollX: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                processing: true,
                serverSide: true,
                searching: false,
                ordering: false,
                scrollY: '50vh',
                scrollCollapse: true,
                paging: false,
                bInfo: false,
                ajax: {
                    "url": "{{ url('admin/pasar-sekunder/fetch-rejection') }}",
                    "type": "POST"
                }
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                datatable_rejection.columns.adjust();
            });
        });

        $(document).on('click', '.delete-auto-rejection', function() {
            var id = $(this).val();

            Swal.fire({
                title: 'Hapus data auto rejection',
                html: 'Yakin akan menghapus data auto rejection ?',
                icon: 'info',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    $.ajax({
                        url: '{{ url('admin/pasar-sekunder/delete_auto_rejection') }}' + '/' + id,
                        type: 'GET',
                        timeout: 20000, // sets timeout to 20 seconds
                        cache: false,
                        success: function(data) {
                            $("#loader").hide();
                            data = JSON.parse(data);
                            if (data.msg == 200) {
                                Swal.fire(
                                    'Berhasil',
                                    'Data auto rejection berhasil dihapus',
                                    'success'
                                ).then((result) => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire("Error!", data.msg, "error");
                            }

                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", "Data gagal diambil", "error").then((
                                result) => {
                                location.reload();
                            });
                        }
                    });
                }
            });
        });

        function toggleAutoRejection(id, status, range) {
            var rejection_revert = ($("#rejection_" + id).is(":checked")) ? false : true;
            var status_text = (status) ? 'Aktifkan' : 'Non Aktifkan';

            Swal.fire({
                title: range,
                text: status_text + ' status Auto rejection ?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    var data = {
                        id: id,
                        status: status
                    };

                    $.ajax({
                        url: "{{ url('admin/pasar-sekunder/update_status_autorejection') }}",
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire('Berhasil', 'berhasil diproses.', 'success').then((
                                    result) => {
                                    location.reload();
                                });
                            } else {
                                $("#rejection_" + uuid)[0].checked = rejection_revert;
                                Swal.fire("Error!", 'gagal diproses', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            $("#rejection_" + id)[0].checked = rejection_revert;
                            Swal.fire("Error!", 'gagal diproses', "error");
                        }
                    });
                } else {
                    $("#rejection_" + id)[0].checked = rejection_revert;
                }
            });
        }


        // Fraksi
        $(document).ready(function() {
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };

            var datatable_fraction = $("#datatable_fraction").DataTable({
                buttons: [
                    'print', 'csv'
                ],
                search: {
                    "caseInsensitive": false
                },
                scrollX: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                processing: true,
                serverSide: true,
                searching: false,
                ordering: false,
                scrollY: '50vh',
                scrollCollapse: true,
                paging: false,
                bInfo: false,
                ajax: {
                    "url": "{{ url('admin/pasar-sekunder/fetch-fraction') }}",
                    "type": "POST"
                }
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                datatable_fraction.columns.adjust();
            });
        });

        $(document).on('click', '.delete-fraction', function() {
            var id = $(this).val();

            Swal.fire({
                title: 'Hapus data fraksi',
                html: 'Yakin akan menghapus data fraksi ?',
                icon: 'info',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    $.ajax({
                        url: '{{ url('admin/pasar-sekunder/delete_fraction') }}' + "/" + id,
                        type: 'GET',
                        timeout: 20000, // sets timeout to 20 seconds
                        cache: false,
                        success: function(data) {
                            $("#loader").hide();
                            data = JSON.parse(data);
                            if (data.msg == 200) {
                                Swal.fire(
                                    'Berhasil',
                                    'Data fraksi berhasil dihapus',
                                    'success'
                                ).then((result) => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire("Error!", data.msg, "error");
                            }

                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", "Data gagal diambil", "error").then((
                                result) => {
                                location.reload();
                            });
                        }
                    });
                }
            })
        });

        function toggleFraction(id, status, range) {
            var fraction_revert = ($("#fraction_" + id).is(":checked")) ? false : true;
            var status_text = (status) ? 'Aktifkan' : 'Non Aktifkan';

            Swal.fire({
                title: range,
                text: status_text + ' status Fraksi ?',
                icon: 'info',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then((result) => {
                if (result.value) {
                    $("#loader").show();
                    var data = {
                        id: id,
                        status: status
                    };

                    $.ajax({
                        url: "{{ url('admin/pasar-sekunder/update_status_fraction') }}",
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire('Berhasil', 'berhasil diproses.', 'success').then((
                                    result) => {
                                    location.reload();
                                });
                            } else {
                                $("#fraction_" + uuid)[0].checked = fraction_revert;
                                Swal.fire("Error!", 'gagal diproses', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            $("#fraction_" + id)[0].checked = fraction_revert;
                            Swal.fire("Error!", 'gagal diproses', "error");
                        }
                    });
                } else {
                    $("#fraction_" + id)[0].checked = fraction_revert;
                }
            });
        }

        function btnSaveMinPurchase() {
            var data = {
                'value': $("#value").val()
            };
            $.ajax({
                url: '{{ url('admin/pasar-sekunder/save_min_purchase') }}',
                type: 'POST',
                cache: false,
                data: data,
                beforeSend: function() {
                    $("#loader").show();
                },
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data minimal pembelian berhasil disimpan',
                            'success'
                        ).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        };

        $(".format-number").on("keyup keypress blur", function(e) {
            this.value = parseInt(this.value.replace(/\./g, ""));
            if (this.value != "" && !isNaN(this.value)) {
                this.value = formatNumber(this.value);
            } else {
                this.value = 0;
            }
        });

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
    </script>
@endsection
@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
