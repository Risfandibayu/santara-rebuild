@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Fraksi</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form id="formFraction">
                                            <input type="hidden" class="form-control" id="type" name="type"
                                                value="<?= $type ?>">
                                            <input type="hidden" class="form-control" id="id" name="id"
                                                value="<?= $type == 'edit' ? $id : '' ?>">

                                            <div class="form-group">
                                                <label><b>Rentang Harga</b></label>
                                                <div class="row">
                                                    <div class="input-group col-md-2">
                                                        <input type="text"
                                                            class="form-control format-number fraction_price_lower"
                                                            name="price_lower" id="price_lower" onkeyup='checkMaxFraction()'
                                                            value="<?= isset($data) ? number_format($data['price_lower'], 0, ',', '.') : 0 ?>">
                                                    </div>

                                                    <div class="input-group col-md-2">
                                                        <label><b> </b></label>
                                                        <input type="text" class="form-control-plaintext text-center"
                                                            name="price_compare_type" id="price_compare_type"
                                                            value="-">
                                                    </div>

                                                    <div class="input-group col-md-2">
                                                        <label><b> </b></label>
                                                        <input type="text"
                                                            class="form-control format-number fraction_price_upper"
                                                            name="price_upper" id="price_upper" onkeyup='checkMaxFraction()'
                                                            value="<?= isset($data) ? number_format($data['price_upper'], 0, ',', '.') : 0 ?>">
                                                    </div>

                                                    <div class="input-group col-md-6 content-center fraction-limit">
                                                        <small class="red"><b>Harga akhir harus lebih besar dari harga
                                                                awal</b></small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label><b>Fraksi</b></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control format-number"
                                                            id="fraction" name="fraction"
                                                            value="<?= isset($data) ? number_format($data['fraction'], 0, ',', '.') : 0 ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label><b>Max. Jenjang Perubahan</b></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control format-number"
                                                            id="max_change" name="max_change"
                                                            value="<?= isset($data) ? number_format($data['max_change'], 0, ',', '.') : 0 ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <a class="btn btn-santara-white btn-block"
                                                        href="javascript:window.history.go(-1);">Kembali</a>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <button type="button" class="btn btn-santara-red btn-block"
                                                        id="submitFraction" onClick="btnSaveFraction()">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function() {
            $("#submitFraction").attr("disabled", true);
            checkMaxFraction();
        })

        function checkMaxFraction() {
            $('.fraction-limit').addClass('hidden');
            var price_lower = parseInt($('.fraction_price_lower').val().replace(/\./g, ''));
            var price_upper = parseInt($('.fraction_price_upper').val().replace(/\./g, ''));
            var price_compare_type = $("select[name='price_compare_type']").val();
            var compare = (price_compare_type == '<') ? price_lower < price_upper : price_lower <= price_upper;

            if (compare) {
                $('.fraction-limit').addClass('hidden');
                $("#submitFraction").attr("disabled", false);
            } else {
                $('.fraction-limit').removeClass('hidden');
                $("#submitFraction").attr("disabled", true);
            }
        }

        function btnSaveFraction() {
            var form = '#formFraction';
            var data = $(form).serializeArray();
            $("#loader").show();

            $.ajax({
                url: '{{ url("admin/pasar-sekunder/save_auto_fraction") }}',
                type: 'POST',
                cache: false,
                data: data,
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data fraksi berhasil disimpan',
                            'success'
                        ).then((result) => {
                            window.location = '{{ url("admin/pasar-sekunder/batasan?tab=fraksi") }}';
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        };

        $(".format-number").on("keyup keypress blur", function(e) {
            this.value = parseInt(this.value.replace(/\./g, ""));
            if (this.value != "" && !isNaN(this.value)) {
                this.value = formatNumber(this.value);
            } else {
                this.value = 0;
            }
        });

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
    </script>
@endsection
