<div>
    <div class="mt-2 mb-2">
        <a href="{{ url('admin/pasar-sekunder/add-auto-rejection') }}" class="btn btn-primary btn-block col-md-3 offset-md-9">Tambah</a>
    </div>

    <div class="table-responsive">
        <table class="table" id="datatable_rejection" style="width: 100%">
            <thead>
                <tr>
                    <th class="border-top-0">No</th>
                    <th class="border-top-0">Rentang Harga</th>
                    <th class="border-top-0">ARA</th>
                    <th class="border-top-0">ARB</th>
                    <th class="border-top-0">Status</th>
                    <th class="border-top-0">Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
