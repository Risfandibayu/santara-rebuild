@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Auto Rejection</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form id="formAutoRejection">
                                            <input type="hidden" class="form-control" id="type" name="type"
                                                value="<?= $type ?>">
                                            <input type="hidden" class="form-control" id="id" name="id"
                                                value="<?= $type == 'edit' ? $id : '' ?>">

                                            <div class="form-group">
                                                <label><b>Rentang Harga</b></label>
                                                <div class="row">
                                                    <div class="input-group col-md-2">
                                                        <input type="text"
                                                            class="form-control format-number autorejection_price_lower"
                                                            name="price_lower" id="price_lower"
                                                            onkeyup='checkMaxAutorejection()'
                                                            value="<?= isset($data) ? number_format($data['price_lower'], 0, ',', '.') : 0 ?>">
                                                    </div>

                                                    <div class="input-group col-md-2">
                                                        <label><b> </b></label>
                                                        <input type="text" class="form-control-plaintext text-center"
                                                            name="price_compare_type" id="price_compare_type"
                                                            value="-">
                                                    </div>

                                                    <div class="input-group col-md-2">
                                                        <label><b> </b></label>
                                                        <input type="text"
                                                            class="form-control format-number autorejection_price_upper"
                                                            name="price_upper" id="price_upper"
                                                            onkeyup='checkMaxAutorejection()'
                                                            value="<?= isset($data) ? number_format($data['price_upper'], 0, ',', '.') : 0 ?>">
                                                    </div>

                                                    <div class="input-group col-md-6 content-center autorejection-limit">
                                                        <small class="red"><b>Harga akhir harus lebih besar dari harga
                                                                awal</b></small>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label><b>ARA</b></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control format-number"
                                                            name="ara" id="ara"
                                                            value="<?= isset($data) ? number_format($data['ara'], 0, ',', '.') : 0 ?>">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <label><b>ARB</b></label>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control format-number"
                                                            name="arb" id="arb"
                                                            value="<?= isset($data) ? number_format($data['arb'], 0, ',', '.') : 0 ?>">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text">%</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-md-6">
                                                    <a class="btn btn-santara-white btn-block"
                                                        href="javascript:window.history.go(-1);">Kembali</a>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <button type="button" class="btn btn-santara-red btn-block"
                                                        id="submitAutoRejection"
                                                        onClick="btnSaveAutoRejection()">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script>
        $(document).ready(function() {
            $("#submitAutoRejection").attr("disabled", true);
            checkMaxAutorejection();
        })

        function checkMaxAutorejection() {
            $('.autorejection-limit').addClass('hidden');
            var price_lower = parseInt($('.autorejection_price_lower').val().replace(/\./g, ''));
            var price_upper = parseInt($('.autorejection_price_upper').val().replace(/\./g, ''));
            var compare = price_lower < price_upper;

            if (compare) {
                $('.autorejection-limit').addClass('hidden');
                $("#submitAutoRejection").attr("disabled", false);
            } else {
                $('.autorejection-limit').removeClass('hidden');
                $("#submitAutoRejection").attr("disabled", true);
            }
        }

        function btnSaveAutoRejection() {
            var form = '#formAutoRejection';
            var data = $(form).serializeArray();
            $("#loader").show();

            $.ajax({
                url: '{{ url("admin/pasar-sekunder/save_auto_rejection") }}',
                type: 'POST',
                cache: false,
                data: data,
                success: function(data) {
                    $("#loader").hide();
                    data = JSON.parse(data);
                    if (data.msg == 200) {
                        Swal.fire(
                            'Berhasil',
                            'Data auto rejection berhasil disimpan',
                            'success'
                        ).then((result) => {
                            window.location = '{{ url("admin/pasar-sekunder/batasan?tab=autorejection") }}';
                        });
                    } else {
                        Swal.fire("Error!", data.msg, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!", data.msg, "error");
                }
            });
        };


        $(".format-number").on("keyup keypress blur", function(e) {
            this.value = parseInt(this.value.replace(/\./g, ""));
            if (this.value != "" && !isNaN(this.value)) {
                this.value = formatNumber(this.value);
            } else {
                this.value = 0;
            }
        });

        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        }
    </script>
@endsection
