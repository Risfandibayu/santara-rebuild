<div class="form-group">
    <label><b>Minimal Pembelian Saat Ini</b></label>
    <div class="row">
        <div class="input-group col-md-6">
            <input type="text" readonly class="form-control-plaintext" value="Rp. <?= number_format( $min , 0, ',', '.' ) ?>">
        </div>
    </div>
</div>

<div class="form-group">
    <label><b>Minimal Pembelian Baru</b></label>
    <div class="row">
        <div class="input-group col-md-6">
            <input type="text" class="form-control format-number" id="value">
            <div class="input-group-append">
                <button class="btn btn-outline-info" type="button" onClick="btnSaveMinPurchase()">Update</button>
            </div>
        </div>
    </div>
</div>  