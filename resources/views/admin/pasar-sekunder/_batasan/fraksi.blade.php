<div>
    <div class="mt-2 mb-2">
        <a href="{{ url('admin/pasar-sekunder/add-fraction') }}" class="btn btn-primary btn-block col-md-3 offset-md-9">Tambah</a>
    </div>

    <div class="table-responsive">
        <table class="table table-hover dataTable-table" id="datatable_fraction" style="width: 100%">
            <thead>
                <tr>
                    <th class="border-top-0">No</th>
                    <th class="border-top-0">Rentang Harga</th>
                    <th class="border-top-0">Fraksi</th>
                    <th class="border-top-0">Max. Jenjang Perubahan</th>
                    <th class="border-top-0">Status</th>
                    <th class="border-top-0">Aksi</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
