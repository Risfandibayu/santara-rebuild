@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Rasio Fundamental</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <div class="row justify-content-between mb-2">
                                            <div class="col-4">
                                                <div class="form-inline">
                                                    <select id="start" class="form-control mr-2">
                                                        <option disabled selected>Periode</option>
                                                        <?php foreach (
                                                            [
                                                                '1-2020' => 'Semester 1 2020',
                                                                '2-2020' => 'Semester 2 2020',
                                                                '1-2021' => 'Semester 1 2021',
                                                                '2-2021' => 'Semester 1 2021' 
                                                            ] as $key => $value
                                                        ): ?>
                                                        <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                    <select id="end" class="form-control">
                                                        <option disabled selected>Periode</option>
                                                        <?php foreach (
                                                            [
                                                                '1-2020' => 'Semester 1 2020',
                                                                '2-2020' => 'Semester 2 2020',
                                                                '1-2021' => 'Semester 1 2021',
                                                                '2-2021' => 'Semester 2 2021' 
                                                            ] as $key => $value
                                                        ): ?>
                                                        <option value="<?= $key ?>"><?= $value ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <a href="{{ url('admin/pasar-sekunder/add-fundamental/tambah') }}"
                                                    class="btn btn-primary btn-block">Tambah</a>
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-hover" id="datatable_emiten_fundamental"
                                                style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Kode</th>
                                                        <th>Periode</th>
                                                        <th>Sales Growth ( YID )</th>
                                                        <th>GPS</th>
                                                        <th>OPM</th>
                                                        <th>NPM</th>
                                                        <th>Market Cap</th>
                                                        <th>PER</th>
                                                        <th>BPVS</th>
                                                        <th>PBV</th>
                                                        <th>ROA</th>
                                                        <th>ROE</th>
                                                        <th>EV / EBITDA</th>
                                                        <th>DER</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public/admin') }}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
    <script src="{{ asset('public/admin') }}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {
            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
                return {
                    "iStart": oSettings._iDisplayStart,
                    "iEnd": oSettings.fnDisplayEnd(),
                    "iLength": oSettings._iDisplayLength,
                    "iTotal": oSettings.fnRecordsTotal(),
                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                };
            };


            var datatable_emiten_fundamental = $("#datatable_emiten_fundamental").DataTable({
                buttons: [
                    'print', 'csv'
                ],
                search: {
                    "caseInsensitive": false
                },
                scrollX: true,
                oLanguage: {
                    sProcessing: '<div id="tableloading" class="tableloading"></div>',
                    sZeroRecords: 'Data tidak tersedia'
                },
                processing: true,
                serverSide: true,
                searching: true,
                scrollY: '50vh',
                scrollCollapse: true,
                paging: false,
                bInfo: false,
                ajax: {
                    "url": "{{ url('admin/pasar-sekunder/fetch-rasio-fundamental') }}",
                    "type": "POST",
                    "data": function(data) {
                        data.start = $('#start').val();
                        data.end = $('#end').val();
                    }
                },
                order: [
                    [0, 'desc']
                ],
                columnDefs: [{
                    "targets": [3, 4, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15],
                    "orderable": false
                }, ],
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                datatable_emiten_fundamental.columns.adjust();
            });

            $('#start').change(function() {
                datatable_emiten_fundamental.draw();
            });

            $('#end').change(function() {
                datatable_emiten_fundamental.draw();
            });
        });

        $(document).on('click', '.delete-emiten-fundamental', function() {
            var id = $(this).val();

            Swal.fire({
                title: "Apakah anda yakin?",
                text: "Data yang sudah anda hapus tidak akan bisa kembali!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Ya, Hapus"
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                        url: '{{ url('admin/pasar-sekunder/delete-fundamental') }}' + '/' + id,
                        type: 'GET',
                        timeout: 20000, // sets timeout to 20 seconds
                        cache: false,
                        beforeSend: function() {
                            $("#loader").show();
                        },
                        success: function(data) {
                            $("#loader").hide();
                            data = JSON.parse(data);
                            if (data.msg == 200) {
                                Swal.fire(
                                    'Berhasil',
                                    'Data emiten fundamental berhasil dihapus',
                                    'success'
                                ).then((result) => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire("Error!", data.msg, "error");
                            }

                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", "Data gagal diambil", "error").then((
                            result) => {
                                location.reload();
                            });
                        }
                    });
                }
            });

        });
    </script>
@endsection

@section('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css"
        href="{{ asset('public/admin') }}/app-assets/vendors/css/tables/datatable/datatables.min.css">
@endsection
