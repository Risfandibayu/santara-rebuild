@extends('admin.layout.master')
@section('content')
    <div class="app-content content">
        <div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h1 class="card-title-member">Edit Laporan Keuangan Tahunan</h1>
                                    <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <form method="POST" enctype="multipart/form-data"
                                            action="{{ url('admin/laporan-keuangan/update-laporan-tahunan/'.$laporanKeuangan->id) }}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6 form-group">
                                                    <label>Penerbit</label>
                                                    <select class="custom-select" name="emiten_id" id="penerbit"
                                                        style="width: 100%;" required>
                                                        <option value="{{ $laporanKeuangan->emiten_id }}">{{ $laporanKeuangan->company_name }}</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Tahun</label>
                                                    <input type="number" class="form-control" name="year"
                                                        placeholder="contoh : 2022" value="{{ $laporanKeuangan->year }}" required />
                                                </div>
                                                <div class="col-md-6">
                                                    <label>Profit</label>
                                                    <input type="text" class="form-control" name="net_profit"
                                                        id="net_profit" value="{{ $laporanKeuangan->net_profit }}" required />
                                                </div>
                                                <div class="col-md-6">
                                                    <label>File Laporan Tahunan</label>
                                                    <div class="custom-file">
                                                        <input type="file" name="file_report"
                                                            accept="application/pdf" class="custom-file-input"
                                                            id="customFile">
                                                        <label class="custom-file-label" for="customFile">Pilih File</label>
                                                    </div>
                                                    <a target="__blank" class="mt-2" href="{{ config("global.STORAGE_GOOGLE")."token/".$laporanKeuangan->file_report }}">Lihat Laporan</a>
                                                </div>
                                                <div class="col-md-6 mt-3">
                                                    <a href="{{ url()->previous() }}"
                                                        class="btn btn-outline-primary btn-block">Kembali</a>
                                                </div>
                                                <div class="col-md-6 mt-3">
                                                    <button class="btn btn-primary btn-block" type="submit">Simpan</button>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('public') }}/assets/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>

    <script>
        $(document).ready(function() {
            bsCustomFileInput.init()
        })

        $('#penerbit').select2().val('{{ $laporanKeuangan->emiten_id }}').trigger("change");

        $("#penerbit").select2({
            placeholder: "Contoh: PT. Jogja",
            closeOnSelect: false,
            allowClear: true,
            delay: 250, // wait 250 milliseconds before triggering the request
            ajax: {
                url: "{{ url('emiten/fetch-emiten') }}",
                dataType: "json",
                data: function(params) {
                    return {
                        emiten: params.term
                    };
                },
                processResults: function(data) {
                    var results = [];
                    $.each(data, function(index, item) {
                        results.push({
                            id: item.id,
                            text: item.company_name,
                            value: item.company_name
                        })
                    })
                    return {
                        results: results
                    };
                }
            }
        });

        var net_profit = document.getElementById('net_profit');
        net_profit.addEventListener('keyup', function(e) {
            net_profit.value = formatRupiah(this.value, '');
        });

        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
        }



    </script>
@endsection

@section('style')
    <link href="{{ asset('public') }}/assets/css/select2.min.css" rel="stylesheet" />
@endsection
