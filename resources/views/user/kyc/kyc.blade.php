@extends('user.layout.master')
@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h1 class="card-title-member">{{ $title }}</h1>
                                <a class="heading-elements-toggle"><i class="la la-ellipsis-v font-medium-3"></i></a>
                                <div class="heading-elements"></div>
                            </div>
                            <div class="card-content collapse show">
                                <div class="card-body card-dashboard">
                                    <?php $currentTab = Request::get('tab'); ?>
                                    <ul class="nav nav-justified mb-3" id="pills-tab" role="tablist">
                                        <?php foreach ($tab as $key => $value):
                                            if($value->status == 'kustodian_verifying'){
                                                $icon = 'fa fa-check red';
                                                $status = $value->status;
                                            }else if($value->status == 'verifying'){
                                                $icon = 'fa fa-stopwatch red';
                                                $status = $value->status;
                                            }else if($value->status == 'verified'){
                                                $icon = 'fas fa-check-double red';
                                                $status = $value->status;
                                            }else{
                                                $icon = 'far fa-times-circle red';
                                                $status = $value->status;
                                            }
                                        ?>
                                        <li class="nav-item member-nav">
                                            <a class="nav-link member-nav-link <?= $currentTab == $value->page ? 'active' : '' ?>"
                                                id="pills-<?= $value->page ?>-tab" data-toggle="tab"
                                                href="{{ url()->current().'?tab=' }}<?= $value->page ?>" role="tab"
                                                data-id="<?= $value->page ?>"
                                                aria-controls="pills-<?= $value->page ?>" aria-selected="true">
                                                <span>
                                                    <i style="font-size: 1.2rem; margin-right: 0;"
                                                        class="<?= $icon ?>"></i>
                                                    <?= $value->title ?>
                                                </span>
                                            </a>
                                        </li>
                                        <?php $s = $status?>
                                        <?php endforeach; ?>
                                    </ul>

                                    <div class="tab-content" id="pills-tabContent">
                                        <?php foreach ($kyc as $key => $value): ?>
                                        
                                        <div class="tab-pane fade show <?= $currentTab == $value->page ? 'active' : '' ?>"
                                            id="pills-<?= $value->page ?>" role="tabpanel"
                                            aria-labelledby="pills-<?= $value->page ?>-tab">
                                            {{-- @dump($s) --}}
                                            <input type="hidden" id="trader_uuid" value="<?= $value->uuid ?>" />
                                            <input type="hidden" id="kyc_url"
                                                value="<?= $action == 'edit' ? $update_url : $confirm_url ?>" />
                                            {{-- <input type="hidden" id="last_kyc_submission_id"
                                                value="<?= $value->data ? $value->data->last_kyc_submission_id : '' ?>" /> --}}
                                            {{-- <form
                                                enctype="multipart/form-data"
                                                action="{{ url("/user/kyc-individu/update-kyc-individu") }}" method="post">
                                                @csrf --}}
                                                <form
                                                enctype="multipart/form-data"
                                                id="<?= 'formKycConfirm' . $key ?>">
                                                <input type="hidden" id="trader_uuid2" name="trader_uuid2" value="<?= $value->uuid ?>" />
                                                
                                                @include('user/kyc/_detail/' . $value->page, [
                                                    'data' => $value->data,
                                                    'itemSelect' => $itemSelect,
                                                    'status' => $t['status_kyc'.$key],
                                                    // 'ktp' => str_replace(
                                                    //     '/uploads/trader/',
                                                    //     '',
                                                    //     $traderKTP->idcard_photo
                                                    // ),
                                                    'address' => $action == 'edit' ? $kyc2['address'] : null,
                                                ])

                                                @include('user/kyc/_detail/footer', [
                                                    'is_empty' => $value->data ? false : true,
                                                    'data' => $value->data,
                                                    'key' => $key,
                                                    'status' => $tab->$key->status,
                                                ])
                                                {{-- <button type="submit">ok</button> --}}
                                            </form>
                                        </div>
                                        <?php endforeach; ?>
                                        {{-- @dump($kyc) --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection
@section('js')
<script src="{{asset('public/admin')}}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{asset('public/admin')}}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.all.min.js"
    integrity="sha512-IZ95TbsPTDl3eT5GwqTJH/14xZ2feLEGJRbII6bRKtE/HC6x3N4cHye7yyikadgAsuiddCY2+6gMntpVHL1gHw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            responsive: true,

        });
    });
</script>

<script src="{{ asset('public') }}/assets/js/jquery.steps.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>

    var tabId = "{{ $currentTab }}";
    $('.nav-item').on('click', 'a.nav-link', function(e) {
        e.preventDefault();
        var data_id = $(this).data('id');
        tabId = data_id;
        $(".tab-content .active").removeClass("active");
        $("#pills-"+data_id).addClass("active");
        var url = $(this).attr('href');
        window.history.pushState("", "", url);
    });

    $(document).ready(function() {
        validateComfirm();
    });

    $(document).on("keyup", ".required-form-kyc", function() {
        validateComfirm();
    });

    $(".number-tab-steps").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "slideLeft",
        enableAllSteps: true,
        enablePagination: false,
        titleTemplate: '<span class="step">#index#</span> #title#'
    });

    $('.radioUtama').change(function() {
        let value = $(this).attr('value');
        let name = $(this).attr('name');
        document.getElementById("error_" + name).disabled = false;
        document.getElementById("error_" + name).classList.add("required-form-kyc");
        if (value == 1) {
            document.getElementById("error_" + name).value = '';
            document.getElementById("error_" + name).disabled = true;
            document.getElementById("error_" + name).classList.remove("required-form-kyc");
            document.getElementById(name+"_option_ditolak").classList.add("hidden");
        }
        if(value == 0){
            document.getElementById(name+"_option_ditolak").classList.remove("hidden");
        }
        validateComfirm();
    });

    $('.radio-tolak').change(function() {
        let value = $(this).attr('value');
        let id = $(this).attr('id');
        let radioTolakID = id.split("-");
        console.log(radioTolakID);
        document.getElementById("error_" + radioTolakID[0]).value = value;
        validateComfirm();
    });

    $(document).on("click", ".open-imageDialog", function() {
        var image = $(this).data('image');
        $('#popup_image').attr('src', image);
    })

    $("#company_province_address").on("change", function(){
        var value = $(this).val();
        $.ajax({
            url: '{{ url("user/get-regency-by") }}'+'/'+value,
            type: 'GET',
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(response){
                $("#loader").hide();
                $("#company_regency_address").html("");
                let valueKota = "";
                response.data.forEach(row => {
                    valueKota += '<option value="'+row.id+'">'+row.label+'</option>';
                });
                $("#company_regency_address").html(valueKota);
            }
        })
    });

    function btnVerify(title, text, url, uuid, phase) {
        Swal.fire({
            title: title,
            text: text,
            type: 'info',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak'
        }).then((result) => {
            if (result.value) {
                $("#loader").show();
                var data = {
                    uuid: uuid,
                    phase: phase
                };

                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: "json",
                    data: data,
                    success: function(data) {
                        $("#loader").hide();
                        if (data.msg == 200) {
                            Swal.fire('Berhasil', title + ' berhasil dilakukan.', 'success').then((
                                result) => {
                                location.reload();
                            });
                        } else {
                            $("#loader").hide();
                            Swal.fire("Error!", title + ' gagal melakukan', "error");
                        }
                    },
                    error: function(msg) {
                        $("#loader").hide();
                        Swal.fire("Error!", title + ' gagal melakukan', "error");
                    }
                });
            }
        })
    }

    function btnUnverify(title, text, url, uuid) {
        Swal.fire({
            title: "Batalkan KYC Bisnis",
            text: 'Masukan alasan pembatalan KYC',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Ok',
            showLoaderOnConfirm: true,
            preConfirm: (input) => {
                if (input === '') {
                    Swal.showValidationMessage('alasan penolakan tidak boleh kosong')
                } else {
                    $("#loader").show();
                    var data = {
                        uuid: uuid,
                        reason: input
                    };

                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire("Success!", 'Penolakan data KYC berhasil dilakukan.',
                                    "success").then((result) => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire("Error!", title + ' gagal melakukan', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", title + ' gagal melakukan', "error");
                        }
                    });
                }
            }
        })
    }

    function btnReject(url, uuid, phase) {
        Swal.fire({
            title: "Tolak KYC",
            text: 'Masukan alasan penolakan KYC',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Ok',
            showLoaderOnConfirm: true,
            preConfirm: (input) => {
                if (input === '') {
                    Swal.showValidationMessage('alasan penolakan tidak boleh kosong')
                } else {
                    $("#loader").show();
                    var data = {
                        uuid: uuid,
                        phase: phase,
                        reason: input
                    };

                    $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: "json",
                        data: data,
                        success: function(data) {
                            $("#loader").hide();
                            if (data.msg == 200) {
                                Swal.fire("Success!", 'Penolakan data KYC berhasil dilakukan.',
                                    "success").then((result) => {
                                    location.reload();
                                });
                            } else {
                                Swal.fire("Error!", title + ' gagal melakukan', "error");
                            }
                        },
                        error: function(msg) {
                            $("#loader").hide();
                            Swal.fire("Error!", title + ' gagal melakukan', "error");
                        }
                    });
                }
            }
        })
    }

    function detailPhoto(title, photo) {
        Swal.fire({
            title: "<strong> " + title + " </strong>",
            html: "<img onClick='rotateMe()' class='swal2-image rotateimg' src=" + photo +
                "><br /><p>* Klik pada gambar untuk memutar posisi gambar</p>",
            customClass: 'swal-wide'
        });
    };

    var degrees = 0;

    function rotateMe() {
        degrees += 90;

        $('.rotateimg').css({
            'transform': 'rotate(' + degrees + 'deg)',
            '-ms-transform': 'rotate(' + degrees + 'deg)',
            '-moz-transform': 'rotate(' + degrees + 'deg)',
            '-webkit-transform': 'rotate(' + degrees + 'deg)',
            '-o-transform': 'rotate(' + degrees + 'deg)'
        });
    };

    function btnConfirm(key) {
        $("#loader").show();
        var actionurl = document.getElementById('kyc_url').value;
        var form = '#formKycConfirm' + key;
        var kyc = $(form).serializeArray();
        var user = {
            trader_uuid: document.getElementById('trader_uuid').value,
            step_id: key,
            // last_kyc_submission_id: document.getElementById('last_kyc_submission_id').value
        };

        var dataKyc = {
            kyc,
            user
        };

        $.ajax({
            url: actionurl,
            type: 'POST',
            cache: false,
            data: dataKyc,
            success: function(data) {
                $("#loader").hide();
                data = JSON.parse(data);
                if (data.msg == 200) {
                    location.href = "{{ url()->current().'?tab=' }}"+tabId;
                    Swal.fire(
                        'Berhasil',
                        'Konfirmasi KYC berhasil dilakukan',
                        'success'
                    ).then((result) => {
                        location.reload();
                    });
                } else {
                    Swal.fire("Error!", data.msg, "error");
                }
            },
            error: function(data) {
                $("#loader").hide();
                Swal.fire("Error!", data.msg, "error");
            }
        });
    };

    function btnUpdate(key) {
        $("#loader").show();
        var actionurl = '{{ url("/user/kyc-individu/update-kyc-individu-") }}'+key;

        $("#formKycConfirm"+key).on('submit', function(e){
            var data = new FormData(this);
            console.log(data);
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: actionurl,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend: function(){
                    $("#loader").show();
                },
                success: function(response){ //console.log(response);
                    $("#loader").hide();
                    if (response.code == 200) {
                        Swal.fire(
                            'Berhasil',
                            response.message,
                            'success'
                        ).then((result) => {
                            location.reload();
                        });
                    } else {
                        Swal.fire("Error!", response.message, "error");
                    }
                },
                error: function(data) {
                    $("#loader").hide();
                    Swal.fire("Error!"+data.message, data.message, "error");
                }
            });
        });
    }

    function validateComfirm() {
        $(".submit-form-kyc").prop('disabled', true);
        var requiredAllCompleted = true;
        $('.required-form-kyc').each(function() {
            if ($(this).val() == "") requiredAllCompleted = false;
        });
        if (requiredAllCompleted) $(".submit-form-kyc").prop("disabled", false);
    }
</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
    integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" type="text/css"
    href="{{asset('public/admin')}}/app-assets/vendors/css/tables/datatable/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('public') }}/app-assets/yearpicker/yearpicker.css" />
<link href="{{ asset('public') }}/assets/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css"
integrity="sha512-gMjQeDaELJ0ryCI+FtItusU9MkAifCZcGq789FrzkiM49D8lbDhoaUaIX4ASU187wofMNlgBJ4ckbrXM9sE6Pg=="
crossorigin="anonymous" referrerpolicy="no-referrer" />
@endsection