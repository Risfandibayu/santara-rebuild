@if (Auth::user()->trader->status_kyc1 == 'empty' || Auth::user()->trader->status_kyc2 == 'empty' || Auth::user()->trader->status_kyc3 == 'empty' || Auth::user()->trader->status_kyc4 == 'empty' || Auth::user()->trader->status_kyc5 == 'empty' || Auth::user()->trader->status_kyc6 == 'empty' || Auth::user()->trader->status_kyc1 == 'rejected' || Auth::user()->trader->status_kyc2 == 'rejected' || Auth::user()->trader->status_kyc3 == 'rejected' || Auth::user()->trader->status_kyc4 == 'rejected' || Auth::user()->trader->status_kyc5 == 'rejected' || Auth::user()->trader->status_kyc6 == 'rejected' )
<div class="card">
    <div class="card-body p-0">
        <div class="card-content">
            <div class="notification_kyc col-xs-12 col-xl-12 text-center">
                <div style="line-height: 30px; color: #000000;">
                    <div style="font-size: 18px;">
                        <b>Untuk melakukan transaksi. Anda harus melengkapi profil di
                            aplikasi Santara</b>
                    </div>
                    <div style="font-size: 15px; padding-bottom: 18px;">
                        Profile kamu akan diverifikasi oleh tim kami dalam waktu 1 x 24, yuk
                        lengkapi profilmu transaksi dapat berjalan dengan lancar.
                    </div>
                    <div><b><a href="{{url('/kyc_individu')}}/{{Auth::user()->uuid}}"
                                style="text-decoration: underline;" class="label-font"
                                >
                                Ke Halaman Verifikasi
                            </a></b></div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endif
