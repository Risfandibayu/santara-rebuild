@extends('user.layout.master')
@section('content')

<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="configuration">

                <div class="row match-height">

                    {{-- <div class="col-xl-6 col-md-6 col-sm-12"> --}}
                        {{--
                        <link rel="stylesheet" type="text/css"
                            href="<?php echo base_url(); ?>assets/css/member/penerbit.css?v=<?= WEB_VERSION; ?>"> --}}

                        <?php if (isset($data2) && ($data2)) :
    foreach ($data2 as $key => $value) : ?>
    <div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="card">

                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="card-content row m-0">
                                            <div class="col-3 p-0 text-center">
                                                {{-- <?php $picture = explode(',', $value['picture']); ?> --}}
                                                <img class="penerbit-item-img lazyload img-fluid"
                                                    src="{{env('STORAGE_GOOGLE')}}token/<?= $value['picture']; ?>" onerror="this.onerror=null;this.src='{{env('STORAGE_GOOGLE')}}images/error/no-image.png';"
                                                    >
                                            </div>
                                            <div class="col-9">
                                                <h3>
                                                    <?= $value['trademark']; ?>
                                                </h3>
                                                <div style="color: #6b6f82;">
                                                    <?= $value['company_name']; ?>
                                                    <span><i class="la la-tag ml-2"></i>
                                                        <?= $value['category']; ?>
                                                    </span>
                                                </div>
                                                <?php if ($value['status'] == "Penawaran Saham") : ?>
                                                <div class="my-2">
                                                    <div class="progress mb-0" style="height: 12px;">
                                                        <div class="progress-bar bg-gradient-x-success <?= (($value['percent'] * 100) < 100) ? 'progress-bar-animated' : ''; ?>"
                                                            role="progressbar"
                                                            style="width: <?= $value['percent'] * 100; ?>%;"
                                                            aria-valuenow="80" aria-valuemin="0" aria-valuemax="100">
                                                            <b style="color: blue;">
                                                                <?= $value['percent'] * 100; ?> %
                                                            </b>
                                                        </div>
                                                    </div>
                                                    <h6 class="penerbit-diff">Sisa waktu
                                                        <span class="red-santara" ?>
                                                            <?= $value['day_remaining']; ?> Hari -
                                                            <?= $value['total_investor']; ?> Investor
                                                        </span>
                                                    </h6>
                                                </div>
                                                <?php if ($value['last_report']) : ?>
                                                <div
                                                    class="alert alert-info-dashboard penerbit-info-report col-md-12 created">
                                                    <h3><b>Laporan Keuangan Terakhir</b></h3>
                                                    <div class="d-flex justify-content-between mt-1"
                                                        style="color: #6b6f82;">
                                                        <div>Tanggal :
                                                            <?= month(date('Y-m-d', strtotime($value['last_report']['updated_at']))); ?>
                                                        </div>
                                                        <div>Omset Terakhir :
                                                            <?= 'Rp. ' . number_format($value['last_report']['sales_revenue'], 0, ',', '.') ?>
                                                        </div>
                                                        <div>Profit Terakhir :
                                                            <?= 'Rp. ' . number_format($value['last_report']['net_income'], 0, ',', '.') ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php else : ?>
                                                <div class="alert alert-info-dashboard penerbit-info-report col-md-12">
                                                    <h4><b>Anda belum membuat laporan keuangan</b></h4>
                                                    <p>Segera buat laporan keuangan Anda </p>
                                                </div>
                                                <?php endif; ?>
                                                <?php endif; ?>

                                                <?php if ($value['status'] == "Pra Penawaran Saham") : ?>
                                                <div class="alert alert-info-dashboard penerbit-info-report col-md-12">
                                                    <h4><b>Bisnis Anda Masuk Ke Coming Soon</b></h4>
                                                    <p>Segera lengkapi data untuk dapat listing di Santara.co.id </p>
                                                </div>
                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <a href="{{url('/edit_bisnis')}}/<?= $value['auuid']; ?>" class="btn btn-warning btn-block btn-sm">Edit</a href="{{url('/edit_bisnis')}}/<?= $value['auuid']; ?>">
                                                    </div>
                                                    {{-- <div class="col-md-6">
                                                        <a href="{{url('/kyc_bisnis')}}/{{Auth::user()->uuid}}" class="btn btn-success btn-block btn-sm">KYC Bisnis</a href="{{url('/kyc_bisnis')}}/{{Auth::user()->uuid}}">
                                                    </div> --}}
                                                </div>
                                                <?php else : ?>
                                                
                                                <div class="row mt-1">
                                                    <div class="col-md-6">
                                                        <a href="{{url('penerbit/bisnisdetail')}}/<?= $value['auuid']; ?>" class="btn btn-primary btn-block btn-sm">Laporan Keuangan Bulanan</a>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <a href="{{url('penerbit/lapkeutahunan')}}/<?= $value['auuid']; ?>" class="btn btn-primary btn-block btn-sm">Laporan Keuangan Tahunan</a>
                                                    </div>
                                                    
                                                </div>
                                                <div class="row mt-1">
                                                    <div class="col-md-6">
                                                        <a href="{{url('/edit_bisnis')}}/<?= $value['auuid']; ?>" class="btn btn-warning btn-block btn-sm">Edit</a href="{{url('/edit_bisnis')}}/<?= $value['auuid']; ?>">
                                                    </div>
                                                    <div class="col-md-6">

                                                        <button type="button" id="btnDetail"
                                                        data-id="{{ $value['id'] }}" class="btn btn-success btn-block btn-sm">Perhitungan Dividen</button>
                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </div>
                        </div>
                        <?php endforeach;
else : ?>
<div class="col-xl-6 col-md-6 col-sm-12">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="card-content text-center p-3">
                                        {{-- <img src="<?= base_url() ?>assets/images/content/finance-empty.png"
                                            class="mb-2"> --}}
                                        <h3><b>Belum Ada Bisnis Terdaftar</b></h3>
                                        <p>Anda belum memiliki bisnis untuk didanai</p>
                                        <a href="{{url('daftar-bisnis/create')}}" type="button" class="btn btn-santara-red btn-block>
                    <span class=" menu-title" data-i18n="">Daftarkan Bisnis Anda</span>
                                        </a>
                                        <!-- <div class="center-pralisting">

                    <h3><b>Daftarkan Bisnis Anda Melalui :</b></h3>
                    <div class="apps-download">
                        <a href="https://santara.co.id/android">
                            <img src="https://storage.googleapis.com/asset-santara/santara.co.id/images/mobile/playstore.png" alt="santara google play">
                        </a>
                        <a href="https://santara.co.id/ios">
                            <img src="https://storage.googleapis.com/asset-santara/santara.co.id/images/mobile/appstore.png" alt="santara app store">
                        </a>
                    </div>
                </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <?php endif; ?>
                </div>


            </section>
        </div>
    </div>
</div>

<div class="modal fade" id="detailData" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Perhitungan Dividen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-4">
                    <div class="form-group">
                        <label><strong>Periode</strong></label>
                        <select class="custom-select" onchange="pilihTahap()" id="tahap_dividen"></select>
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th colspan="2">Periode</th>
                            <th>Laba/Rugi Setelah Pajak</th>
                        </tr>
                    </thead>
                    <tbody id="loadDataDividen"></tbody>
                    <tbody>
                        <tr>
                            <th colspan="2">Total Laba / Rugi</th>
                            <th>=</th>
                            <th id="totalNetProfit"></th>
                        </tr>
                        <tr>
                            <th colspan="2" style="line-height: 3;">Laba Ditahan</th>
                            <th style="line-height: 3;">=</th>
                            <th >
                                <div class="row">

                                    <div class="col-1" style="line-height: 3;">Rp. </div>
                                    <div class="col-11">
                                        <input type="text" id="laba_ditahan" class="form-control ribuan" value="0" />
                                    </div>
                                </div>
                                </th>
                        </tr>
                        <tr>
                            <th colspan="2">Presentase Dividen</th>
                            <th><span id="presentaseDividen"></span> % </th>
                            <th id="habisPersen" style="color: #28d094"></th>
                        </tr>
                        <tr>
                            <th colspan="4"></th>
                        </tr>
                        <tr>
                            <th colspan="2">Dividen untuk masyarakat</th>
                            <th>=</th>
                            <th id="dividenMasyarakat" style="color: #28d094"></th>
                        </tr>
                        <tr>
                            <th colspan="2">Nilai Penawaran</th>
                            <th>=</th>
                            <th id="nilaiPenawaran"></th>
                        </tr>
                        <tr>
                            <th colspan="2">Pesan Saham Dilepas</th>
                            <th> = </th>
                            <th > <span id="sahamDilepas"></span> %</th>
                        </tr>
                        <tr>
                            <th colspan="2">Yield Dividen</th>
                            <th> = </th>
                            <th ><span id="yielDividen"></span> %</th>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="sendEmail()">Kirim Email</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('public/admin')}}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{asset('public/admin')}}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.all.min.js"
    integrity="sha512-IZ95TbsPTDl3eT5GwqTJH/14xZ2feLEGJRbII6bRKtE/HC6x3N4cHye7yyikadgAsuiddCY2+6gMntpVHL1gHw=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('#tabel').DataTable({
            responsive: true,

        });
    });
</script>
<script>
    document.querySelectorAll('.ribuan2').forEach(inp => new Cleave(inp, {
        numeral: true,
        numeralDecimalMark: ',',
        delimiter: '.'
    }));
</script>
<script>
    var tahun = '';
    var totalNetProfit = 0;
    var emiten_id = '';
    let yielDividen = 0;
    var totalAfterLaba = 0;
    var totalPersen = 0;
    var penawaranSaham = 0;
    var dividen = 0;
    $('body').on('click', '#btnDetail', function() {
        var data_id = $(this).data('id');
        emiten_id = $(this).data('id');
        getTahapDividen(data_id);
        $("#detailData").modal('show');
    });

    function pilihTahap() {
        var tahap_dividen = $("#tahap_dividen").val();
        $("#laba_ditahan").val(0);
        getPeriode(emiten_id, tahap_dividen);
        sumDataNet(tahap_dividen, emiten_id);
    }

    function getTahapDividen(emiten_id){
        $.ajax({
            url: "{{ url('user/perhitungan-dividen/list-tahap') }}"+'/'+emiten_id,
            type: 'GET',
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(res) {
                $("#loader").hide();
                var tahaps = "";
                var no = 0;
                res.data.forEach(e => {
                    no++;
                    tahaps += '<option value="'+e.devidend_date+'">Periode '+no+' '+ (e.devidend_date == '' ? 'on going' : '' )+'</option>';
                });
                $("#tahap_dividen").html(tahaps);
            }
        });
    }

    function getPeriode(emiten_id, tgl){
        $.ajax({
            url: "{{ url('user/perhitungan-dividen/interval-periode') }}",
            data: {
                devidend_date: tgl,
                emiten_id: emiten_id,
            },
            type: 'GET',
            beforeSend: function() {
                $("#loader").show();
            },
            success: function(result) {
                $("#loader").hide();
                let listData = "";
                let no = 0;
                result.data.forEach((e, index)  => {
                    if(index != 0){
                        var tahun = e.split("-");
                        bulanBaru = tahun[1];
                        if(tahun[1] != "10"){
                            bulanBaru = tahun[1].replace("0", "");
                        }
                        no++;
                        listData += '<tr><td>'+no+'</td>'+
                            '<td>'+getBulanName(e)+'</td>'+
                            '<td id="tahun'+bulanBaru+''+tahun[0]+'">'+tahun[0]+'</td>'+
                            '<td id="netProfit'+bulanBaru+''+tahun[0]+'">'+getDetail(emiten_id, bulanBaru, tahun[0], bulanBaru+''+tahun[0])+'</td></tr>';
                    }
                });
                $("#loadDataDividen").html(listData);
            }
        })
    }

    function getDetail(emiten_id, bulan, tahun, idLabel) {
        $.ajax({
            url: "{{ url('user/penerbit/perhitungan-detail') }}",
            type: 'GET',
            data: {
                bulan: bulan,
                tahun: tahun,
                emiten_id: emiten_id
            },
            success: function(res) {
                let net_profit = "-";
                if (res.data != null) {
                    net_profit = res.data.net_profit;
                    $("#netProfit" + idLabel).html(formatRupiah(net_profit));
                } else {
                    $("#netProfit" + idLabel).html("-");
                }
            }
        });
    }

    function sumDataNet(devidend_date, emiten_id) {
        $("#totalNetProfit").html("-");
        $("#presentaseDividen").html("-");
        $("#habisPersen").html("-");
        $("#dividenMasyarakat").html("-");
        $("#nilaiPenawaran").html("-");
        $("#sahamDilepas").html("-");
        $("#yielDividen").html("-");
        $.ajax({
            url: "{{ url('user/penerbit/sum-net-profit') }}",
            type: 'GET',
            data: {
                devidend_date: devidend_date,
                emiten_id: emiten_id
            },
            success: function(res) {
                if(res.avg_general_share_amount != null){
                    dividen = parseInt(res.avg_general_share_amount);
                }
                if(res.avg_capital_needs != null){
                    penawaranSaham = parseInt(res.avg_capital_needs);
                }

                totalNetProfit = parseInt(res.totalNetProfit)
                $("#totalNetProfit").html(formatRupiah(res.totalNetProfit));
                
                hitungData(0, dividen, totalNetProfit, penawaranSaham, yielDividen);
            }
        });
    }

    function formatRupiah(angka) {
        const format = angka.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
        return format;
    }
    function angkaribuan(x) {
    return x.toLocaleString('id-ID');
    }

    function formatr(n) {
          // format number 1000000 to 1,234,567
          return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
        }

    $("#laba_ditahan").keyup(function(){
        var getLaba = this.value;
        // getLaba.replaceAll('.', '');
        // console.log(getLaba);
        var selection = window.getSelection().toString(); 
        if (selection !== '') {
            return; 
        }       
        // When the arrow keys are pressed, abort.
        if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
            return; 
        }       
        var $this = $(this);            
        // Get the value.
        var input = $this.val();            
        input = input.replace(/[\D\s\._\-]+/g, ""); 
        input = input?parseInt(input, 10):0; 
        $this.val(function () {
            return (input === 0)?"":input.toLocaleString("id-ID"); 
        }); 
        var labaDitahan = formatr(getLaba);
        // console.log(formatr(getLaba));
        hitungData(labaDitahan, dividen, totalNetProfit, penawaranSaham, yielDividen);
    });
    

    function hitungData(labaDitahan, dividen, totalNetProfit, penawaranSaham, yielDividen) {
        totalAfterLaba = totalNetProfit - labaDitahan;
        totalPersen = dividen * totalAfterLaba / 100;
        $("#presentaseDividen").html(dividen.toFixed(2));
        $("#habisPersen").html(formatRupiah(totalPersen));
        $("#dividenMasyarakat").html(formatRupiah(totalPersen));
        $("#nilaiPenawaran").html(formatRupiah(penawaranSaham));
        $("#sahamDilepas").html(dividen.toFixed(2));
        if(penawaranSaham == 0){
            yielDividen = 0;
        }else{
            yielDividen = totalPersen / penawaranSaham * 100;
        }
        $("#yielDividen").html(yielDividen.toFixed(2));
    } 

    function getBulanName(bulanTanggal) {
        let listBulan = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ];
        let bt = bulanTanggal.split("-");

        let bulan = "";
        if(bt[1] == "01"){
            bulan = listBulan[0];
        }
        if(bt[1] == "02"){
            bulan = listBulan[1];
        }
        if(bt[1] == "03"){
            bulan = listBulan[2];
        }
        if(bt[1] == "04"){
            bulan = listBulan[3];
        }
        if(bt[1] == "05"){
            bulan = listBulan[4];
        }
        if(bt[1] == "06"){
            bulan = listBulan[5];
        }
        if(bt[1] == "07"){
            bulan = listBulan[6];
        }
        if(bt[1] == "08"){
            bulan = listBulan[7];
        }
        if(bt[1] == "09"){
            bulan = listBulan[8];
        }
        if(bt[1] == "10"){
            bulan = listBulan[9];
        }
        if(bt[1] == "11"){
            bulan = listBulan[10];
        }
        if(bt[1] == "12"){
            bulan = listBulan[11];
        }
        return bulan;
    }

    function sendEmail() {
        Swal.fire({
            title: "Konfirmasi ?",
            text: "Pastikan perhitungan dividend telah sesuai dan benar !",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya Sudah Benar"
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                    url: '{{ url("user/perhitungan-dividen/send-email") }}',
                    type: 'POST',
                    data: {
                        emiten_id: emiten_id,
                        dividend: totalPersen
                    },
                    beforeSend: function() {
                        $("#loader").show();
                    },
                    success: function(result) {
                        $("#loader").hide();
                        Swal.fire(
                            'Berhasil!',
                            result.message,
                            'success'
                        );
                    }
                });
            }
        });
    }

</script>
@endsection
@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.1.9/sweetalert2.min.css"
    integrity="sha512-cyIcYOviYhF0bHIhzXWJQ/7xnaBuIIOecYoPZBgJHQKFPo+TOBA+BY1EnTpmM8yKDU4ZdI3UGccNGCEUdfbBqw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" type="text/css"
    href="{{asset('public/admin')}}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('public') }}/app-assets/yearpicker/yearpicker.css" />
@endsection
