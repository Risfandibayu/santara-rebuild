@extends('user.layout.master')
@section('content')
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="configuration">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-xl-10 col-md-10">
                                        <h1 class="card-title-member">Laporan Keuangan Tahunan</h1>
                                    </div>
                                    <div class="col-xl-2 col-md-2">
                                        <button class="btn btn-primary btn-block" data-toggle="modal"
                                        data-target="#modalTambah">Upload Laporan</button>
                                    </div>

                                </div>


                            </div>
                            <div class="card-content">
                                
                                <div class="card-body px-1 pb-3">
                                    <div class="table-responsive">
                                        <table class="table table-hover dataTable-table" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tahun</th>
                                                    <th>Laporan</th>
                                                    <th>Created At</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 0;?>

                                                @foreach ($lap as $item)
                                                <?php $no++; ?>
                                                <tr>
                                                    <td>{{$no}}</td>
                                                    <td>{{$item->year}}</td>
                                                    <td><a target="__blank" href="{{config("global.STORAGE_GOOGLE")."token/".$item->file_report}}">Lihat Laporan</a></td>
                                                    <td>{{tgl_indo(date('Y-m-d', strtotime($item->created_at)))}}</td>
                                                    <td>
                                                        <button class="btn btn-primary btnEdit" 
                                                        data-id="{{ $item->id }}"
                                                        data-year="{{ $item->year }}"
                                                        data-net_profit="{{ $item->net_profit }}"
                                                        data-created_at="{{ $item->created_at }}"
                                                        data-file_report="{{ $item->file_report }}"
                                                        ><i class="icon-pencil icons"></i></button>

                                                        <button type="button" class="btn btn-danger btnDel" data-id="{{ $item->id }}">
                                                            <i class="icon-trash icons"></i></button>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal fade" id="modalTambah" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Upload Laporan Keuangan Tahunan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ url('penerbit/lapkeutahunan/store') }}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- @dump($uuid) --}}
                    <input type="hidden" name="uuid" value="{{$uuid}}">
                    <div class="form-group">
                        <label>Tahun</label>
                        <input type="number" class="form-control" name="year"
                            placeholder="contoh : 2022" required />
                    </div>
                    <div class="form-group">
                        <label>Profit</label>
                        <input type="text" class="form-control" name="net_profit"
                            id="net_profit" required />
                    </div>
                    <div class="form-group">
                        <label>File Laporan Tahunan</label>
                        {{-- <div class="custom-file">
                            <input type="file" required name="file_report"
                                accept="application/pdf" class="custom-file-input"
                                id="customFile">
                            <label class="custom-file-label" for="customFile">Pilih File</label>

                            
                        </div> --}}

                        <div class="custom-file">
                            <input class="custom-file-input req" name="file_report"
                                id="file_report" accept="application/pdf" type="file" />
                            <label class="custom-file-label ssa" id="ssa"
                                for="inputGroupFile02"
                                aria-describedby="inputGroupFile02">Pilih File</label>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalEdit" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Edit Laporan Keuangan Tahunan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{url('penerbit/lapkeutahunan/edit/')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- @dump($uuid) --}}
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label>Tahun</label>
                        <input type="number" class="form-control" id="year" name="year"
                            placeholder="contoh : 2022" required />
                    </div>
                    <div class="form-group">
                        <label>Profit</label>
                        <input type="text" class="form-control" id="net_profit" name="net_profit"
                            id="net_profit" required />
                    </div>
                    <div class="form-group">
                        <label>File Laporan Tahunan</label>
                        {{-- <div class="custom-file">
                            <input type="file" required name="file_report"
                                accept="application/pdf" class="custom-file-input"
                                id="customFile">
                            <label class="custom-file-label" for="customFile">Pilih File</label>

                            
                        </div> --}}

                        <div class="custom-file">
                            <input class="custom-file-input req" id="file_report" name="file_report"
                                id="file_report" accept="application/pdf" type="file" />
                            <label class="custom-file-label ssa" id="ssa"
                                for="inputGroupFile02"
                                aria-describedby="inputGroupFile02">Pilih File</label>
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="modalDel" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Hapus Laporan Keuangan Tahunan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{url('penerbit/lapkeutahunan/delete/')}}" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    {{-- @dump($uuid) --}}
                    <input type="hidden" name="id" id="id">
                    <p>Apakah anda yakin untuk menghapus laporan ini?</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Ya, Hapus</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script src="{{asset('public/admin')}}/app-assets/vendors/js/tables/datatable/datatables.min.js"></script>
<script src="{{asset('public/admin')}}/app-assets/js/scripts/tables/datatables/datatable-basic.js"></script>
<script src="{{ asset('public') }}/assets/js/select2.min.js"></script>
<script src="{{asset('public/admin')}}/app-assets/js/scripts/forms/custom-file-input.js"></script>
<script>
    $(document).ready(function() {
        $('#datatable').DataTable({
            responsive: true,

        });
    });
</script>

<script>
    $('.btnEdit').on('click', function () {
                // console.log($(this).data('weight'));
                var modal = $('#modalEdit');
                modal.find('#id').val($(this).data('id'));
                modal.find('#year').val($(this).data('year'));
                modal.find('#net_profit').val($(this).data('net_profit'));
                modal.find('#ssa').text($(this).data('file_report'));

                modal.modal('show');
            });
</script>
<script>
    $('.btnDel').on('click', function () {
                // console.log($(this).data('weight'));
                var modal = $('#modalDel');
                modal.find('#id').val($(this).data('id'));

                modal.modal('show');
            });
</script>
@endsection
@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" type="text/css"
    href="{{asset('public/admin')}}/app-assets/vendors/css/tables/datatable/datatables.min.css">
    
    <link href="{{ asset('public') }}/assets/css/select2.min.css" rel="stylesheet" />
@endsection