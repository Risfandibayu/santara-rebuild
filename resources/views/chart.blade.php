@extends('admin.layout.master')
@section('content')
   <div class="container">
       <div class="row">
           <div class="col-md-6">
               {!! $chartConv->container() !!}
           </div>
           <div class="col-md-6">
               {!! $chartGroup->container() !!}
           </div>
       </div>
       <div class="row mt-5">
           <div class="col-md-12">
               {!! $chartUser->container() !!}
           </div>
       </div>
       <div class="row mt-5">
           <div class="col-md-12">
               {!! $chartEmiten->container() !!}
           </div>
       </div>
   </div>
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    {!! $chartConv->script() !!}
    {!! $chartGroup->script() !!}
    {!! $chartUser->script() !!}
    {!! $chartEmiten->script() !!}
@endsection
